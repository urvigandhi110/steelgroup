﻿<%@ Page Title="Steel Events in India &amp; World- Steel Group" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="Events.aspx.cs" Inherits="Events" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="pageheading">Steel <span>Events</span></h1>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row eventspage">
            <%--Second fold from here on in 2 columns--%>
            <%--Column 1 : Content--%>
            <div class="col-12 col-md-8">
                <section id="slider">
                    <div class="swiper-container swiper-slider">
                        <div class="swiper-wrapper">
                            <asp:Repeater ID="rptEventSlider" runat="server">
                                <ItemTemplate>
                                    <div class="swiper-slide">
                                        <img class="img-fluid" src='<%# Page.ResolveUrl("~/cms") + Eval("eventImage1").ToString().Replace("~/","/") + "?w=1920&h=600&mode=crop" %>' />

                                        <div class="caption">
                                            <p class="text-center d-block eventdetails">
                                                <span class="fas fa-calendar"></span>
                                                <span class="mr-md-4"><%# DateTime.Parse(Eval("eventStartDate").ToString()).ToString("MMMM dd, yyyy") %></span>
                                                <span class="fas fa-map-marker"></span>
                                                <span><%# Eval("eventVenue") %></span>
                                            </p>
                                            <h2 class="cover-heading"><%# Eval("eventName") %></h2>
                                            <p class="lead mt-md-3">
                                                <a href='<%# Page.ResolveUrl("~") +  "events/" + Eval("eventName").ToString().Replace(" ","-").Replace("-&","").ToLower() %>' class="btn btn-info">View Details</a>
                                            </p>
                                        </div>

                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>

                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </section>
                <section id="eventscorner">
                    <h4 class="eventsheading">Upcoming Events</h4>
                    <div class="row">
                        <asp:Repeater ID="rptEventsUpcoming" runat="server">
                            <ItemTemplate>
                                <div class="col-12 col-md-4">
                                    <a href='<%# Page.ResolveUrl("~") +  "events/" + Eval("eventName").ToString().Replace(" ","-").Replace("-&","").ToLower() %>'>
                                        <div class="eventsingle">
                                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventImage1").ToString().Replace("~/","/") + "?w=500&h=500&mode=crop" %>' alt="" class="img-fluid" />
                                            <div class="eventsingleoverlay">
                                                <h4 class="text-uppercase"><%# Eval("eventName") %></h4>
                                                <p><%# DateTime.Parse(Eval("eventStartDate").ToString()).ToString("dd MMM yyyy") %>, <%# Eval("eventCity").ToString() + ", " + Eval("eventCountry").ToString() %></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </section>
                <section id="eventscorner">
                    <h4 class="eventsheading">Past Events</h4>
                    <div class="row">
                        <asp:Repeater ID="rptEventsPast" runat="server">
                            <ItemTemplate>
                                <div class="col-12 col-md-4">
                                    <a href='<%#  Page.ResolveUrl("~") + "past-events/" + Eval("eventName").ToString().Replace(" ","-").Replace("-&","").ToLower() %>'>
                                        <div class="eventsingle">
                                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventImage1").ToString().Replace("~/","/") + "?w=500&h=500&mode=crop" %>' alt="" class="img-fluid" />
                                            <div class="eventsingleoverlay">
                                                <h4 class="text-uppercase"><%# Eval("eventName") %></h4>
                                                <p><%# DateTime.Parse(Eval("eventStartDate").ToString()).ToString("dd MMM yyyy") %>, <%# Eval("eventCity").ToString() + ", " + Eval("eventCountry").ToString() %></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </section>
            </div>
            <%--Column 2 : Ads and others--%>

            <div class="col-12 col-md-4">
                <h4 class="eventsheading">Sponsored Events</h4>
                <asp:Repeater ID="rptThirdParty" runat="server">
                    <ItemTemplate>
                        <div class="thirdparty">
                            <img src="<%# Page.ResolveUrl("~/cms") + Eval("tpEventImage").ToString().Replace("~/","/") + "?w=250&h=250&mode=crop" %>" />
                            <div class="eventcontent">
                                <p><%# Eval("tpEventName") %></p>
                                <span class="d-block"><i class="fas fa-fw fa-calendar-alt"></i> <%# Convert.ToDateTime(Eval("tpEventStartDate").ToString())==Convert.ToDateTime(Eval("tpEventEndDate").ToString())?Convert.ToDateTime(Eval("tpEventStartDate").ToString()).ToString("dd MMM, yyyy"):Convert.ToDateTime(Eval("tpEventStartDate").ToString()).ToString("dd") + " - " + Convert.ToDateTime(Eval("tpEventEndDate").ToString()).ToString("dd MMM, yyyy") %></span>
                                <span><i class="fas fa-fw fa-map-marker"></i> <%# Eval("tpEventVenue") %></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

                <div class="clearfix">
                    <div class="row mb-5">
                        <div class="col-12 text-center">
                            <a href="<%= Page.ResolveUrl("~") %>sponsored-events" class="btn btn-danger btn-events mt-4 btn-block">
                                <i class="fas fa-bullhorn"></i>
                                View All Sponsored Events</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
    <script>
        $(document).ready(function () {
            //initialize swiper when document ready
            var mySwiper = new Swiper('.swiper-container', {
                autoplay: {
                    delay: 5000,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                }
            })
        });
    </script>
</asp:Content>
