﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;


public partial class SteelAdmin : System.Web.UI.MasterPage
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT * FROM adminMaster WHERE adminID=@adminID");
            steelcmd.Parameters.AddWithValue("@adminID", Session["adminID"].ToString());

            steelcmd.Connection = steelcon;
            steelcon.Open();
            rdrd = steelcmd.ExecuteReader();

            if (rdrd.Read())
            {
                ltrlAdminName.Text = rdrd["adminName"].ToString();
                imgAdmin.ImageUrl = rdrd["adminPhoto"].ToString();
            }

            steelcon.Close();
            steelcon.Dispose();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        { }
    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut();
        Response.Redirect("Default.aspx", false);
    }
}
