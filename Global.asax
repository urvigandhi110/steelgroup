﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System.IO" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        RouteTable.Routes.Add("AllEventsRoute", new Route("events", new PageRouteHandler("~/Events.aspx")));
        RouteTable.Routes.Add("EventRoute", new Route("events/{eventname}", new PageRouteHandler("~/EventDetails.aspx")));
        RouteTable.Routes.Add("EventRegRoute", new Route("events/{eventname}/Register", new PageRouteHandler("~/Register.aspx")));
        RouteTable.Routes.Add("EventPayRoute", new Route("events/{eventname}/Online-Payment", new PageRouteHandler("~/explara.aspx")));
        RouteTable.Routes.Add("EventSponsorsRoute", new Route("events/{eventname}/sponsors/", new PageRouteHandler("~/SponsorDetail.aspx")));
        RouteTable.Routes.Add("EventSpeakersRoute", new Route("events/{eventname}/speakers/", new PageRouteHandler("~/Speakers.aspx")));
        RouteTable.Routes.Add("EventSpeakerDetailsRoute", new Route("events/{eventname}/speakers/{speakername}", new PageRouteHandler("~/SpeakerDetails.aspx")));
        RouteTable.Routes.Add("EventSpeakerBecomeRoute", new Route("events/{eventname}/become-a-speaker", new PageRouteHandler("~/SpeakerBecome.aspx")));
        
        //Past Event
        RouteTable.Routes.Add("PastEventRoute", new Route("past-events/{eventname}", new PageRouteHandler("~/PastEvent.aspx")));

        //Third Party Events
        RouteTable.Routes.Add("ThirdPartyRoute", new Route("sponsored-events", new PageRouteHandler("~/SponsoredEvents.aspx")));

        //Add post event login, downloads, gallery URLS later
        RouteTable.Routes.Add("AllNewsRoute", new Route("news", new PageRouteHandler("~/News.aspx")));
        RouteTable.Routes.Add("NewsCatRoute", new Route("news/{newscat}", new PageRouteHandler("~/Newscat.aspx")));
        RouteTable.Routes.Add("NewsArticleRoute", new Route("news/{newscat}/{newsheadline}", new PageRouteHandler("~/NewsDetails.aspx")));

        RouteTable.Routes.Add("AllJobsRoute", new Route("steel-jobs", new PageRouteHandler("~/Jobs.aspx")));
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

</script>
