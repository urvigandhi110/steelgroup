﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Dashboard</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="row">
        <div class="statistics col-lg-3 col-12">
            <div class="statistic d-flex align-items-center bg-white has-shadow">
                <div class="icon bg-red"><i class="fa fa-ticket"></i></div>
                <div class="text">
                    <strong>
                        <asp:Literal ID="ltrlEventCount" runat="server"></asp:Literal></strong><br>
                    <small>Events Posted</small>
                </div>
            </div>
        </div>
        <div class="statistics col-lg-3 col-12">
            <div class="statistic d-flex align-items-center bg-white has-shadow">
                <div class="icon bg-green"><i class="fa fa-newspaper-o"></i></div>
                <div class="text">
                    <strong>
                        <asp:Literal ID="ltrlNewsCount" runat="server"></asp:Literal></strong><br>
                    <small>News Posted</small>
                </div>
            </div>
        </div>
        <div class="statistics col-lg-3 col-12">
            <div class="statistic d-flex align-items-center bg-white has-shadow">
                <div class="icon bg-orange"><i class="fa fa-bullhorn"></i></div>
                <div class="text">
                    <strong>
                        <asp:Literal ID="ltrlAdsCount" runat="server"></asp:Literal></strong><br>
                    <small>Ads Posted</small>
                </div>
            </div>
        </div>
        <div class="statistics col-lg-3 col-12">
            <div class="statistic d-flex align-items-center bg-white has-shadow">
                <div class="icon bg-violet"><i class="fa fa-envelope-o"></i></div>
                <div class="text">
                    <strong>
                        <asp:Literal ID="ltrlSubscriberCount" runat="server"></asp:Literal></strong><br>
                    <small>Subscribers</small>
                </div>
            </div>
        </div>
         <div class="statistics col-lg-3 col-12 mt-4">
            <div class="statistic d-flex align-items-center bg-white has-shadow">
                <div class="icon bg-yellow"><i class="fa fa-users"></i></div>
                <div class="text">
                    <strong>
                        <asp:Literal ID="ltrlEventReg" runat="server"></asp:Literal></strong><br>
                    <small>Registrations</small>
                </div>
            </div>
        </div>
         <div class="statistics col-lg-3 col-12 mt-4">
            <div class="statistic d-flex align-items-center bg-white has-shadow">
                <div class="icon bg-blue"><i class="fa fa-briefcase"></i></div>
                <div class="text">
                    <strong>
                        <asp:Literal ID="ltrlJobs" runat="server"></asp:Literal></strong><br>
                    <small>Jobs Posted</small>
                </div>
            </div>
        </div>
         <div class="statistics col-lg-3 col-12 mt-4">
            <div class="statistic d-flex align-items-center bg-white has-shadow">
                <div class="icon bg-gray"><i class="fa fa-user"></i></div>
                <div class="text">
                    <strong>
                        <asp:Literal ID="ltrlAdminCount" runat="server"></asp:Literal></strong><br>
                    <small>Admins</small>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

