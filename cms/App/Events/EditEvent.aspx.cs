﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_EditEvent : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {

                string eventID = Request.QueryString["eventID"];

                steelcon = new SqlConnection(connstr);
                steelcmd = new SqlCommand("SELECT * FROM eventsmaster WHERE eventID=@eventID; ");
                steelcmd.Parameters.AddWithValue("@eventID", eventID);

                steelcmd.Connection = steelcon;
                steelcon.Open();

                rdrd = steelcmd.ExecuteReader();

                if (rdrd.Read())
                {
                    txtEventName.Text = rdrd["eventName"].ToString();
                    txtEventDesc.Text = rdrd["eventDesc"].ToString();
                    txtEventDescOld.Text = rdrd["eventDescOld"].ToString();
                    txtStartDate.Text = DateTime.Parse(rdrd["eventStartDate"].ToString()).ToString("yyyy-MM-dd");
                    txtEndDate.Text = DateTime.Parse(rdrd["eventEndDate"].ToString()).ToString("yyyy-MM-dd");
                    txtVideo.Text = rdrd["eventVideo"].ToString();

                    txtVenue.Text = rdrd["eventVenue"].ToString();
                    txtVenueCity.Text = rdrd["eventCity"].ToString();
                    txtVenueState.Text = rdrd["eventState"].ToString();
                    txtVenueCountry.Text = rdrd["eventCountry"].ToString();
                    txtEventAddress.Text = rdrd["eventVenueAddress"].ToString();
                    txtMapUrl.Text = rdrd["eventVenueMapURL"].ToString();


                    txtCompanies.Text = rdrd["eventCompanies"].ToString();
                    txtDelegates.Text = rdrd["eventDelegates"].ToString();
                    txtPanelists.Text = rdrd["eventPanelists"].ToString();
                    txtExhibitors.Text = rdrd["eventExhibitors"].ToString();

                    if (rdrd["publish"].ToString() == "Yes")
                    {
                        chkPublish.Checked = true;
                    }

                    chkPublish.InputAttributes.Add("class", "checkbox-template");

                }

                rdrd.Close();
                steelcon.Close();
                steelcmd.Dispose();
            }
        }
        catch (Exception ex)
        { }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            string eventID = Request.QueryString["eventID"].ToString();

            steelcmd.CommandText = "UPDATE eventsmaster SET eventName=@eventName,eventStartDate=@eventStartDate,eventEndDate=@eventEndDate,eventVenue=@eventVenue,eventCity=@eventCity,eventState=@eventState,eventCountry=@eventCountry,eventVenueAddress=@eventVenueAddress,eventVenueMapURL=@eventVenueMapURL,eventCompanies=@eventCompanies,eventDelegates=@eventDelegates,eventPanelists=@eventPanelists,eventExhibitors=@eventExhibitors,eventDesc=@eventDesc,eventDescOld=@eventDescOld,eventVideo=@eventVideo,datemodified=@datemodified,modifiedby=@modifiedby,modifiedIP=@modifiedIP,publish=@publish WHERE eventID=@eventID";

            steelcmd.Parameters.AddWithValue("@eventID", eventID);
            steelcmd.Parameters.AddWithValue("@eventName", txtEventName.Text);
            steelcmd.Parameters.AddWithValue("@eventStartDate", txtStartDate.Text);
            steelcmd.Parameters.AddWithValue("@eventEndDate", txtEndDate.Text);
            steelcmd.Parameters.AddWithValue("@eventVenue", txtVenue.Text);
            steelcmd.Parameters.AddWithValue("@eventCity", txtVenueCity.Text);
            steelcmd.Parameters.AddWithValue("@eventState", txtVenueState.Text);
            steelcmd.Parameters.AddWithValue("@eventCountry", txtVenueCountry.Text);
            steelcmd.Parameters.AddWithValue("@eventVenueAddress", txtEventAddress.Text);
            steelcmd.Parameters.AddWithValue("@eventVenueMapURL", txtMapUrl.Text);
            steelcmd.Parameters.AddWithValue("@eventCompanies", txtCompanies.Text);
            steelcmd.Parameters.AddWithValue("@eventDelegates", txtDelegates.Text);
            steelcmd.Parameters.AddWithValue("@eventPanelists", txtPanelists.Text);
            steelcmd.Parameters.AddWithValue("@eventExhibitors", txtExhibitors.Text);
            steelcmd.Parameters.AddWithValue("@eventDesc", txtEventDesc.Text);
            steelcmd.Parameters.AddWithValue("@eventDescOld", txtEventDescOld.Text);
            steelcmd.Parameters.AddWithValue("@eventVideo", txtVideo.Text);
            steelcmd.Parameters.AddWithValue("@datemodified", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@modifiedby", HttpContext.Current.User.Identity.Name);
            steelcmd.Parameters.AddWithValue("@modifiedIP", Request.UserHostAddress);
            steelcmd.Parameters.AddWithValue("@publish", chkPublish.Checked ? "Yes" : "No");

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Event " + txtEventName.Text + " details has been updated Successfully.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();

            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Event details could not be updated. Please check all the details and try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }

            chkPublish.InputAttributes.Add("class", "checkbox-template");

        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}