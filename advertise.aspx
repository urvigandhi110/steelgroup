﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="advertise.aspx.cs" Inherits="advertise" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="pageheading">Steel Group <span>Ads</span></h1>
                <p class="mx-auto text-center mb-4">
                    Grow more with our ad plans and placements that reach over a lakh views a month.
                </p>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12 text-center">
                <img src="images/adv-img.jpg" class="img-fluid" />
            </div>
            <div class="col-12 text-center">
                <h4 class="text-center my-4">For Related Enquiries</h4>
                <img src="images/offline-ads.jpg" class="img-fluid" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

