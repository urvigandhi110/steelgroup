﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class App_Sliders_AddSlider : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        steelcmd = new SqlCommand();
        steelcon = new SqlConnection(connstr);
        steelcon.Open();
        steelcmd.Connection = steelcon;

        string filename = "~/uploads/sliders/" + fileImage.FileName;

        if (fileImage.HasFile && !System.IO.File.Exists(Server.MapPath(filename)))
        {
            fileImage.SaveAs(Server.MapPath(filename));
        }
        else
        {
            filename = "~/uploads/sliders/" + Guid.NewGuid().ToString().Replace("-", "") + fileImage.FileName;
            fileImage.SaveAs(Server.MapPath(filename));
        }


        string newSliderid = "SLD" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(100, 999);

        steelcmd.CommandText = "INSERT INTO slidermaster(SliderID,SliderTitle,SliderText,sliderHighlightText,SliderImage,datecreated,createdby,createdIP,publish) VALUES(@SliderID,@SliderTitle,@SliderText,@sliderHighlightText,@SliderImage,@datecreated,@createdby,@createdIP,@publish)";
        steelcmd.Parameters.AddWithValue("@SliderID", newSliderid);
        steelcmd.Parameters.AddWithValue("@SliderTitle", txtSliderTitle.Text);
        steelcmd.Parameters.AddWithValue("@SliderText", txtSliderSubtitle.Text);
        steelcmd.Parameters.AddWithValue("@sliderHighlightText", txtSliderHighlight.Text);
        steelcmd.Parameters.AddWithValue("@SliderImage", filename);
        steelcmd.Parameters.AddWithValue("@datecreated", DateTime.Now);
        steelcmd.Parameters.AddWithValue("@createdby", HttpContext.Current.User.Identity.Name);
        steelcmd.Parameters.AddWithValue("@createdIP", Request.UserHostAddress);
        steelcmd.Parameters.AddWithValue("@publish", "Yes");


        int res = steelcmd.ExecuteNonQuery();

        if (res > 0)
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-success alert-dismissible";
            ltrlmsg.Text = "New Slider has been uploaded Successfully.";
            steelcon.Close();

            steelcmd.Dispose();
            steelcon.Dispose();

        }
        else
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-warning alert-dismissible";
            ltrlmsg.Text = "Slider could not be saved. Please check file details, size, type and try again.";
            steelcon.Close();

            steelcmd.Dispose();
            steelcon.Dispose();
        }
    }
}