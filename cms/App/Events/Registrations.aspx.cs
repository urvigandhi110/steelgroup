﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_Registrations : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    void Bindlist()
    {
        try
        {
            string eventID = Request.QueryString["eventID"].ToString();

            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT row_number() over(order by datecreated) as sno,regID,regType,regName,regDesignation,regOrg,regEmailOrg,regEmailPersonal,regMobile,regCity,regState,regCountry,datecreated,numDelegates,interestedAs,companyType FROM eventregistration WHERE eventID=@eventID");
            steelcmd.Parameters.AddWithValue("@eventID", eventID);

            steelcmd.Connection = steelcon;
            steelcon.Open();
            rdrd = steelcmd.ExecuteReader();

            rptList.DataSource = rdrd;
            rptList.DataBind();

            steelcon.Close();
        }
        catch (Exception ex)
        { }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bindlist();
        }

    }

}