﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class App_Events_PostEvent_Default : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    void Bindlist()
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT row_number() over(order by eventID) as sno,eventID,eventName,eventStartDate,eventEndDate,eventVenue,publish FROM eventsmaster WHERE eventEndDate<GETDATE()");

            steelcmd.Connection = steelcon;
            steelcon.Open();
            rdrd = steelcmd.ExecuteReader();

            rptList.DataSource = rdrd;
            rptList.DataBind();

            steelcon.Close();
        }
        catch (Exception ex)
        { }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bindlist();
        }

    }
    protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if  (e.CommandName == "adddelegates")
        {
            Response.Redirect("UploadDelegates.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "addppt")
        {
            Response.Redirect("UploadPPT.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "addphotos")
        {
            Response.Redirect("UploadPhotos.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "addcontent")
        {
            Response.Redirect("Content.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "speakerppt")
        {
            Response.Redirect("SpeakerEmailPPT.aspx?eventID=" + e.CommandArgument);
        }
        else { }
    }
}