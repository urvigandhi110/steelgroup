﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="UploadPhotos.aspx.cs" Inherits="App_Events_PostEvent_UploadPhotos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Add Event Gallery</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Browse and select photos-</h3>
            </div>
            <div class="card-body">
                <asp:Panel ID="pnlUpload" runat="server">
                    <div class="form-group row">
                        <label class="col-sm-12 form-control-label">Select upto 20 Images (Jpg or Png files of size 1 MB or less accepted.)</label>
                        <div class="col-sm-10">
                            <asp:FileUpload ID="fileImage" runat="server" placeholder="Select upto 10 Images" AllowMultiple="true" />
                        </div>
                        <div class="col-sm-2">
                            <asp:RegularExpressionValidator ID="rexpfileDocument" runat="server" ControlToValidate="fileImage" ValidationGroup="images"
                                ErrorMessage="Only .jpg, .jpeg, .png are allowed" Text="*" CssClass="errorvalidator"
                                ValidationExpression="(.*\.([Jj][Pp][Gg])|.*\.([Jj][Pp][Ee][Gg])|.*\.([Pp][Nn][Gg])$)"></asp:RegularExpressionValidator>
                            <asp:ValidationSummary ID="valsumm" ValidationGroup="images" ShowMessageBox="true" ShowSummary="false" runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlDetails" CssClass="detailsform" Visible="false" runat="server">
                    <div class="row">
                        <asp:Repeater ID="rptImages" runat="server">
                            <ItemTemplate>
                                <div class="col-12 col-md-3">
                                    <div class="card bg-light">
                                        <asp:Image ID="imgUploaded" ImageUrl=' <%# Page.ResolveUrl("~") + Container.DataItem.ToString().Replace("~","") + "?w=500&h=325&mode=crop&anchor=Topcenter" %>' runat="server" CssClass="card-img-top" AlternateText='<%# Container.DataItem %>' />
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label class="form-control-label">Photo Title</label>
                                                <asp:TextBox class="form-control form-control-sm" placeholder="Enter Photo Title here" ID="txtPhotoTitle" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="form-group row">
                                                <label class="form-control-label">Photo Description</label>
                                                <asp:TextBox class="form-control form-control-sm" placeholder="Enter Photo Description here" ID="txtPhotoDesc" runat="server"></asp:TextBox>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </asp:Panel>

                <div class="form-group">
                    <asp:Button ID="btnSubmit" ValidationGroup="images" OnClick="btnSubmit_Click" CssClass="btn btn-primary" runat="server" Text="Upload Images" />
                    <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-success" runat="server" Text="Save Images" Visible="false" />
                    <asp:Button ID="btnAddPhotos" OnClick="btnAddPhotos_Click" CssClass="btn btn-primary" runat="server" Text="Add More Photos to Event" Visible="false" />
                    <a href="<%= Page.ResolveUrl("~") %>App/Events/PostEvent/Default.aspx" class="btn btn-secondary">Go Back to Events</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script>
        function ValidateFileCount() {
            var fileCount = document.getElementById('<%= fileImage.ClientID%>').files.length;
            if (fileCount > 20) // Selected images with in 10 count
            {
                alert("Please select only 20 images..!!!");
                return false;
            }
            else if (fileCount <= 0) // Selected atleast 1 image check
            {
                alert("Please select atleast 1 image..!!!");
                return false;
            }

            return true;  // Good to go
        }
    </script>
</asp:Content>

