﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Sliders_Default : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    void Bindlist()
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT row_number() over(order by sliderID) as sno,sliderID,sliderTitle,sliderText,sliderImage,sliderHighlightText,publish FROM slidermaster");

            steelcmd.Connection = steelcon;
            steelcon.Open();
            rdrd = steelcmd.ExecuteReader();

            rptList.DataSource = rdrd;
            rptList.DataBind();

            steelcon.Close();
        }
        catch (Exception ex)
        { }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bindlist();
        }

    }

    protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "deleterecord")
        {
            try
            {
                steelcon = new SqlConnection(connstr);
                steelcmd = new SqlCommand();
                steelcmd.Connection = steelcon;
                steelcmd.CommandText = "DELETE FROM slidermaster WHERE sliderID=@sliderID";

                steelcmd.Parameters.Add("@sliderID", SqlDbType.VarChar);
                steelcmd.Parameters["@sliderID"].Value = e.CommandArgument;

                steelcon.Open();
                int res = steelcmd.ExecuteNonQuery();

                if (res > 0)
                {
                    pnlMessage.Visible = true;
                    pnlMessage.CssClass = "alert alert-success";
                    ltrlmsg.Text = "Slider has been deleted successfully.";

                    steelcon.Close();
                    steelcmd.Dispose();
                    steelcon.Dispose();

                    Bindlist();
                }
                else
                {
                    pnlMessage.Visible = true;
                    pnlMessage.CssClass = "alert alert-danger";
                    ltrlmsg.Text = "Slider could not be deleted. Please Try Again";

                    steelcon.Close();
                    steelcmd.Dispose();
                    steelcon.Dispose();
                }
            }
            catch (Exception ex)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-danger";
                ltrlmsg.Text = ex.ToString();

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
    }
}