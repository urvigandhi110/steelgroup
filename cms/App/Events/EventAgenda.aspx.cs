﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_EventAgenda : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;
    SqlDataAdapter da;
    DataSet ds;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                int[] arrNum = new int[20];
                for (int i = 0; i < 20; i++)
                {
                    arrNum[i] = i + 1;
                }
                rptInputAgenda.DataSource = arrNum;
                rptInputAgenda.DataBind();

                string eventID = Request.QueryString["eventID"];

                steelcon = new SqlConnection(connstr);
                steelcmd = new SqlCommand("SELECT eventID,eventName,eventStartDate,eventEndDate FROM eventsmaster WHERE eventID=@eventID; SELECT * FROM eventAgenda WHERE eventID=@eventID ORDER BY eventAgendaDate");
                steelcmd.Parameters.AddWithValue("@eventID", eventID);

                steelcmd.Connection = steelcon;
                steelcon.Open();
                da = new SqlDataAdapter();
                ds = new DataSet();
                da.SelectCommand = steelcmd;

                da.Fill(ds);

                //Fill DDL on First Load
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //If only 1-day event
                    if (ds.Tables[0].Rows[0]["eventStartDate"].ToString() == ds.Tables[0].Rows[0]["eventEndDate"].ToString())
                    {
                        ddlEventDate.Items.Add(new ListItem(DateTime.Parse(ds.Tables[0].Rows[0]["eventStartDate"].ToString()).ToString("dd MMM yyyy"), ds.Tables[0].Rows[0]["eventStartDate"].ToString()));
                    }
                    //If multiple-day event
                    else
                    {
                        DateTime startDt = DateTime.Parse(ds.Tables[0].Rows[0]["eventStartDate"].ToString());
                        DateTime endDt = DateTime.Parse(ds.Tables[0].Rows[0]["eventEndDate"].ToString());

                        int interval = 1;

                        for (DateTime dt = startDt; dt <= endDt; dt += TimeSpan.FromDays(interval))
                        {
                            ddlEventDate.Items.Add(new ListItem(dt.ToString("dd MMM yyyy"), dt.ToString("yyyy-MM-dd mm:mm:ss.fff")));
                        }
                    }
                }
                //Bind Agenda Data if exists for 1st Day


                steelcon.Close();
                steelcmd.Dispose();
                ds.Dispose();
                da.Dispose();
            }
        }
        catch (Exception ex)
        { }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string qry = "";
        string eventID = Request.QueryString["eventID"].ToString();
        Random rnd = new Random();
        foreach (RepeaterItem itm in rptInputAgenda.Items)
        {
            string newAgendaid = "AGD" + DateTime.Today.ToString("ddMMyyyy") + rnd.Next(10000, 99999);

            TextBox tbFrom = (TextBox)itm.FindControl("txtTimeFrom");
            TextBox tbTo = (TextBox)itm.FindControl("txtTimeTo");
            TextBox tbTitle = (TextBox)itm.FindControl("txtTitle");
            TextBox tbDesc = (TextBox)itm.FindControl("txtDesc");

            if (tbFrom.Text != "" && tbTo.Text != "" && tbTitle.Text != "" && tbDesc.Text != "")
            {
                if (tbFrom.ToolTip != "" && tbTo.ToolTip != "" && tbTitle.ToolTip != "" && tbDesc.ToolTip != "")
                {
                    qry += "UPDATE eventAgenda SET eventAgendaTime='" + tbFrom.Text + " - " + tbTo.Text + "',eventAgendaTitle='" + tbTitle.Text + "',eventAgendaText='" + tbDesc.Text + "',datemodified='" + DateTime.Now + "',modifiedby='" + HttpContext.Current.User.Identity.Name + "',modifiedIP='" + Request.UserHostAddress + "' WHERE eventAgendaID='" + tbTitle.ToolTip + "';";
                }
                else
                {
                    qry += "INSERT INTO eventAgenda(eventAgendaID,eventAgendaDate,eventAgendaTime,eventAgendaTitle,eventAgendaText,eventID,datecreated,createdby,createdIP) VALUES('" + newAgendaid + "','" + ddlEventDate.SelectedValue + "','" + tbFrom.Text + " - " + tbTo.Text + "','" + tbTitle.Text + "','" + HttpUtility.HtmlEncode(tbDesc.Text) + "','" + eventID + "','" + DateTime.Now + "','" + HttpContext.Current.User.Identity.Name + "','" + Request.UserHostAddress + "');";
                }
            }
            else
            {
                if (tbFrom.ToolTip != "" && tbTo.ToolTip != "" && tbTitle.ToolTip != "" && tbDesc.ToolTip != "")
                {
                    qry += "DELETE FROM eventAgenda WHERE eventAgendaID='" + tbTitle.ToolTip + "';";
                }
            }
        }

        steelcon = new SqlConnection(connstr);
        steelcmd = new SqlCommand(qry);
        steelcmd.Connection = steelcon;
        steelcon.Open();
        int res = steelcmd.ExecuteNonQuery();

        if (res > 0)
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-success alert-dismissible";
            ltrlmsg.Text = "Agenda for " + ddlEventDate.SelectedItem.Text + " with details saved successfully.";

            steelcon.Close();
            steelcmd.Dispose();
            steelcon.Dispose();

        }
        else
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-warning alert-dismissible";
            ltrlmsg.Text = "Agenda could not be saved. Please check details and try again.";

            steelcon.Close();
            steelcmd.Dispose();
            steelcon.Dispose();
        }
    }

    protected void ddlEventDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string eventID = Request.QueryString["eventID"];

            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT * FROM eventAgenda WHERE eventID=@eventID AND eventAgendaDate=@eventAgendaDate");
            steelcmd.Parameters.AddWithValue("@eventID", eventID);
            steelcmd.Parameters.AddWithValue("@eventAgendaDate", ddlEventDate.SelectedValue);

            steelcmd.Connection = steelcon;
            steelcon.Open();
            da = new SqlDataAdapter();
            ds = new DataSet();
            da.SelectCommand = steelcmd;

            da.Fill(ds);

            //Bind Agenda Data if exists for 1st Day
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (RepeaterItem itm in rptInputAgenda.Items)
                {
                    if (itm.ItemIndex < ds.Tables[0].Rows.Count)
                    {
                        TextBox tbFrom = (TextBox)itm.FindControl("txtTimeFrom");
                        TextBox tbTo = (TextBox)itm.FindControl("txtTimeTo");
                        TextBox tbTitle = (TextBox)itm.FindControl("txtTitle");
                        TextBox tbDesc = (TextBox)itm.FindControl("txtDesc");

                        tbFrom.Text = ds.Tables[0].Rows[itm.ItemIndex]["eventAgendaTime"].ToString().Split('-')[0].Trim();
                        tbTo.Text = ds.Tables[0].Rows[itm.ItemIndex]["eventAgendaTime"].ToString().Split('-')[1].Trim();
                        tbTitle.Text = ds.Tables[0].Rows[itm.ItemIndex]["eventAgendaTitle"].ToString();
                        tbDesc.Text = ds.Tables[0].Rows[itm.ItemIndex]["eventAgendaText"].ToString();

                        tbFrom.ToolTip = ds.Tables[0].Rows[itm.ItemIndex]["eventAgendaID"].ToString();
                        tbTo.ToolTip = ds.Tables[0].Rows[itm.ItemIndex]["eventAgendaID"].ToString();
                        tbTitle.ToolTip = ds.Tables[0].Rows[itm.ItemIndex]["eventAgendaID"].ToString();
                        tbDesc.ToolTip = ds.Tables[0].Rows[itm.ItemIndex]["eventAgendaID"].ToString();

                    }
                }
            }
            else
            {
                foreach (RepeaterItem itm in rptInputAgenda.Items)
                {

                    TextBox tbFrom = (TextBox)itm.FindControl("txtTimeFrom");
                    TextBox tbTo = (TextBox)itm.FindControl("txtTimeTo");
                    TextBox tbTitle = (TextBox)itm.FindControl("txtTitle");
                    TextBox tbDesc = (TextBox)itm.FindControl("txtDesc");

                    tbFrom.Text = "";
                    tbTo.Text = "";
                    tbTitle.Text = "";
                    tbDesc.Text = "";

                    tbFrom.ToolTip = "";
                    tbTo.ToolTip = "";
                    tbTitle.ToolTip = "";
                    tbDesc.ToolTip = "";
                }
            }

            steelcon.Close();
            steelcmd.Dispose();
            ds.Dispose();
            da.Dispose();

        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}