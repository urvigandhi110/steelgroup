﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventMaster.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <section id="register">
        <div class="row no-gutters">
            <div class="col-12 text-center">
                <h3 class="sectionheading">Register for Event</h3>
                <p class="sectionsubheading">Hurry before it closes</p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-12 col-md-8 offset-md-2">
                <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible" role="alert">
                    <h5 class="font-weight-bold">Registration Message</h5>
                    <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </asp:Panel>
                <div class="form-group">
                    <label>Registration Type:</label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <asp:RadioButton AutoPostBack="true" ID="rbIndividual" runat="server" GroupName="type" Checked OnCheckedChanged="rbIndividual_CheckedChanged" />
                        <label class="custom-control-label" for='<%= rbIndividual.ClientID %>'>Individual</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <asp:RadioButton AutoPostBack="true" ID="rbGroup" runat="server" GroupName="type" OnCheckedChanged="rbIndividual_CheckedChanged" />
                        <label class="custom-control-label" for='<%= rbGroup.ClientID %>'>Group</label>
                    </div>
                </div>
                <hr />
                <div class="form-group row">
                    <div class="col-6">
                        <label>Interested As</label>
                        <asp:DropDownList ID="ddlInterestedAs" runat="server" CssClass="form-control" required>
                            <asp:ListItem Value="">Select One of these</asp:ListItem>
                            <asp:ListItem Value="Delegate">Delegate</asp:ListItem>
                            <asp:ListItem Value="Speaker">Speaker</asp:ListItem>
                            <asp:ListItem Value="Exhibitor">Exhibitor</asp:ListItem>
                            <asp:ListItem Value="Sponsor">Sponsor</asp:ListItem>
                            <asp:ListItem Value="Partner">Partner</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Panel Visible="false" ID="pnlNumDelegates" CssClass="col" runat="server">
                        <label>Number of Delegates</label>
                        <asp:TextBox ID="txtDelegates" Text="0" required placeholder="Enter Number of Delegates" CssClass="form-control" MaxLength="3" type="number" runat="server"></asp:TextBox>
                    </asp:Panel>
                </div>
                <asp:HiddenField ID="hfEventID" runat="server" />
                <asp:HiddenField ID="mailvals" runat="server" />
                <div class="form-group row">
                    <div class="col-6">
                        <label>Your Name</label>
                        <asp:TextBox ID="txtFullName" MaxLength="100" required placeholder="Enter your Full Name" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-6">
                        <label>Your Designation</label>
                        <asp:TextBox ID="txtDesignation" MaxLength="100" required placeholder="Enter your Designation" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label>Your Company/Organization</label>
                    <asp:TextBox ID="txtOrgName" MaxLength="200" required placeholder="Enter your Company/Organization Name" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label>Company Type/Nature of Business</label>
                    <asp:TextBox ID="txtCompanyType" MaxLength="100" required placeholder="Enter your Nature of Business" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group row">
                    <div class="col-6">
                        <label>Your Email <small>(Personal)</small> </label>
                        <asp:TextBox ID="txtPersonalEmail" MaxLength="100" required placeholder="Enter your Personal Email" CssClass="form-control" runat="server" type="email"></asp:TextBox>
                    </div>
                    <div class="col-5">
                        <label>Company Email <small>(Not Gmail,Hotmail etc..)</small> </label>
                        <asp:TextBox ID="txtOrgEmail" MaxLength="100" placeholder="example@yourcompany.com" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-1">
                        <asp:RegularExpressionValidator ForeColor="Red" Font-Size="10" ID="regexpVal" Text="*" runat="server" ValidationGroup="reg" ErrorMessage="Please enter corporate email address. Not Gmail/Yahoo etc." ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(?!hotmail|gmail|yahoo)(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$" ControlToValidate="txtOrgEmail"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-6">
                        <label>Your Mobile <small>(Personal)</small> </label>
                        <asp:TextBox ID="txtMobile" MaxLength="15" required placeholder="Enter your Personal Mobile" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-6">
                        <label>Company Telephone <small>(Office)</small> </label>
                        <asp:TextBox ID="txtOrgPhone" MaxLength="20" placeholder="Enter Company Phone" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label>Mailing Address</label>
                    <asp:TextBox ID="txtAddress" MaxLength="500" required placeholder="Enter your Address" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="form-group row">
                    <div class="col-6">
                        <label>City </label>
                        <asp:TextBox ID="txtCity" MaxLength="100" required placeholder="Enter your City" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-6">
                        <label>State</label>
                        <asp:TextBox ID="txtState" MaxLength="100" required placeholder="Enter your State" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-6">
                        <label>Zip/Pin Code </label>
                        <asp:TextBox ID="txtZip" MaxLength="6" placeholder="Enter your Zip/Pin" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-6">
                        <label>Country</label>
                        <asp:DropDownList ID="ddlCountry" required runat="server" CssClass="form-control">
                            <asp:ListItem Value="">Select a Country</asp:ListItem>
                            <asp:ListItem>Afghanistan</asp:ListItem>
                            <asp:ListItem>Albania</asp:ListItem>
                            <asp:ListItem>Algeria</asp:ListItem>
                            <asp:ListItem>Andorra</asp:ListItem>
                            <asp:ListItem>Angola</asp:ListItem>
                            <asp:ListItem>Antigua and Barbuda</asp:ListItem>
                            <asp:ListItem>Argentina</asp:ListItem>
                            <asp:ListItem>Armenia</asp:ListItem>
                            <asp:ListItem>Australia</asp:ListItem>
                            <asp:ListItem>Austria</asp:ListItem>
                            <asp:ListItem>Azerbaijan</asp:ListItem>
                            <asp:ListItem>Bahamas</asp:ListItem>
                            <asp:ListItem>Bahrain</asp:ListItem>
                            <asp:ListItem>Bangladesh</asp:ListItem>
                            <asp:ListItem>Barbados</asp:ListItem>
                            <asp:ListItem>Belarus</asp:ListItem>
                            <asp:ListItem>Belgium</asp:ListItem>
                            <asp:ListItem>Belize</asp:ListItem>
                            <asp:ListItem>Benin</asp:ListItem>
                            <asp:ListItem>Bhutan</asp:ListItem>
                            <asp:ListItem>Bolivia</asp:ListItem>
                            <asp:ListItem>Bosnia and Herzegovina</asp:ListItem>
                            <asp:ListItem>Botswana</asp:ListItem>
                            <asp:ListItem>Brazil</asp:ListItem>
                            <asp:ListItem>Brunei</asp:ListItem>
                            <asp:ListItem>Bulgaria</asp:ListItem>
                            <asp:ListItem>Burkina Faso</asp:ListItem>
                            <asp:ListItem>Burundi</asp:ListItem>
                            <asp:ListItem>Cabo Verde</asp:ListItem>
                            <asp:ListItem>Cambodia</asp:ListItem>
                            <asp:ListItem>Cameroon</asp:ListItem>
                            <asp:ListItem>Canada</asp:ListItem>
                            <asp:ListItem>Central African Republic (CAR)</asp:ListItem>
                            <asp:ListItem>Chad</asp:ListItem>
                            <asp:ListItem>Chile</asp:ListItem>
                            <asp:ListItem>China</asp:ListItem>
                            <asp:ListItem>Colombia</asp:ListItem>
                            <asp:ListItem>Comoros</asp:ListItem>
                            <asp:ListItem>Democratic Republic of the Congo</asp:ListItem>
                            <asp:ListItem>Republic of the Congo</asp:ListItem>
                            <asp:ListItem>Costa Rica</asp:ListItem>
                            <asp:ListItem>Cote d'Ivoire</asp:ListItem>
                            <asp:ListItem>Croatia</asp:ListItem>
                            <asp:ListItem>Cuba</asp:ListItem>
                            <asp:ListItem>Cyprus</asp:ListItem>
                            <asp:ListItem>Czech Republic</asp:ListItem>
                            <asp:ListItem>Denmark</asp:ListItem>
                            <asp:ListItem>Djibouti</asp:ListItem>
                            <asp:ListItem>Dominica</asp:ListItem>
                            <asp:ListItem>Dominican Republic</asp:ListItem>
                            <asp:ListItem>Ecuador</asp:ListItem>
                            <asp:ListItem>Egypt</asp:ListItem>
                            <asp:ListItem>El Salvador</asp:ListItem>
                            <asp:ListItem>Equatorial Guinea</asp:ListItem>
                            <asp:ListItem>Eritrea</asp:ListItem>
                            <asp:ListItem>Estonia</asp:ListItem>
                            <asp:ListItem>Ethiopia</asp:ListItem>
                            <asp:ListItem>Fiji</asp:ListItem>
                            <asp:ListItem>Finland</asp:ListItem>
                            <asp:ListItem>France</asp:ListItem>
                            <asp:ListItem>Gabon</asp:ListItem>
                            <asp:ListItem>Gambia</asp:ListItem>
                            <asp:ListItem>Georgia</asp:ListItem>
                            <asp:ListItem>Germany</asp:ListItem>
                            <asp:ListItem>Ghana</asp:ListItem>
                            <asp:ListItem>Greece</asp:ListItem>
                            <asp:ListItem>Grenada</asp:ListItem>
                            <asp:ListItem>Guatemala</asp:ListItem>
                            <asp:ListItem>Guinea</asp:ListItem>
                            <asp:ListItem>Guinea-Bissau</asp:ListItem>
                            <asp:ListItem>Guyana</asp:ListItem>
                            <asp:ListItem>Haiti</asp:ListItem>
                            <asp:ListItem>Honduras</asp:ListItem>
                            <asp:ListItem>Hungary</asp:ListItem>
                            <asp:ListItem>Iceland</asp:ListItem>
                            <asp:ListItem>India</asp:ListItem>
                            <asp:ListItem>Indonesia</asp:ListItem>
                            <asp:ListItem>Iran</asp:ListItem>
                            <asp:ListItem>Iraq</asp:ListItem>
                            <asp:ListItem>Ireland</asp:ListItem>
                            <asp:ListItem>Israel</asp:ListItem>
                            <asp:ListItem>Italy</asp:ListItem>
                            <asp:ListItem>Jamaica</asp:ListItem>
                            <asp:ListItem>Japan</asp:ListItem>
                            <asp:ListItem>Jordan</asp:ListItem>
                            <asp:ListItem>Kazakhstan</asp:ListItem>
                            <asp:ListItem>Kenya</asp:ListItem>
                            <asp:ListItem>Kiribati</asp:ListItem>
                            <asp:ListItem>Kosovo</asp:ListItem>
                            <asp:ListItem>Kuwait</asp:ListItem>
                            <asp:ListItem>Kyrgyzstan</asp:ListItem>
                            <asp:ListItem>Laos</asp:ListItem>
                            <asp:ListItem>Latvia</asp:ListItem>
                            <asp:ListItem>Lebanon</asp:ListItem>
                            <asp:ListItem>Lesotho</asp:ListItem>
                            <asp:ListItem>Liberia</asp:ListItem>
                            <asp:ListItem>Libya</asp:ListItem>
                            <asp:ListItem>Liechtenstein</asp:ListItem>
                            <asp:ListItem>Lithuania</asp:ListItem>
                            <asp:ListItem>Luxembourg</asp:ListItem>
                            <asp:ListItem>Macedonia (FYROM)</asp:ListItem>
                            <asp:ListItem>Madagascar</asp:ListItem>
                            <asp:ListItem>Malawi</asp:ListItem>
                            <asp:ListItem>Malaysia</asp:ListItem>
                            <asp:ListItem>Maldives</asp:ListItem>
                            <asp:ListItem>Mali</asp:ListItem>
                            <asp:ListItem>Malta</asp:ListItem>
                            <asp:ListItem>Marshall Islands</asp:ListItem>
                            <asp:ListItem>Mauritania</asp:ListItem>
                            <asp:ListItem>Mauritius</asp:ListItem>
                            <asp:ListItem>Mexico</asp:ListItem>
                            <asp:ListItem>Micronesia</asp:ListItem>
                            <asp:ListItem>Moldova</asp:ListItem>
                            <asp:ListItem>Monaco</asp:ListItem>
                            <asp:ListItem>Mongolia</asp:ListItem>
                            <asp:ListItem>Montenegro</asp:ListItem>
                            <asp:ListItem>Morocco</asp:ListItem>
                            <asp:ListItem>Mozambique</asp:ListItem>
                            <asp:ListItem>Myanmar (Burma)</asp:ListItem>
                            <asp:ListItem>Namibia</asp:ListItem>
                            <asp:ListItem>Nauru</asp:ListItem>
                            <asp:ListItem>Nepal</asp:ListItem>
                            <asp:ListItem>Netherlands</asp:ListItem>
                            <asp:ListItem>New Zealand</asp:ListItem>
                            <asp:ListItem>Nicaragua</asp:ListItem>
                            <asp:ListItem>Niger</asp:ListItem>
                            <asp:ListItem>Nigeria</asp:ListItem>
                            <asp:ListItem>North Korea</asp:ListItem>
                            <asp:ListItem>Norway</asp:ListItem>
                            <asp:ListItem>Oman</asp:ListItem>
                            <asp:ListItem>Pakistan</asp:ListItem>
                            <asp:ListItem>Palau</asp:ListItem>
                            <asp:ListItem>Palestine</asp:ListItem>
                            <asp:ListItem>Panama</asp:ListItem>
                            <asp:ListItem>Papua New Guinea</asp:ListItem>
                            <asp:ListItem>Paraguay</asp:ListItem>
                            <asp:ListItem>Peru</asp:ListItem>
                            <asp:ListItem>Philippines</asp:ListItem>
                            <asp:ListItem>Poland</asp:ListItem>
                            <asp:ListItem>Portugal</asp:ListItem>
                            <asp:ListItem>Qatar</asp:ListItem>
                            <asp:ListItem>Romania</asp:ListItem>
                            <asp:ListItem>Russia</asp:ListItem>
                            <asp:ListItem>Rwanda</asp:ListItem>
                            <asp:ListItem>Saint Kitts and Nevis</asp:ListItem>
                            <asp:ListItem>Saint Lucia</asp:ListItem>
                            <asp:ListItem>Saint Vincent and the Grenadines</asp:ListItem>
                            <asp:ListItem>Samoa</asp:ListItem>
                            <asp:ListItem>San Marino</asp:ListItem>
                            <asp:ListItem>Sao Tome and Principe</asp:ListItem>
                            <asp:ListItem>Saudi Arabia</asp:ListItem>
                            <asp:ListItem>Senegal</asp:ListItem>
                            <asp:ListItem>Serbia</asp:ListItem>
                            <asp:ListItem>Seychelles</asp:ListItem>
                            <asp:ListItem>Sierra Leone</asp:ListItem>
                            <asp:ListItem>Singapore</asp:ListItem>
                            <asp:ListItem>Slovakia</asp:ListItem>
                            <asp:ListItem>Slovenia</asp:ListItem>
                            <asp:ListItem>Solomon Islands</asp:ListItem>
                            <asp:ListItem>Somalia</asp:ListItem>
                            <asp:ListItem>South Africa</asp:ListItem>
                            <asp:ListItem>South Korea</asp:ListItem>
                            <asp:ListItem>South Sudan</asp:ListItem>
                            <asp:ListItem>Spain</asp:ListItem>
                            <asp:ListItem>Sri Lanka</asp:ListItem>
                            <asp:ListItem>Sudan</asp:ListItem>
                            <asp:ListItem>Suriname</asp:ListItem>
                            <asp:ListItem>Swaziland</asp:ListItem>
                            <asp:ListItem>Sweden</asp:ListItem>
                            <asp:ListItem>Switzerland</asp:ListItem>
                            <asp:ListItem>Syria</asp:ListItem>
                            <asp:ListItem>Taiwan</asp:ListItem>
                            <asp:ListItem>Tajikistan</asp:ListItem>
                            <asp:ListItem>Tanzania</asp:ListItem>
                            <asp:ListItem>Thailand</asp:ListItem>
                            <asp:ListItem>Timor-Leste</asp:ListItem>
                            <asp:ListItem>Togo</asp:ListItem>
                            <asp:ListItem>Tonga</asp:ListItem>
                            <asp:ListItem>Trinidad and Tobago</asp:ListItem>
                            <asp:ListItem>Tunisia</asp:ListItem>
                            <asp:ListItem>Turkey</asp:ListItem>
                            <asp:ListItem>Turkmenistan</asp:ListItem>
                            <asp:ListItem>Tuvalu</asp:ListItem>
                            <asp:ListItem>Uganda</asp:ListItem>
                            <asp:ListItem>Ukraine</asp:ListItem>
                            <asp:ListItem>United Arab Emirates (UAE)</asp:ListItem>
                            <asp:ListItem>United Kingdom (UK)</asp:ListItem>
                            <asp:ListItem>United States of America (USA)</asp:ListItem>
                            <asp:ListItem>Uruguay</asp:ListItem>
                            <asp:ListItem>Uzbekistan</asp:ListItem>
                            <asp:ListItem>Vanuatu</asp:ListItem>
                            <asp:ListItem>Vatican City (Holy See)</asp:ListItem>
                            <asp:ListItem>Venezuela</asp:ListItem>
                            <asp:ListItem>Vietnam</asp:ListItem>
                            <asp:ListItem>Yemen</asp:ListItem>
                            <asp:ListItem>Zambia</asp:ListItem>
                            <asp:ListItem>Zimbabwe</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label>Registration Type:</label>
                    <div class="custom-control custom-radio custom-control-inline">
                        <asp:RadioButton ID="rbOnline" runat="server" GroupName="payment" />
                        <label class="custom-control-label" for='<%= rbOnline.ClientID %>'>Online</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <asp:RadioButton ID="rbPayAtEvent" runat="server" Checked GroupName="payment" />
                        <label class="custom-control-label" for='<%= rbPayAtEvent.ClientID %>'>Pay At Event / Offline</label>
                    </div>
                </div>
                <hr />
                <div class="form-group row">
                    <div class="col-12 text-center">
                        <small class="text-secondary">By Registering to the event, you agree to the <a href="<%= Page.ResolveUrl("~") %>Event-Terms.aspx"><strong>Terms &amp; Conditions</strong></a> of Steel Group.</small>
                        <br />
                        <br />
                        <asp:Button ValidationGroup="reg" ID="btnRegister" OnClick="btnRegister_Click" CssClass="btn btn-lg btn-info" runat="server" Text="Register Now" />
                        <asp:ValidationSummary ID="valsum" ValidationGroup="reg" ShowMessageBox="true" ShowSummary="false" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

