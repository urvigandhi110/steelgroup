﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="event-new.aspx.cs" Inherits="event_new" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <title></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="/images/favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="msapplication-config" content="/images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#000000">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css">
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">--%>
    <script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css" />
    <link href="css/steelgroup.css" rel="stylesheet" />
    <link href="css/eventpage-alt.css" rel="stylesheet" />
</head>
<body class="eventpage">
    <form id="frmEvents" runat="server">

        <section id="menu">
            <nav class="navbar nav-masthead navbar-expand-md navbar-dark fixed-top">
                <a class="navbar-brand" href="#">
                    <img class="w-50" alt="" src="images/steelgroup-logo-svg-eventspage.svg" />
                    <span>Conferences</span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav">
                        <a class="nav-link" href="event-new.aspx"><i class="fas fa-home"></i>&nbsp;Home</a>
                        <a class="nav-link" href="Register.aspx"><i class="fas fa-pen-square"></i>&nbsp;Register</a>
                        <a class="nav-link" href="event-new.aspx#downloads"><i class="fas fa-file-pdf"></i>&nbsp;Brochure</a>
                        <a class="nav-link" href="event-new.aspx#agenda"><i class="fas fa-calendar-alt"></i>&nbsp;Agenda</a>
                        <a class="nav-link" href="event-new.aspx#venue"><i class="fas fa-map-marker"></i>&nbsp;Venue</a>
                        <a class="nav-link" href="event-new.aspx#speakers"><i class="fas fa-microphone"></i>&nbsp;Speakers</a>
                        <a class="nav-link" href="event-new.aspx#sponsors"><i class="fas fa-bullhorn"></i>&nbsp;Sponsors</a>
                    </div>

                </div>
            </nav>
        </section>
        <section id="slider">
            <div class="swiper-container swiper-slider">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">

                        <img class="img-fluid" src="images/event-bg-new.jpg" />
                        <div class="caption">
                            <p class="text-center d-block eventdetails">
                                <span class="fas fa-calendar"></span>
                                <span class="mr-4">April 13, 2018</span>
                                <span class="fas fa-map-marker"></span>
                                <span>The Westin Goregaon, Mumbai</span>
                            </p>
                            <h1 class="cover-heading">6th Galvanizing Coating &amp; Profiling Summit</h1>
                            <p class="lead mt-3">
                                <a href="Register.aspx" class="btn btn-lg btn-info">Register Now</a>
                            </p>
                        </div>

                    </div>
                    <div class="swiper-slide">
                        <img class="img-fluid" src="images/event-bg-new-2.jpg" />
                        <div class="caption">
                            <p class="text-center d-block eventdetails">
                                <span class="fas fa-calendar"></span>
                                <span class="mr-4">April 13, 2018</span>
                                <span class="fas fa-map-marker"></span>
                                <span>The Westin Goregaon, Mumbai</span>
                            </p>
                            <h1 class="cover-heading">6th Galvanizing Coating &amp; Profiling Summit</h1>
                            <p class="lead mt-3">
                                <a href="Register.aspx" class="btn btn-lg btn-info">Register Now</a>
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <img class="img-fluid" src="images/event-bg-new-3.jpg" />
                        <div class="caption">
                            <p class="text-center d-block eventdetails">
                                <span class="fas fa-calendar"></span>
                                <span class="mr-4">April 13, 2018</span>
                                <span class="fas fa-map-marker"></span>
                                <span>The Westin Goregaon, Mumbai</span>
                            </p>
                            <h1 class="cover-heading">6th Galvanizing Coating &amp; Profiling Summit</h1>
                            <p class="lead mt-3">
                                <a href="Register.aspx" class="btn btn-lg btn-info">Register Now</a>
                            </p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <img class="img-fluid" src="images/event-bg-new-4.jpg" />
                        <div class="caption">
                            <p class="text-center d-block eventdetails">
                                <span class="fas fa-calendar"></span>
                                <span class="mr-4">April 13, 2018</span>
                                <span class="fas fa-map-marker"></span>
                                <span>The Westin Goregaon, Mumbai</span>
                            </p>
                            <h1 class="cover-heading">6th Galvanizing Coating &amp; Profiling Summit</h1>
                            <p class="lead mt-3">
                                <a href="Register.aspx" class="btn btn-lg btn-info">Register Now</a>
                            </p>
                        </div>
                    </div>
                </div>

                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 col-12"></div>
                <div class="col-md-8 col-12">
                    <div class="highlights highlights-dark">

                        <div class="row no-gutters">
                            <div class="col-12 timer text-center">
                                <div id="days"></div>
                                <div id="hours"></div>
                                <div id="minutes"></div>
                                <div id="seconds"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-12"></div>
            </div>

        </div>
        <div class="clearfix"></div>
        <section id="timer">
            <div class="col-12 col-md-6 offset-md-3">
                <div class="row no-gutters">
                    <div class="col-3 highlightsingle">
                        <img src="images/eventicons-alt/companies.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                        <h4>250+</h4>
                        <p>Companies</p>
                    </div>
                    <div class="col-3 highlightsingle">
                        <img src="images/eventicons-alt/delegates.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                        <h4>400+</h4>
                        <p>Delegates</p>
                    </div>
                    <div class="col-3 highlightsingle">
                        <img src="images/eventicons-alt/panelists.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                        <h4>20+</h4>
                        <p>Panelists</p>
                    </div>
                    <div class="col-3 highlightsingle">
                        <img src="images/eventicons-alt/exhibitors.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                        <h4>20</h4>
                        <p>Exhibitors</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="aboutevent">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">About Conference</h3>
                        <p class="sectionsubheading">What is 6th GCP All About</p>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-5 text-center">
                            <!-- Large modal -->
                            <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
                                <img src="images/videobg.jpg" />
                            </a>

                            <div class="modal fade bd-example-modal-lg videomodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Conference Video</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <iframe width="100%" height="400" src="https://www.youtube.com/embed/SKKOLy8ADGY?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-7 abouttabs">

                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">About 6th GCP</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">5th GCP Summary</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">About Steel Group</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <p class="w-100 text-justify">
                                        Here we bring the 6th edition of the most popular and grand conferences - Galvanizing Coating & Proﬁling (GCP) Summit in, in the commercial capital of India, Mumbai.
                                    </p>
                                    <p class="w-100 text-justify">
                                        The 6th segment of GCP franchise brings a lot more of discussions than the presentations. The purpose of this conference is to bring interactive sessions for all delegates and better business opportunities.
                                    </p>
                                </div>
                                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <p class="w-100 text-justify">
                                        The 6th segment of GCP franchise brings a lot more of discussions than the presentations. The purpose of this conference is to bring interactive sessions for all delegates and better business opportunities.
                                    </p>
                                </div>
                                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                    <p class="w-100 text-justify">
                                        Steelgroup is serving to Steel industry as one of the leading information and networking platform. We organize very focussed conferences for steel industry. Steelgroup e-newsletter about recent happenings in the industry is received by more than 10,000 recipients. Steelgroup is formed in the year 2010 by Mr. Ajay Tambe. In the year 2013, We have started the initiative of very focussed conferences on Galvanizing and Colour Coating in Mumbai JSW Steel was main Sponsor. Since than steelgroup is organizing very focussed and successful conferences.
                                    </p>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </section>
        <a name="agenda"></a>
        <section id="agenda pb-0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center pt-5 mt-2">
                        <h3 class="sectionheading">Conference Agenda</h3>
                        <p class="sectionsubheading">Do not miss a Topic</p>
                    </div>
                </div>
                <div class="row bg-light mt-3 py-5">
                    <div class="col-12 col-md-8 offset-md-2">
                        <div class="agendatabs">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Day-1</a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Day-2</a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Day-3</a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="row no-gutters agendaitem">
                                        <div class="col-3 text-center py-3">
                                            <img src="images/agenda/reg-image.jpg" class="rounded-circle agendaimg" />
                                        </div>
                                        <div class="col-9 py-3 px-1">
                                            <div class="agendadetails">
                                                <span>08:00 AM - 09:30 AM</span>
                                                <h4>Registration</h4>
                                                <p>Pick up your name badge and goodie bag.</p>
                                            </div>
                                        </div>
                                        <hr class="offset-4 w-50 border-secondary" />
                                    </div>
                                    <div class="row no-gutters agendaitem">
                                        <div class="col-3 text-center py-3">
                                            <img src="images/agenda/speak-image.jpg" class="rounded-circle agendaimg" />
                                        </div>
                                        <div class="col-9 py-3 px-1">
                                            <div class="agendadetails">
                                                <span>09:30 AM - 09:45 AM</span>
                                                <h4>Welcome Remark by Organizer</h4>
                                                <p>We welcome you at our best.</p>
                                            </div>
                                        </div>
                                        <hr class="offset-4 w-50 border-secondary" />
                                    </div>
                                    <div class="row no-gutters agendaitem">
                                        <div class="col-3 text-center py-3">
                                            <img src="images/agenda/inaug-image.jpg" class="rounded-circle agendaimg" />
                                        </div>
                                        <div class="col-9 py-3 px-1">
                                            <div class="agendadetails">
                                                <span>09:30 AM - 09:45 AM</span>
                                                <h4>Inauguration: Lamp Lighting</h4>
                                                <p class="font-italic">Guest of Honor :Mr. Sajjan Jindal ,Chairman & Managing Director, JSW Steel (I).</p>
                                                <ul>
                                                    <li>Mr. Mohammed Iqbal Hossain Managing Director, PHP Cold Rolling Mills Ltd, Bangladesh (I)</li>
                                                    <li>Mr.Pradeep Agarwal ,Vice Chairman Asian Colour Coated Ispat Ltd (I)</li>
                                                    <li>Mr.Mitesh Shah –MD, Steelco Gujarat Ltd (I)</li>
                                                    <li>Mr.Suresh Joshi ,Chairman ,Esmech Equipments Pvt.Ltd.,SMS Group (I)</li>
                                                    <li>Mr.Navneet Gill ,Managing Director YOGIJI-DIGI (I)</li>
                                                    <li>Mr.Munir H Khan ,Director &CEO ,KYCR Bangladesh</li>
                                                    <li>Mr.R.V.Sridhar ,CEO Essar Steel Pune Facility (I)</li>
                                                    <li>Mr.Rajesh Mehrotra-CEO Berger Becker Coatings Pvt.Ltd. (I)</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <hr class="offset-4 w-50 border-secondary" />
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">...</div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <a name="downloads"></a>
        <section id="downloads">
            <div class="row no-gutters">
                <div class="col-12 text-center">
                    <h3 class="sectionheading text-white">Downloads</h3>
                </div>
            </div>
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row no-gutters pt-5">
                    <div class="col-2 highlightsingle text-center offset-1">
                        <a href="http://www.samparkdemo.in/steelgroup/6thgcp/downloads/Registration-Form-6th-GCP-Mumbai-2018.docx">
                            <i class="fas fa-file-alt"></i>
                            <h4>Registration Form</h4>
                        </a>
                    </div>
                    <div class="col-2 highlightsingle text-center">
                        <a href="http://www.samparkdemo.in/steelgroup/6thgcp/downloads/Sponsorship-6th-GCP-Mumbai-2018.pdf">
                            <i class="fas fa-bullhorn"></i>
                            <h4>Sponsorship Form</h4>
                        </a>
                    </div>
                    <div class="col-2 highlightsingle text-center">
                        <a href="http://www.samparkdemo.in/steelgroup/6thgcp/downloads/Brochure-6th-GCP-Mumbai-2018.pdf">
                            <i class="fas fa-book"></i>
                            <h4>Brochure</h4>
                        </a>
                    </div>
                    <div class="col-2 highlightsingle text-center">
                        <a href="http://www.samparkdemo.in/steelgroup/6thgcp/downloads/Agenda-6th-GCP-Mumbai-2018.pdf">
                            <i class="fas fa-calendar-alt"></i>
                            <h4>Agenda</h4>
                        </a>
                    </div>
                    <div class="col-2 highlightsingle text-center">
                        <a href="http://www.samparkdemo.in/steelgroup/6thgcp/downloads/Exhibition-Layout-6th-GCP-Mumbai-2018.jpg">
                            <i class="fas fa-map"></i>
                            <h4>Layout</h4>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <a name="speakers"></a>
        <section id="speakers">
            <svg width="26" height="28" style="display: none;">
                <symbol id="twitter" viewBox="0 0 26 28">
                    <path d="M25.312 6.375c-0.688 1-1.547 1.891-2.531 2.609 0.016 0.219 0.016 0.438 0.016 0.656 0 6.672-5.078 14.359-14.359 14.359-2.859 0-5.516-0.828-7.75-2.266 0.406 0.047 0.797 0.063 1.219 0.063 2.359 0 4.531-0.797 6.266-2.156-2.219-0.047-4.078-1.5-4.719-3.5 0.313 0.047 0.625 0.078 0.953 0.078 0.453 0 0.906-0.063 1.328-0.172-2.312-0.469-4.047-2.5-4.047-4.953v-0.063c0.672 0.375 1.453 0.609 2.281 0.641-1.359-0.906-2.25-2.453-2.25-4.203 0-0.938 0.25-1.797 0.688-2.547 2.484 3.062 6.219 5.063 10.406 5.281-0.078-0.375-0.125-0.766-0.125-1.156 0-2.781 2.25-5.047 5.047-5.047 1.453 0 2.766 0.609 3.687 1.594 1.141-0.219 2.234-0.641 3.203-1.219-0.375 1.172-1.172 2.156-2.219 2.781 1.016-0.109 2-0.391 2.906-0.781z"></path>
                </symbol>
                <symbol id="codepen" viewBox="0 0 28 28">
                    <path d="M3.375 18.266l9.422 6.281v-5.609l-5.219-3.484zM2.406 16.016l3.016-2.016-3.016-2.016v4.031zM15.203 24.547l9.422-6.281-4.203-2.812-5.219 3.484v5.609zM14 16.844l4.25-2.844-4.25-2.844-4.25 2.844zM7.578 12.547l5.219-3.484v-5.609l-9.422 6.281zM22.578 14l3.016 2.016v-4.031zM20.422 12.547l4.203-2.812-9.422-6.281v5.609zM28 9.734v8.531c0 0.391-0.203 0.781-0.531 1l-12.797 8.531c-0.203 0.125-0.438 0.203-0.672 0.203s-0.469-0.078-0.672-0.203l-12.797-8.531c-0.328-0.219-0.531-0.609-0.531-1v-8.531c0-0.391 0.203-0.781 0.531-1l12.797-8.531c0.203-0.125 0.438-0.203 0.672-0.203s0.469 0.078 0.672 0.203l12.797 8.531c0.328 0.219 0.531 0.609 0.531 1z"></path>
                </symbol>
                <symbol id="linkedin" viewBox="0 0 24 28">
                    <path d="M5.453 9.766v15.484h-5.156v-15.484h5.156zM5.781 4.984c0.016 1.484-1.109 2.672-2.906 2.672v0h-0.031c-1.734 0-2.844-1.188-2.844-2.672 0-1.516 1.156-2.672 2.906-2.672 1.766 0 2.859 1.156 2.875 2.672zM24 16.375v8.875h-5.141v-8.281c0-2.078-0.75-3.5-2.609-3.5-1.422 0-2.266 0.953-2.641 1.875-0.125 0.344-0.172 0.797-0.172 1.266v8.641h-5.141c0.063-14.031 0-15.484 0-15.484h5.141v2.25h-0.031c0.672-1.062 1.891-2.609 4.672-2.609 3.391 0 5.922 2.219 5.922 6.969z"></path>
                </symbol>
            </svg>
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">Speakers</h3>
                        <p class="sectionsubheading">Know Who's Saying What</p>
                    </div>
                </div>

                <div class="row no-gutters">
                    <div class="col-3 text-center speakerdetails">
                        <div class="imgblur rounded-circle  w-75 mx-auto">
                            <img src="images/speaker-4.jpg" alt="" class="img-fluid d-block mx-auto rounded-circle" />
                        </div>
                        <a href="#" class="btn btn-sm btn-outline-light detailsbtn">View Details</a>
                        <h4>Andrew</h4>
                        <p class="badge badge-pill">MRM</p>
                    </div>
                    <div class="col-3 text-center speakerdetails">
                        <div class="imgblur rounded-circle  w-75 mx-auto">
                            <img src="images/speaker-1.jpg" alt="" class="img-fluid d-block mx-auto rounded-circle" />
                        </div>
                        <a href="#" class="btn btn-sm btn-outline-light detailsbtn">View Details</a>
                        <h4>Navneet Gill</h4>
                        <p class="badge badge-pill">YOGIJI DIGI</p>
                    </div>
                    <div class="col-3 text-center speakerdetails">
                        <div class="imgblur rounded-circle  w-75 mx-auto">
                            <img src="images/speaker-2.jpg" alt="" class="img-fluid d-block mx-auto rounded-circle" />
                        </div>
                        <a href="#" class="btn btn-sm btn-outline-light detailsbtn">View Details</a>
                        <h4>D van Deventer</h4>
                        <p class="badge badge-pill">BECKERS</p>
                    </div>
                    <div class="col-3 text-center speakerdetails">
                        <div class="imgblur rounded-circle  w-75 mx-auto">
                            <img src="images/speaker-3.jpg" alt="" class="img-fluid d-block mx-auto rounded-circle" />
                        </div>
                        <a href="#" class="btn btn-sm btn-primary detailsbtn">View Details</a>
                        <h4>Mahesh</h4>
                        <p class="badge badge-pill">BRONX</p>
                    </div>
                </div>
            </div>
        </section>
        <a name="sponsors"></a>
        <section id="sponsors">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">Premium Sponsors</h3>
                        <p class="sectionsubheading">Know Our Partners</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-3">
                        <div class="card-flipper effect__hover" data-id="1">
                            <div class="card__front">
                                <div class="card card-01">
                                    <div class="profile-box-01">
                                        <img class="card-img-top" src="images/sponsor-1.jpg" alt="Card image cap">
                                    </div>
                                </div>
                            </div>
                            <div class="card__back">
                                <div class="card card-01">
                                    <div class="card-body text-center">
                                        <h4 class="card-title">Nippon Paints</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <span class="social-box">
                                            <a href="#"><i class="fab fa-facebook"></i></a>
                                            <a href="#"><i class="fab fa-twitter"></i></a>
                                            <a href="#"><i class="fab fa-google-plus"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="card-flipper effect__hover" data-id="2">
                            <div class="card__front">
                                <div class="card card-01">
                                    <div class="profile-box-01">
                                        <img class="card-img-top" src="images/sponsor-2.jpg" alt="Card image cap">
                                    </div>
                                </div>
                            </div>
                            <div class="card__back">
                                <div class="card card-01">
                                    <div class="card-body text-center">
                                        <h4 class="card-title">Inductotherm</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <span class="social-box">
                                            <a href="#"><i class="fab fa-facebook"></i></a>
                                            <a href="#"><i class="fab fa-twitter"></i></a>
                                            <a href="#"><i class="fab fa-google-plus"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="card-flipper effect__hover" data-id="3">
                            <div class="card__front">
                                <div class="card card-01">
                                    <div class="profile-box-01">
                                        <img class="card-img-top" src="images/sponsor-3.jpg" alt="Card image cap">
                                    </div>
                                </div>
                            </div>
                            <div class="card__back">
                                <div class="card card-01">
                                    <div class="card-body text-center">
                                        <h4 class="card-title">Sinoko Cranes</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <span class="social-box">
                                            <a href="#"><i class="fab fa-facebook"></i></a>
                                            <a href="#"><i class="fab fa-twitter"></i></a>
                                            <a href="#"><i class="fab fa-google-plus"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="card-flipper effect__hover" data-id="4">
                            <div class="card__front">
                                <div class="card card-01">
                                    <div class="profile-box-01">
                                        <img class="card-img-top" src="images/sponsor-4.jpg" alt="Card image cap">
                                    </div>
                                </div>
                            </div>
                            <div class="card__back">
                                <div class="card card-01">
                                    <div class="card-body text-center">
                                        <h4 class="card-title">Bronx</h4>
                                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <span class="social-box">
                                            <a href="#"><i class="fab fa-facebook"></i></a>
                                            <a href="#"><i class="fab fa-twitter"></i></a>
                                            <a href="#"><i class="fab fa-google-plus"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <div class="clearfix"></div>
        <section id="othersponsors">
            <div class="row no-gutters">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Exhibitors</h3>
                    <p class="sectionsubheading">Know more</p>
                </div>
            </div>
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp1.jpg" class="img-fluid" />
                    </div>
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp2.jpg" class="img-fluid" />
                    </div>
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp3.jpg" class="img-fluid" />
                    </div>
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp4.jpg" class="img-fluid" />
                    </div>
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp5.jpg" class="img-fluid" />
                    </div>
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp6.jpg" class="img-fluid" />
                    </div>
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp7.jpg" class="img-fluid" />
                    </div>
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp8.jpg" class="img-fluid" />
                    </div>
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp9.jpg" class="img-fluid" />
                    </div>
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp10.jpg" class="img-fluid" />
                    </div>
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp11.jpg" class="img-fluid" />
                    </div>
                    <div class="col-6 col-md-2">
                        <img src="images/sponsors/sp12.jpg" class="img-fluid" />
                    </div>
                </div>
            </div>
        </section>
        <section id="layout">
            <div class="row no-gutters">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Exhibition Layout</h3>
                    <p class="sectionsubheading">Space for Everyone</p>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-12">
                    <img class="img-fluid" src="images/6thgcp-layout.jpg" />
                </div>
            </div>
        </section>
        <a name="contact"></a>
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">Contact us</h3>
                        <p class="sectionsubheading">The Organizers</p>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-12 col-md-4 offset-md-4">
                        <div class="row no-gutters">
                            <div class="col-6 text-center teambox">
                                <img src="images/ajay-sir.jpg" class="rounded-circle m-3" />
                                <h4>Ajay Tambe</h4>
                                <p>CEO &amp; Founder</p>
                                <ul class="usersocial">
                                    <li><a href="mailto:ajay.tambe@steelgroup.co.in"><i class="fas fa-envelope"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/ajay-tambe-steelgroup-b8415920/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                </ul>
                            </div>
                            <div class="col-6 text-center teambox">
                                <img src="images/amit-ji.jpg" class="rounded-circle m-3" />
                                <h4>Amit Purohit</h4>
                                <p>Marketing Head</p>
                                <ul class="usersocial">
                                    <li><a href="mailto:amit@steelgroup.co.in"><i class="fas fa-envelope"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/ap6887/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <a name="venue"></a>
        <section id="venue">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">The Venue</h3>
                        <p class="sectionsubheading">Where to come</p>
                    </div>
                </div>
                <div class="row contactdetails">
                    <div class="col-12 col-md-4">
                        <div class="row no-gutters">
                            <div class="col-4 text-right pr-4">
                                <i class="fas fa-map-marker"></i>
                            </div>
                            <div class="col-8">
                                <h4>Venue</h4>
                                <p>The Westin Mumbai Garden City</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="row no-gutters">
                            <div class="col-4 text-right pr-4">
                                <i class="fas fa-envelope"></i>
                            </div>
                            <div class="col-8">
                                <h4>Email Us</h4>
                                <p>info@steelgroup.co.in</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="row no-gutters">
                            <div class="col-4 text-right pr-4">
                                <i class="fas fa-phone"></i>
                            </div>
                            <div class="col-8">
                                <h4>Call us</h4>
                                <p>+91 98589 66666</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-12">
                    <div class="gmap">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3768.509926835645!2d72.86067078211448!3d19.17291775925607!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b7a8545ce553%3A0xdab2a72a6041be58!2sThe+Westin+Mumbai+Garden+City!5e0!3m2!1sen!2sin!4v1516710479993" width="100%" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

        </section>
        <section id="footer">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col">
                        <div class="social">
                            <h2>Follow us</h2>
                            <a href="#">
                                <img src="images/social/facebook.svg" /></a>
                            <a href="#">
                                <img src="images/social/linkedin.svg" /></a>
                            <a href="#">
                                <img src="images/social/instagram.svg" /></a>
                            <a href="#">
                                <img src="images/social/twitter.svg" /></a>
                            <a href="#">
                                <img src="images/social/google.svg" /></a>
                            <a href="#">
                                <img src="images/social/youtube.svg" /></a>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters footerrow">
                    <div class="col">
                        <h4>COMPANY</h4>
                        <a href="#">About us</a>
                        <a href="#">Team</a>
                        <a href="#">Services</a>
                        <a href="#">Privacy Policy</a>
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                    <div class="col">
                        <h4>EVENTS</h4>
                        <a href="#">3rd CRPT</a>
                        <a href="#">2nd CRPT</a>
                        <a href="#">Automotive</a>
                    </div>
                    <div class="col">
                        <h4>NEWS</h4>

                    </div>
                    <div class="col">
                        <img src="images/steel-group-footer-logo.png" class="img-fluid w-75" />
                    </div>
                </div>
                <div class="row no-gutters copyright">
                    <div class="col">
                        <p class="text-center">Copyright &copy; 2018 Steel Group | All Rights Reserved | All logos, symbols &amp; Trademarks belong to their respective owners.</p>
                    </div>
                </div>
            </div>
        </section>
    </form>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
    <script src="js/timer.js"></script>
    <script>
        $(window).scroll(function () {
            var wscroll = $(this).scrollTop();
            if (wscroll > 80) {
                $(".navbar").addClass("shrink-nav-dark animated fadeInDown");
            }
            else {
                $(".navbar").removeClass("shrink-nav-dark");
                $(".navbar").removeClass("animated");
                $(".navbar").removeClass("fadeInDown");
            }
        });
        $(document).ready(function () {
            //initialize swiper when document ready
            var mySwiper = new Swiper('.swiper-container', {
                autoplay: {
                    delay: 5000,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                loop: true
            })
        });
        var loc = window.location.pathname;

        $('.navbar-nav').find('a.nav-link').each(function () {
            if ('/' + $(this).attr('href') == loc) {
                $(this).toggleClass('active');
                return;
            }
        });//active open

    </script>
</body>
</html>
