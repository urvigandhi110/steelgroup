﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Events : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;
    SqlDataAdapter da;
    DataSet ds;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT eventID,eventName,eventImage1,eventStartDate,eventCity,eventCountry,eventVenue FROM eventsmaster WHERE publish='Yes'; SELECT TOP 10 * FROM thirdPartyEvents WHERE tpEventStartDate>GETDATE() ORDER BY tpEventStartDate");
            steelcmd.Connection = steelcon;
            steelcon.Open();


            da = new SqlDataAdapter();
            ds = new DataSet();

            da.SelectCommand = steelcmd;
            da.Fill(ds);

            ds.Tables[0].DefaultView.RowFilter = "eventStartDate>=#" + DateTime.Today.ToString("yyyy-MM-dd hh:mm:ss.fff") + "#";
            ds.Tables[0].DefaultView.Sort = "eventStartDate";

            //Upcoming Events
            rptEventsUpcoming.DataSource = ds.Tables[0].DefaultView;
            rptEventsUpcoming.DataBind();

            rptEventSlider.DataSource = SteelFunc.GetTopDataViewRows(ds.Tables[0].DefaultView, 2);
            rptEventSlider.DataBind();

            ds.Tables[0].DefaultView.RowFilter = "";
            ds.Tables[0].DefaultView.RowFilter = "eventStartDate<#" + DateTime.Today.ToString("yyyy-MM-dd hh:mm:ss.fff") + "#";
            ds.Tables[0].DefaultView.Sort = "eventStartDate DESC";

            //Past Events
            rptEventsPast.DataSource = ds.Tables[0].DefaultView;
            rptEventsPast.DataBind();

            rptThirdParty.DataSource = ds.Tables[1];
            rptThirdParty.DataBind();

            da.Dispose();
            ds.Clear();
            ds.Dispose();
            steelcon.Close();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}