﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

public partial class SteelMain : System.Web.UI.MasterPage
{

    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                steelcon = new SqlConnection(connstr);
                steelcmd = new SqlCommand("SELECT TOP 1 advDimensions,advImage,advLinkURL FROM adsmaster WHERE advEndDate>=GETDATE() AND publish='Yes' AND advType LIKE '%Top:%' ORDER BY datecreated desc; SELECT eventID,eventName,eventImage1,eventStartDate,eventCity,eventCountry,eventVenue FROM eventsmaster WHERE publish='Yes' AND eventStartDate>=GETDATE()");

                steelcmd.Connection = steelcon;
                steelcon.Open();

                rdrd = steelcmd.ExecuteReader();

                if (rdrd.Read())
                {
                    lnkTopAd.NavigateUrl = rdrd["advLinkURL"].ToString();
                    lnkTopAd.Target = "_blank";
                    imgAdonTop.ImageUrl = Page.ResolveUrl("~/cms") + rdrd["advImage"].ToString().Replace("~/", "/") + "?w=" + rdrd["advDimensions"].ToString().Split('x')[0] + "&h=" + rdrd["advDimensions"].ToString().Split('x')[1] + "&mode=crop";
                }
                if (rdrd.NextResult())
                {
                    rptEventFooter.DataSource = rdrd;
                    rptEventFooter.DataBind();
                }

                rdrd.Close();
                steelcon.Close();
                steelcmd.Dispose();
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }
    }

    protected void lnkSubscribe_Click(object sender, EventArgs e)
    {
        try
        {
            string newRegID = "SUB" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(1000, 9999);

            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("INSERT INTO subscribermaster(subscriberID,subscriberEmail,datecreated,createdby,createdIP,subscriberStatus) VALUES(@subscriberID,@subscriberEmail,@datecreated,@createdby,@createdIP,@subscriberStatus)");
            steelcmd.Parameters.AddWithValue("@subscriberID", newRegID);
            steelcmd.Parameters.AddWithValue("@subscriberEmail", txtEmailtoSubscribe.Text);
            steelcmd.Parameters.AddWithValue("@datecreated", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@createdby", "");
            steelcmd.Parameters.AddWithValue("@createdIP", Request.UserHostAddress);
            steelcmd.Parameters.AddWithValue("@subscriberStatus", "Subscribed");

            steelcmd.Connection = steelcon;
            steelcon.Open();

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlSubscribe.Visible = false;
                pnlSubscribeMessage.Visible = true;

                ltrlSubscribeMessageText.Text = "Your email ID has been subscribed successfully to our email newsletters. Hope you enjoy a better time with our updates and news along with 10,000 persons everyday. You can also contact us to put your advertisement on email newsletters.";
                //Preparing registrant mail to send
                StreamReader readerRegistrant = new StreamReader(Server.MapPath("~/templates/steelsubscribe.txt"));
                string readFileRegistrant = readerRegistrant.ReadToEnd();
                readFileRegistrant = readFileRegistrant.Replace("$$SUBSCRIBERID$$", newRegID);
                
                string resp = SteelFunc.sendmail(txtEmailtoSubscribe.Text, "You have been subscribed to our Newsletters", readFileRegistrant);
                readerRegistrant.Close();
            }
            else
            {
                ltrlErrorMessage.Text = "Subscription Failed. Please try again.";
            }

            steelcon.Close();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        {
            ltrlErrorMessage.Text = "You are already subscribed to us.";
        }
    }
}
