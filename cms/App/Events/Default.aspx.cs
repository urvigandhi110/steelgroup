﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_Default : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    void Bindlist()
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT row_number() over(order by eventID) as sno,eventID,eventName,eventStartDate,eventEndDate,eventVenue,publish FROM eventsmaster");

            steelcmd.Connection = steelcon;
            steelcon.Open();
            rdrd = steelcmd.ExecuteReader();

            rptList.DataSource = rdrd;
            rptList.DataBind();

            steelcon.Close();
        }
        catch (Exception ex)
        { }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bindlist();
        }

    }

    protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "deleterecord")
        {
            try
            {
                steelcon = new SqlConnection(connstr);
                steelcmd = new SqlCommand();
                steelcmd.Connection = steelcon;
                steelcmd.CommandText = "DELETE FROM eventsmaster WHERE eventID=@eventID";

                steelcmd.Parameters.Add("@eventID", SqlDbType.VarChar);
                steelcmd.Parameters["@eventID"].Value = e.CommandArgument;

                steelcon.Open();
                int res = steelcmd.ExecuteNonQuery();

                if (res > 0)
                {
                    pnlMessage.Visible = true;
                    pnlMessage.CssClass = "alert alert-success";
                    ltrlmsg.Text = "Event has been deleted successfully.";

                    steelcon.Close();
                    steelcmd.Dispose();
                    steelcon.Dispose();

                    Bindlist();
                }
                else
                {
                    pnlMessage.Visible = true;
                    pnlMessage.CssClass = "alert alert-danger";
                    ltrlmsg.Text = "Event could not be deleted. Please Try Again";

                    steelcon.Close();
                    steelcmd.Dispose();
                    steelcon.Dispose();
                }
            }
            catch (Exception ex)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-danger";
                ltrlmsg.Text = ex.ToString();

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        else if(e.CommandName== "addagenda")
        {
            Response.Redirect("EventAgenda.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "addsponsor")
        {
            Response.Redirect("AddEventSponsor.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "addspeaker")
        {
            Response.Redirect("AddEventSpeaker.aspx?eventID=" + e.CommandArgument);
        }
        else if(e.CommandName== "viewsponsor")
        {
            Response.Redirect("ViewSponsors.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "viewspeaker")
        {
            Response.Redirect("ViewSpeakers.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "viewreg")
        {
            Response.Redirect("Registrations.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "editDL")
        {
            Response.Redirect("EditDownloads.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "editEvent")
        {
            Response.Redirect("EditEvent.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "addhashtags")
        {
            Response.Redirect("EventFeatures.aspx?eventID=" + e.CommandArgument);
        }
        else if (e.CommandName == "viewinvites")
        {
            Response.Redirect("ViewInvites.aspx?eventID=" + e.CommandArgument);
        }
        else { }
    }
}