﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="Event-Terms.aspx.cs" Inherits="Event_Terms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="row no-gutters py-4">
        <div class="col-12 text-center">
            <h3 class="sectionheading">Event Terms &amp; Conditions</h3>
            <p class="sectionsubheading">By registering you agree to these</p>
        </div>
    </div>
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 col-md-10 offset-md-1 text-justify">
                <p><strong>General Terms and Conditions</strong></p>

                <p><strong>DEFINITIONS</strong></p>
                <p>These Terms &amp; Conditions for delegates/speakers/sponsors/partners apply to every person registering as a regular delegate (Steelgroup member/non-member/trainee/counselor/student) for conferences, organized/co-organized by Steelgroup (hereafter referred to as &ldquo;the conference organizers&rdquo;).</p>

                <p>All references to a date or a deadline, mentioned in these terms and on other conference documents, refer to the Indian Standard Time (IST).</p>

                <p><strong>CONFERENCE REGISTRATION</strong></p>
                <p>Only fully completed registration forms will be accepted. An invoice will be sent after the receipt of your registration, a confirmation of payment will be sent upon receipt of payment in full. If the full payment has not been received before the deadline indicated, the registration will remain valid, however the due fee will be increased according to the payment period (early/regular/on site).</p>

                <p>To be able to register as a student or postgraduate trainee, individuals must present proof of fulltime enrolment at a recognized university or college or an according program (i.e. student ID, confirmation of head of department, etc.) either by fax at the time of pre-registration or during the on site registration process.</p>

                <p>The registration fee for delegate includes entry to all conference sessions, the exhibition and poster area, the networking lunch/dinner/both and delegate kit, provided that sufficient seating space is available. Please note that the security personnel of the conference centre are responsible for granting or denying access to conference halls in case of overfilling, according to room capacities and national legislation. The conference organizer cannot guarantee available space in every session.</p>

                <p>Family members will not be allowed to enter the conference or exhibition area. They must be linked to a regular delegate. Parents/guardians of children agree to take full responsibility for them while at the conference site or a networking event.</p>

                <p>The conference organizers cannot guarantee that a delegate kit and other conference materials will be available for late registrations. All conference materials will be distributed on-site.</p>
                <p>If the maximum delegate capacity is reached, the conference organizers reserve the right to refuse additional registrations.</p>

                <p><strong>REGISTRATION CONFIRMATION/RECEIPT</strong></p>
                <p>A registration confirmation/receipt will be sent by email after the online registration, a related payment and any necessary documents have been received by the registration department. Delegates may be requested to present this registration confirmation/receipt at the registration counter as proof of their registration and payment.</p>

                <p><strong>METHODS OF PAYMENT</strong></p>
                <p>Payments should be made in advance and in INR or USD only, using a credit card/debit card/cheque/demand draft or by bank transfer (NEFT/IMPS/RTGS).</p>
                <p>All bank fees and money transfer costs must be paid by the transmitter. Any negative balance will be collected on site. Indicate the registration number and the delegate&rsquo;s full name as a reference on all bank transfers.</p>

                <p><strong>REGISTRATION NAME CHANGE</strong></p>
                <p>For every name change to an existing conference registration, a prior intimation via official medium will be mandatory. A new registration form for the substitute delegate should be submitted. Name changes will be accepted by email indicating the old and new names including the required contact details.</p>

                <p><strong>CANCELLATION POLICY</strong></p>
                <p>Notice of cancellation must be made in writing by email or fax to the Steelgroup Head Office. The notification must include all relevant information regarding the bank account to which a possible refund may be remitted. The cancellation will not be effective until a written acknowledgement from Steelgroup Conference Registration Department is received.</p>
                <p>Registration fees may be refunded as follows:</p>
                <p>Written cancellation received:</p>
                <p>&ndash; 90 days before the conference official date: 100% refund</p>
                <p>&ndash; 60 days before the conference official date: 75% refund</p>
                <p>&ndash; 30 days before the conference official date: 25% refund</p>
                <p>&ndash; Less than 30 days: no refund</p>

                <p>The date of the email receipt date or fax ID will be the basis for considering refunds. Please indicate your bank details on your written cancellation. Refunds will be made after the conference.</p>

                <p>In the case of over-payment or double payment, refund requests must be made in writing and sent to the Steelgroup Conference Registration Department, by email.</p>

                <p><strong>No refunds</strong> will be granted for unattended events or early termination of attendance, in case of cancellation of speakers, lack of space in the conference room or any other incidents during the conference, which are beyond the control of the conference organizers.</p>

                <p>By registering to the Steelgroup conferences participants agree that neither the organizing committee nor the Steelgroup office assume any liability whatsoever. Participants are requested to make their own arrangements for health and travel insurance. The conference fee does not include insurance, travel and accommodation.</p>

                <p><strong>CANCELLATION OF THE CONFERENCE</strong></p>
                <p>In the event that the conference cannot be held or is postponed due to events beyond the control of the conference organizers or due to events which are not attributable to wrongful intent or gross negligence of the conference organizers, the conference organizers cannot be held liable by delegates for any damages, costs, or losses incurred, such as transportation costs, accommodation costs, costs for additional orders, financial losses, etc.</p>

                <p>Under these circumstances, the conference organizers reserve the right to either retain the entire registration fee and to credit it for a future conference, or to reimburse the delegate after deducting costs already incurred for the organisation of the conference and which could not be recovered from third parties.</p>

                <p><strong>MODIFICATION OF THE PROGRAMME</strong></p>
                <p>The conference organizers reserve the right to modify the programme. No refunds can be granted in case of cancellation of speakers, lack of space in the conference room or any other incidents during the conference which are beyond the control of the conference organizers.</p>

                <p><strong>LOST NAME BADGE</strong></p>
                <p>The conference name badge must be worn at all times during the conference. Access to the conference venue will not be granted without the name badge issued by the conference organizer. If a delegate loses, misplaces or forgets the name badge, applicable fee will be charged for a new name badge. Upon handing out a new name badge, the lost badge will be deactivated and become invalid.</p>

                <p><strong>LETTER OF INVITATION</strong></p>
                <p>Individuals requiring an official Letter of Invitation from the conference organizers can request one through the online registration form or by contacting the Steelgrou Registration Department. To receive a Letter of Invitation, delegates must first register to the conference and submit any necessary data, as stated on the online form.</p>

                <p>The Letter of Invitation does not financially obligate the conference organizers in any way. All expenses incurred in relation to the conference, the registration and the attendance are the sole responsibility of the delegate.</p>

                <p><strong>VISA REQUIREMENTS</strong></p>
                <p>It is the sole responsibility of the delegate to take care of his/her visa requirements. Delegates who require an entry visa must allow sufficient time for the application procedure. Delegates should contact the nearest embassy or consulate to determine the appropriate timing of their visa applications.</p>
                <p><strong>Note:</strong> Embassies and consulates-general are independent decision-making agencies for visas, sometimes after specific checks have been made with other countries. You cannot appeal against the rejection of a visa application; neither can Steelgroup intervene on your behalf in case of a rejection or to speed up the procedure.</p>

                <p>Delegate registration details will be shared with the immigration authorities to assist in the immigration process. However, Steelgroup Registration Department will not directly contact embassies and consulates on behalf of visa applicants.</p>

                <p>The registration fee minus a handling fee will be refunded after the conference if the visa was applied for in time and proof is shown that a visa could not be granted even though all requested documents were submitted. Refund requests must be made in writing and sent to the Steelgroup Registration Department, by email, no later within 30 days fro the conference date, the date after which requests for such refunds cannot be taken into consideration.</p>

                <p><strong>TRAVEL-HEALTH INSURANCE</strong></p>
                <p>As a part of a visa application, individuals must have travel-health insurance for the duration of their stay in the host country. This insurance can be obtained from any approved insurer. Individuals need to check with the responsible embassy/consulate for a list of approved insurers available in their country.</p>

                <p>Individuals must have insurance for their entire stay in the host country and therefore are encouraged to ensure and pay for the correct number of days. The visa will only be given for the dates that are covered by the insurance policy, which need to relate to the travel dates.</p>

                <p><strong>DATA PROTECTION AND SHARING OF CONTACT DETAILS</strong></p>
                <p>The protection of your data and the observance of your right of informational self-determination with regard to the collection, processing and use of your personal data are important to us.</p>

                <p>Steelgroup will collect and store all personal data for the preparation and execution of the conferences, organized/co-organized/supported by Steelgroup.</p>

                <p>In order to facilitate border entry and visa applications, Steelgroup will share contact details of concerned delegates with immigration authorities.</p>
                <p>Steelgroup periodically performs e-mailings to delegates on behalf of third parties regarding activities at the conference or other communications, which may be of interest. Contact details of delegates will not be shared to third parties, except the registered delegates of the conference. Participants may opt out of these emailings at any time. This will not affect official correspondence from the conference authorities.</p>

                <p>A number of exhibitors will be using a lead retrieval system linked to the participant data base and may ask you to scan your badge in order to obtain your contact information. This information contains postal address, email and phone number as provided during the registration process.</p>
                <p>Scanning of your badge should always happen on a voluntary basis. Please do report any misuse of the scanning device to the exhibition management.</p>

                <p><strong>LIABILITY</strong></p>
                <p>The conference organizers shall be held liable in the framework of a duty of care as a respectable businessman according to statutory provisions. The liability of the conference organizers &ndash; for whatever legal reason &ndash; shall be limited to intent and gross negligence. The liability of commissioned service providers shall remain unaffected by this. The delegate shall take part in the conference at his/her own risk. Oral agreements shall not be binding if these have not been confirmed in writing by Steelgroup.</p>

                <p><strong>FULFILMENT AND JURISDICTION</strong></p>
                <p>The terms of this contract shall be fulfilled in Mumbai (India) and host country where the conference will be organized/co-organized/supported by Steelgroup and Mumbai shall be the sole court of jurisdiction.</p>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

