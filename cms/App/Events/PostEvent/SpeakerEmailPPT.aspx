﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="SpeakerEmailPPT.aspx.cs" Inherits="App_Events_PostEvent_SpeakerEmailPPT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
     <style type="text/css">
        table.dataTable {
            border-collapse: collapse !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Speaker Email PPTs</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>

        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Enter following details about Presentation-</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="form-control-label">Topic Name <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtTopicName" placeholder="Please enter Topic Name here" runat="server" CssClass="form-control contentBox" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Speaker Name <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtSpeakerName" placeholder="Please enter Speaker Name here" runat="server" CssClass="form-control contentBox"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Speaker Email<small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtSpeakerEmail" placeholder="Please enter Speaker Email here" runat="server" CssClass="form-control contentBox"></asp:TextBox>
                </div>

                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Add Presentation" CssClass="btn btn-primary" />
                </div>
            </div>

        </div>


        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Following Speaker PPTs exist-</h3>
            </div>
            <div class="card-body">
                <table id="list" class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Speaker Name</th>
                            <th>Topic</th>
                            <th>Speaker Email</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><%# Eval("Sno") %></th>
                                    <td><%# Eval("pptSpeaker") %></td>
                                    <td><%# Eval("pptTitle") %></td>
                                    <td><%# Eval("pptFile") %></td>
                                   
                                    <td>
                                    <asp:LinkButton ID="lnkDelete" CssClass="btn btn-danger btn-sm" runat="server" OnClientClick='<%# "return getConfirmation(this,\"" + Eval("pptTitle").ToString() + "\");" %>' CommandArgument='<%# Eval("pptID") %>' CommandName="deleterecord">Delete</asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>

                    </tbody>
                </table>
            </div>
        </div>
        <div id="ConfirmationPopup" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Please Confirm Deletion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure you want to delete <strong><span id="spnMsg"></span></strong>&nbsp;PPT? This will delete it from past event website also. This action is irreversible.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">Yes, Delete</button>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
     <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#list').DataTable();
        });
        function getConfirmation(sender, CourseName) {
            $("#spnMsg").text(CourseName);
            $('#ConfirmationPopup').modal('show');
            $('#btnConfirm').attr('onclick', "$('#ConfirmationPopup').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
</asp:Content>

