﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="Content.aspx.cs" Inherits="App_Events_PostEvent_Content" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://cdn.ckeditor.com/4.7.0/standard/ckeditor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Event Text Content</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>

        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Please Enter following details about Event-</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="form-control-label">Post Event Press Release <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtEventPress" placeholder="Please enter Press Release here" runat="server" TextMode="MultiLine" CssClass="form-control contentBox" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Objectives <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtEventObjectives" placeholder="Please enter Objectives here" TextMode="MultiLine" runat="server" CssClass="form-control contentBox"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">About Organizers <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtAbout" placeholder="Please enter About Organizers here" TextMode="MultiLine" runat="server" CssClass="form-control contentBox"></asp:TextBox>
                </div>

                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Save" CssClass="btn btn-primary" />
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script>
        CKEDITOR.replaceAll('contentBox');
        CKEDITOR.config.htmlEncodeOutput = true;

    </script>
</asp:Content>

