﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="about.aspx.cs" Inherits="about" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="pageheading">About <span>Us</span></h1>
                <p class="mx-auto text-justify mb-4">
                    Steelgroup.co.in is now a reputed and popular name amongst the Steel industry and serving to Steel industry as one of the leading information and networking platforms. We organize focused subject driven conferences for steel industry especially into Steel Flat products. The company was founded in 2010 by Mr. Ajay Tambe who is the Founder & the CEO.
                </p>
                <p class="mx-auto text-justify">
                    Moreover, Steelgroup keep its members and subscribers updated with the e-newsletters and current affairs of the market, being circulated to over 15,000 industry professionals.
                </p>
                <p class="mx-auto text-justify">
                    With the support of over 25000 subscribers, Steelgroup has a wide range of audience which helps to create a great platform to promote products/brands/services related to Steel industry. We also play a major role into buy & sell of new/used machineries and plant through online and offline enquiries.
                </p>
                <p class="mx-auto text-justify">
                    Steelgroup has started its journey from 2013, by organizing the 1st segment of Galvanizing Coating & Profiling (GCP) Summit franchise, which now has become the annual program. In 2016, 4th GCP Summit in Mumbai had witnessed over 350 delegates, the most successful conference till now.
                </p>
                <p class="mx-auto text-justify">
                    Globally, Steelgroup has a strong presence. It is one of the leading conference organizers overseas as well. With 2 conferences every year in overseas, Steelgroup is on the journey of connecting people together and creating a huge networking platform to bring the industry professionals under one roof for a better business and trade opportunities.
                </p>
                <p>To review our past conference, please visit <a href="/events" class="btn btn-sm btn-dark">Past Conferences</a></p>
                <p>We look forward to a long term business relationship with you. </p>
            </div>
        </div>
    </div>
    <div class="graybgsection">
        <div class="container">
            <div class="row mt-4">
                <div class="col-12 col-md-4 abtsection">
                    <i class="fas fa-eye fa-4x"></i>
                    <h4>Vision</h4>
                    <p>
                        A company known globally for quality of initiative they take for spreading awareness about steel in the world.
                    </p>
                </div>
                <div class="col-12 col-md-4 abtsection">
                    <i class="fas fa-thumbs-up fa-4x"></i>
                    <h4>Values</h4>
                    <p>
                        <strong>Innovation</strong> : We will innovate new ways to help the persons to network better.<br />
                        <strong>Simplicity</strong> : What ever we will do ,we will make it simple yet effective.<br />
                        <strong>Take Initiative</strong> : We will take initiative to understand problems of steel industry and will try to come up with right solution.<br />
                    </p>
                </div>
                <div class="col-12 col-md-4 abtsection">
                    <i class="fas fa-bullseye fa-4x"></i>
                    <h4>Mission</h4>
                    <p>
                        Connecting the Steel World in a Network For Sustainable Growth.
                    </p>
                </div>
            </div>

        </div>
    </div>
    <div class="container mb-5">
        <h3 class="pageheading">Our <span>Team</span></h3>
        <div class="row my-4">
            <div class="col-12 col-md-4 text-center teambox">
                <img src="images/team/ajay-team.jpg" class="rounded-circle m-3 img-fluid w-75" />
                <h4>Ajay Tambe</h4>
                <p>Founder &amp; CEO</p>
                <ul class="usersocial">
                    <li><a href="mailto:ajay.tambe@steelgroup.co.in" target="_blank"><i class="fas fa-envelope"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/ajay-tambe-steelgroup-b8415920/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                    <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="https://api.whatsapp.com/send?phone=919322199557" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4 text-center teambox">
                <img src="images/team/amit-team.jpg" class="rounded-circle m-3 img-fluid w-75" />
                <h4>Amit Purohit</h4>
                <p>Manager - Marketing</p>
                <ul class="usersocial">
                    <li><a href="mailto:amit@steelgroup.co.in" target="_blank"><i class="fas fa-envelope"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/ap6887/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                    <li><a href="https://twitter.com/amit6887" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="https://api.whatsapp.com/send?phone=919926744333" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4 text-center teambox">
                <img src="images/team/dipti-team.jpg" class="rounded-circle m-3 img-fluid w-75" />
                <h4>Dipti Mistry</h4>
                <p>Executive - Marketing</p>
                <ul class="usersocial">
                    <li><a href="mailto:dipti@steelgroup.co.in" target="_blank"><i class="fas fa-envelope"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/dipti-dhon-59bb62155/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

