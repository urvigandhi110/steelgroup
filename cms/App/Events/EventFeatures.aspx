﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="EventFeatures.aspx.cs" Inherits="App_Events_EventFeatures" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Add Features to Event</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Manage tags for event- <strong><asp:Literal ID="ltrlEventName" runat="server"></asp:Literal></strong></h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label>Enter Hastags here <small class="text-muted">without a HASH(#) symbol, comma separated</small>:</label>
                    <asp:TextBox ID="txtHashtags" data-role="tagsinput" CssClass="form-control" runat="server"></asp:TextBox>
                </div>

                <div class="form-group">
                    <asp:Button ID="btnSubmitTags" OnClick="btnSubmitTags_Click" runat="server" Text="Update Tags" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="../../js/tagsinput.js"></script>
</asp:Content>

