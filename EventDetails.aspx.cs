﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.IO;

public partial class EventDetails : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;
    SqlDataAdapter da;
    DataSet ds;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            string eventName = Page.RouteData.Values["eventname"].ToString();
            //Eval("eventName").ToString().Replace(" ","-").Replace("-&","").ToLower()


            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT eventID,eventHashTags,eventName,eventImage1,eventImage2,eventImage3,eventImage4,eventStartDate,eventVenue,eventCity,eventCountry,eventCompanies,eventDelegates,eventPanelists,eventExhibitors,eventVideo,eventDesc,eventDescOld,eventRegForm,eventSponsorshipForm,eventBrochure,eventAgenda,eventLayout,eventParticipatingPDF,eventVenueAddress,eventVenueMapURL FROM eventsmaster WHERE publish='Yes' AND Lower(replace(replace(eventName,' ','-'),'-&',''))=@eventname; SELECT s.eventSpeakerID,s.eventSpeakerName,s.eventSpeakerOrg,s.eventSpeakerPhoto,s.eventSpeakerDesignation,s.eventSpeakerCountry,s.eventSpeakerType,s.eventSpeakerIntro,s.eventSpeakerLinkedin,s.eventSpeakerTwitter FROM eventSpeakers s JOIN eventsmaster e ON e.eventID=s.eventID WHERE Lower(replace(replace(e.eventName,' ','-'),'-&',''))=@eventname ORDER BY eventSpeakerName; SELECT * FROM eventAgenda a JOIN eventsmaster e ON e.eventID=a.eventID WHERE Lower(replace(replace(e.eventName,' ','-'),'-&',''))=@eventname ORDER BY eventAgendaDate; SELECT spo.eventSponsorID,spo.eventSponsorName,spo.eventSponsorLogo,spo.eventSponsorURL,spo.eventSponsorType FROM eventSponsors spo JOIN eventsmaster e ON e.eventID=spo.eventID WHERE Lower(replace(replace(e.eventName,' ','-'),'-&',''))=@eventname ORDER BY case when eventSponsorType like 'knowledge' then 0 else 1 end, case when eventSponsorType like 'marketing' then 0 else 1 end, case when eventSponsorType like 'media' then 0 else 1 end,case when eventSponsorType like 'it pa' then 0 else 1 end,case when eventSponsorType like 'online' then 0 else 1 end,case when eventSponsorType like 'travel' then 0 else 1 end,case when eventSponsorType like 'design' then 0 else 1 end; SELECT eventID,eventName,eventImage1,eventStartDate,eventCity,eventCountry,eventVenue FROM eventsmaster WHERE publish='Yes' AND eventStartDate>=GETDATE();");
            steelcmd.Parameters.AddWithValue("eventname", eventName);
            steelcmd.Connection = steelcon;
            steelcon.Open();


            da = new SqlDataAdapter();
            ds = new DataSet();

            da.SelectCommand = steelcmd;
            da.Fill(ds);

            //Binding Slider converting columns to rows in new DT
            DataTable dt = new DataTable();
            dt.Columns.Add("eventImage");
            dt.Columns.Add("eventStartDate");
            dt.Columns.Add("eventVenue");
            dt.Columns.Add("eventName");

            for (int i = 1; i <= 4; i++)
            {
                if (ds.Tables[0].Rows[0]["eventImage" + i.ToString()].ToString() != "")
                {
                    dt.Rows.Add(ds.Tables[0].Rows[0]["eventImage" + i.ToString()].ToString(), ds.Tables[0].Rows[0]["eventStartDate"].ToString(), ds.Tables[0].Rows[0]["eventVenue"].ToString(), ds.Tables[0].Rows[0]["eventName"].ToString());
                }
            }

            rptEventSlider.DataSource = dt;
            rptEventSlider.DataBind();

            downloads.Attributes.Add("style", "background-image: linear-gradient( rgba(0,0,0,.7), rgba(0,0,0,.7) ), url(" + Page.ResolveUrl("~/cms/") + ds.Tables[0].Rows[0]["eventImage1"].ToString().Replace("~/", "") + ");");
            shareevent.Attributes.Add("style", "background-image: linear-gradient( rgba(0,0,0,.7), rgba(0,0,0,.7) ), url(" + Page.ResolveUrl("~/cms/") + ds.Tables[0].Rows[0]["eventImage1"].ToString().Replace("~/", "") + ");");
            contactdetails.Attributes.Add("style", "background-image: linear-gradient( rgba(0,0,0,.7), rgba(0,0,0,.7) ), url(" + Page.ResolveUrl("~/cms/") + ds.Tables[0].Rows[0]["eventImage1"].ToString().Replace("~/", "") + ");");

            Title = ds.Tables[0].Rows[0]["eventName"].ToString() + " - Steel Group";
            MetaDescription = ds.Tables[0].Rows[0]["eventDesc"].ToString();

            //Binding Other Controls
            //Timer
            hfeventDate.Value = DateTime.Parse(ds.Tables[0].Rows[0]["eventStartDate"].ToString()).ToString("MMMM dd, yyyy");
            hfEventID.Value = ds.Tables[0].Rows[0]["eventID"].ToString();
            mailvals.Value = ds.Tables[0].Rows[0]["eventName"].ToString() + "$$" + ds.Tables[0].Rows[0]["eventImage1"].ToString() + "$$" + ds.Tables[0].Rows[0]["eventAgenda"].ToString() + "$$" + ds.Tables[0].Rows[0]["eventBrochure"].ToString();
            hfHashTags.Value = ds.Tables[0].Rows[0]["eventHashTags"].ToString();
            //Stats
            ltrlCompanies.Text = ds.Tables[0].Rows[0]["eventCompanies"].ToString();
            ltrlDelegates.Text = ds.Tables[0].Rows[0]["eventDelegates"].ToString();
            ltrlExhibitors.Text = ds.Tables[0].Rows[0]["eventExhibitors"].ToString();
            ltrlPanelists.Text = ds.Tables[0].Rows[0]["eventPanelists"].ToString();

            //About Us
            ltrlVideoURL.Text = "<iframe width=\"100%\" height=\"400\" src=\"" + ds.Tables[0].Rows[0]["eventVideo"].ToString() + "\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>";
            ltrlEventDesc.Text = ds.Tables[0].Rows[0]["eventDesc"].ToString();
            ltrlEventDescOld.Text = ds.Tables[0].Rows[0]["eventDescOld"].ToString();

            //Downloads
            lnkAgenda.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventAgenda"].ToString().Replace("~/", "/");
            lnkAgenda.Attributes.Add("download", "Agenda-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());
            lnkDownloadAgenda.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventAgenda"].ToString().Replace("~/", "/");
            lnkDownloadAgenda.Attributes.Add("download", "Agenda-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());

            lnkBrochure.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventBrochure"].ToString().Replace("~/", "/");
            lnkBrochure.Attributes.Add("download", "Brochure-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());
            lnkDownloadBrochure.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventBrochure"].ToString().Replace("~/", "/");
            lnkDownloadBrochure.Attributes.Add("download", "Brochure-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());

            lnkLayout.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventLayout"].ToString().Replace("~/", "/");
            lnkLayout.Attributes.Add("download", "Layout-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());
            lnkDownloadLayout.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventLayout"].ToString().Replace("~/", "/");
            lnkDownloadLayout.Attributes.Add("download", "Layout-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());

            lnkRegform.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventRegForm"].ToString().Replace("~/", "/");
            lnkRegform.Attributes.Add("download", "Registration-Form-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());

            lnkSponsorshipForm.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventSponsorshipForm"].ToString().Replace("~/", "/");
            lnkSponsorshipForm.Attributes.Add("download", "Sponsorship-Form-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());
            lnkDownloadSponsorship.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventSponsorshipForm"].ToString().Replace("~/", "/");
            lnkDownloadSponsorship.Attributes.Add("download", "Sponsorship-Form-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());

            imgLayout.ImageUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventLayout"].ToString().Replace("~/", "/") + "?w=1920&mode=crop";


            ds.Tables[1].DefaultView.RowFilter = "eventSpeakerType='Key Note Speaker'";

            if (ds.Tables[1].DefaultView.ToTable().Rows.Count > 0)
            {
                pnlTopSpeaker.Visible = true;
                imgTopSpeaker.ImageUrl = Page.ResolveUrl("~/cms") + ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerPhoto"].ToString().Replace("~/", "/") + "?w=200&h=250&mode=crop";
                ltrlTopSpeakerName.Text = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerName"].ToString();
                ltrlTopSpeakerType.Text = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerType"].ToString();
                ltrlTopSpeakerDetails.Text = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerOrg"].ToString() + " | " + ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerDesignation"].ToString() + " | " + ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerCountry"].ToString();
                ltrlTopSpeakerIntro.Text = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerIntro"].ToString();
                lnkSpeakerLinkedIn.NavigateUrl = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerLinkedin"].ToString() != "" ? ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerLinkedin"].ToString() : "#";
                lnkSpeakerTwitter.NavigateUrl = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerTwitter"].ToString() != "" ? ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerTwitter"].ToString() : "#";
                lnkSpeakerDetails.NavigateUrl = Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "/speakers/" + SteelFunc.GenerateUrl(ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerName"].ToString());
            }
            else
            {
                pnlTopSpeaker.Visible = false;
            }
            //Speakers - ds.tables[1]
            ds.Tables[1].DefaultView.RowFilter = "";
            ds.Tables[1].DefaultView.RowFilter = "eventSpeakerType<>'Key Note Speaker'";
            rptEventSpeaker.DataSource = ds.Tables[1].DefaultView;
            rptEventSpeaker.DataBind();

            //Venue
            ltrlVenueAddress.Text = ds.Tables[0].Rows[0]["eventVenueAddress"].ToString();
            ltrlVenueMap.Text = "<iframe src=\"" + ds.Tables[0].Rows[0]["eventVenueMapURL"].ToString() + "\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border: 0\" allowfullscreen></iframe>";

            //Agenda - ds.tables[2]
            DataTable dtt = ds.Tables[2].DefaultView.ToTable(true, "eventAgendaDate");
            rptAgendaDaysTab.DataSource = dtt;
            rptAgendaDaysTab.DataBind();

            rptAgendaMainTabBody.DataSource = dtt;
            rptAgendaMainTabBody.DataBind();

            foreach (RepeaterItem item in rptAgendaMainTabBody.Items)
            {
                Repeater rptAg = (Repeater)item.FindControl("rptAgenda");
                HiddenField hf = (HiddenField)item.FindControl("hfday");
                ds.Tables[2].DefaultView.RowFilter = "";
                ds.Tables[2].DefaultView.RowFilter = "eventAgendaDate='" + hf.Value + "'";
                ds.Tables[2].DefaultView.Sort = "eventAgendaTime ASC";
                rptAg.DataSource = ds.Tables[2].DefaultView;
                rptAg.DataBind();
            }

            //Sponsors - ds.tables[3] - All read at once, filter and bind separate repeaters or hide section

            //0. Principal Sponsors
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Principal'";
            ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

            if (ds.Tables[3].DefaultView.Count > 0)
            {
                rptPrincipal.DataSource = ds.Tables[3].DefaultView;
                rptPrincipal.DataBind();
            }
            else
            {
                principalsponsors.Visible = false;
            }

            //1. Platinum Sponsors
            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Platinum'";
            ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

            if (ds.Tables[3].DefaultView.Count > 0)
            {
                rptPlatinum.DataSource = ds.Tables[3].DefaultView;
                rptPlatinum.DataBind();
            }
            else
            {
                platinumsponsors.Visible = false;
            }

            //2. Gold Sponsors
            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Gold'";
            ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

            if (ds.Tables[3].DefaultView.Count > 0)
            {
                rptGold.DataSource = ds.Tables[3].DefaultView;
                rptGold.DataBind();
            }
            else
            {
                goldsponsors.Visible = false;
            }

            //3. Silver Sponsors
            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Silver'";
            ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

            if (ds.Tables[3].DefaultView.Count > 0)
            {
                rptSilver.DataSource = ds.Tables[3].DefaultView;
                rptSilver.DataBind();
            }
            else
            {
                silversponsors.Visible = false;
            }

            //4. Associate Sponsors
            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Associate'";
            ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

            if (ds.Tables[3].DefaultView.Count > 0)
            {
                rptAssociate.DataSource = ds.Tables[3].DefaultView;
                rptAssociate.DataBind();
            }
            else
            {
                associatesponsors.Visible = false;
            }

            //5. Company Highlight Sponsors - Networking Left


            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Company Highlight'";
            ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

            if (ds.Tables[3].DefaultView.Count > 0)
            {
                rptCompanyHighlight.DataSource = ds.Tables[3].DefaultView;
                rptCompanyHighlight.DataBind();
            }
            else
            {
                clubbedsponsors.Visible = false;
            }
            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%networking%'";
            ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

            if (ds.Tables[3].DefaultView.Count > 0)
            {
                clubbedsponsors.Visible = true;

                foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
                {
                    if (rw["eventSponsorType"].ToString() == "Networking Dinner")
                    {
                        pnlNetworkingDinner.Visible = true;
                        imgNetworkingDinner.ImageUrl = Page.ResolveUrl("~/cms") + rw["eventSponsorLogo"].ToString().Replace("~/", "/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff";
                        lnkNetworkingDinner.NavigateUrl = Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(rw["eventSponsorName"].ToString());
                    }
                    else if (rw["eventSponsorType"].ToString() == "Networking Lunch")
                    {
                        pnlNetworkingLunch.Visible = true;
                        imgNetworkingLunch.ImageUrl = Page.ResolveUrl("~/cms") + rw["eventSponsorLogo"].ToString().Replace("~/", "/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff";
                        lnkNetworkingLunch.NavigateUrl = Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(rw["eventSponsorName"].ToString());
                    }
                }
            }
            else
            {
                pnlNetworkingDinner.Visible = false;
                pnlNetworkingLunch.Visible = false;
            }
            //Delegate Kit Sponsor Addition New
            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%delegate kit%'";
            ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";
            if (ds.Tables[3].DefaultView.Count > 0)
            {
                pnlDelegateKit.Visible = true;
                imgDelegateKit.ImageUrl = Page.ResolveUrl("~/cms") + ds.Tables[3].DefaultView.ToTable().Rows[0]["eventSponsorLogo"].ToString().Replace("~/", "/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff";
                lnkDelegateKit.NavigateUrl = Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(ds.Tables[3].DefaultView.ToTable().Rows[0]["eventSponsorName"].ToString());

            }
            else
            {
                pnlDelegateKit.Visible = false;
            }

            //6. Exhibitor Sponsors
            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Exhibitor'";
            ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

            if (ds.Tables[3].DefaultView.Count > 0)
            {
                rptExhibitors.DataSource = ds.Tables[3].DefaultView;
                rptExhibitors.DataBind();
            }
            else
            {
                othersponsors.Visible = false;
            }

            //7. Other Partners
            DataTable dtpartners = new DataTable();
            dtpartners.Columns.Add("eventSponsorID");
            dtpartners.Columns.Add("eventSponsorName");
            dtpartners.Columns.Add("eventSponsorLogo");
            dtpartners.Columns.Add("eventSponsorURL");
            dtpartners.Columns.Add("eventSponsorType");
            //eventSponsorID,eventSponsorName,eventSponsorLogo,eventSponsorURL,eventSponsorType


            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%knowledge%' AND eventSponsorType LIKE '%partner%'";

            int rwcount = 0;
            foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
            {
                if (rwcount == 0)
                {
                    dtpartners.Rows.Add(rw.ItemArray);
                }
                else
                {
                    dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                }
                rwcount++;

            }

            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%marketing%' AND eventSponsorType LIKE '%partner%'";

            rwcount = 0;
            foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
            {
                if (rwcount == 0)
                {
                    dtpartners.Rows.Add(rw.ItemArray);
                }
                else
                {
                    dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                }
                rwcount++;

            }

            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%media%' AND eventSponsorType LIKE '%partner%'";

            rwcount = 0;
            foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
            {
                if (rwcount == 0)
                {
                    dtpartners.Rows.Add(rw.ItemArray);
                }
                else
                {
                    dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                }
                rwcount++;

            }

            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%it pa%' AND eventSponsorType LIKE '%partner%'";
            rwcount = 0;
            foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
            {
                if (rwcount == 0)
                {
                    dtpartners.Rows.Add(rw.ItemArray);
                }
                else
                {
                    dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                }
                rwcount++;

            }

            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%online%' AND eventSponsorType LIKE '%partner%'";

            rwcount = 0;
            foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
            {
                if (rwcount == 0)
                {
                    dtpartners.Rows.Add(rw.ItemArray);
                }
                else
                {
                    dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                }
                rwcount++;

            }

            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%travel%' AND eventSponsorType LIKE '%partner%'";

            rwcount = 0;
            foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
            {
                if (rwcount == 0)
                {
                    dtpartners.Rows.Add(rw.ItemArray);
                }
                else
                {
                    dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                }
                rwcount++;

            }

            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%design%' AND eventSponsorType LIKE '%partner%'";

            rwcount = 0;
            foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
            {
                if (rwcount == 0)
                {
                    dtpartners.Rows.Add(rw.ItemArray);
                }
                else
                {
                    dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                }
                rwcount++;

            }


            if (dtpartners.Rows.Count > 0)
            {
                rptPartners.DataSource = dtpartners;
                rptPartners.DataBind();

            }
            else
            {
                partners.Visible = false;
            }

            //8. Participating companies
            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Participating Companies'";
            ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

            if (ds.Tables[3].DefaultView.Count > 0)
            {
                rptParticipating.DataSource = ds.Tables[3].DefaultView;
                rptParticipating.DataBind();
                lnkParticipatingDownload.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventParticipatingPDF"].ToString().Replace("~/", "/");
                lnkParticipatingDownload.Attributes.Add("download", "Participating-Companies-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());
            }
            else
            {
                participating.Visible = false;
            }

            //9. In association with 

            ds.Tables[3].DefaultView.RowFilter = "";
            ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%association%'";

            if (ds.Tables[3].DefaultView.Count > 0)
            {
                imgassociation.ImageUrl = Page.ResolveUrl("~/cms/") + ds.Tables[3].DefaultView.ToTable().Rows[0]["eventSponsorLogo"].ToString().Replace("~/", "");
                lnkAssociation.NavigateUrl = ds.Tables[3].DefaultView.ToTable().Rows[0]["eventSponsorURL"].ToString();
            }
            else
            {
                association.Visible = false;
                organizedby.Attributes.Remove("class");
                organizedby.Attributes.Add("class", "col-12 col-md-6 text-center");
            }

            rptEventFooter.DataSource = ds.Tables[4];
            rptEventFooter.DataBind();


            da.Dispose();
            ds.Clear();
            ds.Dispose();
            dt.Dispose();
            steelcon.Close();
            steelcmd.Dispose();

            //Trying to read from Blog Feed
            XmlReader reader = XmlReader.Create("http://blog.steelgroup.co.in/feeds/posts/default?alt=rss&max-results=4&redirect=false");
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            if (feed.Items.Count() > 0)
            {
                DataTable dtblog = new DataTable();
                dtblog.Columns.Add("thumbnail");
                dtblog.Columns.Add("publishdate");
                dtblog.Columns.Add("title");
                dtblog.Columns.Add("description");
                dtblog.Columns.Add("posturl");

                foreach (SyndicationItem item in feed.Items)
                {
                    //your code for rendering each item thumbUrl.replace("/s72-c/","/s"+ImageSize+"/")
                    //ltrlTest.Text += item.Summary.Text + ", ";
                    if (item.ElementExtensions.Where(p => p.OuterName == "thumbnail").Count() != 0)
                    {
                        string imgPath = item.ElementExtensions.Where(p => p.OuterName == "thumbnail").First().GetObject<XElement>().Attribute("url").Value;
                        dtblog.Rows.Add(imgPath, item.PublishDate.ToString(), item.Title.Text, Regex.Replace(item.Summary.Text, @"<.+?>", String.Empty).Substring(0, 200) + "...", item.Links[0].Uri.AbsoluteUri);
                        //ltrlimages.Text += imgPath + ", "; //use it to show the img in DIV or whereever you wish.
                    }

                    //ltrlURL.Text += item.Links[0].Uri.AbsoluteUri + "<br>";
                    //ltrlpublishDate.Text += item.PublishDate.ToString() + "<br>";
                    //ltrlTitle.Text += item.Title.Text + "<br>";
                    //ltrldescription.Text += Regex.Replace(item.Summary.Text, @"<.+?>", String.Empty).Substring(0, 100) + "<br>";
                }

                rptBlogposts.DataSource = dtblog;
                rptBlogposts.DataBind();

            }
            else
            {
                blogposts.Visible = false;
            }


        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }


    }

    protected void btnInvite_Click(object sender, EventArgs e)
    {
        try
        {

            string eventname = Page.RouteData.Values["eventname"].ToString();

            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            string newRegID = "INV" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(1000, 9999);

            steelcmd.CommandText = "INSERT INTO eventInvites(eventInviteID,eventID,friendName,friendEmail,friendMobile,personName,datecreated,createdby,createdIP) VALUES(@eventInviteID,@eventID,@friendName,@friendEmail,@friendMobile,@personName,@datecreated,@createdby,@createdIP)";

            steelcmd.Parameters.AddWithValue("@eventInviteID", newRegID);
            steelcmd.Parameters.AddWithValue("@eventID", hfEventID.Value);
            steelcmd.Parameters.AddWithValue("@friendName", txtName.Text);
            steelcmd.Parameters.AddWithValue("@friendEmail", txtEmail.Text);
            steelcmd.Parameters.AddWithValue("@friendMobile", txtMobile.Text);
            steelcmd.Parameters.AddWithValue("@personName", txtYourName.Text);
            steelcmd.Parameters.AddWithValue("@datecreated", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@createdby", txtYourName.Text);
            steelcmd.Parameters.AddWithValue("@createdIP", Request.UserHostAddress);

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {

                //Preparing registrant mail to send
                StreamReader readerRegistrant = new StreamReader(Server.MapPath("~/templates/inviteemail.txt"));
                string readFileRegistrant = readerRegistrant.ReadToEnd();
                readFileRegistrant = readFileRegistrant.Replace("$$REGURL$$", "https://www.steelgroup.co.in/events/" + eventname + "/Register");
                readFileRegistrant = readFileRegistrant.Replace("$$SPONSORSURL$$", "https://www.steelgroup.co.in/events/" + eventname + "/sponsors");
                readFileRegistrant = readFileRegistrant.Replace("$$SPEAKERSURL$$", "https://www.steelgroup.co.in/events/" + eventname + "/speakers");
                //eventname,image,agenda,brochure
                string[] stringSeparators = new string[] { "$$" };

                readFileRegistrant = readFileRegistrant.Replace("$$AGENDAURL$$", "https://www.steelgroup.co.in/cms/" + mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[2].Replace("~/", ""));
                readFileRegistrant = readFileRegistrant.Replace("$$BROCHUREURL$$", "https://www.steelgroup.co.in/cms/" + mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[3].Replace("~/", ""));
                readFileRegistrant = readFileRegistrant.Replace("$$REGNAME$$", txtYourName.Text.Split(' ')[0]);
                readFileRegistrant = readFileRegistrant.Replace("$$INVNAME$$", txtName.Text.Split(' ')[0]);
                readFileRegistrant = readFileRegistrant.Replace("$$EVENTNAME$$", mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[0]);
                readFileRegistrant = readFileRegistrant.Replace("$$IMAGEURL$$", "https://www.steelgroup.co.in/cms/" + mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[1].Replace("~/", "") + "?w=1260&h=300&mode=crop");

                string resp = SteelFunc.sendmail(txtEmail.Text, "Event Invitation from " + txtYourName.Text, readFileRegistrant);
                readerRegistrant.Close();

                pnlInviteForm.Visible = false;
                pnlInviteMessage.Visible = true;

                steelcon.Close();
                steelcmd.Dispose();


            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}