﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class NewsDetails : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string newsheadline = Page.RouteData.Values["newsheadline"].ToString();

            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT * FROM newsmaster WHERE LOWER(replace(replace(replace(replace(replace(replace(replace(newsHeadline,'%','-'),':','-'),',','-'),'.','-'),' ','-'),'--','-'),'''',''))=@newsheadline; SELECT advDimensions,advImage,advLinkURL,advType FROM adsmaster WHERE advType LIKE '%NEWS:%' AND advEndDate>=GETDATE() AND publish='Yes' ORDER BY datecreated desc; SELECT TOP 5 newsID,newsCategory, newsHeadline, newsImage, dbo.fngettimeinagoformat(datecreated,GETDATE()) as datediff FROM newsmaster WHERE publish='Yes' AND newsCategory<>'Ticker' ORDER BY datecreated DESC;");
            steelcmd.Parameters.AddWithValue("newsheadline", newsheadline);
            steelcmd.Connection = steelcon;
            steelcon.Open();

            rdrd = steelcmd.ExecuteReader();
            if (rdrd.Read())
            {
                Title = rdrd["newsHeadline"].ToString() + " - Steel Group";
                MetaKeywords = "steel news, steel industry, " + rdrd["newsHeadline"].ToString(); ;
                MetaDescription = rdrd["newsLeadPara"].ToString();

                ltrlNewsHeading.Text = rdrd["newsHeadline"].ToString();
                ltrlBreadcrumbNewsHeading.Text = rdrd["newsHeadline"].ToString();

                ltrlLeadPara.Text = rdrd["newsLeadPara"].ToString();
                ltrlNewsDate.Text = DateTime.Parse(rdrd["newsDate"].ToString()).ToString("dd MMM, yyyy");

                imgNews.ImageUrl = Page.ResolveUrl("~/cms") + rdrd["newsImage"].ToString().Replace("~/", "/") + "?w=1000&h=500&scale=both&mode=crop";

                if (rdrd["newsImageDesc"].ToString() != "")
                {
                    ltrlImageSource.Text = rdrd["newsImageDesc"].ToString();
                }
                else
                {
                    ltrlImageSource.Visible = false;
                }

                ltrlNewsArticle.Text = HttpUtility.HtmlDecode(rdrd["newsText"].ToString());

                if (rdrd["newsSourceLink"].ToString() != "")
                {
                    lnkNewsArticle.NavigateUrl = rdrd["newsSourceLink"].ToString();
                }
                else
                {
                    lnkNewsArticle.Visible = false;
                }

            }

            if (rdrd.NextResult())
            {
                if (rdrd.HasRows)
                {
                    rptNewsAds.DataSource = rdrd;
                    rptNewsAds.DataBind();
                }
                else
                {
                    pnlNewsSideAds.Visible = false;
                }
            }

            if (rdrd.NextResult())
            {
                rptSidebarNews.DataSource = rdrd;
                rptSidebarNews.DataBind();
            }
            else
            {
                pnlTopNews.Visible = false;
            }

        }
        catch (Exception ex)
        {

        }
    }
}