﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="unsubscribe.aspx.cs" Inherits="unsubscribe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="pageheading">Unsubscribe <span>Us</span></h1>
                <asp:Panel ID="pnlSubscribe" runat="server">
                    <p class="mx-auto text-center mb-4">
                        Do you really want to unsubscribe your email ID
                    <asp:Literal ID="ltrlEmailID" runat="server"></asp:Literal>
                        from future mails?
                    </p>
                    <p class="text-center my-5">
                        <asp:LinkButton ID="btnUnsubscribe" OnClick="btnUnsubscribe_Click" runat="server" CssClass="btn btn-dark" Text="Yes, Unsubscribe me" />
                        <a href="Default.aspx" class="btn btn-link">Don't Mind!</a>

                    </p>
                </asp:Panel>
                <asp:Panel ID="pnlSubscribeMessage" Visible="false" runat="server">
                    <p class="text-center text-success mt-3">
                        <span class="fas fa-5x fa-check-circle"></span>
                    </p>
                    <p class="text-dark my-5 text-center">
                        <asp:Literal ID="ltrlSubscribeMessageText" runat="server"></asp:Literal>
                    </p>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

