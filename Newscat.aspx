﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="Newscat.aspx.cs" Inherits="Newscat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .carousel-control-next, .carousel-control-prev {
            top: -27%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="pageheading"><%= Page.RouteData.Values["newscat"].ToString() %>&nbsp;<span>News</span></h1>
            </div>
        </div>
    </div>
    
    <section id="newscorner" class="banner-sec float-left w-100 pt-4 pb-5 newspage">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9 top-slider">
                    <ul class="list-unstyled newsmain">
                         <asp:GridView ID="gvNews" AllowPaging="true" PageSize="10" OnPageIndexChanging="gvNews_PageIndexChanging" AutoGenerateColumns="false" runat="server" CssClass="gvnews" ShowHeader="false" ShowFooter="false" BorderWidth="0" PagerStyle-BorderWidth="0" PagerStyle-CssClass="pagination-ys">
                            <Columns>
                                <asp:TemplateField ItemStyle-BorderWidth="0">
                                    <ItemTemplate>
                                        <li class="media my-1 border border-left-0 border-top-0 border-right-0 py-3">
                                            <img class="align-self-center mr-3" src="<%# Page.ResolveUrl("~/cms") + Eval("newsImage").ToString().Replace("~/","/") + "?w=300&h=150&scale=both&bgcolor=white" %>" alt='<%# Eval("newsHeadline") %>'>
                                            <div class="media-body">
                                                <a href='<%# Page.ResolveUrl("~/news/") + Eval("newsCategory").ToString() + "/" + SteelFunc.GenerateUrl(Eval("newsHeadline").ToString()) %>'>
                                                    <h6 class="mt-0 mb-1"><%# Eval("newsHeadline") %></h6>
                                                    <p><%# Eval("newsLeadPara") %></p>
                                                    <small class="text-muted font-italic">Posted Under <span class="badge badge-pill badge-danger"><%# Eval("newsCategory").ToString() %></span> <%# Eval("datediff") %></small>
                                                </a>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ul>
                </div>
                <div class="col-12 col-md-3">
                    <asp:Panel ID="pnlNewsSideAds" runat="server">
                        <h5 class="font-weight-bold mb-5"><i class="fas fa-bullhorn"></i>&nbsp;Sponsored</h5>
                        <asp:Repeater ID="rptNewsAds" runat="server">
                            <ItemTemplate>
                                <a href='<%# Eval("advLinkURL") %>' target="_blank" class="sfsidead">
                                    <img class="img-fluid" src="<%# Page.ResolveUrl("~/cms") + Eval("advImage").ToString().Replace("~/","/") + "?w=" + Eval("advDimensions").ToString().Split('x')[0] + "&h=" + Eval("advDimensions").ToString().Split('x')[1] + "&mode=crop" %>" />
                                </a>
                            </ItemTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                    <asp:Panel ID="pnlTopNews" runat="server">
                        <h5 class="font-weight-bold mb-3"><i class="far fa-newspaper"></i>&nbsp;Latest News</h5>
                        <ul class="list-unstyled newssidebar">
                            <asp:Repeater ID="rptSidebarNews" runat="server">
                                <ItemTemplate>
                                    <li class="media my-1 border border-left-0 border-top-0 border-right-0 py-3">
                                        <img class="align-self-center mr-3" src="<%# Page.ResolveUrl("~/cms") + Eval("newsImage").ToString().Replace("~/","/") + "?w=80&h=80&scale=both&bgcolor=white" %>" alt='<%# Eval("newsHeadline") %>'>
                                        <div class="media-body">
                                            <a href='<%# Page.ResolveUrl("~/news/") + Eval("newsCategory").ToString() + "/" + SteelFunc.GenerateUrl(Eval("newsHeadline").ToString()) %>'>
                                                <h6 class="mt-0 mb-1"><%# Eval("newsHeadline") %></h6>
                                                <small class="text-muted font-italic"><%# Eval("datediff") %></small>
                                            </a>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix mb-5"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

