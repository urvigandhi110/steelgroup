﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_EventFeatures : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {

                string eventID = Request.QueryString["eventID"];

                steelcon = new SqlConnection(connstr);
                steelcmd = new SqlCommand("SELECT eventName,eventHashTags FROM eventsmaster WHERE eventID=@eventID; ");
                steelcmd.Parameters.AddWithValue("@eventID", eventID);

                steelcmd.Connection = steelcon;
                steelcon.Open();

                rdrd = steelcmd.ExecuteReader();

                if (rdrd.Read())
                {
                    ltrlEventName.Text = rdrd["eventName"].ToString();
                    txtHashtags.Text = rdrd["eventHashTags"].ToString();
                }

                rdrd.Close();
                steelcon.Close();
                steelcmd.Dispose();
            }
        }
        catch (Exception ex)
        { }
    }

    protected void btnSubmitTags_Click(object sender, EventArgs e)
    {
        try
        {
            string eventID = Request.QueryString["eventID"].ToString();

            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            steelcmd.CommandText = "UPDATE eventsmaster SET eventHashTags=@eventHashTags,datemodified=@datemodified,modifiedby=@modifiedby,modifiedIP=@modifiedIP WHERE eventID=@eventID";


            steelcmd.Parameters.AddWithValue("@eventID", eventID);
            steelcmd.Parameters.AddWithValue("@eventHashTags", txtHashtags.Text);
            steelcmd.Parameters.AddWithValue("@datemodified", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@modifiedby", HttpContext.Current.User.Identity.Name);
            steelcmd.Parameters.AddWithValue("@modifiedIP", Request.UserHostAddress);


            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Hashtags Updated Successfully to event.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();

            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Hashtags could not be Updated. Please check all details try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}