﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_PostEvent_UploadPPT : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            steelcmd.CommandText = "SELECT row_number() over(order by pptid) as sno, * FROM eventPPT WHERE eventID=@eventID AND pptUploadedFileName is not NULL";
            steelcmd.Parameters.AddWithValue("@eventID", Request.QueryString["eventID"].ToString());

            rdrd = steelcmd.ExecuteReader();

            rptList.DataSource = rdrd;
            rptList.DataBind();

            steelcon.Close();
            steelcmd.Dispose();
            steelcon.Dispose();

        }
        catch (Exception ex)
        {

        }
    }

    protected void btnAddPresentations_Click(object sender, EventArgs e)
    {
        string eventID = Request.QueryString["eventID"].ToString();
        Response.Redirect("UploadPPT.aspx?eventID=" + eventID, false);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string qry = "";
        string eventID = Request.QueryString["eventID"].ToString();

        foreach (RepeaterItem itm in rptPresentation.Items)
        {
            Label lbl = (Label)itm.FindControl("lblPPTName");
            TextBox tbTitle = (TextBox)itm.FindControl("txtPPTTitle");
            TextBox tbDesc = (TextBox)itm.FindControl("txtPPTDesc");

            qry += "UPDATE eventppt SET pptTitle='" + tbTitle.Text + "', pptSpeaker='" + tbDesc.Text + "' WHERE pptUploadedFileName='" + lbl.ToolTip + "' AND eventID='" + eventID + "'; ";
        }

        steelcmd = new SqlCommand();
        steelcon = new SqlConnection(connstr);
        steelcon.Open();
        steelcmd.Connection = steelcon;
        steelcmd.CommandText = qry;

        int res = steelcmd.ExecuteNonQuery();

        if (res > 0)
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-success alert-dismissible";
            ltrlmsg.Text = "Presentations with details saved successfully.";

            steelcon.Close();
            steelcmd.Dispose();
            steelcon.Dispose();


            pnlUpload.Visible = false;
            pnlDetails.Visible = false;

            btnSave.Visible = false;
            btnSubmit.Visible = false;
            btnAddPresentations.Visible = true;
        }
        else
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-warning alert-dismissible";
            ltrlmsg.Text = "Presentations could not be uploaded. Please check file details, size, type and try again.";

            steelcon.Close();
            steelcmd.Dispose();
            steelcon.Dispose();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (filePresentation.PostedFiles.Count <= 20)
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;
            steelcmd.CommandText = "";

            string eventID = Request.QueryString["eventID"].ToString();
            Random rnd = new Random();
            int filecount = 0;
            string[] filenames = new string[filePresentation.PostedFiles.Count];

            foreach (HttpPostedFile postedFile in filePresentation.PostedFiles)
            {
                string fileName = "~/uploads/event/PPT/" + Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(postedFile.FileName);

                Regex reg = new Regex(@"^.*\.(ppt|PPT|pptx|PPTX|pdf|PDF)$");
                if (reg.IsMatch(fileName))
                {
                    string newpptid = "PPT" + DateTime.Today.ToString("ddMMyyyy") + rnd.Next(10000, 99999);
                    postedFile.SaveAs(Server.MapPath(fileName));
                    steelcmd.CommandText += "INSERT INTO eventppt(pptID,pptFile,pptUploadedFileName,eventID,datecreated,createdby,createdIP,publish) VALUES('" + newpptid + "','" + fileName + "','" + newpptid + "-" + postedFile.FileName + "','" + eventID + "','" + DateTime.Now + "','" + HttpContext.Current.User.Identity.Name + "','" + Request.UserHostAddress + "','Yes'); ";

                    filenames[filecount] = newpptid + "-" + postedFile.FileName;
                    filecount++;
                }
            }

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                Session["filecount"] = filecount;
                rptPresentation.DataSource = filenames;
                rptPresentation.DataBind();

                pnlUpload.Visible = false;
                pnlDetails.Visible = true;

                btnSave.Visible = true;
                btnSubmit.Visible = false;

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Presentations could not be uploaded. Please check file details, size, type and try again.";

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        else
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-warning alert-dismissible";
            ltrlmsg.Text = "Presentations could not be saved. Please select maximum 20 files.";
        }
    }

    protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "deleterecord")
        {
            try
            {
                steelcon = new SqlConnection(connstr);
                steelcmd = new SqlCommand();
                steelcmd.Connection = steelcon;
                steelcmd.CommandText = "DELETE FROM eventppt WHERE pptID=@pptID";

                steelcmd.Parameters.Add("@pptID", SqlDbType.VarChar);
                steelcmd.Parameters["@pptID"].Value = e.CommandArgument;

                steelcon.Open();
                int res = steelcmd.ExecuteNonQuery();

                if (res > 0)
                {
                    pnlMessage.Visible = true;
                    pnlMessage.CssClass = "alert alert-success";
                    ltrlmsg.Text = "PPT has been deleted successfully.";

                    steelcon.Close();
                    steelcmd.Dispose();
                    steelcon.Dispose();

                }
                else
                {
                    pnlMessage.Visible = true;
                    pnlMessage.CssClass = "alert alert-danger";
                    ltrlmsg.Text = "PPT could not be deleted. Please Try Again";

                    steelcon.Close();
                    steelcmd.Dispose();
                    steelcon.Dispose();
                }
            }
            catch (Exception ex)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-danger";
                ltrlmsg.Text = ex.ToString();

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
    }
}