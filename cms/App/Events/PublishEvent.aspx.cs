﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_PublishEvent : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        chkPublish.InputAttributes.Add("class", "checkbox-template");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            string mainquery = "INSERT INTO eventsmaster(eventID,eventName,eventStartDate,eventEndDate,eventVenue,eventCity,eventState,eventCountry,eventVenueAddress,eventVenueMapURL,eventCompanies,eventDelegates,eventPanelists,eventExhibitors,eventDesc,eventDescOld,eventVideo,";
            string valuesquery = "VALUES(@eventID,@eventName,@eventStartDate,@eventEndDate,@eventVenue,@eventCity,@eventState,@eventCountry,@eventVenueAddress,@eventVenueMapURL,@eventCompanies,@eventDelegates,@eventPanelists,@eventExhibitors,@eventDesc,@eventDescOld,@eventVideo,";

            string filenameeventRegForm, filenameeventSponsorshipForm, filenameeventBrochure, filenameeventAgenda, filenameeventLayout, filenameeventParticipating, filenameeventImage1, filenameeventImage2, filenameeventImage3, filenameeventImage4;

            if (fileAgenda.HasFile)
            {
                filenameeventAgenda = "~/uploads/event/agenda/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileAgenda.FileName);
                fileAgenda.SaveAs(Server.MapPath(filenameeventAgenda));
                steelcmd.Parameters.AddWithValue("@eventAgenda", filenameeventAgenda);
                mainquery += "eventAgenda,";
                valuesquery += "@eventAgenda,";
            }

            if (fileSponsorshipForm.HasFile)
            {
                filenameeventSponsorshipForm = "~/uploads/event/Sponsorship-Form/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileSponsorshipForm.FileName);
                fileSponsorshipForm.SaveAs(Server.MapPath(filenameeventSponsorshipForm));
                steelcmd.Parameters.AddWithValue("@eventSponsorshipForm", filenameeventSponsorshipForm);
                mainquery += "eventSponsorshipForm,";
                valuesquery += "@eventSponsorshipForm,";
            }

            if (fileRegForm.HasFile)
            {
                filenameeventRegForm = "~/uploads/event/Registration-Form/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileRegForm.FileName);
                fileRegForm.SaveAs(Server.MapPath(filenameeventRegForm));
                steelcmd.Parameters.AddWithValue("@eventRegForm", filenameeventRegForm);
                mainquery += "eventRegForm,";
                valuesquery += "@eventRegForm,";
            }

            if (fileLayout.HasFile)
            {
                filenameeventLayout = "~/uploads/event/layout/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileLayout.FileName);
                fileLayout.SaveAs(Server.MapPath(filenameeventLayout));
                steelcmd.Parameters.AddWithValue("@eventLayout", filenameeventLayout);
                mainquery += "eventLayout,";
                valuesquery += "@eventLayout,";
            }

            if (fileBrochure.HasFile)
            {
                filenameeventBrochure = "~/uploads/event/brochure/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileBrochure.FileName);
                fileBrochure.SaveAs(Server.MapPath(filenameeventBrochure));
                steelcmd.Parameters.AddWithValue("@eventBrochure", filenameeventBrochure);
                mainquery += "eventBrochure,";
                valuesquery += "@eventBrochure,";
            }

            if (fileParticipating.HasFile)
            {
                filenameeventParticipating = "~/uploads/event/Participating/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileParticipating.FileName);
                fileParticipating.SaveAs(Server.MapPath(filenameeventParticipating));
                steelcmd.Parameters.AddWithValue("@eventParticipatingPDF", filenameeventParticipating);
                mainquery += "eventParticipatingPDF,";
                valuesquery += "@eventParticipatingPDF,";
            }

            if (fileImage1.HasFile)
            {
                filenameeventImage1 = "~/uploads/event/images/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage1.FileName);
                fileImage1.SaveAs(Server.MapPath(filenameeventImage1));
                steelcmd.Parameters.AddWithValue("@eventImage1", filenameeventImage1);
                mainquery += "eventImage1,";
                valuesquery += "@eventImage1,";
            }
            if (fileImage2.HasFile)
            {
                filenameeventImage2 = "~/uploads/event/images/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage2.FileName);
                fileImage2.SaveAs(Server.MapPath(filenameeventImage2));
                steelcmd.Parameters.AddWithValue("@eventImage2", filenameeventImage2);
                mainquery += "eventImage2,";
                valuesquery += "@eventImage2,";
            }
            if (fileImage3.HasFile)
            {
                filenameeventImage3 = "~/uploads/event/images/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage3.FileName);
                fileImage3.SaveAs(Server.MapPath(filenameeventImage3));
                steelcmd.Parameters.AddWithValue("@eventImage3", filenameeventImage3);
                mainquery += "eventImage3,";
                valuesquery += "@eventImage3,";
            }
            if (fileImage4.HasFile)
            {
                filenameeventImage4 = "~/uploads/event/images/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage4.FileName);
                fileImage4.SaveAs(Server.MapPath(filenameeventImage4));
                steelcmd.Parameters.AddWithValue("@eventImage4", filenameeventImage4);
                mainquery += "eventImage4,";
                valuesquery += "@eventImage4,";
            }

            mainquery += "datecreated,createdby,createdIP,publish)";
            valuesquery += "@datecreated,@createdby,@createdIP,@publish)";

            string newEventID = "EVT" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(10000, 99999);

            steelcmd.CommandText = mainquery + " " + valuesquery;

            steelcmd.Parameters.AddWithValue("@eventID", newEventID);
            steelcmd.Parameters.AddWithValue("@eventName", txtEventName.Text);
            steelcmd.Parameters.AddWithValue("@eventStartDate", txtStartDate.Text);
            steelcmd.Parameters.AddWithValue("@eventEndDate", txtEndDate.Text);
            steelcmd.Parameters.AddWithValue("@eventVenue", txtVenue.Text);
            steelcmd.Parameters.AddWithValue("@eventCity", txtVenueCity.Text);
            steelcmd.Parameters.AddWithValue("@eventState", txtVenueState.Text);
            steelcmd.Parameters.AddWithValue("@eventCountry", txtVenueCountry.Text);
            steelcmd.Parameters.AddWithValue("@eventVenueAddress", txtEventAddress.Text);
            steelcmd.Parameters.AddWithValue("@eventVenueMapURL", txtMapUrl.Text);
            steelcmd.Parameters.AddWithValue("@eventCompanies", txtCompanies.Text);
            steelcmd.Parameters.AddWithValue("@eventDelegates", txtDelegates.Text);
            steelcmd.Parameters.AddWithValue("@eventPanelists", txtPanelists.Text);
            steelcmd.Parameters.AddWithValue("@eventExhibitors", txtExhibitors.Text);
            steelcmd.Parameters.AddWithValue("@eventDesc", txtEventDesc.Text);
            steelcmd.Parameters.AddWithValue("@eventDescOld", txtEventDescOld.Text);
            steelcmd.Parameters.AddWithValue("@eventVideo", txtVideo.Text);
            steelcmd.Parameters.AddWithValue("@datecreated", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@createdby", HttpContext.Current.User.Identity.Name);
            steelcmd.Parameters.AddWithValue("@createdIP", Request.UserHostAddress);
            steelcmd.Parameters.AddWithValue("@publish", chkPublish.Checked ? "Yes" : "No");

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Event " + txtEventName.Text + " has been posted Successfully.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();

            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Event could not be saved. Please check details, images and document sizes and try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}