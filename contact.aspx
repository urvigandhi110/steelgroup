﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="contact.aspx.cs" Inherits="contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .errorvalidator {
            color: #ff0000;
            font-weight: bold;
            font-size: 0.9rem;
            margin-top: 3rem;
            display: block;
            margin-left: 0.5rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-12">
                <h1 class="pageheading">Contact <span>Us</span></h1>
            </div>
        </div>
    </div>
    <a name="contact"></a>
    <section class="contactboxes">
        <div class="container">
            <div class="row mt-4">
                <div class="col-12 col-md-4">
                    <div class="box text-center contact-us-box">
                        <div class="incon-box">
                            <i class="fas fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <h3 class="box-tittle">Corporate Address </h3>
                        <p class="box-text">301, Dev Corpora, Opp. Cadbury, Pokhran Road no. 1, Khopat, Thane(W), Maharashtra 400602</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="box text-center contact-us-box">
                        <div class="incon-box">
                            <i class="fas fa-phone" aria-hidden="true"></i>
                        </div>
                        <h3 class="box-tittle">Call Us</h3>
                        <p class="box-text">Ajay Tambe: (+91) 9322 199 557 </p>
                        <p class="box-text">Amit Purohit: (+91) 9926 744 333 </p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="box text-center contact-us-box">
                        <div class="incon-box">
                            <i class="fas fa-envelope" aria-hidden="true"></i>
                        </div>
                        <h3 class="box-tittle">Drop Email</h3>
                        <p class="box-text">ajay.tambe@steelgroup.co.in</p>
                        <p class="box-text">amit@steelgroup.co.in</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contactform">
        <div class="row no-gutters">
            <div class="col-md-6">
                <div class="px-3 px-md-5 py-1">
                    <h2 class="pageheading">Leave a <span>Message</span></h2>
                    <div class="panel panel-default panel-box">
                        <div class="panel-body">
                            <asp:UpdatePanel ID="updEnq" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlEnq" runat="server" DefaultButton="btnSubmit">
                                        <div class="row no-gutters">
                                            <div class="col-12">
                                                <div class="form-group row no-gutters contact-form">
                                                    <div class="col-11">
                                                        <label>Your Name:</label>
                                                        <asp:TextBox ID="txtName" CssClass="form-control" runat="server" placeholder="Your full name here" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                    <div class="col-1">
                                                        <asp:RequiredFieldValidator CssClass="errorvalidator" ID="rqtxtName" ControlToValidate="txtName" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="form-group row no-gutters contact-form">
                                                    <div class="col-11">
                                                        <label>Your Email:</label>
                                                        <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" placeholder="Your email here" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                    <div class="col-1">
                                                        <asp:RequiredFieldValidator CssClass="errorvalidator" ID="rqtxtEmail" ControlToValidate="txtEmail" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="form-group row no-gutters contact-form">
                                                    <div class="col-11">
                                                        <label>Your Mobile:</label>
                                                        <asp:TextBox ID="txtMobile" CssClass="form-control" runat="server" placeholder="Your mobile number with country code" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="col-1">
                                                        <asp:RequiredFieldValidator CssClass="errorvalidator" ID="rqtxtMobile" ControlToValidate="txtMobile" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="form-group row no-gutters contact-form">
                                                    <div class="col-11">
                                                        <label>Message:</label>
                                                        <asp:TextBox ID="txtMessage" TextMode="MultiLine" Rows="4" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>
                                                    <div class="col-1">
                                                        <asp:RequiredFieldValidator CssClass="errorvalidator" ID="rqtxtMessage" ControlToValidate="txtMessage" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:LinkButton ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" CssClass="btn btn-default send-btn col-11 col-md-4">Send Enquiry</asp:LinkButton>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                        <p class="text-center text-success mt-5">
                                            <span class="fas fa-4x fa-check-circle"></span>
                                        </p>
                                        <p class="text-dark mt-3 text-center">
                                            <asp:Literal ID="ltrlEnqMessageText" runat="server"></asp:Literal>
                                        </p>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default panel-box">
                    <div class="panel-body">
                        <iframe class="iframe-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6156.2856363942265!2d72.96613713706596!3d19.203439730297454!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b915fb7b915f%3A0x1ef665c0356165fc!2sGloria+Business+Centres!5e0!3m2!1sen!2sin!4v1518683976032" width="600" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

