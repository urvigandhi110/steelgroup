﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="SponsoredEvents.aspx.cs" Inherits="SponsoredEvents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="pageheading">Sponsored <span>Events</span></h1>
            </div>
        </div>
    </div>
    <section style="background-color: #f5f5f5; padding: 1.5rem 0;">
        <div class="container pb-5">
            <div class="row no-gutters">
                <div class="col-12 col-md-9">
                    <div class="tab-content" id="v-pills-tabContent">
                        <asp:Repeater ID="rptMainTabs" runat="server">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfyear" Value='<%# Eval("tpyear") %>' runat="server" />
                                <div class="<%# Container.ItemIndex==0?"tab-pane fade show active":"tab-pane fade" %>" id="<%# "v-pills-" + Eval("tpyear") %>" role="tabpanel" aria-labelledby="<%# "v-pills-" + Eval("tpyear") + "-tab" %>">
                                    <ul class="list-unstyled tpeventslist">
                                        <asp:Repeater ID="rptSponsored" runat="server">
                                            <ItemTemplate>
                                                <li class="media">
                                                    <div class="eventdt">
                                                        <span class="h3"><%# Convert.ToDateTime(Eval("tpEventStartDate").ToString()).ToString("dd") %></span>
                                                        <span><%# Convert.ToDateTime(Eval("tpEventStartDate").ToString()).ToString("MMM") %></span>
                                                    </div>
                                                    <img class="mr-4" src='<%# Page.ResolveUrl("~/cms") + Eval("tpEventImage").ToString().Replace("~/","/") + "?w=250&h=250&mode=crop" %>' alt="">
                                                    <div class="media-body">
                                                        <h5 class="mt-0 mb-1"><%# Eval("tpEventName").ToString() %></h5>
                                                        <p class="basicdetails">
                                                            <%# (Convert.ToDateTime(Eval("tpEventStartDate").ToString())==Convert.ToDateTime(Eval("tpEventEndDate").ToString())?Convert.ToDateTime(Eval("tpEventStartDate").ToString()).ToString("dd MMM, yyyy"):Convert.ToDateTime(Eval("tpEventStartDate").ToString()).ToString("dd") + " - " + Convert.ToDateTime(Eval("tpEventEndDate").ToString()).ToString("dd MMM, yyyy")) + " | " + "<strong>Organized by:</strong> " + Eval("tpEventOrganizer").ToString() + " | <strong>Venue:</strong> " + Eval("tpEventVenue").ToString() %>
                                                        </p>
                                                        <div class="clearfix"></div>
                                                        <span class="desc"><%# HttpUtility.HtmlDecode(Eval("tpEventDesc").ToString()).Replace("<p>&nbsp;</p>","") %></span>
                                                        <hr class="clearfix" />
                                                        <a class="btn btn-danger btn-sm btn-events float-left" href="<%# Eval("tpEventWebsite").ToString().StartsWith("http")?Eval("tpEventWebsite").ToString():"http://" + Eval("tpEventWebsite").ToString() %>"><i class="fas fa-globe"></i>Visit Website</a>
                                                        <a class="btn btn-danger btn-sm btn-events float-left" href="<%# Page.ResolveUrl("~/cms") + Eval("tpEventBrochure").ToString().Replace("~/","/") %>" download="<%# Eval("tpEventName").ToString().Replace(" ","-") + System.IO.Path.GetExtension(Eval("tpEventBrochure").ToString()) %>"><i class="far fa-file-pdf"></i>Download Brochure</a>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="col-12 col-md-3 px-4">
                    <div class="nav flex-column nav-pills yeartabs" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <asp:Repeater ID="rptMainTabPills" runat="server">
                            <ItemTemplate>
                                <a class="<%# Container.ItemIndex==0?"nav-link active":"nav-link" %>" id="<%# "v-pills-" + Eval("tpyear") + "-tab" %>" data-toggle="pill" href="<%# "#v-pills-" + Eval("tpyear") %>" role="tab" aria-controls="<%# "v-pills-" + Eval("tpyear") %>" aria-selected="<%# Container.ItemIndex==0?"true":"false" %>"><%# Eval("tpyear") %></a>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>

        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

