﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class App_Jobs_PostJob : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        chkFeatured.InputAttributes.Add("class", "checkbox-template");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;
            string filename = "~/uploads/jobs/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage.FileName);

            if (fileImage.HasFile && !System.IO.File.Exists(Server.MapPath(filename)))
            {
                fileImage.SaveAs(Server.MapPath(filename));
            }
            else
            {
                filename = "~/uploads/jobs/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage.FileName);
                fileImage.SaveAs(Server.MapPath(filename));
            }

            string newjobsID = "JOB" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(1000, 9999);

            steelcmd.CommandText = "INSERT INTO jobsmaster(jobID,jobHeading,jobLocation,jobCompany,jobImage,jobSalary,jobQualification,jobExperience,jobNumPosts,jobSkills,jobDescription,jobURL,jobLastDate,jobFeatured,datecreated,createdby,createdIP,publish) VALUES(@jobID,@jobHeading,@jobLocation,@jobCompany,@jobImage,@jobSalary,@jobQualification,@jobExperience,@jobNumPosts,@jobSkills,@jobDescription,@jobURL,@jobLastDate,@jobFeatured,@datecreated,@createdby,@createdIP,@publish)";

            steelcmd.Parameters.AddWithValue("@jobID", newjobsID);
            steelcmd.Parameters.AddWithValue("@jobHeading", txtJobHeading.Text);
            steelcmd.Parameters.AddWithValue("@jobLocation", txtLocation.Text);
            steelcmd.Parameters.AddWithValue("@jobCompany", txtCompanyName.Text);
            steelcmd.Parameters.AddWithValue("@jobImage", filename);
            steelcmd.Parameters.AddWithValue("@jobSalary", txtSalary.Text);
            steelcmd.Parameters.AddWithValue("@jobQualification", txtQualification.Text);
            steelcmd.Parameters.AddWithValue("@jobExperience", txtExperience.Text);
            steelcmd.Parameters.AddWithValue("@jobNumPosts", txtNumPostst.Text);
            steelcmd.Parameters.AddWithValue("@jobSkills", txtSkillset.Text);
            steelcmd.Parameters.AddWithValue("@jobDescription", txtDescription.Text);
            steelcmd.Parameters.AddWithValue("@jobURL", txtURL.Text);
            steelcmd.Parameters.AddWithValue("@jobLastDate", txtLastDate.Text);
            steelcmd.Parameters.AddWithValue("@jobFeatured", chkFeatured.Checked ? "Yes" : "No");
            steelcmd.Parameters.AddWithValue("@datecreated", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@createdby", HttpContext.Current.User.Identity.Name);
            steelcmd.Parameters.AddWithValue("@createdIP", Request.UserHostAddress);
            steelcmd.Parameters.AddWithValue("@publish", "Yes");

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Job has been posted Successfully.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();

            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Job could not be saved. Please check details, image size, type and try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}