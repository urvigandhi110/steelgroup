﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class App_Events_EditDownloads : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
              
                string eventID = Request.QueryString["eventID"];

                steelcon = new SqlConnection(connstr);
                steelcmd = new SqlCommand("SELECT eventName FROM eventsmaster WHERE eventID=@eventID; ");
                steelcmd.Parameters.AddWithValue("@eventID", eventID);

                steelcmd.Connection = steelcon;
                steelcon.Open();

                rdrd = steelcmd.ExecuteReader();
                
                if(rdrd.Read())
                {
                    ltrlEventName.Text = rdrd["eventName"].ToString();
                }

                rdrd.Close();
                steelcon.Close();
                steelcmd.Dispose();
            }
        }
        catch (Exception ex)
        { }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            string mainquery = "UPDATE eventsmaster SET ";

            string filenameeventRegForm, filenameeventSponsorshipForm, filenameeventParticipating, filenameeventBrochure, filenameeventAgenda, filenameeventLayout, filenameeventImage1, filenameeventImage2, filenameeventImage3, filenameeventImage4;

            if (fileAgenda.HasFile)
            {
                filenameeventAgenda = "~/uploads/event/agenda/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileAgenda.FileName);
                fileAgenda.SaveAs(Server.MapPath(filenameeventAgenda));
                steelcmd.Parameters.AddWithValue("@eventAgenda", filenameeventAgenda);
                mainquery += "eventAgenda=@eventAgenda,";
            }

            if (fileSponsorshipForm.HasFile)
            {
                filenameeventSponsorshipForm = "~/uploads/event/Sponsorship-Form/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileSponsorshipForm.FileName);
                fileSponsorshipForm.SaveAs(Server.MapPath(filenameeventSponsorshipForm));
                steelcmd.Parameters.AddWithValue("@eventSponsorshipForm", filenameeventSponsorshipForm);
                mainquery += "eventSponsorshipForm=@eventSponsorshipForm,";
            }

            if (fileRegForm.HasFile)
            {
                filenameeventRegForm = "~/uploads/event/Registration-Form/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileRegForm.FileName);
                fileRegForm.SaveAs(Server.MapPath(filenameeventRegForm));
                steelcmd.Parameters.AddWithValue("@eventRegForm", filenameeventRegForm);
                mainquery += "eventRegForm=@eventRegForm,";

            }

            if (fileLayout.HasFile)
            {
                filenameeventLayout = "~/uploads/event/layout/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileLayout.FileName);
                fileLayout.SaveAs(Server.MapPath(filenameeventLayout));
                steelcmd.Parameters.AddWithValue("@eventLayout", filenameeventLayout);
                mainquery += "eventLayout=@eventLayout,";

            }
            if (fileParticipating.HasFile)
            {
                filenameeventParticipating = "~/uploads/event/Participating/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileParticipating.FileName);
                fileParticipating.SaveAs(Server.MapPath(filenameeventParticipating));
                steelcmd.Parameters.AddWithValue("@eventParticipatingPDF", filenameeventParticipating);
                mainquery += "eventParticipatingPDF=@eventParticipatingPDF,";
            }
            if (fileBrochure.HasFile)
            {
                filenameeventBrochure = "~/uploads/event/brochure/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileBrochure.FileName);
                fileBrochure.SaveAs(Server.MapPath(filenameeventBrochure));
                steelcmd.Parameters.AddWithValue("@eventBrochure", filenameeventBrochure);
                mainquery += "eventBrochure=@eventBrochure,";

            }

            if (fileImage1.HasFile)
            {
                filenameeventImage1 = "~/uploads/event/images/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage1.FileName);
                fileImage1.SaveAs(Server.MapPath(filenameeventImage1));
                steelcmd.Parameters.AddWithValue("@eventImage1", filenameeventImage1);
                mainquery += "eventImage1=@eventImage1,";
            }
            if (fileImage2.HasFile)
            {
                filenameeventImage2 = "~/uploads/event/images/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage2.FileName);
                fileImage2.SaveAs(Server.MapPath(filenameeventImage2));
                steelcmd.Parameters.AddWithValue("@eventImage2", filenameeventImage2);
                mainquery += "eventImage2=@eventImage2,";
            }
            if (fileImage3.HasFile)
            {
                filenameeventImage3 = "~/uploads/event/images/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage3.FileName);
                fileImage3.SaveAs(Server.MapPath(filenameeventImage3));
                steelcmd.Parameters.AddWithValue("@eventImage3", filenameeventImage3);
                mainquery += "eventImage3=@eventImage3,";
            }
            if (fileImage4.HasFile)
            {
                filenameeventImage4 = "~/uploads/event/images/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage4.FileName);
                fileImage4.SaveAs(Server.MapPath(filenameeventImage4));
                steelcmd.Parameters.AddWithValue("@eventImage4", filenameeventImage4);
                mainquery += "eventImage4=@eventImage4,";
            }
            string eventID = Request.QueryString["eventID"].ToString();
            mainquery += "datemodified=@datemodified,modifiedby=@modifiedby,modifiedIP=@modifiedIP WHERE eventID=@eventID";
            mainquery = mainquery.Replace(",WHERE", " WHERE");
            steelcmd.CommandText = mainquery;

            steelcmd.Parameters.AddWithValue("@eventID", eventID);           
            steelcmd.Parameters.AddWithValue("@datemodified", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@modifiedby", HttpContext.Current.User.Identity.Name);
            steelcmd.Parameters.AddWithValue("@modifiedIP", Request.UserHostAddress);

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Event " + ltrlEventName.Text + " Downloads has been updated Successfully.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();

            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Event Downloads could not be updated. Please check details, images and sizes and try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}