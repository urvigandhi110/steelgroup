﻿<%@ Page MaintainScrollPositionOnPostback="true" Title="" Language="C#" MasterPageFile="~/EventMaster.master" AutoEventWireup="true" CodeFile="PastEvent.aspx.cs" Inherits="PastEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

    <style type="text/css">
        .cover-heading {
            font-family: 'Pacifico', cursive;
            text-transform: capitalize;
            font-weight: normal;
        }

            .cover-heading small {
                font-family: 'Oswald', sans-serif;
                font-style: italic;
                text-transform: uppercase;
                width: 70%;
                margin: 0 auto;
                display: block;
                margin-top: 1rem;
            }

        #speakers img {
            filter: grayscale(1);
            opacity: 0.7;
        }


        #mainContent_principalsponsors .sectionheading, #mainContent_platinumsponsors .sectionheading, #mainContent_goldsponsors .sectionheading, #mainContent_silversponsors .sectionheading, #mainContent_associatesponsors .sectionheading {
            border-bottom: solid 1px #f3f3f3;
            padding-bottom: 1rem;
        }

        #mainContent_platinumsponsors, #mainContent_principalsponsors {
            padding: 2rem 0;
        }

        #mainContent_goldsponsors {
            padding: 1.5rem 0;
        }

        #mainContent_silversponsors {
            padding: 1.5rem 0;
        }

        #mainContent_associatesponsors {
            padding: 1.5rem 0;
        }

        #mainContent_clubbedsponsors {
            padding: 1.5rem 0;
        }

            #mainContent_clubbedsponsors h6 {
                font-size: 0.9rem;
                font-weight: 700;
                color: #00406e;
                margin-bottom: 1rem;
            }

        #picturegallery .sectionheading {
            font-size: 1.5rem;
        }

        .ekko-lightbox .modal-content {
            border-radius: 0;
        }

        .ekko-lightbox .modal-body {
            padding: 0.5rem;
        }

        #mainContent_goldsponsors .sectionheading {
            font-size: 2rem;
        }

        #mainContent_silversponsors .sectionheading {
            font-size: 1.75rem;
        }

        #mainContent_associatesponsors .sectionheading {
            font-size: 1.5rem;
        }

        #mainContent_othersponsors .sectionheading, #mainContent_partners .sectionheading, #mainContent_clubbedsponsors h6, #mainContent_organizers .sectionheading, #mainContent_participating .sectionheading {
            font-size: 1.2rem;
            border-bottom: solid 1px #f3f3f3;
            padding-bottom: 0.8rem;
        }

        #mainContent_partners, #mainContent_othersponsors, #mainContent_organizers, #mainContent_participating {
            padding: 1.5rem 0;
        }

            #mainContent_partners a {
                font-size: 0.8rem;
                text-align: center;
                display: block;
                color: #000;
                margin-bottom: 1rem;
            }

                #mainContent_partners a:hover {
                    text-decoration: none;
                }

            #mainContent_othersponsors img, #mainContent_allsponsors img, #mainContent_partners img {
                filter: grayscale(100%);
                opacity: 0.5;
            }

                #mainContent_othersponsors img:hover, #mainContent_allsponsors img:hover, #mainContent_partners img:hover {
                    filter: grayscale(0%);
                    opacity: 1;
                }

        .ekko-lightbox .modal-header .close {
            background: rgba(255,255,255,1);
            opacity: 1;
            z-index: 99;
            border-radius: 50%;
            width: 2rem;
            height: 2rem;
            padding: 0;
        }

        .ekko-lightbox .modal-header {
            height: 0;
            padding: 0;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">

    <section id="timer">
        <div class="col-12 col-md-6 offset-md-3">
            <div class="row no-gutters">
                <div class="col-6 col-md-3 highlightsingle">
                    <img src="<%= Page.ResolveUrl("~") %>images/eventicons-alt/companies.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                    <h4>
                        <asp:Literal ID="ltrlCompanies" runat="server"></asp:Literal></h4>
                    <p>Companies</p>
                </div>
                <div class="col-6 col-md-3 highlightsingle">
                    <img src="<%= Page.ResolveUrl("~") %>images/eventicons-alt/delegates.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                    <h4>
                        <asp:Literal ID="ltrlDelegates" runat="server"></asp:Literal></h4>
                    <p>Delegates</p>
                </div>
                <div class="col-6 col-md-3 highlightsingle">
                    <img src="<%= Page.ResolveUrl("~") %>images/eventicons-alt/panelists.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                    <h4>
                        <asp:Literal ID="ltrlPanelists" runat="server"></asp:Literal></h4>
                    <p>Panelists</p>
                </div>
                <div class="col-6 col-md-3 highlightsingle">
                    <img src="<%= Page.ResolveUrl("~") %>images/eventicons-alt/exhibitors.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                    <h4>
                        <asp:Literal ID="ltrlExhibitors" runat="server"></asp:Literal></h4>
                    <p>Exhibitors</p>
                </div>
            </div>
        </div>
    </section>
    <section id="aboutevent">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 abouttabs">

                        <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-1-tab" data-toggle="pill" href="#pills-1" role="tab" aria-controls="pills-1" aria-selected="true">Post Event Press Release</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-2-tab" data-toggle="pill" href="#pills-2" role="tab" aria-controls="pills-2" aria-selected="false">Event Objectives</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-3-tab" data-toggle="pill" href="#pills-3" role="tab" aria-controls="pills-3" aria-selected="false">About Organizers</a>
                            </li>
                        </ul>
                        <a name="agenda"></a>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-1" role="tabpanel" aria-labelledby="pills-1-tab">
                                <asp:Literal ID="ltrlPressRelease" runat="server"></asp:Literal>
                            </div>
                            <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-2-tab">
                                <asp:Literal ID="ltrlObjectives" runat="server"></asp:Literal>
                            </div>
                            <div class="tab-pane fade" id="pills-3" role="tabpanel" aria-labelledby="pills-3-tab">
                                <asp:Literal ID="ltrlAboutOrganizers" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <a name="speakers"></a>
    <section id="speakers">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Speakers</h3>
                    <p class="sectionsubheading">Know Who Spoke at the Event</p>
                </div>
            </div>
            <asp:Panel ID="pnlTopSpeaker" CssClass="row mb-4" runat="server">
                <div class="col-12 col-md-10 offset-md-1">
                    <ul class="list-unstyled speakerslist">
                        <li class="media bg-light">
                            <asp:Image ID="imgTopSpeaker" CssClass="mr-4" runat="server" />
                            <div class="media-body">
                                <h5 class="mt-0 mb-1">
                                    <asp:Literal ID="ltrlTopSpeakerName" runat="server"></asp:Literal><span class="badge badge-pill badge-dark font-weight-light small float-right"><asp:Literal ID="ltrlTopSpeakerType" runat="server"></asp:Literal></span></h5>
                                <p class="font-italic text-muted mb-2">
                                    <asp:Literal ID="ltrlTopSpeakerDetails" runat="server"></asp:Literal>
                                </p>
                                <div class="clearfix"></div>
                                <p class="desc">
                                    <asp:Literal ID="ltrlTopSpeakerIntro" runat="server"></asp:Literal>
                                </p>
                                <div class="clearfix"></div>
                                <asp:HyperLink ID="lnkSpeakerLinkedIn" runat="server" CssClass="speakerlinks" Target="_blank">
                                        <div class="text-center">
                                            <span class="fab fa-linkedin"></span>
                                        </div>
                                        LinkedIn
                                </asp:HyperLink>
                                <asp:HyperLink ID="lnkSpeakerTwitter" runat="server" CssClass="speakerlinks" Target="_blank">
                                        <div class="text-center">
                                            <span class="fab fa-twitter"></span>
                                        </div>
                                        Twitter
                                </asp:HyperLink>
                                <asp:HyperLink ID="lnkSpeakerDetails" runat="server" CssClass="speakerlinks" Target="_blank">
                                          <div class="text-center">
                                            <span class="far fa-user"></span>
                                        </div>
                                        Details
                                </asp:HyperLink>

                            </div>
                        </li>
                    </ul>
                </div>
            </asp:Panel>
            <div class="row no-gutters justify-content-center">
                <asp:Repeater ID="rptEventSpeaker" runat="server">
                    <ItemTemplate>
                        <div class="col-12 col-md-3 text-center speakerdetails mb-4">
                            <div class="imgblur rounded-circle  w-75 mx-auto">
                                <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSpeakerPhoto").ToString().Replace("~/","/") + "?w=500&h=500&mode=crop&scale=both" %>' alt="" class="img-fluid d-block mx-auto rounded-circle" />
                            </div>
                            <h4><%# Eval("eventSpeakerName") %></h4>
                            <p class="badge badge-pill"><%# Eval("eventSpeakerOrg").ToString() + " | " + Eval("eventSpeakerDesignation").ToString() + " | " + Eval("eventSpeakerCountry").ToString() %></p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

            </div>
            <%-- <div class="clearfix"></div>
            <div class="row justify-content-center mt-4">
                <a href="<%= Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/speakers" %>" class="btn btn-default eventbtn mr-3">View All Speakers</a>
                <a href="<%= Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/become-a-speaker" %>" class="btn btn-default eventbtn">Become a Speaker</a>
            </div>--%>
        </div>
    </section>

    <a name="downloads"></a>
    <section id="pptdownloads" class="py-5 pptdownloads" runat="server">
        <div class="row no-gutters">
            <div class="col-12 text-center">
                <h3 class="sectionheading">Download Presentations</h3>
            </div>
        </div>
        <asp:ScriptManager ID="scrptDownloads" runat="server"></asp:ScriptManager>
        <asp:Panel ID="pnlDownloadsAvailable" runat="server">
            <div class="container">
                <div class="row no-gutters">
                    <table class="downloadstable">
                        <thead>
                            <tr>
                                <th class="col-1">S.No.</th>
                                <th class="col-3">Presentation Title</th>
                                <th class="col-4">Speaker</th>
                                <th class="col-3 text-right">
                                    <asp:LinkButton ID="lnkDownloadAdd" data-toggle="modal" data-target="#downloadModal" CssClass="btn btn-sm btn-success" runat="server" OnClick="lnkDownloadAdd_Click">Download All</asp:LinkButton>
                                </th>
                            </tr>
                        </thead>

                        <tbody class="downloadstable-scrollbar">
                            <asp:Repeater ID="rptDownloadsTable" runat="server" OnItemCommand="rptDownloadsTable_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td class="col-1"><%# Eval("sno") %></td>
                                        <td class="col-3"><%# Eval("ppttitle") %></td>
                                        <td class="col-4"><%# Eval("pptspeaker") %></td>
                                        <td class="col-3 text-right">
                                            <asp:LinkButton ID="lnkDownload" data-toggle="modal" data-target="#downloadModal" CommandArgument='<%# Eval("pptID").ToString() %>' CommandName="downloadppt" CssClass="btn btn-sm btn-warning" runat="server" data-pptid='<%# Eval("pptID").ToString() %>' data-eventid='<%# Eval("eventID").ToString() %>' Text="Download"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr id="headingtrEmail" visible="false" runat="server">
                                <td colspan="4" class="font-weight-bold text-center d-block">Download the following presentations by Emailing to the given Speaker Email-IDs below:
                                </td>
                            </tr>
                            <asp:Repeater ID="rptDownloadFromEmail" Visible="false" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td class="col-1"><%# Eval("sno") %></td>
                                        <td class="col-3"><%# Eval("ppttitle") %></td>
                                        <td class="col-4"><%# Eval("pptspeaker") %></td>
                                        <td class="col-3 text-right">
                                            <asp:Literal ID="ltrlEmail" Text='<%# Eval("pptFile") %>' runat="server"></asp:Literal>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>

                    </table>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlNoDownloads" runat="server" Visible="false" CssClass="text-center p-3">
            No Downloads available now. Please come back later.
        </asp:Panel>
    </section>

    <div id="downloadModal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Presentation Download Authentication</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="updDownloadPPT" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="hfCurrPPTID" runat="server" />
                            <asp:HiddenField ID="hfCurrEventID" runat="server" />
                            <asp:Panel ID="pnlSend" runat="server">
                                <div class="form-group">
                                    <label>Please enter your registered email ID</label>
                                    <asp:TextBox ID="txtAuthEmail" type="email" MaxLength="200" placeholder="Enter Email ID" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:Button ID="btnSend" OnClick="btnSend_Click" CssClass="btn btn-success" runat="server" Text="Send on Email" />
                                    <br />
                                    <asp:Label ID="lblError" runat="server" Text="" Visible="false" CssClass="text-danger font-weight-bold small pl-3"></asp:Label>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlSent" Visible="false" runat="server">
                                <div class="text-center p-4">
                                    <i class="fa fa-check-circle text-success fa-3x"></i>
                                    <br />
                                    <br />
                                    <span class="h3 text-success d-block font-weight-light">Presentation sent on Email.</span>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Done</button>
                </div>
            </div>
        </div>
    </div>

    <a name="sponsors"></a>
    <%-- <section id="principalsponsors" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Principal Sponsors</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-10 offset-md-1">
                    <div class="row justify-content-center">
                        <asp:Repeater ID="rptPrincipal" runat="server">
                            <ItemTemplate>
                                <div class="col-12 col-md-4 mt-5">
                                    <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="platinumsponsors" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Platinum Sponsors</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-10 offset-md-1">
                    <div class="row justify-content-center">
                        <asp:Repeater ID="rptPlatinum" runat="server">
                            <ItemTemplate>
                                <div class="col-12 col-md-4 mt-5">
                                    <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <section id="goldsponsors" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Gold Sponsors</h3>
                </div>
            </div>
            <div class="row justify-content-center">
                <asp:Repeater ID="rptGold" runat="server">
                    <ItemTemplate>
                        <div class="col-6 col-md-3 mt-5">
                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <section id="silversponsors" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Silver Sponsors</h3>
                </div>
            </div>
            <div class="row justify-content-center">

                <asp:Repeater ID="rptSilver" runat="server">
                    <ItemTemplate>
                        <div class="col-6 col-md-n mt-5">
                            <div>
                                <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <section id="associatesponsors" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Associate Sponsors</h3>
                </div>
            </div>
            <div class="row justify-content-center">
                <asp:Repeater ID="rptAssociate" runat="server">
                    <ItemTemplate>
                        <div class="col-6 col-md-2 mt-5">
                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid d-block" />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <section id="clubbedsponsors" runat="server">
        <div class="container-fluid">
            <div class="row justify-content-center">

                <asp:Panel ID="pnlNetworkingLunch" Visible="false" CssClass="col-6 col-md-m mt-5" runat="server">
                    <h6>Networking Lunch</h6>
                    <asp:Image ID="imgNetworkingLunch" CssClass="img-fluid d-block" runat="server" />
                </asp:Panel>

                <asp:Panel ID="pnlNetworkingDinner" Visible="false" CssClass="col-6 col-md-m mt-5" runat="server">
                    <h6>Networking Dinner</h6>
                    <asp:Image ID="imgNetworkingDinner" CssClass="img-fluid d-block" runat="server" />
                </asp:Panel>
                <asp:Panel ID="pnlDelegateKit" Visible="false" CssClass="col-6 col-md-m mt-5" runat="server">
                    <h6>Delegate Kit Sponsor</h6>
                    <asp:Image ID="imgDelegateKit" CssClass="img-fluid d-block" runat="server" />
                </asp:Panel>
                <asp:Repeater ID="rptCompanyHighlight" runat="server">
                    <ItemTemplate>
                        <div class="col-6 col-md-m mt-5">
                            <h6><%# Container.ItemIndex==0?"Company Highlight":"<br><br>" %></h6>
                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid d-block" />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </section>--%>

    <section id="allsponsors" runat="server">
        <div class="row no-gutters">
            <div class="col-12 text-center">
                <h3 class="sectionheading">Sponsors</h3>
            </div>
        </div>
        <div class="container">
            <div class="row no-gutters justify-content-center">
                <asp:Repeater ID="rptAllSponsors" runat="server">
                    <ItemTemplate>
                        <div class="col-6 col-md-2 mt-5">
                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=canvas&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </section>

    <section id="othersponsors" runat="server">
        <div class="row no-gutters">
            <div class="col-12 text-center">
                <h3 class="sectionheading">Exhibitors</h3>
            </div>
        </div>
        <div class="container">
            <div class="row no-gutters justify-content-center">
                <asp:Repeater ID="rptExhibitors" runat="server">
                    <ItemTemplate>
                        <div class="col-6 col-md-2 mt-5">
                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=canvas&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </section>
    <section id="partners" runat="server">
        <div class="row no-gutters">
            <div class="col-12 text-center">
                <h3 class="sectionheading">Partners</h3>
            </div>
        </div>
        <div class="container">
            <div class="row no-gutters justify-content-center">
                <asp:Repeater ID="rptPartners" runat="server">
                    <ItemTemplate>
                        <div class="col-6 col-md-m">
                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=canvas&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>

        </div>
    </section>
    <section id="organizers">
        <div class="container">
            <div class="row no-gutters justify-content-center">
                <div class="col-12 col-md-6 text-center text-md-right pr-3" id="organizedby" runat="server">
                    <h3 class="sectionheading">Organized By</h3>
                    <img src="<%= Page.ResolveUrl("~") %>images/organizer-steel-group.jpg" />
                </div>
                <div class="col-12 col-md-6 text-center text-md-left pl-3" id="association" runat="server">
                    <h3 class="sectionheading">In Association with</h3>
                    <asp:HyperLink ID="lnkAssociation" runat="server" Target="_blank">
                        <asp:Image ID="imgassociation" runat="server" />
                    </asp:HyperLink>
                </div>
            </div>
        </div>

    </section>
    <section id="participating" runat="server">
        <div class="row no-gutters">
            <div class="col-12 text-center">
                <h3 class="sectionheading">Participating Companies</h3>
            </div>
        </div>
        <div class="swiper-container swiper-container-participating">
            <div class="swiper-wrapper swiper-wrapper-participating">
                <asp:Repeater ID="rptParticipating" runat="server">
                    <ItemTemplate>
                        <div class="swiper-slide">
                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=300&mode=pad&scale=canvas&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="swiper-pagination swiper-pagination-participating"></div>
        </div>
        <div class="w-100 d-block py-2 text-center">
            <asp:HyperLink ID="lnkParticipatingDownload" CssClass="btn btn-default eventbtn" runat="server">Download List of Participants</asp:HyperLink>
        </div>
    </section>

    <section id="picturegallery">
        <div class="container">
            <div class="row no-gutters mb-4">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Conference Photo Gallery</h3>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-12">
                    <div class="row">
                        <asp:Repeater ID="rptImageGallery" runat="server">
                            <ItemTemplate>
                                <a href='<%# Page.ResolveUrl("~/cms") + Eval("photoFile").ToString().Replace("~/","/") + "?w=1000&h=600&mode=crop" %>' data-toggle="lightbox" data-gallery="event-gallery" class="col-6 col-md-3 mb-4">
                                    <img src='<%# Page.ResolveUrl("~/cms") + Eval("photoFile").ToString().Replace("~/","/") + "?w=500&h=300&mode=crop" %>' class="img-fluid">
                                </a>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <asp:HiddenField ID="hfeventDate" runat="server" />
    <asp:HiddenField ID="hfEventID" runat="server" />
    <asp:HiddenField ID="mailvals" runat="server" />
    <asp:HiddenField ID="hfHashTags" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
    <script>
        var swiper = new Swiper('.swiper-container-participating', {
            slidesPerView: 8,
            spaceBetween: 20,
            autoplay: {
                delay: 3000,
                disableOnInteraction: true,
            },
            pagination: {
                el: '.swiper-pagination-participating',
                clickable: true,
            },
        });

        $('.videomodal').on('hidden.bs.modal', function () {
            $(".videomodal iframe").attr("src", $(".videomodal iframe").attr("src"));
        });

        $('#downloadModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var eventID = button.data('eventid');// Extract info from data-* attributes
            var pptID = button.data('pptid');// Extract info from data-* attributes

            var modal = $(this);
            modal.find('#<%= hfCurrPPTID.ClientID %>').val(pptID);
            modal.find('#<%= hfCurrEventID.ClientID %>').val(eventID);
        })
        $('#downloadModal').on('hidden.bs.modal', function (event) {
            location.reload();
        });
        $(document).on('click', '[data-toggle="lightbox"]', function (event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true
            });
        });

    </script>
</asp:Content>

