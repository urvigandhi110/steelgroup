﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="ThirdPartyEvent.aspx.cs" Inherits="App_Events_ThirdPartyEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/themes/dark.css" />
    <script src="https://cdn.ckeditor.com/4.7.0/standard/ckeditor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Add 3rd Party Event</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Please Enter the following Event details-</h3>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Start Date <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtStartDate" AutoCompleteType="Disabled" placeholder="Please enter Start Date here" runat="server" CssClass="form-control flatpickr" required></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">End Date <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtEndDate" AutoCompleteType="Disabled" placeholder="Please enter End Date here" runat="server" CssClass="form-control flatpickr" required></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Event Name <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtEventName" CssClass="form-control" placeholder="Enter Event name here" required runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Event Short Description <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtEventDesc" placeholder="Please enter Description here" runat="server" TextMode="MultiLine" CssClass="form-control contentBox" required></asp:TextBox>
                </div>
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Event Organizer <small class="text-danger">*Required</small></label>
                            <asp:TextBox ID="txtOrganizer" placeholder="Please enter Organizer Name here" runat="server" CssClass="form-control" required></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Event Website</label>
                            <asp:TextBox ID="txtEventWebsite" placeholder="Please enter Website here" runat="server" CssClass="form-control" required></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Event Venue <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtVenue" placeholder="Please enter Venue here" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>
                  <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Event Brochure</label>
                            <asp:FileUpload ID="fileBrochure" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Event Image <small class="text-danger">*Keep it Square</small></label>
                            <asp:FileUpload ID="fileImage" CssClass="form-control" runat="server" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Add Event" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script>
        CKEDITOR.replaceAll('contentBox');
        CKEDITOR.config.htmlEncodeOutput = true;
    </script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.js"></script>
    <script>
        $(".flatpickr").flatpickr({
            dateFormat: "Y-m-d", allowInput: true
        });
    </script>
</asp:Content>

