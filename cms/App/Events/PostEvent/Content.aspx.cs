﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_PostEvent_Content : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string eventID = Request.QueryString["eventID"].ToString();

            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            steelcmd.CommandText = "UPDATE eventsmaster SET postEventPress=@postEventPress,postEventObjectives=@postEventObjectives,postEventAbout=@postEventAbout WHERE eventID=@eventID";
            steelcmd.Parameters.AddWithValue("@postEventPress", txtEventPress.Text);
            steelcmd.Parameters.AddWithValue("@postEventObjectives", txtEventObjectives.Text);
            steelcmd.Parameters.AddWithValue("@postEventAbout", txtAbout.Text);
            steelcmd.Parameters.AddWithValue("@eventID", eventID);

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Text content has been saved Successfully.";

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Text content could not be saved. Please check details and try again.";

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        catch(Exception ex)
        {
            Response.Write(ex.ToString());
        }

    }
}