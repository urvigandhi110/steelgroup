﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="Jobs.aspx.cs" Inherits="Jobs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css" />
    <style>
        .swiper-container {
            width: 100%;
            height: 25rem;
        }

        .swiper-wrapper {
            justify-content: center !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <section id="jobs">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="pageheading">Steel <span>Jobs</span></h1>
                </div>
            </div>
            <div class="row">

                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <asp:Repeater ID="rptFeaturedJobs" runat="server">
                            <ItemTemplate>
                                <div class="swiper-slide">
                                    <a target="_blank" href='#'>
                                        <div class="featuredjobs">
                                            <div class="d-block w-100 text-center topimg">
                                                <img src='<%# Page.ResolveUrl("~/cms") + Eval("jobImage").ToString().Replace("~/","/") + "?w=208&h=128&mode=pad&scale=canvas&bg=ffffff" %>' alt="" />
                                                <%--<img src='<%# Eval("jobImage").ToString().Replace("~/","http://localhost/steeladmin/") + "?w=208&h=128&mode=pad&scale=canvas&bg=ffffff" %>' alt="" />--%>
                                            </div>
                                            <div class="iconicdetails">
                                                <div class="row h-100 no-gutters">
                                                    <div class="col">
                                                        <p><span class="fas fa-briefcase"></span></p>
                                                        <p class="val">
                                                            <%# Eval("jobExperience") %>
                                                            <br />
                                                            Experience
                                                        </p>
                                                    </div>
                                                    <div class="col secondcol">
                                                        <p><span class="fas fa-calendar-alt"></span></p>
                                                        <p class="val">
                                                            <%# DateTime.Parse(Eval("jobLastDate").ToString()).ToString("dd MMM yyyy") %>
                                                            <br />
                                                            Last Date
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="otherdetails">
                                                <h5><%# Eval("jobHeading") %></h5>
                                                <p><span class="fas fa-building fa-fw"></span><%# Eval("jobCompany") %></p>
                                                <p><span class="fas fa-map-marker fa-fw"></span><%# Eval("jobLocation") %></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination swiper-pagination-partners"></div>
                </div>
            </div>
        </div>
    </section>
    <section id="otherjobs">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9">
                    <div class="singlejob">
                        <div class="firstrow">
                            <img src="images/sponsor-4.jpg?w=130&h=80&mode=pad&scale=canvas" />
                            <span class="jobtitle">Production Manager for Steel/iron Industry for Singapore and Australia</span>
                            <span class="company">MPL International Consulting Group</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="secondrow row no-gutters">
                            <div class="col-4 col-md-2 graycol">
                                <span class="fas fa-briefcase fa-fw"></span>
                                <span>11-12 Years</span>
                            </div>
                            <div class="col-4 col-md-5 graycol">
                                <span class="fas fa-map-marker fa-fw"></span>
                                <span>Mumbai, Bengaluru, Coimbatore</span>
                            </div>
                            <div class="col-4 col-md-5 graycol">
                                <span class="fas fa-calendar-alt fa-fw"></span>
                                <span>28 Feb 2018</span>
                            </div>

                            <div class="col-4 col-md-2 graycol">
                                Skills:
                            </div>
                            <div class="col-4 col-md-10">
                                Should have minimum 10 to 15 years of Experience in heading Manufacturing/ Production/ Engineering.
                            </div>

                            <div class="col-4 col-md-2 graycol pt-2">
                                Job Description:
                            </div>
                            <div class="col-4 col-md-10 pt-2">
                                1) To manage all plant operations with effective utilization of all resources and implementing industry best practices that contributes to improve productivity and efficiency.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
    <script>
        $(document).ready(function () {

            var swiper = new Swiper('.swiper-container', {
                slidesPerView: 3,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                autoplay: {
                    delay: 3000,
                },
            });
        });
    </script>
</asp:Content>

