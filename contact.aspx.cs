﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class contact : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            string newEnqID = "ENQ" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(1000, 9999);

            steelcmd.CommandText = "INSERT INTO enquirymaster(enquiryID,enquiryName,enquiryEmailID,enquiryMobile,enquiryMessage,enquiryType,enquirySubject,datecreated,createdby,createdIP) VALUES(@enquiryID,@enquiryName,@enquiryEmailID,@enquiryMobile,@enquiryMessage,@enquiryType,@enquirySubject,@datecreated,@createdby,@createdIP)";

            steelcmd.Parameters.AddWithValue("@enquiryID", newEnqID);
            steelcmd.Parameters.AddWithValue("@enquiryName", txtName.Text);
            steelcmd.Parameters.AddWithValue("@enquiryEmailID", txtEmail.Text);
            steelcmd.Parameters.AddWithValue("@enquiryMobile", txtMobile.Text);
            steelcmd.Parameters.AddWithValue("@enquiryMessage", txtMessage.Text);
            steelcmd.Parameters.AddWithValue("@enquiryType", "");
            steelcmd.Parameters.AddWithValue("@enquirySubject", "");
            steelcmd.Parameters.AddWithValue("@datecreated", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@createdby", "User");
            steelcmd.Parameters.AddWithValue("@createdIP", Request.UserHostAddress);
            
            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlEnq.Visible = false;
                ltrlEnqMessageText.Text = "<br><br>Your Enquiry has been recieved by us. We will get back to you soon. Meanwhile you can contact us here if necessary -<br><br> <a class=\"btn btn-sm btn-success\" href=\"#contact\">Contact us</a>";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();

            }
            else
            {
                pnlMessage.Visible = true;
                pnlEnq.Visible = false;
                ltrlEnqMessageText.Text = "<br><br>Your enquiry could not be sent. Please check all details and try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        catch (Exception ex)
        {
            pnlMessage.Visible = true;
            pnlEnq.Visible = false;
            ltrlEnqMessageText.Text = ex.ToString();
            steelcon.Close();
        }
    }
}