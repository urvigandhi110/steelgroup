﻿<%@ Page Title="Steel News, World Steel Industry - Steel Group" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="News.aspx.cs" Inherits="News" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .carousel-control-next, .carousel-control-prev {
            top: -27%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="pageheading">Steel <span>News</span></h1>
            </div>
        </div>
    </div>

    <section id="midad">
        <div class="container-fluid">
            <div class="pref pref-mid">
                <asp:HyperLink ID="lnkMidAd" runat="server">
                    <asp:Image ID="imgAdinMid" CssClass="img-fluid" runat="server" />
                </asp:HyperLink>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <section id="newsticker" class="widget-block flasher-sec float-left w-100">
        <div class="container">
            <div class="row">
                <div class="col-md-2 pr-0">
                    <div class="heading-box">News Flash</div>
                </div>
                <div class="col-md-10 pl-0">
                    <div class="content-box">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <asp:Repeater ID="rptNewsTicker" runat="server">
                                    <ItemTemplate>
                                        <div class="carousel-item <%# Container.ItemIndex==0?"active":"" %>">
                                            <span class="time-box"><%# Convert.ToDateTime(Eval("datecreated").ToString()).ToString("h:mm tt") %></span><a href='<%# Page.ResolveUrl("~/news/")  + Eval("newsCategory").ToString() + "/" + SteelFunc.GenerateUrl(Eval("newsHeadline").ToString()) %>' class="text-dark"><%# Eval("newsHeadline") %></a>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="control-box">
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <section id="newscorner" class="banner-sec float-left w-100 pt-4 pb-5 newspage">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9 top-slider pr-3">
                    <div id="steelFeaturedNews" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <asp:Repeater ID="rptFeaturedNewsButtons" runat="server">
                                <ItemTemplate>
                                    <li data-target="#steelFeaturedNews" data-slide-to="<%# Container.ItemIndex %>" class="<%# Container.ItemIndex==0?"active":"" %>"></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <asp:Repeater ID="rptFeaturedNews" runat="server">
                                <ItemTemplate>
                                    <div class="carousel-item <%# Container.ItemIndex==0?"active":"" %>">
                                        <div class="news-block">
                                            <div class="news-media">
                                                <img class="img-fluid" src="<%# Page.ResolveUrl("~/cms") + Eval("newsImage").ToString().Replace("~/","/") + "?w=1000&h=400&scale=both&bgcolor=white" %>" alt='<%# Eval("newsHeadline") %>'>
                                            </div>
                                            <div class="news-title">
                                                <h2 class=" title-large"><a href='<%# Page.ResolveUrl("~/news/") + Eval("newsCategory").ToString() + "/" + SteelFunc.GenerateUrl(Eval("newsHeadline").ToString()) %>'><%# Eval("newsHeadline") %></a></h2>
                                            </div>
                                            <div class="news-des"><%# Eval("newsLeadPara").ToString().Length>250?Eval("newsLeadPara").ToString().Substring(0,250):Eval("newsLeadPara").ToString() %>...</div>
                                            <div class="time-text"><strong><%# Eval("datediff") %></strong></div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <a class="carousel-control-prev" href="#steelFeaturedNews" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#steelFeaturedNews" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <div class="clearfix mb-5"></div>
                    <ul class="list-unstyled newsmain">
                        <asp:GridView ID="gvNews" AllowPaging="true" PageSize="10" OnPageIndexChanging="gvNews_PageIndexChanging" AutoGenerateColumns="false" runat="server" CssClass="gvnews" ShowHeader="false" ShowFooter="false" BorderWidth="0" PagerStyle-BorderWidth="0" PagerStyle-CssClass="pagination-ys">
                            <Columns>
                                <asp:TemplateField ItemStyle-BorderWidth="0">
                                    <ItemTemplate>
                                        <li class="media my-1 border border-left-0 border-top-0 border-right-0 py-3">
                                            <img class="align-self-center mr-3" src="<%# Page.ResolveUrl("~/cms") + Eval("newsImage").ToString().Replace("~/","/") + "?w=300&h=150&scale=both&bgcolor=white" %>" alt='<%# Eval("newsHeadline") %>'>
                                            <div class="media-body">
                                                <a href='<%# Page.ResolveUrl("~/news/") + Eval("newsCategory").ToString() + "/" + SteelFunc.GenerateUrl(Eval("newsHeadline").ToString()) %>'>
                                                    <h6 class="mt-0 mb-1"><%# Eval("newsHeadline") %></h6>
                                                    <p><%# Eval("newsLeadPara") %></p>
                                                    <small class="text-muted font-italic">Posted Under <span class="badge badge-pill badge-danger"><%# Eval("newsCategory").ToString() %></span> <%# Eval("datediff") %></small>
                                                </a>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </ul>
                </div>
                <div class="col-12 col-md-3">
                    <asp:Panel ID="pnlNewsSideAds" runat="server">
                        <h5 class="font-weight-bold mb-2"><i class="fas fa-bullhorn"></i>&nbsp;Sponsored</h5>
                        <asp:Repeater ID="rptNewsAds" runat="server">
                            <ItemTemplate>
                                <a href='<%# Eval("advLinkURL") %>' target="_blank" class="sfsidead">
                                    <img class="img-fluid" src="<%# Page.ResolveUrl("~/cms") + Eval("advImage").ToString().Replace("~/","/") + "?w=" + Eval("advDimensions").ToString().Split('x')[0] + "&h=" + Eval("advDimensions").ToString().Split('x')[1] + "&mode=crop" %>" />
                                </a>
                            </ItemTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                    <asp:Panel ID="pnlTopNews" runat="server">
                        <h5 class="font-weight-bold my-3"><i class="far fa-newspaper"></i>&nbsp;Latest News</h5>
                        <ul class="list-unstyled newssidebar">
                            <asp:Repeater ID="rptSidebarNews" runat="server">
                                <ItemTemplate>
                                    <li class="media my-1 border border-left-0 border-top-0 border-right-0 py-3">
                                        <img class="align-self-center mr-3" src="<%# Page.ResolveUrl("~/cms") + Eval("newsImage").ToString().Replace("~/","/") + "?w=80&h=80&scale=both&bgcolor=white" %>" alt='<%# Eval("newsHeadline") %>'>
                                        <div class="media-body">
                                            <a href='<%# Page.ResolveUrl("~/news/") + Eval("newsCategory").ToString() + "/" + SteelFunc.GenerateUrl(Eval("newsHeadline").ToString()) %>'>
                                                <h6 class="mt-0 mb-1"><%# Eval("newsHeadline") %></h6>
                                                <small class="text-muted font-italic"><%# Eval("datediff") %></small>
                                            </a>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix mb-5"></div>

    <section id="blogposts" runat="server" class="blogposts">
        <div class="row no-gutters">
            <div class="col-12 text-center">
                <h3 class="sectionheading">From the Blog</h3>
                <p class="sectionsubheading">Where we put our ideas</p>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <asp:Repeater ID="rptBlogposts" runat="server">
                    <ItemTemplate>
                        <div class="col-6 mt-4">
                            <a href='<%# Eval("posturl") %>' target="_blank" class="visitblog">
                                <div class="blogpost">
                                    <div class="imgthumb">
                                        <asp:Image ID="imgfeatured" CssClass="img-fluid p-2" ImageUrl='<%# Eval("thumbnail").ToString().Replace("s72-c","s200-c") %>' runat="server" />
                                    </div>
                                    <div class="postdetails px-3">
                                        <small><%# Eval("publishdate") %></small>
                                        <h5><%# Eval("title") %></h5>
                                        <p><%# Eval("description") %></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="clearfix"></div>
            <div class="d-block text-center mt-5">
                <a href="http://blog.steelgroup.co.in" class="btn btn-dark">Visit Steel Group Blog</a>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

