﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class Newscat : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;
    SqlDataAdapter da;
    DataSet ds;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    void Bindnews()
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT newsID,newsCategory, newsHeadline, newsImage,newsLeadPara,newsOnTop,newsFeatured, dbo.fngettimeinagoformat(datecreated,GETDATE()) as datediff,datecreated,newsdate FROM newsmaster WHERE publish='Yes'; ");

            steelcmd.Connection = steelcon;
            steelcon.Open();


            da = new SqlDataAdapter();
            ds = new DataSet();

            da.SelectCommand = steelcmd;
            da.Fill(ds);

            ds.Tables[0].DefaultView.RowFilter = "newsCategory<>'Ticker' AND newsFeatured<>'Yes'";
            ds.Tables[0].DefaultView.Sort = "newsdate DESC";

            //Second Fold News
            gvNews.DataSource = ds.Tables[0].DefaultView;
            gvNews.DataBind();

            da.Dispose();
            ds.Clear();
            ds.Dispose();
            steelcon.Close();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        { }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT newsID,newsCategory, newsHeadline, newsImage,newsLeadPara,newsOnTop,newsFeatured, dbo.fngettimeinagoformat(datecreated,GETDATE()) as datediff,datecreated,newsdate FROM newsmaster WHERE publish='Yes'; SELECT advDimensions,advImage,advLinkURL,advType FROM adsmaster WHERE advType LIKE '%NEWS:%' AND advEndDate>=GETDATE() AND publish='Yes' ORDER BY datecreated desc;");
            steelcmd.Connection = steelcon;
            steelcon.Open();


            da = new SqlDataAdapter();
            ds = new DataSet();

            da.SelectCommand = steelcmd;
            da.Fill(ds);

            ds.Tables[0].DefaultView.RowFilter = "newsCategory='" + Page.RouteData.Values["newscat"].ToString() + "'";
            ds.Tables[0].DefaultView.Sort = "newsdate DESC";

            //Second Fold News
            gvNews.DataSource = ds.Tables[0].DefaultView;
            gvNews.DataBind();

        
            ds.Tables[0].DefaultView.RowFilter = "";
            ds.Tables[0].DefaultView.RowFilter = "newsCategory<>'Ticker' AND newsCategory<>'" + Page.RouteData.Values["newscat"].ToString() + "'";
            ds.Tables[0].DefaultView.Sort = "datecreated DESC";

            //Sidebar News 
            rptSidebarNews.DataSource = SteelFunc.GetTopDataViewRows(ds.Tables[0].DefaultView, 5);
            rptSidebarNews.DataBind();

            if (ds.Tables[1].Rows.Count > 0)
            {
                rptNewsAds.DataSource = ds.Tables[1];
                rptNewsAds.DataBind();
            }
            else
            {
                pnlNewsSideAds.Visible = false;
            }

            da.Dispose();
            ds.Clear();
            ds.Dispose();
            steelcon.Close();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void gvNews_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvNews.PageIndex = e.NewPageIndex;
        Bindnews();
    }
}