﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;

public partial class App_Admin_ChangePassword : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT * FROM adminMaster WHERE adminID=@adminID");
            steelcmd.Parameters.AddWithValue("@adminID", Session["adminID"].ToString());

            steelcmd.Connection = steelcon;
            steelcon.Open();
            rdrd = steelcmd.ExecuteReader();

            if (rdrd.Read())
            {
                ltrlAdminName.Text = rdrd["adminName"].ToString();
            }

            steelcon.Close();
            steelcon.Dispose();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        { }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("UPDATE adminMaster SET adminPasswd=@NewadminPasswd WHERE adminID=@adminID AND adminPasswd=@adminPasswd");
            steelcmd.Parameters.AddWithValue("@adminID", Session["adminID"].ToString());
            steelcmd.Parameters.AddWithValue("@adminPasswd", FormsAuthentication.HashPasswordForStoringInConfigFile(txtOld.Text, "sha1"));
            steelcmd.Parameters.AddWithValue("@NewadminPasswd", FormsAuthentication.HashPasswordForStoringInConfigFile(txtNew.Text, "sha1"));

            steelcmd.Connection = steelcon;
            steelcon.Open();
            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Your password has been updated Successfully.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();

            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Your password could not be updated. Please try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }
            steelcon.Close();
            steelcon.Dispose();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        { }
    }
}