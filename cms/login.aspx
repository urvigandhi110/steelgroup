﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Steel Group Admin Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <link rel="stylesheet" href="css/style.blue.css">
    <link rel="stylesheet" href="css/steeladmin.css">
</head>
<body>
    <form id="frmLogin" runat="server">
        <div class="page login-page">
            <div class="container d-flex align-items-center">
                <div class="form-holder has-shadow">
                    <div class="row">
                        <!-- Logo & Information Panel-->
                        <div class="col-lg-6">
                            <div class="info d-flex align-items-center">
                                <div class="content">
                                    <div class="logo">
                                        <h1>SteelGroup CMS</h1>
                                    </div>
                                    <p>Steel Events Management Portal.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Form Panel    -->
                        <div class="col-lg-6 bg-white">
                            <div class="form d-flex align-items-center">
                                <div class="content">

                                    <div class="form-group">
                                        <asp:TextBox ID="txtUsername" runat="server" required CssClass="input-material"></asp:TextBox>
                                        <label for="txtUsername" class="label-material">User Name</label>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" required CssClass="input-material"></asp:TextBox>
                                        <label for="txtPassword" class="label-material">Password</label>
                                    </div>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Login to CMS" CssClass="btn btn-primary" OnClick="btnSubmit_Click" />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
    <!-- Javascript files-->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Main File-->
    <script src="js/front.js"></script>
</body>
</html>
