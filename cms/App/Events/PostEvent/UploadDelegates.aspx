﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="UploadDelegates.aspx.cs" Inherits="App_Events_PostEvent_UploadDelegates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Add Event Delegates Login Access</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header">
                <h3 class="h6">Please Filled Template Select Excel Sheet- <a href="delegates-template.xlsx" class="btn btn-sm btn-info float-right" download>Download Template here</a></h3>
            </div>
            <div class="card-body">
                <asp:Panel ID="pnlUpload" runat="server">
                    <div class="form-group row">
                        <label class="col-sm-12 form-control-label">Select Delegates Excel (Excel files of size 1 MB or less accepted.)</label>
                        <div class="col-sm-10">
                            <asp:FileUpload ID="fileExcel" runat="server" AllowMultiple="false" />
                        </div>
                        <div class="col-sm-2">
                            <asp:RegularExpressionValidator ID="rexpfileDocument" runat="server" ControlToValidate="fileExcel" ValidationGroup="images"
                                ErrorMessage="Only .xls, .xlsx are allowed" Text="*" CssClass="errorvalidator"
                                ValidationExpression="(.*\.([Xx][Ll][Ss])|.*\.([Xx][Ll][Ss][Xx])$)"></asp:RegularExpressionValidator>
                            <asp:ValidationSummary ID="valsumm" ValidationGroup="images" ShowMessageBox="true" ShowSummary="false" runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlDetails" Visible="false" runat="server">
                    <div class="form-group">
                        <label class="form-control-label">Total Records Uploaded</label>
                        <asp:Label ID="lblNumRecords" runat="server" Text="" CssClass="font-weight-bold d-block form-control-static"></asp:Label>
                    </div>
                </asp:Panel>
                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Upload Excel Sheet" CssClass="btn btn-primary" />
                    <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-success" runat="server" Text="Save Delegates" Visible="false" />
                    <a href="<%= Page.ResolveUrl("~") %>App/Events/PostEvent/Default.aspx" class="btn btn-secondary">Go Back to Events</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

