﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="NewsDetails.aspx.cs" Inherits="NewsDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script defer src="https://use.fontawesome.com/releases/v5.0.2/js/v4-shims.js"></script>
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="container newsdetails">
        <div class="row no-gutters">
            <div class="col-12 col-md-9  pr-md-5">
                <div class="row no-gutters">
                    <div class="col-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb rounded-0 bg-white p-0 pt-5 small">
                                <li class="breadcrumb-item"><a href="<%= Page.ResolveUrl("~/")  %>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<%= Page.ResolveUrl("~/news/")  %>">News</a></li>
                                <li class="breadcrumb-item"><a href="<%= Page.ResolveUrl("~/news/") + Page.RouteData.Values["newscat"].ToString()  %>"><%= Page.RouteData.Values["newscat"].ToString() %></a></li>
                                <li class="breadcrumb-item active" aria-current="page">
                                    <asp:Literal ID="ltrlBreadcrumbNewsHeading" runat="server"></asp:Literal></li>
                            </ol>
                        </nav>
                        <hr />
                        <h1 class="pageheading pb-2 text-left">
                            <asp:Literal ID="ltrlNewsHeading" runat="server"></asp:Literal></h1>
                        <h6 class="d-block text-muted font-italic mb-4 small">Posted on:
                    <asp:Literal ID="ltrlNewsDate" runat="server"></asp:Literal></h6>
                        <p class="mx-auto text-justify mb-4 lead">
                            <asp:Literal ID="ltrlLeadPara" runat="server"></asp:Literal>
                        </p>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-12 text-center my-4">
                        <asp:Image ID="imgNews" CssClass="img-fluid" runat="server" />
                        <br />
                        <small class="font-italic text-muted">
                            <asp:Literal ID="ltrlImageSource" runat="server"></asp:Literal></small>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-12 text-justify">
                        <asp:Literal ID="ltrlNewsArticle" runat="server"></asp:Literal>
                        <div class="clearfix"></div>
                        <div id="sharearticle"></div>
                        <asp:HyperLink ID="lnkNewsArticle" Target="_blank" CssClass="mt-4 small" runat="server">Link to Original Article</asp:HyperLink>
                    </div>

                </div>
            </div>
            <div class="col-12 col-md-3 pt-4">
                <asp:Panel ID="pnlNewsSideAds" runat="server">
                    <h5 class="font-weight-bold pt-4 mt-2 mb-3"><i class="fas fa-bullhorn"></i>&nbsp;Sponsored</h5>
                    <asp:Repeater ID="rptNewsAds" runat="server">
                        <ItemTemplate>
                            <a href='<%# Eval("advLinkURL") %>' target="_blank" class="sfsidead">
                                <img class="img-fluid" src="<%# Page.ResolveUrl("~/cms") + Eval("advImage").ToString().Replace("~/","/") + "?w=" + Eval("advDimensions").ToString().Split('x')[0] + "&h=" + Eval("advDimensions").ToString().Split('x')[1] + "&mode=crop" %>" />
                            </a>
                        </ItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <asp:Panel ID="pnlTopNews" runat="server">
                    <h5 class="font-weight-bold pt-4 mt-2 mb-3"><i class="far fa-newspaper"></i>&nbsp;Latest News</h5>
                    <ul class="list-unstyled newssidebar">
                        <asp:Repeater ID="rptSidebarNews" runat="server">
                            <ItemTemplate>
                                <li class="media my-1 border border-left-0 border-top-0 border-right-0 py-3">
                                    <img class="align-self-center mr-3" src="<%# Page.ResolveUrl("~/cms") + Eval("newsImage").ToString().Replace("~/","/") + "?w=80&h=80&scale=both&bgcolor=white" %>" alt='<%# Eval("newsHeadline") %>'>
                                    <div class="media-body">
                                        <a href='<%# Page.ResolveUrl("~/news/") + Eval("newsCategory").ToString() + "/" + SteelFunc.GenerateUrl(Eval("newsHeadline").ToString()) %>'>
                                            <h6 class="mt-0 mb-1"><%# Eval("newsHeadline") %></h6>
                                            <small class="text-muted font-italic"><%# Eval("datediff") %></small>
                                        </a>
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </asp:Panel>
                <div class="text-center">
                    <a href='<%= Page.ResolveUrl("~/news/") %>' class="btn btn-dark btn-sm">View All News</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
    <script>
        $("#sharearticle").jsSocials({
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "whatsapp"]
        });
    </script>
</asp:Content>

