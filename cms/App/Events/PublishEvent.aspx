﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="PublishEvent.aspx.cs" Inherits="App_Events_PublishEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/themes/dark.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Publish New Event</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Please Enter the following details about Event-</h3>
            </div>
            <div class="card-body">
                <h6>Event Basic Details</h6>
                <hr />
                <div class="form-group">
                    <label class="form-control-label">Event Name <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtEventName" CssClass="form-control" placeholder="Enter Event name here" required runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Event Description <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtEventDesc" placeholder="Please enter Description here" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Previous Event Description <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtEventDescOld" placeholder="Please enter Previous Description here" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>

                <div class="form-group">
                    <label class="form-control-label">Event Video <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtVideo" placeholder="Please enter youtube URL here" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Start Date <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtStartDate" placeholder="Please enter Start Date here" runat="server" CssClass="form-control flatpickr" required></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">End Date <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtEndDate" placeholder="Please enter End Date here" runat="server" CssClass="form-control flatpickr" required></asp:TextBox>
                    </div>
                </div>

                <h6 class="mt-5">Venue Details</h6>
                <hr />

                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Venue Name (*Small Name) <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtVenue" placeholder="eg: Hotel Westin" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Venue City <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtVenueCity" placeholder="eg: Mumbai" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Venue State <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtVenueState" placeholder="eg: Maharashtra" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Venue Country <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtVenueCountry" placeholder="eg: India" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <label class="form-control-label">Event Address <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtEventAddress" placeholder="Please enter Address of Venue here" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>

                <div class="form-group">
                    <label class="form-control-label">Event Map URL <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtMapUrl" placeholder="Please enter Google Maps URL here" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>

                <h6 class="mt-5">Stats Details</h6>
                <hr />

                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Companies Count <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtCompanies" placeholder="eg: 250+" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Delegates Count <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtDelegates" placeholder="eg: 15" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Panelists Count <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtPanelists" placeholder="eg: 8" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Exhibitors Count <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtExhibitors" placeholder="eg: 100+" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                </div>


                <h6 class="mt-5">Downloads Details</h6>
                <hr />

                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Registration Form</label>
                        <asp:FileUpload ID="fileRegForm" CssClass="form-control" runat="server" />
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Sponsorship Form</label>
                        <asp:FileUpload ID="fileSponsorshipForm" CssClass="form-control" runat="server" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Brochure</label>
                        <asp:FileUpload ID="fileBrochure" CssClass="form-control" runat="server" />
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Agenda</label>
                        <asp:FileUpload ID="fileAgenda" CssClass="form-control" runat="server" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Layout</label>
                        <asp:FileUpload ID="fileLayout" CssClass="form-control" runat="server" />
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Participating Companies</label>
                        <asp:FileUpload ID="fileParticipating" CssClass="form-control" runat="server" />
                    </div>
                </div>
                <h6 class="mt-5">Images for Event</h6>
                <hr />
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Event Image 1 (Main Photo) <small class="text-danger">*Required</small></label>
                        <asp:FileUpload ID="fileImage1" CssClass="form-control" runat="server" required />
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Event Image 2 </label>
                        <asp:FileUpload ID="fileImage2" CssClass="form-control" runat="server" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Event Image 3 </label>
                        <asp:FileUpload ID="fileImage3" CssClass="form-control" runat="server" />
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Event Image 4 </label>
                        <asp:FileUpload ID="fileImage4" CssClass="form-control" runat="server" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-6">
                        <div class="i-checks">
                            <asp:CheckBox ID="chkPublish" runat="server" />
                            <label for="chkPublish">Publish Event <small>(Right Away)</small></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Publish Event" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.js"></script>
    <script>
        $(".flatpickr").flatpickr({
            dateFormat: "Y-m-d", allowInput: true
        });
    </script>
</asp:Content>

