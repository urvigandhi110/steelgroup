﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Jobs : System.Web.UI.Page
{

    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;
    SqlDataAdapter da;
    DataSet ds;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT row_number() over(order by jobID) as sno,jobID,jobHeading,jobLocation,jobCompany,jobLastDate,jobFeatured,jobImage,jobExperience,datecreated,publish FROM jobsmaster");

            steelcmd.Connection = steelcon;
            steelcon.Open();


            da = new SqlDataAdapter();
            ds = new DataSet();

            da.SelectCommand = steelcmd;
            da.Fill(ds);

            ds.Tables[0].DefaultView.RowFilter = "jobFeatured='Yes'";

            rptFeaturedJobs.DataSource = SteelFunc.GetTopDataViewRows(ds.Tables[0].DefaultView, 3);
            rptFeaturedJobs.DataBind();

            steelcon.Close();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}