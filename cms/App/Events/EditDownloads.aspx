﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="EditDownloads.aspx.cs" Inherits="App_Events_EditDownloads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Edit Event Downloads</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Modify the download files about Event- <strong><asp:Literal ID="ltrlEventName" runat="server"></asp:Literal></strong></h3>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Registration Form</label>
                        <asp:FileUpload ID="fileRegForm" CssClass="form-control" runat="server" />
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Sponsorship Form</label>
                        <asp:FileUpload ID="fileSponsorshipForm" CssClass="form-control" runat="server" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Brochure</label>
                        <asp:FileUpload ID="fileBrochure" CssClass="form-control" runat="server" />
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Agenda</label>
                        <asp:FileUpload ID="fileAgenda" CssClass="form-control" runat="server" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Layout</label>
                        <asp:FileUpload ID="fileLayout" CssClass="form-control" runat="server" />
                    </div>
                    <div class="col-md-6 col-12">
                         <label class="form-control-label">Participating Companies</label>
                        <asp:FileUpload ID="fileParticipating" CssClass="form-control" runat="server" />
                    </div>
                </div>

                <h6 class="mt-5">Images for Event</h6>
                <hr />
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Event Image 1 (Main Photo)</label>
                        <asp:FileUpload ID="fileImage1" CssClass="form-control" runat="server" />
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Event Image 2 </label>
                        <asp:FileUpload ID="fileImage2" CssClass="form-control" runat="server" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Event Image 3 </label>
                        <asp:FileUpload ID="fileImage3" CssClass="form-control" runat="server" />
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Event Image 4 </label>
                        <asp:FileUpload ID="fileImage4" CssClass="form-control" runat="server" />
                    </div>
                </div>
                   <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Update Files" CssClass="btn btn-primary" />
                       <a href="Default.aspx" class="btn btn-warning">Back</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

