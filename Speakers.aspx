﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventMaster.master" AutoEventWireup="true" CodeFile="Speakers.aspx.cs" Inherits="Speakers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css" />
    <style>
        .swiperslidersponsor
        {
            height: 13rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <section class="speakersdetails">
        <div class="row no-gutters py-4">
            <div class="col-12 text-center">
                <h3 class="sectionheading">Conference Speakers</h3>
                <p class="sectionsubheading">Know who You're talking to</p>
            </div>
        </div>
        <div class="container pb-5">
            <div class="row no-gutters">
                <div class="col-12 col-md-9">
                    <ul class="list-unstyled speakerslist">
                        <asp:Repeater ID="rptSpeakers" runat="server">
                            <ItemTemplate>
                                <li class="media">
                                    <a name='<%# SteelFunc.GenerateUrl(Eval("eventSpeakerName").ToString()) %>'></a>
                                    <img class="mr-4" src='<%# Page.ResolveUrl("~/cms") + Eval("eventSpeakerPhoto").ToString().Replace("~/","/") + "?w=200&h=250&mode=crop" %>' alt="">
                                    <div class="media-body">
                                        <h5 class="mt-0 mb-1"><%# Eval("eventSpeakerName").ToString() %><span class="badge badge-pill badge-dark font-weight-light small float-right"><%# Eval("eventSpeakerType").ToString() %></span></h5>
                                        <p class="font-italic text-muted mb-2">
                                            <%# Eval("eventSpeakerOrg").ToString() %> | <%# Eval("eventSpeakerDesignation").ToString() %> | <%# Eval("eventSpeakerCountry").ToString() %>
                                        </p>
                                        <div class="clearfix"></div>
                                        <p class="desc"><%# Eval("eventSpeakerIntro").ToString() %></p>
                                        <%--<p>
                                            <span class="font-weight-bold">Company:</span><br />
                                            <%# Eval("eventSpeakerOrg").ToString() %>
                                        </p>
                                        <p class="mb-0">
                                            <span class="font-weight-bold">Designation:</span><br />
                                            <%# Eval("eventSpeakerDesignation").ToString() %>
                                        </p>--%>
                                        <div class="clearfix"></div>
                                        <a class="speakerlinks" href="<%# Eval("eventSpeakerLinkedin").ToString()!=""?Eval("eventSpeakerLinkedin").ToString():"#" %>" target="_blank">
                                            <div class="text-center">
                                                <span class="fab fa-linkedin"></span>
                                            </div>
                                            LinkedIn
                                        </a>
                                        <a class="speakerlinks" href="<%# Eval("eventSpeakerTwitter").ToString()!=""?Eval("eventSpeakerTwitter").ToString():"#" %>" target="_blank">
                                            <div class="text-center">
                                                <span class="fab fa-twitter"></span>
                                            </div>
                                            Twitter
                                        </a>
                                        <a class="speakerlinks" href="<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/speakers/" + SteelFunc.GenerateUrl(Eval("eventSpeakerName").ToString()) %>">
                                            <div class="text-center">
                                                <span class="far fa-user"></span>
                                            </div>
                                            Details
                                        </a>

                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="col-12 col-md-3 px-3">
                       <h6 class="sectionsubheading font-weight-bold text-center">Our Sponsors</h6>
                    <div class="swiper-container swiperslidersponsor swiper-container-sponsors">
                        <div class="swiper-wrapper">
                            <asp:Repeater ID="rptSponsorAds" runat="server">
                                <ItemTemplate>
                                    <div class="swiper-slide text-center">
                                        <a target="_blank" class="text-dark" href='<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(Eval("eventSponsorName").ToString()) %>'>
                                            <small class="text-dark font-weight-bold mb-2"><%# Eval("eventSponsorType") %> Sponsor</small><br />
                                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="swiper-pagination swiper-pagination-sponsors"></div>
                    </div>
                     <h6 class="sectionsubheading font-weight-bold text-center my-3">Useful Downloads</h6>
                    <asp:HyperLink ID="lnkBrochureSidebar" runat="server" class="text-center text-dark">
                        <asp:Image ID="imgBrochure" runat="server" CssClass="img-fluid mb-3" /><br />
                        <p class="font-weight-bold">Download Brochure</p>
                    </asp:HyperLink>

                    <asp:HyperLink ID="lnkAgendaSidebar" runat="server" class="text-center text-dark">
                        <asp:Image ID="imgAgenda" runat="server" CssClass="img-fluid mb-3" /><br />
                        <p class="font-weight-bold">Download Agenda</p>
                    </asp:HyperLink>

                </div>
            </div>
            <section class="w-100 pt-2 text-center d-block">
                <a class="btn btn-default eventbtn" href='<%= Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/become-a-speaker" %>'>Become A Speaker</a>
            </section>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
    <script>
        $(document).ready(function () {
            //initialize swiper when document ready
            var mySwiper = new Swiper('.swiper-container-sponsors', {
                slidesPerView: 1,
                autoplay: {
                    delay: 5000,
                },
                pagination: {
                    el: '.swiper-pagination-sponsors',
                    clickable: true,
                }
            })
        });
    </script>
</asp:Content>

