﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;

public partial class SponsorDetail : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;
    SqlDataAdapter da;
    DataSet ds;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            HtmlGenericControl contactsection = (HtmlGenericControl)Master.FindControl("contact");
            HtmlGenericControl venuesection = (HtmlGenericControl)Master.FindControl("venue");

            contactsection.Visible = false;
            venuesection.Visible = false;

            string eventname = Page.RouteData.Values["eventname"].ToString();
            //string sponsorname = Page.RouteData.Values["sponsorname"].ToString();

            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            steelcmd.CommandText = "SELECT eventID, eventName,eventImage1,eventStartDate,eventVenue,eventVenueAddress,eventVenueMapURL,eventSponsorshipForm FROM eventsmaster WHERE publish='Yes' AND Lower(replace(replace(eventName,' ','-'),'-&',''))=@eventname; SELECT spo.eventSponsorName,spo.eventSponsorLogo,spo.eventSponsorDesc,spo.eventSponsorURL,spo.eventSponsorType FROM eventSponsors spo JOIN eventsmaster e ON e.eventID=spo.eventID WHERE Lower(replace(replace(e.eventName,' ','-'),'-&',''))=@eventname AND eventSponsorType NOT LIKE '%partner%' AND eventSponsorType NOT LIKE '%association%' ORDER BY case when eventSponsorType like 'platinum' then 0 else 1 end,case when eventSponsorType like 'gold' then 0 else 1 end,case when eventSponsorType like 'silver' then 0 else 1 end,case when eventSponsorType like 'associate' then 0 else 1 end,case when eventSponsorType like 'ing lunch' then 0 else 1 end,case when eventSponsorType like 'ing dinner' then 0 else 1 end,case when eventSponsorType like 'delegate kit' then 0 else 1 end,case when eventSponsorType like 'exhibitor' then 0 else 1 end,case when eventSponsorType like 'company' then 0 else 1 end";// AND Lower(replace(replace(replace(replace(RTRIM(LTRIM(spo.eventSponsorName)),' ','-'),'&',''),'.','-'),',','-'))=@sponsorname
            steelcmd.Parameters.AddWithValue("@eventname", eventname);

            da = new SqlDataAdapter();
            ds = new DataSet();

            da.SelectCommand = steelcmd;
            da.Fill(ds);

            Literal ltrlEventVenue = (Literal)Master.FindControl("ltrlEventVenue");
            Literal ltrlEventDate = (Literal)Master.FindControl("ltrlEventDate");
            Literal ltrlEventName = (Literal)Master.FindControl("ltrlEventName");
            Literal ltrlVenueAddress = (Literal)Master.FindControl("ltrlVenueAddress");
            Literal ltrlVenueMap = (Literal)Master.FindControl("ltrlVenueMap");
            HiddenField hf = (HiddenField)Master.FindControl("hfimageurl");

            hf.Value = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventImage1"].ToString().ToString().Replace("~/", "/") + "?w=1920&h=1200&mode=crop";
            ltrlEventVenue.Text = ds.Tables[0].Rows[0]["eventVenue"].ToString();
            ltrlEventDate.Text = DateTime.Parse(ds.Tables[0].Rows[0]["eventStartDate"].ToString()).ToString("MMMM dd, yyyy");
            ltrlEventName.Text = ds.Tables[0].Rows[0]["eventName"].ToString();
            ltrlVenueAddress.Text = ds.Tables[0].Rows[0]["eventVenueAddress"].ToString();
            ltrlVenueMap.Text = "<iframe src=\"" + ds.Tables[0].Rows[0]["eventVenueMapURL"].ToString() + "\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border: 0\" allowfullscreen></iframe>";

            lnkDownloadSponsorship.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventSponsorshipForm"].ToString().Replace("~/", "/");
            lnkDownloadSponsorship.Attributes.Add("download", "Sponsorship-Form-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());


            rptSponsorDetails.DataSource = ds.Tables[1];
            rptSponsorDetails.DataBind();

            steelcon.Close();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        {

        }
    }
}