﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class EventMaster : System.Web.UI.MasterPage
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            string eventName = Page.RouteData.Values["eventname"].ToString();
            if(Request.Url.AbsolutePath.Contains("past-events"))
            {
                ltrlvenuesubheading.Text = "Where it Happened";
                lnkolpay.Visible = false;
                lnkreg.Visible = false;
                lnkagenda.Visible = false;
                lnkdown.Visible = false;
                lnkDownloads.Visible = true;
                lnkDownloads.HRef = Page.ResolveUrl("~") + "past-events/" + Page.RouteData.Values["eventname"].ToString() + "#downloads";
                homelink.HRef = Page.ResolveUrl("~") + "past-events/" + Page.RouteData.Values["eventname"].ToString();

                eventMasterMenu.Attributes.Add("style", "min-height:28rem");

            }
            else
            {
                ltrlvenuesubheading.Text = "Where to come";
                lnkolpay.Visible = true;
                lnkreg.Visible = true;
                lnkagenda.Visible = true;
                lnkdown.Visible = true;
                lnkreg.HRef = Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "/Register";
                lnkolpay.HRef = Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "/Online-Payment";
                homelink.HRef = Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString();
                eventMasterMenu.Attributes.Add("style", "background-image: linear-gradient( rgba(0,0,0,.4), rgba(0,0,0,.4) ), url('" + hfimageurl.Value + "');");
            }

            try
            {
                steelcon = new SqlConnection(connstr);
                steelcmd = new SqlCommand("SELECT eventID,eventName,eventImage1,eventStartDate,eventCity,eventCountry,eventVenue FROM eventsmaster WHERE publish='Yes' AND eventStartDate>=GETDATE()");

                steelcmd.Connection = steelcon;
                steelcon.Open();

                rdrd = steelcmd.ExecuteReader();

                rptEventFooter.DataSource = rdrd;
                rptEventFooter.DataBind();

                rdrd.Close();
                steelcon.Close();
                steelcmd.Dispose();
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }
    }

}
