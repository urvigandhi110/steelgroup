﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="AddEventSponsor.aspx.cs" Inherits="App_Events_AddEventSponsor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Add Event Sponsor</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Please Enter the following sponsor details-</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="form-control-label">Sponsor Type <small class="text-danger">*Required</small></label>
                    <asp:DropDownList ID="ddlSponsorType" CssClass="form-control" required runat="server">
                        <asp:ListItem Value="">Select an Sponsor Type</asp:ListItem>
                        <asp:ListItem>Principal</asp:ListItem>
                        <asp:ListItem>Platinum</asp:ListItem>
                        <asp:ListItem>Gold</asp:ListItem>
                        <asp:ListItem>Silver</asp:ListItem>
                        <asp:ListItem>Associate</asp:ListItem>
                        <asp:ListItem>Networking Lunch</asp:ListItem>
                        <asp:ListItem>Networking Dinner</asp:ListItem>
                        <asp:ListItem>Delegate Kit Sponsor</asp:ListItem>
                        <asp:ListItem>Exhibitor</asp:ListItem>
                        <asp:ListItem>Company Highlight</asp:ListItem>
                        <asp:ListItem>Knowledge Partner</asp:ListItem>
                        <asp:ListItem>Lead Marketing Partner</asp:ListItem>
                        <asp:ListItem>Marketing Partner</asp:ListItem>
                        <asp:ListItem>Media Partner</asp:ListItem>
                        <asp:ListItem>IT Partner</asp:ListItem>
                        <asp:ListItem>Online Technology Partner</asp:ListItem>
                        <asp:ListItem>Travel Partner</asp:ListItem>
                        <asp:ListItem>Design Partner</asp:ListItem>
                        <asp:ListItem>In Association With</asp:ListItem>
                        <asp:ListItem>Country Partner</asp:ListItem>
                        <asp:ListItem>Participating Companies</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Sponsor Name <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtSponsorName" placeholder="Please enter Sponsor Name here" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Sponsor Description/Introduction <small class="text-dark">(*in case of premium sponsors)</small></label>
                    <asp:TextBox ID="txtDescription" placeholder="Enter Desc (Optional, Not Displayed for All)" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <div class="form-group">
                    <label class="form-control-label">Sponsor Logo <small class="text-danger">*Required</small></label>
                    <asp:FileUpload ID="fileLogo" CssClass="form-control" runat="server" required />
                </div>
                <div class="form-group">
                    <label class="form-control-label">Sponsor Link / Website</label>
                    <asp:TextBox ID="txtSponsorLink" placeholder="Please enter URL here" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Add Sponsor" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

