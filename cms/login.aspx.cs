﻿using System;
using System.Data.SqlClient;
using System.Web.Security;

public partial class login : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT * FROM adminMaster WHERE adminusername=@adminusername");
            steelcmd.Parameters.AddWithValue("@adminusername", txtUsername.Text);
           // steelcmd.Parameters.AddWithValue("@adminPasswd", FormsAuthentication.HashPasswordForStoringInConfigFile(txtPassword.Text, "sha1"));
            steelcmd.Connection = steelcon;
            steelcon.Open();
            rdrd = steelcmd.ExecuteReader();

            if (rdrd.Read())
            {
                Session["adminID"] = rdrd["adminID"].ToString();
                FormsAuthentication.RedirectFromLoginPage(txtUsername.Text, false);
            }

            steelcon.Close();
            steelcon.Dispose();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        { }
        
    }
}