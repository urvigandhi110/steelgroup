﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_PostEvent_UploadPhotos : System.Web.UI.Page
{

    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (fileImage.PostedFiles.Count <= 20)
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;
            steelcmd.CommandText = "";

            string eventID = Request.QueryString["eventID"].ToString();
            Random rnd = new Random();
            int imgcount = 0;
            string[] filenames = new string[fileImage.PostedFiles.Count];

            foreach (HttpPostedFile postedFile in fileImage.PostedFiles)
            {
                string fileName = "~/uploads/event/Gallery/" + Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(postedFile.FileName);

                Regex reg = new Regex(@"^.*\.(jpg|JPG|jpeg|PNG|png)$");
                if (reg.IsMatch(fileName))
                {
                    string newphotoid = "PHT" + DateTime.Today.ToString("ddMMyyyy") + rnd.Next(10000, 99999);
                    postedFile.SaveAs(Server.MapPath(fileName));
                    steelcmd.CommandText += "INSERT INTO galleryphoto(photoID,photoFile,eventID,datecreated,createdby,createdIP,publish) VALUES('" + newphotoid + "','" + fileName + "','" + eventID + "','" + DateTime.Now + "','" + HttpContext.Current.User.Identity.Name + "','" + Request.UserHostAddress + "','Yes'); ";

                    filenames[imgcount] = fileName;
                    imgcount++;
                }
            }

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                Session["imgcount"] = imgcount;
                rptImages.DataSource = filenames;
                rptImages.DataBind();

                pnlUpload.Visible = false;
                pnlDetails.Visible = true;

                btnSave.Visible = true;
                btnSubmit.Visible = false;

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Photos could not be uploaded. Please check file details, size, type and try again.";

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        else
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-warning alert-dismissible";
            ltrlmsg.Text = "Photos could not be saved. Please select maximum 20 images.";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string qry = "";
        string eventID = Request.QueryString["eventID"].ToString();

        foreach (RepeaterItem itm in rptImages.Items)
        {
            Image img = (Image)itm.FindControl("imgUploaded");
            TextBox tbTitle = (TextBox)itm.FindControl("txtPhotoTitle");
            TextBox tbDesc = (TextBox)itm.FindControl("txtPhotoDesc");

            qry += "UPDATE galleryphoto SET photoTitle='" + tbTitle.Text + "', photoDesc='" + tbDesc.Text + "' WHERE photoFile='" + img.AlternateText + "' AND eventID='" + eventID + "'; ";
        }

        steelcmd = new SqlCommand();
        steelcon = new SqlConnection(connstr);
        steelcon.Open();
        steelcmd.Connection = steelcon;
        steelcmd.CommandText = qry;

        int res = steelcmd.ExecuteNonQuery();

        if (res > 0)
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-success alert-dismissible";
            ltrlmsg.Text = "Photos with details saved successfully.";

            steelcon.Close();
            steelcmd.Dispose();
            steelcon.Dispose();


            pnlUpload.Visible = false;
            pnlDetails.Visible = false;

            btnSave.Visible = false;
            btnSubmit.Visible = false;
            btnAddPhotos.Visible = true;
        }
        else
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-warning alert-dismissible";
            ltrlmsg.Text = "Photos could not be uploaded. Please check file details, size, type and try again.";

            steelcon.Close();
            steelcmd.Dispose();
            steelcon.Dispose();
        }
    }

    protected void btnAddPhotos_Click(object sender, EventArgs e)
    {
        string eventID = Request.QueryString["eventID"].ToString();
        Response.Redirect("UploadPhotos.aspx?eventID=" + eventID, false);
    }
}