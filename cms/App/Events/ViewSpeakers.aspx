﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="ViewSpeakers.aspx.cs" Inherits="App_Events_ViewSpeakers" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
     <style type="text/css">
        table.dataTable {
            border-collapse: collapse !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" Runat="Server">
    <h2 class="no-margin-bottom">Manage Speakers</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" Runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Following list shows all Speakers in event-</h3>
            </div>
            <div class="card-body">
                <table id="list" class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Speaker Name</th>
                            <th>Speaker Organization</th>
                            <th>Speaker Photo</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><%# Eval("Sno") %></th>
                                    <td><%# Eval("eventSpeakerName") %></td>
                                    <td><%# Eval("eventSpeakerOrg") %></td>
                                     <td>
                                        <asp:Image ID="imgSpeaker" Width="70" ImageUrl='<%# Eval("eventSpeakerPhoto") %>' runat="server" />
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Manage
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <asp:LinkButton ID="lnkEdit" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventSpeakerID") %>' CommandName="editSpeaker">Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkDelete" CssClass="dropdown-item" runat="server" OnClientClick='<%# "return getConfirmation(this,\"" + Eval("eventSpeakerName").ToString() + "\");" %>' CommandArgument='<%# Eval("eventSpeakerID") %>' CommandName="deleterecord">Delete</asp:LinkButton>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="ConfirmationPopup" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Please Confirm Deletion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure you want to delete <strong><span id="spnMsg"></span></strong>&nbsp;Speaker? This will delete all the details under this Speaker. This action is irreversible.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">Yes, Delete</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" Runat="Server">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#list').DataTable();
        });
        function getConfirmation(sender, CourseName) {
            $("#spnMsg").text(CourseName);
            $('#ConfirmationPopup').modal('show');
            $('#btnConfirm').attr('onclick', "$('#ConfirmationPopup').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
</asp:Content>

