﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="PostAd.aspx.cs" Inherits="App_Ads_PostAd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/themes/dark.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Post New Ad</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Please Enter the following Ad details-</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="form-control-label">Ad Type <small class="text-danger">*Required</small></label>
                    <asp:DropDownList ID="ddlAdType" CssClass="form-control" required runat="server">
                        <asp:ListItem Value="">Select an Ad Type</asp:ListItem>
                        <asp:ListItem Value="TOP;1000x90">Top: Topmost Ad (All Pages)</asp:ListItem>
                        <asp:ListItem Value="FF;426x218">FF: Home Page First Fold</asp:ListItem>
                        <asp:ListItem Value="MID;1644x148">Mid: Home Page Mid Ad</asp:ListItem>
                        <asp:ListItem Value="HS;327x272">HS: Home Page Sidebar</asp:ListItem>
                        <asp:ListItem Value="AS;327x272">AS: All Page Sidebar</asp:ListItem>
                        <asp:ListItem Value="NEWSSIDE;327x272">NEWS-SIDE: News Section Sidebar</asp:ListItem>
                        <asp:ListItem Value="NEWSTOP;1644x148">NEWS-TOP: News Page Top</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Client Name <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtClientName" placeholder="Please enter Client Name here" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Ad Name <small class="text-dark">(*in case of multiple ads of same client)</small></label>
                    <asp:TextBox ID="txtAdName" placeholder="Enter Ad Name (Optional, Not Displayed)" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <div class="form-group">
                    <label class="form-control-label">Ad Image <small class="text-danger">*Required</small></label>
                    <asp:FileUpload ID="fileImage" CssClass="form-control" runat="server" required />
                </div>
                <div class="form-group">
                    <label class="form-control-label">Ad Link to Open</label>
                    <asp:TextBox ID="txtAdLink" placeholder="Please enter URL here" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Start Date <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtStartDate" placeholder="Please enter Start Date here" runat="server" CssClass="form-control flatpickr" required></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">End Date <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtEndDate" placeholder="Please enter End Date here" runat="server" CssClass="form-control flatpickr" required></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Publish Ad" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.js"></script>
    <script>
        $(".flatpickr").flatpickr({
            minDate: "today", dateFormat: "Y-m-d", allowInput: true
        });
    </script>
</asp:Content>

