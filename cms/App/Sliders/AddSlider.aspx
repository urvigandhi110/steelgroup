﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="AddSlider.aspx.cs" Inherits="App_Sliders_AddSlider" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Add Slider</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Please Enter the following details-</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="form-control-label">Slider Title <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtSliderTitle" placeholder="Please enter a Title here (5-6 words)" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Slider Subtitle<small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtSliderSubtitle" placeholder="Please enter a Sub title here (8-9 words)" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Slider Highlight</label>
                    <asp:TextBox ID="txtSliderHighlight" placeholder="Enter a Highlighted Text (Optional, Shown with Highlighted Background, 2-3 words max)" runat="server" CssClass="form-control"></asp:TextBox>
                </div>

                <div class="form-group">
                    <label class="form-control-label">Slider Photo <small class="text-danger">*Required</small></label>
                    <asp:FileUpload ID="fileImage" CssClass="form-control" runat="server" />
                </div>

                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Save Slider" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

