﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_AddEventSponsor : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string eventID = Request.QueryString["eventID"].ToString();

            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            string filename = "~/uploads/event/sponsors/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileLogo.FileName);

            if (fileLogo.HasFile && !System.IO.File.Exists(Server.MapPath(filename)))
            {
                fileLogo.SaveAs(Server.MapPath(filename));
            }
            else
            {
                filename = "~/uploads/event/sponsors/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileLogo.FileName);
                fileLogo.SaveAs(Server.MapPath(filename));
            }

            string newSponsorID = "SPO" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(100, 999);

            steelcmd.CommandText = "INSERT INTO eventsponsors(eventSponsorID,eventSponsorName,eventSponsorLogo,eventSponsorDesc,eventSponsorURL,eventSponsorType,eventID,datecreated,createdby,createdIP,publish) VALUES(@eventSponsorID,@eventSponsorName,@eventSponsorLogo,@eventSponsorDesc,@eventSponsorURL,@eventSponsorType,@eventID,@datecreated,@createdby,@createdIP,@publish)";

            steelcmd.Parameters.AddWithValue("@eventSponsorID", newSponsorID);
            steelcmd.Parameters.AddWithValue("@eventSponsorName", txtSponsorName.Text);
            steelcmd.Parameters.AddWithValue("@eventSponsorLogo", filename);
            steelcmd.Parameters.AddWithValue("@eventSponsorDesc", txtDescription.Text);
            steelcmd.Parameters.AddWithValue("@eventSponsorURL", txtSponsorLink.Text);
            steelcmd.Parameters.AddWithValue("@eventSponsorType", ddlSponsorType.SelectedItem.Text);
            steelcmd.Parameters.AddWithValue("@eventID", eventID);
            steelcmd.Parameters.AddWithValue("@datecreated", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@createdby", HttpContext.Current.User.Identity.Name);
            steelcmd.Parameters.AddWithValue("@createdIP", Request.UserHostAddress);
            steelcmd.Parameters.AddWithValue("@publish", "Yes");

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Sponsor has been added Successfully to event.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();

            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Sponsor could not be added. Please check all details, image size, type and try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}