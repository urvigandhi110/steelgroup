﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventMaster.master" AutoEventWireup="true" CodeFile="SpeakerDetails.aspx.cs" Inherits="SpeakerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css" />
    <style>
        .swiperslidersponsor {
            height: 13rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <section class="speakersdetailspage">
        <div class="container pb-5">
            <div class="row no-gutters pt-5">
                <div class="col-12 col-md-9 whitebox">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb rounded-0 bg-white p-0 small">
                            <li class="breadcrumb-item"><a href="<%= Page.ResolveUrl("~/events/")  %>">Events</a></li>
                            <li class="breadcrumb-item"><a href="<%= Page.ResolveUrl("~/events/") + Page.RouteData.Values["eventname"].ToString()  %>">
                                <asp:Literal ID="ltrlEVentNameBC" runat="server"></asp:Literal></a></li>
                            <li class="breadcrumb-item"><a href="<%= Page.ResolveUrl("~/events/") + Page.RouteData.Values["eventname"].ToString() + "/speakers/"  %>">Speakers</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <asp:Literal ID="ltrlSpeakerNameBC" runat="server"></asp:Literal></li>
                        </ol>
                    </nav>
                    <hr />
                    <div class="row no-gutters">
                        <div class="col-12 col-md-4">
                            <asp:Image ID="imgSpeaker" CssClass="img-fluid" runat="server" />
                            <p class="mb-1 mt-3">
                                Company:<br />
                                <span class="font-weight-bold">
                                    <asp:Literal ID="ltrlOrg" runat="server"></asp:Literal></span>
                            </p>
                            <p class="mb-1">
                                Designation:<br />
                                <span class="font-weight-bold">
                                    <asp:Literal ID="ltrlDesignation" runat="server"></asp:Literal></span>
                            </p>
                            <p class="mb-1">
                                Country:<br />
                                <span class="font-weight-bold">
                                    <asp:Literal ID="ltrlCountry" runat="server"></asp:Literal></span>
                            </p>
                            <div class="clearfix"></div>
                            <asp:HyperLink ID="lnkLinkedin" CssClass="speakerlinks" runat="server" Target="_blank">
                                <div class="text-center">
                                    <span class="fab fa-linkedin"></span>
                                </div>
                                LinkedIn
                            </asp:HyperLink>
                            <asp:HyperLink ID="lnkTwitter" CssClass="speakerlinks" runat="server" Target="_blank">
                                <div class="text-center">
                                    <span class="fab fa-twitter"></span>
                                </div>
                                Twitter
                            </asp:HyperLink>
                        </div>
                        <div class="col-12 col-md-8 speakerpara">
                            <h1 class="speakername">
                                <asp:Literal ID="ltrlSpeakerName" runat="server"></asp:Literal>
                                <span class="badge badge-pill badge-dark font-weight-light small float-right">
                                    <asp:Literal ID="ltrlSpeakerType" runat="server"></asp:Literal></span>
                            </h1>
                            <asp:Literal ID="ltrlSpeakerDetails" runat="server"></asp:Literal>
                        </div>
                        <section class="w-100 pt-2 text-center d-block">
                            <a class="btn btn-default eventbtn" href='<%= Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/become-a-speaker" %>'>Become A Speaker</a>
                        </section>
                    </div>
                </div>
                <div class="col-12 col-md-3 px-3">
                    <h6 class="sectionsubheading font-weight-bold text-center">Our Sponsors</h6>
                    <div class="swiper-container swiperslidersponsor swiper-container-sponsors">
                        <div class="swiper-wrapper">
                            <asp:Repeater ID="rptSponsorAds" runat="server">
                                <ItemTemplate>
                                    <div class="swiper-slide text-center">
                                        <a target="_blank" class="text-dark" href='<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(Eval("eventSponsorName").ToString()) %>'>
                                            <small class="text-dark font-weight-bold mb-2"><%# Eval("eventSponsorType") %> Sponsor</small><br />
                                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="swiper-pagination swiper-pagination-sponsors"></div>
                    </div>

                    <h6 class="sectionsubheading font-weight-bold text-center my-3">Useful Downloads</h6>
                    <asp:HyperLink ID="lnkBrochureSidebar" runat="server" class="text-center text-dark">
                        <asp:Image ID="imgBrochure" runat="server" CssClass="img-fluid mb-3" /><br />
                        <p class="font-weight-bold">Download Brochure</p>
                    </asp:HyperLink>

                    <asp:HyperLink ID="lnkAgendaSidebar" runat="server" class="text-center text-dark">
                        <asp:Image ID="imgAgenda" runat="server" CssClass="img-fluid mb-3" /><br />
                        <p class="font-weight-bold">Download Agenda</p>
                    </asp:HyperLink>


                </div>
            </div>

        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
    <script>
        $(document).ready(function () {
            //initialize swiper when document ready
            var mySwiper = new Swiper('.swiper-container-sponsors', {
                slidesPerView: 1,
                autoplay: {
                    delay: 5000,
                },
                pagination: {
                    el: '.swiper-pagination-sponsors',
                    clickable: true,
                }
            })
        });
    </script>
</asp:Content>

