﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="PostJob.aspx.cs" Inherits="App_Jobs_PostJob" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/themes/dark.css" />
    <script src="https://cdn.ckeditor.com/4.7.0/standard/ckeditor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Post New Job</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Please Enter the following job details-</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="form-control-label">Job Heading/Title <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtJobHeading" CssClass="form-control" placeholder="eg: Senior Store Manager" runat="server" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Company Name <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtCompanyName" placeholder="Please enter Company Name here" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Location <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtLocation" placeholder="Enter Location" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Salary</label>
                        <asp:TextBox ID="txtSalary" placeholder="Please enter Salary here" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <label class="form-control-label">Image/Logo </label>
                    <asp:FileUpload ID="fileImage" CssClass="form-control" runat="server" />
                </div>

                <div class="form-group">
                    <label class="form-control-label">Job Description</label>
                    <asp:TextBox ID="txtDescription" required TextMode="MultiLine" runat="server" CssClass="form-control contentBox"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Job Skillset</label>
                    <asp:TextBox ID="txtSkillset" required TextMode="MultiLine" runat="server" CssClass="form-control contentBox"></asp:TextBox>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Qualification <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtQualification" placeholder="Enter Qualification" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Experience <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtExperience" placeholder="Please enter Experience here" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Number of Positions <small class="text-danger">*Required</small></label>
                        <asp:TextBox ID="txtNumPostst" placeholder="Enter Number of Positions" runat="server" CssClass="form-control" required></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Job Apply Link/URL</label>
                        <asp:TextBox ID="txtURL" placeholder="Please enter URL here" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Last Date</label>
                        <asp:TextBox ID="txtLastDate" placeholder="Please enter Last Date here" runat="server" CssClass="form-control flatpickr"></asp:TextBox>
                    </div>
                    <div class="col-md-6 col-12">
                        <label class="form-control-label">Featured Job?</label>

                        <div class="i-checks mt-2">
                            <asp:CheckBox ID="chkFeatured" runat="server" />
                            <label for="chkFeatured">Make Featured<small> (Displayed on Top)</small></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Post Job" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.js"></script>
    <script>
        CKEDITOR.replaceAll('contentBox');
        CKEDITOR.config.htmlEncodeOutput = true;

        $(".flatpickr").flatpickr({
            minDate: "today", dateFormat: "Y-m-d", allowInput: true
        });
    </script>
</asp:Content>

