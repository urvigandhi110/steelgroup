﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="subscribe.aspx.cs" Inherits="subscribe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="pageheading">Subscribe to <span>Newsletter</span></h1>
                <p class="mx-auto text-justify mb-4">
                    Steel Group adds some value to the steel industry by its News Letter as an information what is happening in steel world across the globe. Our Newsletter currently goes to close to 10,000 persons everyday, companies can put their advertisement and also a case study can be published as a newsletter.
                </p>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-12">
                <h4 class="text-center mb-4">Newsletter Samples</h4>
            </div>
            <div class="col-12 col-md-6">
                <div id="sample1">
                    <div class="border border-dark"></div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div id="sample2">
                    <div class="border border-dark"></div>
                </div>
            </div>
            <div class="col-12 text-center mt-5">
                <p><strong>If you want the goodness of our newsletters to reach you, Subscribe below:</strong></p>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

