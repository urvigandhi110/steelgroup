﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventMaster.master" AutoEventWireup="true" CodeFile="SponsorDetail.aspx.cs" Inherits="SponsorDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="sponsordetailsbox">
        <div class="row no-gutters py-4">
            <div class="col-12 text-center">
                <h3 class="sectionheading">Our Sponsors</h3>
                <p class="sectionsubheading">All who are participating</p>
            </div>
        </div>
        <div class="container pb-5">
            <div class="row no-gutters">
                <div class="col-12 col-md-12">
                    <asp:Repeater ID="rptSponsorDetails" runat="server">
                        <ItemTemplate>
                            <a name='<%# SteelFunc.GenerateUrl(Eval("eventSponsorName").ToString()) %>'></a>
                            <div class="sponsordetails">
                                <div class="row no-gutters">
                                    <div class="col-12 col-md-4 text-center">
                                        <asp:Image ID="imgSponsorLogo" ImageUrl='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().ToString().Replace("~/", "/") + "?w=350&h=200&mode=pad&scale=canvas&bgcolor=ffffff" %>' CssClass="img-fluid" runat="server" />
                                    </div>
                                    <div class="col-12 col-md-8">
                                        <h5 class="font-weight-bold text-dark"><%# Eval("eventSponsorName") %></h5>
                                        <p>
                                            <small><%# Eval("eventSponsorType").ToString().Contains("Exhibitor") || Eval("eventSponsorType").ToString().Contains("Company")?Eval("eventSponsorType").ToString():Eval("eventSponsorType").ToString() + "&nbsp;Sponsor" %></small>
                                        </p>
                                        <p class="text-justify">
                                            <%# Eval("eventSponsorDesc") %>
                                        </p>
                                        <p>
                                            <asp:HyperLink ID="lnkWebsite" NavigateUrl='<%# Eval("eventSponsorURL").ToString()==""?"#":Eval("eventSponsorURL").ToString() %>' CssClass="btn btn-dark mt-4 btn-sm" runat="server" Target="_blank">Visit Sponsor Website</asp:HyperLink>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <section class="w-100 pt-2 text-center d-block">
                <asp:HyperLink ID="lnkDownloadSponsorship" CssClass="btn btn-default eventbtn" runat="server">Check Sponsorship Benefits</asp:HyperLink>
            </section>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

