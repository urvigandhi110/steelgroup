﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.ServiceModel.Syndication;
using System.Xml;
using System.Text.RegularExpressions;
using System.Xml.Linq;

public partial class News : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;
    SqlDataAdapter da;
    DataSet ds;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    void Bindnews()
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT newsID,newsCategory, newsHeadline, newsImage,newsLeadPara,newsOnTop,newsFeatured, dbo.fngettimeinagoformat(datecreated,GETDATE()) as datediff,datecreated,newsdate FROM newsmaster WHERE publish='Yes'; ");

            steelcmd.Connection = steelcon;
            steelcon.Open();


            da = new SqlDataAdapter();
            ds = new DataSet();

            da.SelectCommand = steelcmd;
            da.Fill(ds);

            ds.Tables[0].DefaultView.RowFilter = "newsCategory<>'Ticker' AND newsFeatured<>'Yes'";
            ds.Tables[0].DefaultView.Sort = "newsdate DESC";

            //Second Fold News
            gvNews.DataSource = ds.Tables[0].DefaultView;
            gvNews.DataBind();

            da.Dispose();
            ds.Clear();
            ds.Dispose();
            steelcon.Close();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        { }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT newsID,newsCategory, newsHeadline, newsImage,newsLeadPara,newsOnTop,newsFeatured, dbo.fngettimeinagoformat(datecreated,GETDATE()) as datediff,datecreated,newsdate FROM newsmaster WHERE publish='Yes'; SELECT advDimensions,advImage,advLinkURL,advType FROM adsmaster WHERE advType LIKE '%NEWS%' AND advEndDate>=GETDATE() AND publish='Yes' ORDER BY datecreated desc;");

            steelcmd.Connection = steelcon;
            steelcon.Open();


            da = new SqlDataAdapter();
            ds = new DataSet();

            da.SelectCommand = steelcmd;
            da.Fill(ds);

            ds.Tables[0].DefaultView.RowFilter = "newsCategory<>'Ticker' AND newsFeatured<>'Yes'";
            ds.Tables[0].DefaultView.Sort = "newsdate DESC";

            //Second Fold News
            gvNews.DataSource = ds.Tables[0].DefaultView;
            gvNews.DataBind();

            ds.Tables[0].DefaultView.RowFilter = "";
            ds.Tables[0].DefaultView.RowFilter = "newsFeatured='Yes' AND newsCategory<>'Ticker'";
            ds.Tables[0].DefaultView.Sort = "newsdate DESC";

            //Featured SLider News Buttons
            rptFeaturedNewsButtons.DataSource = SteelFunc.GetTopDataViewRows(ds.Tables[0].DefaultView, 5);
            rptFeaturedNewsButtons.DataBind();

            //Featured SLider News
            rptFeaturedNews.DataSource = SteelFunc.GetTopDataViewRows(ds.Tables[0].DefaultView, 5);
            rptFeaturedNews.DataBind();


            ds.Tables[0].DefaultView.RowFilter = "";
            ds.Tables[0].DefaultView.RowFilter = "newsOnTop<>'Yes' AND newsCategory='Ticker' AND newsFeatured<>'Yes'";
            ds.Tables[0].DefaultView.Sort = "newsdate DESC";

            //Ticker News
            rptNewsTicker.DataSource = ds.Tables[0].DefaultView;
            rptNewsTicker.DataBind();

            ds.Tables[0].DefaultView.RowFilter = "";
            ds.Tables[0].DefaultView.RowFilter = "newsCategory<>'Ticker'";
            ds.Tables[0].DefaultView.Sort = "datecreated DESC";

            //Sidebar News 
            rptSidebarNews.DataSource = SteelFunc.GetTopDataViewRows(ds.Tables[0].DefaultView, 5);
            rptSidebarNews.DataBind();

            //ads filter

            ds.Tables[1].DefaultView.RowFilter = "advType LIKE '%NEWS-TOP%'";

            if (ds.Tables[1].DefaultView.ToTable().Rows.Count > 0)
            {
                lnkMidAd.Visible = true;
                lnkMidAd.NavigateUrl = ds.Tables[1].DefaultView.ToTable().Rows[0]["advLinkURL"].ToString();
                lnkMidAd.Target = "_blank";
                imgAdinMid.ImageUrl = Page.ResolveUrl("~/cms") + ds.Tables[1].DefaultView.ToTable().Rows[0]["advImage"].ToString().Replace("~/", "/") + "?w=" + ds.Tables[1].DefaultView.ToTable().Rows[0]["advDimensions"].ToString().Split('x')[0] + "&h=" + ds.Tables[1].DefaultView.ToTable().Rows[0]["advDimensions"].ToString().Split('x')[1] + "&mode=crop";
            }
            else
            {
                lnkMidAd.Visible = false;
            }

            ds.Tables[1].DefaultView.RowFilter = "";
            ds.Tables[1].DefaultView.RowFilter = "advType LIKE '%NEWS-SIDE%'";

            if (ds.Tables[1].DefaultView.ToTable().Rows.Count > 0)
            {
                rptNewsAds.DataSource = ds.Tables[1].DefaultView;
                rptNewsAds.DataBind();
            }
            else
            {
                pnlNewsSideAds.Visible = false;
            }

            da.Dispose();
            ds.Clear();
            ds.Dispose();
            steelcon.Close();
            steelcmd.Dispose();


            //Trying to read from Blog Feed
            XmlReader reader = XmlReader.Create("http://blog.steelgroup.co.in/feeds/posts/default?alt=rss&max-results=4&redirect=false");
            SyndicationFeed feed = SyndicationFeed.Load(reader);
            if (feed.Items.Count() > 0)
            {
                DataTable dtblog = new DataTable();
                dtblog.Columns.Add("thumbnail");
                dtblog.Columns.Add("publishdate");
                dtblog.Columns.Add("title");
                dtblog.Columns.Add("description");
                dtblog.Columns.Add("posturl");

                foreach (SyndicationItem item in feed.Items)
                {
                    //your code for rendering each item thumbUrl.replace("/s72-c/","/s"+ImageSize+"/")
                    //ltrlTest.Text += item.Summary.Text + ", ";
                    if (item.ElementExtensions.Where(p => p.OuterName == "thumbnail").Count() != 0)
                    {
                        string imgPath = item.ElementExtensions.Where(p => p.OuterName == "thumbnail").First().GetObject<XElement>().Attribute("url").Value;
                        dtblog.Rows.Add(imgPath, item.PublishDate.ToString(), item.Title.Text, Regex.Replace(item.Summary.Text, @"<.+?>", String.Empty).Substring(0, 200) + "...", item.Links[0].Uri.AbsoluteUri);
                        //ltrlimages.Text += imgPath + ", "; //use it to show the img in DIV or whereever you wish.
                    }

                    //ltrlURL.Text += item.Links[0].Uri.AbsoluteUri + "<br>";
                    //ltrlpublishDate.Text += item.PublishDate.ToString() + "<br>";
                    //ltrlTitle.Text += item.Title.Text + "<br>";
                    //ltrldescription.Text += Regex.Replace(item.Summary.Text, @"<.+?>", String.Empty).Substring(0, 100) + "<br>";
                }

                rptBlogposts.DataSource = dtblog;
                rptBlogposts.DataBind();

            }
            else
            {
                blogposts.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }

    protected void gvNews_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvNews.PageIndex = e.NewPageIndex;
        Bindnews();
    }
}