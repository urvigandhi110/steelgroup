﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.ServiceModel.Syndication;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.IO;


public partial class PastEvent : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;
    SqlDataAdapter da;
    DataSet ds;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {


            if (!IsPostBack)
            {
                string eventName = Page.RouteData.Values["eventname"].ToString();
                //Eval("eventName").ToString().Replace(" ","-").Replace("-&","").ToLower()

                steelcon = new SqlConnection(connstr);
                steelcmd = new SqlCommand("SELECT eventID,eventHashTags,eventName,eventImage1,eventImage2,eventImage3,eventImage4,eventStartDate,eventVenue,eventCity,eventCountry,eventCompanies,eventDelegates,eventPanelists,eventExhibitors,eventVideo,eventDesc,eventDescOld,eventRegForm,eventSponsorshipForm,eventBrochure,eventAgenda,eventLayout,eventParticipatingPDF,eventVenueAddress,eventVenueMapURL,postEventPress,postEventAbout,postEventObjectives FROM eventsmaster WHERE publish='Yes' AND Lower(replace(replace(eventName,' ','-'),'-&',''))=@eventname; SELECT s.eventSpeakerID,s.eventSpeakerName,s.eventSpeakerOrg,s.eventSpeakerPhoto,s.eventSpeakerDesignation,s.eventSpeakerCountry,s.eventSpeakerType,s.eventSpeakerIntro,s.eventSpeakerLinkedin,s.eventSpeakerTwitter FROM eventSpeakers s JOIN eventsmaster e ON e.eventID=s.eventID WHERE Lower(replace(replace(e.eventName,' ','-'),'-&',''))=@eventname ORDER BY eventSpeakerName; SELECT row_number() over(order by pptTitle) as sno, pt.pptID,pt.pptTitle,pt.pptSpeaker,pt.eventID,pt.pptUploadedFileName,pt.pptFile FROM eventppt pt JOIN eventsmaster e ON e.eventID=pt.eventID WHERE Lower(replace(replace(e.eventName,' ','-'),'-&',''))=@eventname; SELECT spo.eventSponsorID,spo.eventSponsorName,spo.eventSponsorLogo,spo.eventSponsorURL,spo.eventSponsorType FROM eventSponsors spo JOIN eventsmaster e ON e.eventID=spo.eventID WHERE Lower(replace(replace(e.eventName,' ','-'),'-&',''))=@eventname ORDER BY case eventSponsorType when 'Principal' then 1 when 'Platinum' then 2 when 'Gold' then 3 when 'Silver' then 4 when 'Associate' then 5 when 'Networking Lunch' then 6 when 'Networking Dinner' then 7 when 'Delegate Kit Sponsor' then 8 when 'Company Highlight' then 9 end; SELECT g.photoFile,g.photoDesc,g.photoTitle FROM galleryphoto g JOIN eventsmaster e ON e.eventID=g.eventID WHERE Lower(replace(replace(e.eventName,' ','-'),'-&',''))=@eventname;");
                steelcmd.Parameters.AddWithValue("eventname", eventName);
                steelcmd.Connection = steelcon;
                steelcon.Open();


                da = new SqlDataAdapter();
                ds = new DataSet();

                da.SelectCommand = steelcmd;
                da.Fill(ds);

                rptImageGallery.DataSource = ds.Tables[4];
                rptImageGallery.DataBind();

                //Master COntrols
                Literal ltrlEventVenue = (Literal)Master.FindControl("ltrlEventVenue");
                Literal ltrlEventDate = (Literal)Master.FindControl("ltrlEventDate");
                Literal ltrlEventName = (Literal)Master.FindControl("ltrlEventName");
                Literal ltrlVenueAddress = (Literal)Master.FindControl("ltrlVenueAddress");
                Literal ltrlVenueMap = (Literal)Master.FindControl("ltrlVenueMap");
                HiddenField hf = (HiddenField)Master.FindControl("hfimageurl");

                hf.Value = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventImage1"].ToString().Replace("~/", "/") + "?w=1920&h=1200&mode=crop";
                ltrlEventVenue.Text = ds.Tables[0].Rows[0]["eventVenue"].ToString();
                ltrlEventDate.Text = DateTime.Parse(ds.Tables[0].Rows[0]["eventStartDate"].ToString()).ToString("MMMM dd, yyyy");
                //ltrlEventName.Text = ds.Tables[0].Rows[0]["eventName"].ToString();
                ltrlEventName.Text = "Thank you for attending<small style=\"font-size:1rem;\">" + ds.Tables[0].Rows[0]["eventName"].ToString() + "</small>";
                ltrlVenueAddress.Text = ds.Tables[0].Rows[0]["eventVenueAddress"].ToString();
                ltrlVenueMap.Text = "<iframe src=\"" + ds.Tables[0].Rows[0]["eventVenueMapURL"].ToString() + "\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border: 0\" allowfullscreen></iframe>";


                //downloads.Attributes.Add("style", "background-image: linear-gradient( rgba(0,0,0,.7), rgba(0,0,0,.7) ), url(" + Page.ResolveUrl("~/cms/") + ds.Tables[0].Rows[0]["eventImage1"].ToString().Replace("~/", "") + ");");
                //contactdetails.Attributes.Add("style", "background-image: linear-gradient( rgba(0,0,0,.7), rgba(0,0,0,.7) ), url(" + Page.ResolveUrl("~/cms/") + ds.Tables[0].Rows[0]["eventImage1"].ToString().Replace("~/", "") + ");");

                Title = ds.Tables[0].Rows[0]["eventName"].ToString() + " - Steel Group";
                MetaDescription = ds.Tables[0].Rows[0]["eventDesc"].ToString();

                //Binding Other Controls
                //Timer
                hfeventDate.Value = DateTime.Parse(ds.Tables[0].Rows[0]["eventStartDate"].ToString()).ToString("MMMM dd, yyyy");
                hfEventID.Value = ds.Tables[0].Rows[0]["eventID"].ToString();
                mailvals.Value = ds.Tables[0].Rows[0]["eventName"].ToString() + "$$" + ds.Tables[0].Rows[0]["eventImage1"].ToString() + "$$" + ds.Tables[0].Rows[0]["eventAgenda"].ToString() + "$$" + ds.Tables[0].Rows[0]["eventBrochure"].ToString();
                hfHashTags.Value = ds.Tables[0].Rows[0]["eventHashTags"].ToString();
                //Stats
                ltrlCompanies.Text = ds.Tables[0].Rows[0]["eventCompanies"].ToString();
                ltrlDelegates.Text = ds.Tables[0].Rows[0]["eventDelegates"].ToString();
                ltrlExhibitors.Text = ds.Tables[0].Rows[0]["eventExhibitors"].ToString();
                ltrlPanelists.Text = ds.Tables[0].Rows[0]["eventPanelists"].ToString();

                ////About Us
                //ltrlVideoURL.Text = "<iframe width=\"100%\" height=\"400\" src=\"" + ds.Tables[0].Rows[0]["eventVideo"].ToString() + "\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>";
                //ltrlEventDesc.Text = ds.Tables[0].Rows[0]["eventDesc"].ToString();
                //ltrlEventDescOld.Text = ds.Tables[0].Rows[0]["eventDescOld"].ToString();

                ////Downloads
                //lnkDownloadAgenda.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventAgenda"].ToString().Replace("~/", "/");
                //lnkDownloadAgenda.Attributes.Add("download", "Agenda-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());

                //lnkDownloadBrochure.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventBrochure"].ToString().Replace("~/", "/");
                //lnkDownloadBrochure.Attributes.Add("download", "Brochure-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());

                ltrlPressRelease.Text = Server.HtmlDecode(ds.Tables[0].Rows[0]["postEventPress"].ToString());
                ltrlObjectives.Text = Server.HtmlDecode(ds.Tables[0].Rows[0]["postEventObjectives"].ToString());
                ltrlAboutOrganizers.Text = Server.HtmlDecode(ds.Tables[0].Rows[0]["postEventAbout"].ToString());

                ds.Tables[1].DefaultView.RowFilter = "eventSpeakerType='Key Note Speaker'";

                if (ds.Tables[1].DefaultView.ToTable().Rows.Count > 0)
                {
                    pnlTopSpeaker.Visible = true;
                    imgTopSpeaker.ImageUrl = Page.ResolveUrl("~/cms") + ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerPhoto"].ToString().Replace("~/", "/") + "?w=200&h=250&mode=crop";
                    ltrlTopSpeakerName.Text = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerName"].ToString();
                    ltrlTopSpeakerType.Text = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerType"].ToString();
                    ltrlTopSpeakerDetails.Text = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerOrg"].ToString() + " | " + ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerDesignation"].ToString() + " | " + ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerCountry"].ToString();
                    ltrlTopSpeakerIntro.Text = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerIntro"].ToString();
                    lnkSpeakerLinkedIn.NavigateUrl = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerLinkedin"].ToString() != "" ? ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerLinkedin"].ToString() : "#";
                    lnkSpeakerTwitter.NavigateUrl = ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerTwitter"].ToString() != "" ? ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerTwitter"].ToString() : "#";
                    lnkSpeakerDetails.NavigateUrl = Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "/speakers/" + SteelFunc.GenerateUrl(ds.Tables[1].DefaultView.ToTable().Rows[0]["eventSpeakerName"].ToString());
                }
                else
                {
                    pnlTopSpeaker.Visible = false;
                }
                //Speakers - ds.tables[1]
                ds.Tables[1].DefaultView.RowFilter = "";
                ds.Tables[1].DefaultView.RowFilter = "eventSpeakerType<>'Key Note Speaker'";
                rptEventSpeaker.DataSource = ds.Tables[1].DefaultView;
                rptEventSpeaker.DataBind();


                //Sponsors - ds.tables[3] - All read at once, filter and bind separate repeaters or hide section

                //0. Principal Sponsors
                ds.Tables[3].DefaultView.RowFilter = "eventSponsorType IN ('Principal','Platinum','Gold','Silver','Associate','Networking Lunch','Networking Dinner','Delegate Kit Sponsor','Company Highlight')";
                //ds.Tables[3].DefaultView.Sort = "case eventSponsorType when 'Principal' then 1 when 'Platinum' then 2 when 'Gold' then 3 when 'Silver' then 4 when 'Associate' then 5 when 'Networking Lunch' then 6 when 'Networking Dinner' then 7, when 'Delegate Kit Sponsor' then 8, when 'Company Highlight' then 9 end";

                if (ds.Tables[3].DefaultView.Count > 0)
                {
                    rptAllSponsors.DataSource = ds.Tables[3].DefaultView;
                    rptAllSponsors.DataBind();
                }
                else
                {
                    allsponsors.Visible = false;
                }

                ////1. Platinum Sponsors
                //ds.Tables[3].DefaultView.RowFilter = "";
                //ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Platinum'";
                //ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

                //if (ds.Tables[3].DefaultView.Count > 0)
                //{
                //    rptPlatinum.DataSource = ds.Tables[3].DefaultView;
                //    rptPlatinum.DataBind();
                //}
                //else
                //{
                //    platinumsponsors.Visible = false;
                //}

                ////2. Gold Sponsors
                //ds.Tables[3].DefaultView.RowFilter = "";
                //ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Gold'";
                //ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

                //if (ds.Tables[3].DefaultView.Count > 0)
                //{
                //    rptGold.DataSource = ds.Tables[3].DefaultView;
                //    rptGold.DataBind();
                //}
                //else
                //{
                //    goldsponsors.Visible = false;
                //}

                ////3. Silver Sponsors
                //ds.Tables[3].DefaultView.RowFilter = "";
                //ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Silver'";
                //ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

                //if (ds.Tables[3].DefaultView.Count > 0)
                //{
                //    rptSilver.DataSource = ds.Tables[3].DefaultView;
                //    rptSilver.DataBind();
                //}
                //else
                //{
                //    silversponsors.Visible = false;
                //}

                ////4. Associate Sponsors
                //ds.Tables[3].DefaultView.RowFilter = "";
                //ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Associate'";
                //ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

                //if (ds.Tables[3].DefaultView.Count > 0)
                //{
                //    rptAssociate.DataSource = ds.Tables[3].DefaultView;
                //    rptAssociate.DataBind();
                //}
                //else
                //{
                //    associatesponsors.Visible = false;
                //}

                ////5. Company Highlight Sponsors - Networking Left


                //ds.Tables[3].DefaultView.RowFilter = "";
                //ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Company Highlight'";
                //ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

                //if (ds.Tables[3].DefaultView.Count > 0)
                //{
                //    rptCompanyHighlight.DataSource = ds.Tables[3].DefaultView;
                //    rptCompanyHighlight.DataBind();
                //}
                //else
                //{
                //    clubbedsponsors.Visible = false;
                //}
                //ds.Tables[3].DefaultView.RowFilter = "";
                //ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%networking%'";
                //ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

                //if (ds.Tables[3].DefaultView.Count > 0)
                //{
                //    clubbedsponsors.Visible = true;

                //    foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
                //    {
                //        if (rw["eventSponsorType"].ToString() == "Networking Dinner")
                //        {
                //            pnlNetworkingDinner.Visible = true;
                //            imgNetworkingDinner.ImageUrl = Page.ResolveUrl("~/cms") + rw["eventSponsorLogo"].ToString().Replace("~/", "/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff";
                //        }
                //        else if (rw["eventSponsorType"].ToString() == "Networking Lunch")
                //        {
                //            pnlNetworkingLunch.Visible = true;
                //            imgNetworkingLunch.ImageUrl = Page.ResolveUrl("~/cms") + rw["eventSponsorLogo"].ToString().Replace("~/", "/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff";
                //        }
                //    }
                //}
                //else
                //{
                //    pnlNetworkingDinner.Visible = false;
                //    pnlNetworkingLunch.Visible = false;
                //}
                ////Delegate Kit Sponsor Addition New
                //ds.Tables[3].DefaultView.RowFilter = "";
                //ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%delegate kit%'";
                //ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";
                //if (ds.Tables[3].DefaultView.Count > 0)
                //{
                //    pnlDelegateKit.Visible = true;
                //    imgDelegateKit.ImageUrl = Page.ResolveUrl("~/cms") + ds.Tables[3].DefaultView.ToTable().Rows[0]["eventSponsorLogo"].ToString().Replace("~/", "/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff";

                //}
                //else
                //{
                //    pnlDelegateKit.Visible = false;
                //}

                //6. Exhibitor Sponsors
                ds.Tables[3].DefaultView.RowFilter = "";
                ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Exhibitor'";
                ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

                if (ds.Tables[3].DefaultView.Count > 0)
                {
                    rptExhibitors.DataSource = ds.Tables[3].DefaultView;
                    rptExhibitors.DataBind();
                }
                else
                {
                    othersponsors.Visible = false;
                }

                //7. Other Partners
                DataTable dtpartners = new DataTable();
                dtpartners.Columns.Add("eventSponsorID");
                dtpartners.Columns.Add("eventSponsorName");
                dtpartners.Columns.Add("eventSponsorLogo");
                dtpartners.Columns.Add("eventSponsorURL");
                dtpartners.Columns.Add("eventSponsorType");
                //eventSponsorID,eventSponsorName,eventSponsorLogo,eventSponsorURL,eventSponsorType


                ds.Tables[3].DefaultView.RowFilter = "";
                ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%knowledge%' AND eventSponsorType LIKE '%partner%'";

                int rwcount = 0;
                foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
                {
                    if (rwcount == 0)
                    {
                        dtpartners.Rows.Add(rw.ItemArray);
                    }
                    else
                    {
                        dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                    }
                    rwcount++;

                }

                ds.Tables[3].DefaultView.RowFilter = "";
                ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%marketing%' AND eventSponsorType LIKE '%partner%'";

                rwcount = 0;
                foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
                {
                    if (rwcount == 0)
                    {
                        dtpartners.Rows.Add(rw.ItemArray);
                    }
                    else
                    {
                        dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                    }
                    rwcount++;

                }

                ds.Tables[3].DefaultView.RowFilter = "";
                ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%media%' AND eventSponsorType LIKE '%partner%'";

                rwcount = 0;
                foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
                {
                    if (rwcount == 0)
                    {
                        dtpartners.Rows.Add(rw.ItemArray);
                    }
                    else
                    {
                        dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                    }
                    rwcount++;

                }

                ds.Tables[3].DefaultView.RowFilter = "";
                ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%it pa%' AND eventSponsorType LIKE '%partner%'";
                rwcount = 0;
                foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
                {
                    if (rwcount == 0)
                    {
                        dtpartners.Rows.Add(rw.ItemArray);
                    }
                    else
                    {
                        dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                    }
                    rwcount++;

                }

                ds.Tables[3].DefaultView.RowFilter = "";
                ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%online%' AND eventSponsorType LIKE '%partner%'";

                rwcount = 0;
                foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
                {
                    if (rwcount == 0)
                    {
                        dtpartners.Rows.Add(rw.ItemArray);
                    }
                    else
                    {
                        dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                    }
                    rwcount++;

                }

                ds.Tables[3].DefaultView.RowFilter = "";
                ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%travel%' AND eventSponsorType LIKE '%partner%'";

                rwcount = 0;
                foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
                {
                    if (rwcount == 0)
                    {
                        dtpartners.Rows.Add(rw.ItemArray);
                    }
                    else
                    {
                        dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                    }
                    rwcount++;

                }

                ds.Tables[3].DefaultView.RowFilter = "";
                ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%design%' AND eventSponsorType LIKE '%partner%'";

                rwcount = 0;
                foreach (DataRow rw in ds.Tables[3].DefaultView.ToTable().Rows)
                {
                    if (rwcount == 0)
                    {
                        dtpartners.Rows.Add(rw.ItemArray);
                    }
                    else
                    {
                        dtpartners.Rows.Add(rw[0], rw[1], rw[2], rw[3], "&nbsp;");
                    }
                    rwcount++;

                }


                if (dtpartners.Rows.Count > 0)
                {
                    rptPartners.DataSource = dtpartners;
                    rptPartners.DataBind();

                }
                else
                {
                    partners.Visible = false;
                }

                //8. Participating companies
                ds.Tables[3].DefaultView.RowFilter = "";
                ds.Tables[3].DefaultView.RowFilter = "eventSponsorType='Participating Companies'";
                ds.Tables[3].DefaultView.Sort = "eventSponsorName ASC";

                if (ds.Tables[3].DefaultView.Count > 0)
                {
                    rptParticipating.DataSource = ds.Tables[3].DefaultView;
                    rptParticipating.DataBind();
                    lnkParticipatingDownload.NavigateUrl = Page.ResolveUrl("~/cms") + ds.Tables[0].Rows[0]["eventParticipatingPDF"].ToString().Replace("~/", "/");
                    lnkParticipatingDownload.Attributes.Add("download", "Participating-Companies-" + ds.Tables[0].Rows[0]["eventName"].ToString().Replace(" ", "-").ToLower());
                }
                else
                {
                    participating.Visible = false;
                }

                //9. In association with 

                ds.Tables[3].DefaultView.RowFilter = "";
                ds.Tables[3].DefaultView.RowFilter = "eventSponsorType LIKE '%association%'";

                if (ds.Tables[3].DefaultView.Count > 0)
                {
                    imgassociation.ImageUrl = Page.ResolveUrl("~/cms/") + ds.Tables[3].DefaultView.ToTable().Rows[0]["eventSponsorLogo"].ToString().Replace("~/", "");
                    lnkAssociation.NavigateUrl = ds.Tables[3].DefaultView.ToTable().Rows[0]["eventSponsorURL"].ToString();
                }
                else
                {
                    association.Visible = false;
                    organizedby.Attributes.Remove("class");
                    organizedby.Attributes.Add("class", "col-12 col-md-6 text-center");
                }

                //PPT Downloads - ds.tables[2]
                //Session.Remove("authdelegateEmail");
                //Session.Remove("prevPPT");

                if (ds.Tables[2].Rows.Count > 0)
                {
                    ds.Tables[2].DefaultView.RowFilter = "pptUploadedFileName is Not NULL";

                    if (ds.Tables[2].DefaultView.ToTable().Rows.Count > 0)
                    {
                        rptDownloadsTable.DataSource = ds.Tables[2];
                        rptDownloadsTable.DataBind();

                        lnkDownloadAdd.Attributes.Add("data-pptid", "ALL");
                        lnkDownloadAdd.Attributes.Add("data-eventid", ds.Tables[2].Rows[0]["eventID"].ToString());

                        pnlNoDownloads.Visible = false;
                        pnlDownloadsAvailable.Visible = true;
                    }

                    ds.Tables[2].DefaultView.RowFilter = "";

                    ds.Tables[2].DefaultView.RowFilter = "pptUploadedFileName is NULL";

                    if (ds.Tables[2].DefaultView.ToTable().Rows.Count > 0)
                    {
                        rptDownloadFromEmail.DataSource = ds.Tables[2].DefaultView;
                        rptDownloadFromEmail.DataBind();

                        rptDownloadFromEmail.Visible = true;
                        headingtrEmail.Visible = true;
                    }

                }
                else
                {
                    pnlNoDownloads.Visible = true;
                    pnlDownloadsAvailable.Visible = false;
                }

                da.Dispose();
                ds.Clear();
                ds.Dispose();
                steelcon.Close();
                steelcmd.Dispose();

            }
            if (Session["authdelegateEmail"] != null && Session["authdelegateEmail"].ToString() != "")
            {
                foreach (RepeaterItem item in rptDownloadsTable.Items)
                {
                    LinkButton lnk = (LinkButton)item.FindControl("lnkDownload");
                    if (Session["prevPPT"] != null && lnk.Attributes["data-pptid"].ToString() == Session["prevPPT"].ToString())
                    {
                        lnk.Text = "Sent on Email";
                        lnk.Enabled = false;
                        lnk.Attributes.Add("disabled", "disabled");
                        Session.Remove("prevPPT");
                        lnk.Attributes.Remove("data-toggle");
                        lnk.Attributes.Remove("data-target");
                    }
                    else if (Session["prevPPT"] != null && Session["prevPPT"].ToString() == "ALL")
                    {
                        lnk.Text = "Sent on Email";
                        lnk.Enabled = false;
                        lnk.Attributes.Add("disabled", "disabled");
                        lnk.Attributes.Remove("data-toggle");
                        lnk.Attributes.Remove("data-target");
                    }
                    else if (Session["prevPPT"] != null)
                    {
                        lnk.Attributes.Remove("data-toggle");
                        lnk.Attributes.Remove("data-target");
                    }

                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        try
        {

            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT * FROM delegateslogin WHERE delegateEmail=@delegateEmail AND eventID=@eventID; select pptTitle,pptFile from eventppt WHERE pptID=@pptID");
            steelcmd.Parameters.AddWithValue("eventID", hfEventID.Value);
            steelcmd.Parameters.AddWithValue("pptID", hfCurrPPTID.Value);
            steelcmd.Parameters.AddWithValue("delegateEmail", txtAuthEmail.Text.Trim());
            steelcmd.Connection = steelcon;
            steelcon.Open();

            rdrd = steelcmd.ExecuteReader();
            string delegatename = "";
            string eventname = Page.RouteData.Values["eventname"].ToString();

            if (rdrd.Read())
            {
                Session["authdelegateEmail"] = txtAuthEmail.Text;
                Session["prevPPT"] = hfCurrPPTID.Value;
                delegatename = rdrd["delegateName"].ToString();

            }
            else
            {
                lblError.Text = "This ID is not registered with us. Please use registered email address.";
                lblError.Visible = true;
            }

            rdrd.NextResult();

            if (rdrd.Read())
            {
                //sendemail
                //Preparing registrant mail to send
                StreamReader readerRegistrant = new StreamReader(Server.MapPath("~/templates/pptdownload.txt"));
                string readFileRegistrant = readerRegistrant.ReadToEnd();
                readFileRegistrant = readFileRegistrant.Replace("$$REGURL$$", "https://www.steelgroup.co.in/past-events/" + eventname);

                //eventname,image,agenda,brochure
                string[] stringSeparators = new string[] { "$$" };

                readFileRegistrant = readFileRegistrant.Replace("$$REGNAME$$", delegatename);
                readFileRegistrant = readFileRegistrant.Replace("$$PPTNAME$$", rdrd["pptTitle"].ToString());
                readFileRegistrant = readFileRegistrant.Replace("$$EVENTNAME$$", mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[0]);
                readFileRegistrant = readFileRegistrant.Replace("$$IMAGEURL$$", "https://www.steelgroup.co.in/cms/" + mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[1].Replace("~/", "") + "?w=1260&h=300&mode=crop");

                string resp = SteelFunc.sendPPTmail(txtAuthEmail.Text.Trim(), "Presentation - " + rdrd["pptTitle"].ToString() + " from Steelgroup", readFileRegistrant, "https://www.steelgroup.co.in/cms" + rdrd["pptFile"].ToString().Replace("~/", "/"), SteelFunc.GenerateUrl(rdrd["pptTitle"].ToString()) + Path.GetExtension(rdrd["pptFile"].ToString()));
                readerRegistrant.Close();
                lblError.Visible = true;
                lblError.Text = resp;
                //pnlSend.Visible = false;
                //pnlSent.Visible = true;
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = "No ppt";
            }

            steelcmd.Dispose();
            steelcon.Close();
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.ToString();
        }


    }

    protected void rptDownloadsTable_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "downloadppt")
            {
                if (Session["authdelegateEmail"] != null && Session["authdelegateEmail"].ToString() != "")
                {
                    //send email


                    steelcon = new SqlConnection(connstr);
                    steelcmd = new SqlCommand("SELECT * FROM delegateslogin WHERE delegateEmail=@delegateEmail AND eventID=@eventID; select pptTitle,pptFile from eventppt WHERE pptID=@pptID");
                    steelcmd.Parameters.AddWithValue("eventID", hfEventID.Value);
                    steelcmd.Parameters.AddWithValue("pptID", e.CommandArgument);
                    steelcmd.Parameters.AddWithValue("delegateEmail", Session["authdelegateEmail"].ToString());
                    steelcmd.Connection = steelcon;
                    steelcon.Open();

                    rdrd = steelcmd.ExecuteReader();
                    string delegatename = "";
                    string eventname = Page.RouteData.Values["eventname"].ToString();

                    if (rdrd.Read())
                    {
                        delegatename = rdrd["delegateName"].ToString();
                    }

                    rdrd.NextResult();

                    if (rdrd.Read())
                    {
                        //sendemail
                        //Preparing registrant mail to send
                        StreamReader readerRegistrant = new StreamReader(Server.MapPath("~/templates/pptdownload.txt"));
                        string readFileRegistrant = readerRegistrant.ReadToEnd();
                        readFileRegistrant = readFileRegistrant.Replace("$$REGURL$$", "https://www.steelgroup.co.in/past-events/" + eventname);

                        //eventname,image,agenda,brochure
                        string[] stringSeparators = new string[] { "$$" };

                        readFileRegistrant = readFileRegistrant.Replace("$$REGNAME$$", delegatename);
                        readFileRegistrant = readFileRegistrant.Replace("$$PPTNAME$$", rdrd["pptTitle"].ToString());
                        readFileRegistrant = readFileRegistrant.Replace("$$EVENTNAME$$", mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[0]);
                        readFileRegistrant = readFileRegistrant.Replace("$$IMAGEURL$$", "https://www.steelgroup.co.in/cms/" + mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[1].Replace("~/", "") + "?w=1260&h=300&mode=crop");

                        string resp = SteelFunc.sendPPTmail(Session["authdelegateEmail"].ToString(), "Presentation - " + rdrd["pptTitle"].ToString() + " from Steelgroup", readFileRegistrant, "https://www.steelgroup.co.in/cms" + rdrd["pptFile"].ToString().Replace("~/", "/"), SteelFunc.GenerateUrl(rdrd["pptTitle"].ToString()) + Path.GetExtension(rdrd["pptFile"].ToString()));
                        readerRegistrant.Close();

                    }


                    steelcmd.Dispose();
                    steelcon.Close();

                    LinkButton lnk = (LinkButton)e.CommandSource;
                    lnk.Text = "Sent on Email";
                    lnk.Enabled = false;
                    lnk.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    //Page_Load(this, null);
                }
            }

        }
        catch (Exception ex)
        {
            foreach (RepeaterItem item in rptDownloadsTable.Items)
            {
                LinkButton lnk = (LinkButton)item.FindControl("lnkDownload");
                lnk.Attributes.Add("data-toggle", "modal");
                lnk.Attributes.Add("data-target", "#downloadModal");
            }
        }
    }

    protected void lnkDownloadAdd_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptDownloadsTable.Items)
        {
            LinkButton lnk = (LinkButton)item.FindControl("lnkDownload");
            lnk.Text = "Sent on Email";
            lnk.Enabled = false;
            lnk.Attributes.Add("disabled", "disabled");
            lnk.Attributes.Add("data-toggle", "modal");
            lnk.Attributes.Add("data-target", "#downloadModal");
        }
    }
}