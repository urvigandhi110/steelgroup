﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_PostEvent_SpeakerEmailPPT : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            steelcmd.CommandText = "SELECT row_number() over(order by pptspeaker) as sno, * FROM eventPPT WHERE eventID=@eventID AND pptUploadedFileName is NULL";
            steelcmd.Parameters.AddWithValue("@eventID", Request.QueryString["eventID"].ToString());

            rdrd = steelcmd.ExecuteReader();

            rptList.DataSource = rdrd;
            rptList.DataBind();

            steelcon.Close();
            steelcmd.Dispose();
            steelcon.Dispose();

        }
        catch (Exception ex)
        {

        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            string eventID = Request.QueryString["eventID"].ToString();
            Random rnd = new Random();

            string newpptid = "PPT" + DateTime.Today.ToString("ddMMyyyy") + rnd.Next(10000, 99999);
            steelcmd.CommandText = "INSERT INTO eventppt(pptID,pptFile,pptTitle,pptSpeaker,eventID,datecreated,createdby,createdIP,publish) VALUES('" + newpptid + "','" + txtSpeakerEmail.Text + "','" + txtTopicName.Text + "','" + txtSpeakerName.Text + "','" + eventID + "','" + DateTime.Now + "','" + HttpContext.Current.User.Identity.Name + "','" + Request.UserHostAddress + "','Yes'); ";

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {

                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Presentation with speaker email saved successfully.";

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Presentations could not be uploaded. Please check details and try again.";

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }

        }
        catch (Exception ex)
        {

        }
    }

    protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "deleterecord")
        {
            try
            {
                steelcon = new SqlConnection(connstr);
                steelcmd = new SqlCommand();
                steelcmd.Connection = steelcon;
                steelcmd.CommandText = "DELETE FROM eventppt WHERE pptID=@pptID";

                steelcmd.Parameters.Add("@pptID", SqlDbType.VarChar);
                steelcmd.Parameters["@pptID"].Value = e.CommandArgument;

                steelcon.Open();
                int res = steelcmd.ExecuteNonQuery();

                if (res > 0)
                {
                    pnlMessage.Visible = true;
                    pnlMessage.CssClass = "alert alert-success";
                    ltrlmsg.Text = "Speaker PPT has been deleted successfully.";

                    steelcon.Close();
                    steelcmd.Dispose();
                    steelcon.Dispose();

                }
                else
                {
                    pnlMessage.Visible = true;
                    pnlMessage.CssClass = "alert alert-danger";
                    ltrlmsg.Text = "Speaker PPT could not be deleted. Please Try Again";

                    steelcon.Close();
                    steelcmd.Dispose();
                    steelcon.Dispose();
                }
            }
            catch (Exception ex)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-danger";
                ltrlmsg.Text = ex.ToString();

                steelcon.Close();
                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
    }
}