﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="App_Events_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
    <style type="text/css">
        table.dataTable {
            border-collapse: collapse !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Manage Events</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Following list shows all events posted-</h3>
            </div>
            <div class="card-body">
                <table id="list" class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Event Name</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Venue</th>
                            <th>Publish</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><%# Eval("Sno") %></th>
                                    <td><%# Eval("eventName") %></td>
                                    <td><%# Convert.ToDateTime(Eval("eventStartDate").ToString()).ToString("dd/MM/yyyy") %></td>
                                    <td><%# Convert.ToDateTime(Eval("eventEndDate").ToString()).ToString("dd/MM/yyyy") %></td>
                                    <td><%# Eval("eventVenue") %></td>
                                    <td><%# Eval("publish") %></td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Manage
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <asp:LinkButton ID="lnkAgenda" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="addagenda">Add Agenda</asp:LinkButton>
                                                <asp:LinkButton ID="lnkSponsor" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="addsponsor">Add Sponsors</asp:LinkButton>
                                                <asp:LinkButton ID="lnkSpeaker" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="addspeaker">Add Speakers</asp:LinkButton>
                                                <asp:LinkButton ID="lnkHashTags" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="addhashtags">Hashtags</asp:LinkButton>
                                                <asp:LinkButton ID="lnkViewSponsors" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="viewsponsor">View Sponsors</asp:LinkButton>
                                                <asp:LinkButton ID="lnkViewSpeakers" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="viewspeaker">View Speakers</asp:LinkButton>
                                                <asp:LinkButton ID="lnkViewReg" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="viewreg">View Registrations</asp:LinkButton>
                                                <asp:LinkButton ID="lnkViewInvites" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="viewinvites">View Invites</asp:LinkButton>
                                                <asp:LinkButton ID="lnkEditDL" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="editDL">Edit Downloads</asp:LinkButton>
                                                <asp:LinkButton ID="lnkEditEvent" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="editEvent">Edit Event</asp:LinkButton>
                                                <asp:LinkButton ID="lnkDelete" CssClass="dropdown-item" runat="server" OnClientClick='<%# "return getConfirmation(this,\"" + Eval("eventName").ToString() + "\");" %>' CommandArgument='<%# Eval("eventID") %>' CommandName="deleterecord">Delete</asp:LinkButton>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="ConfirmationPopup" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Please Confirm Deletion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure you want to delete <strong><span id="spnMsg"></span></strong>&nbsp;event? This will delete all the details under this event. This action is irreversible.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">Yes, Delete</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#list').DataTable();
        });
        function getConfirmation(sender, CourseName) {
            $("#spnMsg").text(CourseName);
            $('#ConfirmationPopup').modal('show');
            $('#btnConfirm').attr('onclick', "$('#ConfirmationPopup').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
</asp:Content>

