﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="PostNews.aspx.cs" Inherits="App_News_PostNews" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/themes/dark.css" />
    <script src="https://cdn.ckeditor.com/4.7.0/standard/ckeditor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Post News</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Please Enter the following details-</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="form-control-label">News Category <small class="text-danger">*Required</small></label>
                    <asp:DropDownList ID="ddlNewsCat" runat="server" CssClass="form-control">
                        <asp:ListItem>Select a News Category</asp:ListItem>
                        <asp:ListItem>Market</asp:ListItem>
                        <asp:ListItem>Government</asp:ListItem>
                        <asp:ListItem>Steel</asp:ListItem>
                        <asp:ListItem>Technology</asp:ListItem>
                        <asp:ListItem>Export</asp:ListItem>
                        <asp:ListItem>Tax</asp:ListItem>
                        <asp:ListItem>Company Announcement</asp:ListItem>
                        <asp:ListItem>Ticker</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <label class="form-control-label">News Headline <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtNewsHeadline" placeholder="Please enter a headline here (11-15 words)" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">News Date To Display <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtNewsDate" placeholder="Please enter a Date here" runat="server" CssClass="form-control flatpickr" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Lead Paragraph</label>
                    <asp:TextBox ID="txtLeadPara" placeholder="Enter a lead para (Optional, Shown on top)" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">News Article <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtNewsArticle" required TextMode="MultiLine" runat="server" CssClass="form-control contentBox"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">News Photo</label>
                    <asp:FileUpload ID="fileImage" CssClass="form-control" runat="server" />
                </div>
                <div class="form-group">
                    <label class="form-control-label">Image Description/Source</label>
                    <asp:TextBox ID="txtImageDesc" placeholder="Image Description, eg: Source: http://www.example.com" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">News Source URL <small>(If taken from somewhere)</small></label>
                    <asp:TextBox ID="txtNewsSource" placeholder="eg: http://www.examplenews.com/news-article" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group row no-gutters">
                    <div class="col-2">
                        <div class="i-checks">
                            <asp:CheckBox ID="chkOnTop" runat="server" />
                            <label for="chkOnTop">Post on Top</label>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="i-checks">
                            <asp:CheckBox ID="chkFeatured" runat="server" />
                            <label for="chkFeatured">Make Featured</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Post News" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.js"></script>
    <script>
        CKEDITOR.replaceAll('contentBox');
        CKEDITOR.config.htmlEncodeOutput = true;
        $(".flatpickr").flatpickr({
            maxDate: "today", dateFormat: "Y-m-d", allowInput: true
        });
    </script>
</asp:Content>

