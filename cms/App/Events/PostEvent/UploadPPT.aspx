﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="UploadPPT.aspx.cs" Inherits="App_Events_PostEvent_UploadPPT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
     <style type="text/css">
        table.dataTable {
            border-collapse: collapse !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Add Event Presentations</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Browse and select Files-</h3>
            </div>
            <div class="card-body">
                <asp:Panel ID="pnlUpload" runat="server">
                    <div class="form-group row">
                        <label class="col-sm-12 form-control-label">Select upto 20 files (PPT or PDF files of size 1 MB or less accepted.)</label>
                        <div class="col-sm-10">
                            <asp:FileUpload ValidationGroup="images" ID="filePresentation" runat="server" placeholder="Select upto 10 Images" AllowMultiple="true" />
                        </div>
                        <div class="col-sm-2">
                            <asp:RegularExpressionValidator ID="rexpfileDocument" runat="server" ControlToValidate="filePresentation" ValidationGroup="images"
                                ErrorMessage="Only .ppt, .pptx, .pdf are allowed" Text="*" CssClass="errorvalidator"
                                ValidationExpression="(.*\.([Pp][Pp][Tt])|.*\.([Pp][Pp][Tt][Xx])|.*\.([Pp][Dd][Ff])$)"></asp:RegularExpressionValidator>
                            <asp:ValidationSummary ID="valsumm" ValidationGroup="images" ShowMessageBox="true" ShowSummary="false" runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlDetails" CssClass="detailsform" Visible="false" runat="server">
                    <div class="row">
                        <asp:Repeater ID="rptPresentation" runat="server">
                            <ItemTemplate>
                                <div class="col-12 col-md-3">
                                    <div class="card bg-light">
                                        <div class="card-header">
                                            <asp:Label ID="lblPPTName" CssClass="d-block" ToolTip='<%# Container.DataItem %>' runat="server" Text='<%# (Container.ItemIndex+1).ToString() + ". " +  Container.DataItem %>'></asp:Label>
                                        </div>
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label class="form-control-label">PPT Title</label>
                                                <asp:TextBox class="form-control form-control-sm" placeholder="Enter PPT Title here" ID="txtPPTTitle" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="form-group row">
                                                <label class="form-control-label">PPT Speaker</label>
                                                <asp:TextBox class="form-control form-control-sm" placeholder="Enter PPT Speaker Name here" ID="txtPPTDesc" runat="server"></asp:TextBox>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </asp:Panel>

                <div class="form-group">
                    <asp:Button ID="btnSubmit" ValidationGroup="images" OnClick="btnSubmit_Click" CssClass="btn btn-primary" runat="server" Text="Upload Presentations" />
                    <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-success" runat="server" Text="Save Presentations" Visible="false" />
                    <asp:Button ID="btnAddPresentations" OnClick="btnAddPresentations_Click" CssClass="btn btn-primary" runat="server" Text="Add More Presentations to Event" Visible="false" />
                    <a href="<%= Page.ResolveUrl("~") %>App/Events/PostEvent/Default.aspx" class="btn btn-secondary">Go Back to Events</a>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Following Downloadable PPT Files exist-</h3>
            </div>
            <div class="card-body">
                <table id="list" class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>PPT Topic</th>
                            <th>Speaker</th>
                            <th>File</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><%# Eval("Sno") %></th>
                                    <td><%# Eval("pptTitle") %></td>
                                    <td><%# Eval("pptSpeaker") %></td>
                                     <td>
                                         <asp:HyperLink Target="_blank" ID="lnkPPTFile" NavigateUrl='<%# Eval("pptFile") %>' runat="server">View File</asp:HyperLink>
                                    </td>
                                      <td>
                                    <asp:LinkButton ID="lnkDelete" CssClass="btn btn-danger btn-sm" runat="server" OnClientClick='<%# "return getConfirmation(this,\"" + Eval("pptTitle").ToString() + "\");" %>' CommandArgument='<%# Eval("pptID") %>' CommandName="deleterecord">Delete</asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>

                    </tbody>
                </table>
            </div>
        </div>
        <div id="ConfirmationPopup" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Please Confirm Deletion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure you want to delete <strong><span id="spnMsg"></span></strong>&nbsp;PPT? This will delete it from past event website also. This action is irreversible.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="btnConfirm" class="btn btn-danger">Yes, Delete</button>
                </div>
            </div>
        </div>
    </div>

    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
     <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#list').DataTable();
        });
        function getConfirmation(sender, CourseName) {
            $("#spnMsg").text(CourseName);
            $('#ConfirmationPopup').modal('show');
            $('#btnConfirm').attr('onclick', "$('#ConfirmationPopup').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
    <script>
        function ValidateFileCount() {
            var fileCount = document.getElementById('<%= filePresentation.ClientID%>').files.length;
            if (fileCount > 20) {
                alert("Please select only 20 Presentations..!!!");
                return false;
            }
            else if (fileCount <= 0) {
                alert("Please select atleast 1 Presentations..!!!");
                return false;
            }

            return true;  // Good to go
        }
    </script>
</asp:Content>

