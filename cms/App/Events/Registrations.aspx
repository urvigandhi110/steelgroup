﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="Registrations.aspx.cs" Inherits="App_Events_Registrations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
    <style type="text/css">
        table.dataTable {
            border-collapse: collapse !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Event Registrations</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Following is a list of all who have registered for - </h3>
            </div>
            <div class="card-body">
                <table id="list" class="table table-striped table-responsive">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Type</th>
                            <th>Count</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th>Company</th>
                            <th>Emails</th>
                            <th>Mobile</th>
                            <th>Location</th>
                            <th>Interested As</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptList" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><%# Eval("Sno") %></th>
                                    <td><%# Eval("regType") %></td>
                                    <td><%# Eval("numDelegates") %></td>
                                    <td><%# Eval("regName") %></td>
                                    <td><%# Eval("datecreated") %></td>
                                    <td><%#  Eval("regDesignation").ToString() + ", " + Eval("regOrg").ToString() + ", " + Eval("companyType").ToString() + "Company" %></td>
                                    <td><%# "Corp: " + Eval("regEmailOrg").ToString() + "<br>" + "Personal: " + Eval("regEmailPersonal").ToString() %></td>
                                    <td><%# Eval("regMobile") %></td>
                                    <td><%# Eval("regCity").ToString() + ", " + Eval("regState").ToString() + ", " + Eval("regCountry").ToString() %></td>
                                    <td><%# Eval("interestedAs") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#list').DataTable();
        });
     
    </script>
</asp:Content>

