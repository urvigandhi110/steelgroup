﻿<%@ Master Language="C#" AutoEventWireup="true" CodeFile="EventMaster.master.cs" Inherits="EventMaster" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="180x180" href="<%= Page.ResolveUrl("~") %>images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<%= Page.ResolveUrl("~") %>images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<%= Page.ResolveUrl("~") %>images/favicons/favicon-16x16.png">
    <link rel="manifest" href="<%= Page.ResolveUrl("~") %>images/favicons/site.webmanifest">
    <link rel="mask-icon" href="<%= Page.ResolveUrl("~") %>images/favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="<%= Page.ResolveUrl("~") %>images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="msapplication-config" content="<%= Page.ResolveUrl("~") %>images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#000000">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css">
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">--%>
    <script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css" />
    <link href="<%= Page.ResolveUrl("~") %>css/steelgroup.css" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~") %>css/eventpage-alt.css" rel="stylesheet" />
    <asp:ContentPlaceHolder ID="head" runat="server">
    </asp:ContentPlaceHolder>
</head>
<body class="eventpage">
    <form id="eventMaster" runat="server">
        <section id="eventMasterMenu" style="background-image: linear-gradient( rgba(0,0,0,.4), rgba(0,0,0,.4) ), url('<%= hfimageurl.Value %>');">
            <nav class="navbar nav-masthead navbar-expand-md navbar-dark fixed-top">
                <a class="navbar-brand" href='<%= Page.ResolveUrl("~") +  "events/" %>'>
                    <img class="w-50" alt="" src="<%= Page.ResolveUrl("~") %>images/steelgroup-logo-svg-eventspage.svg" />
                    <span>Conferences</span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav">
                        <a class="nav-link" href='<%= Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() %>'><i class="fas fa-home"></i>&nbsp;Home</a>
                        <a class="nav-link" href='<%= Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "/Register" %>'><i class="fas fa-pen-square"></i>&nbsp;Register</a>
                        <a class="nav-link" href='<%= Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "#downloads" %>'><i class="fas fa-file-pdf"></i>&nbsp;Brochure</a>
                        <a class="nav-link" href='<%= Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "#agenda" %>'><i class="fas fa-calendar-alt"></i>&nbsp;Agenda</a>
                        <a class="nav-link" href='<%= Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "#venue" %>'><i class="fas fa-map-marker"></i>&nbsp;Venue</a>
                        <a class="nav-link" href='<%= Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "#speakers" %>'><i class="fas fa-microphone"></i>&nbsp;Speakers</a>
                        <a class="nav-link" href='<%= Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "#sponsors" %>'><i class="fas fa-bullhorn"></i>&nbsp;Sponsors</a>
                    </div>

                </div>
            </nav>
            <div class="eventsmasterdetails">
                <p class="text-center d-block eventdetails">
                    <span class="fas fa-calendar"></span>
                    <span class="mr-4">
                        <asp:HiddenField ID="hfimageurl" runat="server" />
                        <asp:Literal ID="ltrlEventDate" runat="server"></asp:Literal></span>
                    <span class="fas fa-map-marker"></span>
                    <span>
                        <asp:Literal ID="ltrlEventVenue" runat="server"></asp:Literal></span>
                </p>
                <h1 class="cover-heading">
                    <asp:Literal ID="ltrlEventName" runat="server"></asp:Literal></h1>
            </div>
        </section>
        <asp:ContentPlaceHolder ID="mainContent" runat="server">
        </asp:ContentPlaceHolder>
        <a name="contact"></a>
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">Contact us</h3>
                        <p class="sectionsubheading">The Organizers</p>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-12 col-md-4 offset-md-4">
                        <div class="row no-gutters">
                            <div class="col-6 text-center teambox">
                                <img src="<%= Page.ResolveUrl("~") %>images/ajay-sir.jpg" class="rounded-circle m-3" />
                                <h4>Ajay Tambe</h4>
                                <p>CEO &amp; Founder</p>
                                <ul class="usersocial">
                                    <li><a href="mailto:ajay.tambe@steelgroup.co.in"><i class="fas fa-envelope"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/ajay-tambe-steelgroup-b8415920/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                </ul>
                            </div>
                            <div class="col-6 text-center teambox">
                                <img src="<%= Page.ResolveUrl("~") %>images/amit-ji.jpg" class="rounded-circle m-3" />
                                <h4>Amit Purohit</h4>
                                <p>Marketing Head</p>
                                <ul class="usersocial">
                                    <li><a href="mailto:amit@steelgroup.co.in"><i class="fas fa-envelope"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/ap6887/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <a name="venue"></a>
        <section id="venue">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">The Venue</h3>
                        <p class="sectionsubheading">Where to come</p>
                    </div>
                </div>
                <div class="row contactdetails">
                    <div class="col-12 col-md-4">
                        <div class="row no-gutters">
                            <div class="col-4 text-right pr-4">
                                <i class="fas fa-map-marker"></i>
                            </div>
                            <div class="col-8">
                                <h4>Venue</h4>
                                <p>
                                    <asp:Literal ID="ltrlVenueAddress" runat="server"></asp:Literal>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="row no-gutters">
                            <div class="col-4 text-right pr-4">
                                <i class="fas fa-envelope"></i>
                            </div>
                            <div class="col-8">
                                <h4>Email Us</h4>
                                <p>info@steelgroup.co.in</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="row no-gutters">
                            <div class="col-4 text-right pr-4">
                                <i class="fas fa-phone"></i>
                            </div>
                            <div class="col-8">
                                <h4>Call us</h4>
                                <p>+91 9926 744 333</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-12">
                    <div class="gmap">
                        <asp:Literal ID="ltrlVenueMap" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>

        </section>
        <section id="footer">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col">
                        <div class="social">
                            <h2>Follow us</h2>
                            <a href="#">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/facebook.svg" /></a>
                            <a href="#">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/linkedin.svg" /></a>
                            <a href="#">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/instagram.svg" /></a>
                            <a href="#">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/twitter.svg" /></a>
                            <a href="#">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/google.svg" /></a>
                            <a href="#">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/youtube.svg" /></a>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters footerrow">
                    <div class="col">
                        <h4>COMPANY</h4>
                        <a href="#">About us</a>
                        <a href="#">Team</a>
                        <a href="#">Services</a>
                        <a href="#">Privacy Policy</a>
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                    <div class="col">
                        <h4>EVENTS</h4>
                        <a href="#">3rd CRPT</a>
                        <a href="#">2nd CRPT</a>
                        <a href="#">Automotive</a>
                    </div>
                    <div class="col">
                        <h4>NEWS</h4>

                    </div>
                    <div class="col">
                        <img src="<%= Page.ResolveUrl("~") %>images/steel-group-footer-logo.png" class="img-fluid w-75" />
                    </div>
                </div>
                <div class="row no-gutters copyright">
                    <div class="col">
                        <p class="text-center">Copyright &copy; 2018 Steel Group | All Rights Reserved | All logos, symbols &amp; Trademarks belong to their respective owners.</p>
                    </div>
                </div>
            </div>
        </section>
    </form>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <script>
        $(window).scroll(function () {
            var wscroll = $(this).scrollTop();
            if (wscroll > 80) {
                $(".navbar").addClass("shrink-nav-dark animated fadeInDown");
            }
            else {
                $(".navbar").removeClass("shrink-nav-dark");
                $(".navbar").removeClass("animated");
                $(".navbar").removeClass("fadeInDown");
            }
        });
        var loc = window.location.pathname;

        $('.navbar-nav').find('a.nav-link').each(function () {
            if ('/' + $(this).attr('href') == loc) {
                $(this).toggleClass('active');
                return;
            }
        });//active open
    </script>
    <asp:ContentPlaceHolder ID="endScripts" runat="server">
    </asp:ContentPlaceHolder>
</body>
</html>
