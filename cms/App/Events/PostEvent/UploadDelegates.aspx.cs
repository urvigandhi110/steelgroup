﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;

public partial class App_Events_PostEvent_UploadDelegates : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;
    DataSet ds;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand(Session["UploadQuery"].ToString());

            steelcmd.Connection = steelcon;
            steelcon.Open();


            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                Session.Remove("UploadQuery");

                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Delegates data from the excel file has been saved successfully.";

            }

            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Delegates data from the excel file has been saved successfully. Please check the excel file data and try again.";
            }

            steelcon.Close();
            steelcmd.Dispose();
            steelcon.Dispose();
        }
        catch (Exception ex)
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-warning alert-dismissible";
            ltrlmsg.Text = ex.ToString();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string eventID = Request.QueryString["eventID"].ToString();

            TimeZoneInfo IndianTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime IndianDateTime = TimeZoneInfo.ConvertTime(DateTime.Now, IndianTimeZoneInfo);

            Random rnd = new Random();
            string path = Server.MapPath("~/uploads/event/DelegateExcel/" + DateTime.Today.ToString("ddMMyyyy") + "-" + rnd.Next(1000, 9999).ToString() + "-" + fileExcel.FileName);
            fileExcel.SaveAs(path);

            // Connection String to Excel Workbook
            string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);
            OleDbConnection connection = new OleDbConnection();
            connection.ConnectionString = excelConnectionString;
            OleDbCommand command = new OleDbCommand("select * from [Delegates$]", connection);
            connection.Open();
            // Create DbDataReader to Data Worksheet

            OleDbDataAdapter da = new OleDbDataAdapter(command);
            ds = new DataSet();
            da.Fill(ds, "Delegates");
            connection.Close();
            da.Dispose();
            command.Dispose();


            int count = 0;
            string query = "";

            foreach (DataRow row in ds.Tables["Delegates"].Rows)
            {
                if (!(row["email"].ToString() == string.Empty))
                {
                    Random rndstu = new Random();
                    string newDelegateID = "DEL" + IndianDateTime.ToString("ddMMyyyy") + Guid.NewGuid().ToString().Replace("-", "");
                    count += 1;
                    query += "INSERT INTO delegatesLogin(delegateID,delegateEmail,delegatemobile,delegateName,eventID,creationdate,creationby) VALUES('" + newDelegateID + "','" + row["email"].ToString() + "','" + row["mobile"].ToString() + "','" + row["Name"].ToString() + "','" + eventID + "','" + IndianDateTime + "','" + User.Identity.Name + "')";

                }

            }

            lblNumRecords.Text = count.ToString();
            pnlDetails.Visible = true;
            pnlUpload.Enabled = false;
            fileExcel.Enabled = false;
            Session["UploadQuery"] = query;

            if (count == 0)
            {
                btnSave.Visible = false;
                btnSubmit.Visible = true;
                pnlUpload.Enabled = true;
                fileExcel.Enabled = true;
            }
            else {
                btnSave.Visible = true;
                btnSubmit.Visible = false;
            }
            ds.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}