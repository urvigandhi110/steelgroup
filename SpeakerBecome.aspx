﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventMaster.master" AutoEventWireup="true" CodeFile="SpeakerBecome.aspx.cs" Inherits="SpeakerBecome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css" />
    <style>
        .swiperslidersponsor {
            height: 13rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <section class="speakersdetails">
        <div class="row no-gutters py-4">
            <div class="col-12 text-center">
                <h3 class="sectionheading">Call for Speakers</h3>
                <p class="sectionsubheading">Share your thoughts &amp; Vision. Niche Audience.</p>
            </div>
        </div>
        <div class="container pb-5">
            <div class="row no-gutters">
                <div class="col-12 col-md-9 whitebox">
                    <p><strong>Call for Speakers Guidelines</strong></p>
                    <ul>
                        <li>You may submit applications by email at&nbsp;<a href="mailto:ajay.tambe@steelgroup.co.in">ajay.tambe@steelgroup.co.in</a></li>
                        <li>Please make sure you have the appropriate speaker identified prior to submitting your request</li>
                        <li>Speakers are responsible for their own travel expenses</li>
                        <li>Speakers are entitled to a free entrance to&nbsp;<strong><asp:Literal ID="ltrlEventName2" runat="server"></asp:Literal></strong></li>
                        <li>All conference session times and locations will be listed online in the Scheduled Conference section. Please note that the schedule is subject to change</li>
                    </ul>
                    <p><strong>Speaker&rsquo;s Opportunity Benefits and Cost:</strong></p>
                    <ul>
                        <li>All presentations and panel discussions at the conferences will be recorded and made available after the show</li>
                        <li>2 Complimentary delegates for the conference</li>
                        <li>Max 15 minutes to present the paper through PPT/AV</li>
                        <li>Sharing the database of conference participants</li>
                    </ul>
                    <p><strong>Cost: INR 1 Lac + GST</strong></p>
                    <p><strong>Who&rsquo;s the Audience?</strong></p>
                    <p></p>
                    <p>
                        <asp:Literal ID="ltrlEventName" runat="server"></asp:Literal>
                        will attract the following audience:
                    </p>
                    <ul>
                        <li>Steel producers</li>
                        <li>Equipment &amp; technology suppliers</li>
                        <li>Paint companies</li>
                        <li>EPC contractors and consultants</li>
                        <li>Automation &amp; instrument suppliers</li>
                        <li>Project managers and consultants</li>
                        <li>Business analysts</li>
                        <li>Investors</li>
                        <li>Govt organizations and associations</li>
                        <li>Industry vendors and suppliers</li>
                        <li>Business angels</li>
                        <li>Marketing, media and communications firms</li>
                        <li>Legal firms</li>
                        <li>Financial institutions, accountancy and consultancy firms</li>
                        <li>B2B I.T. and software providers</li>
                        <li>Social media innovators</li>
                    </ul>
                </div>
                <div class="col-12 col-md-3 px-3">
                    <h6 class="sectionsubheading font-weight-bold text-center">Our Sponsors</h6>
                    <div class="swiper-container swiperslidersponsor swiper-container-sponsors">
                        <div class="swiper-wrapper">
                            <asp:Repeater ID="rptSponsorAds" runat="server">
                                <ItemTemplate>
                                    <div class="swiper-slide text-center">
                                        <a target="_blank" class="text-dark" href='<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(Eval("eventSponsorName").ToString()) %>'>
                                            <small class="text-dark font-weight-bold mb-2"><%# Eval("eventSponsorType") %> Sponsor</small><br />
                                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="swiper-pagination swiper-pagination-sponsors"></div>
                    </div>
                    <h6 class="sectionsubheading font-weight-bold text-center my-3">Useful Downloads</h6>
                    <asp:HyperLink ID="lnkBrochureSidebar" runat="server" class="text-center text-dark">
                        <asp:Image ID="imgBrochure" runat="server" CssClass="img-fluid mb-3" /><br />
                        <p class="font-weight-bold">Download Brochure</p>
                    </asp:HyperLink>

                    <asp:HyperLink ID="lnkAgendaSidebar" runat="server" class="text-center text-dark">
                        <asp:Image ID="imgAgenda" runat="server" CssClass="img-fluid mb-3" /><br />
                        <p class="font-weight-bold">Download Agenda</p>
                    </asp:HyperLink>

                </div>
            </div>

        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
    <script>
        $(document).ready(function () {
            //initialize swiper when document ready
            var mySwiper = new Swiper('.swiper-container-sponsors', {
                slidesPerView: 1,
                autoplay: {
                    delay: 5000,
                },
                pagination: {
                    el: '.swiper-pagination-sponsors',
                    clickable: true,
                }
            })
        });
    </script>
</asp:Content>

