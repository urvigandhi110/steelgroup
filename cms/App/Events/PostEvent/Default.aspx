﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="App_Events_PostEvent_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
    <style type="text/css">
        table.dataTable {
            border-collapse: collapse !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Manage Post Event Details</h2>
    <small>Only past events are displayed here</small>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Following list shows all events that have been executed-</h3>
            </div>
            <div class="card-body">
                <table id="list" class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Event Name</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Venue</th>
                            <th>Publish</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptList" OnItemCommand="rptList_ItemCommand" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><%# Eval("Sno") %></th>
                                    <td><%# Eval("eventName") %></td>
                                    <td><%# Convert.ToDateTime(Eval("eventStartDate").ToString()).ToString("dd/MM/yyyy") %></td>
                                    <td><%# Convert.ToDateTime(Eval("eventEndDate").ToString()).ToString("dd/MM/yyyy") %></td>
                                    <td><%# Eval("eventVenue") %></td>
                                    <td><%# Eval("publish") %></td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Manage
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <asp:LinkButton ID="lnkDelegates" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="adddelegates">Upload Delegates</asp:LinkButton>
                                                <asp:LinkButton ID="lnkPPT" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="addppt">Add PPTs</asp:LinkButton>
                                                <asp:LinkButton ID="lnkSpeakerPPT" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="speakerppt">Speaker PPTs</asp:LinkButton>
                                                <asp:LinkButton ID="lnkGallery" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="addphotos">Add Photos</asp:LinkButton>
                                                <asp:LinkButton ID="lnkAddContent" CssClass="dropdown-item" runat="server" CommandArgument='<%# Eval("eventID") %>' CommandName="addcontent">Add Text Content</asp:LinkButton>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#list').DataTable();
        });
        function getConfirmation(sender, CourseName) {
            $("#spnMsg").text(CourseName);
            $('#ConfirmationPopup').modal('show');
            $('#btnConfirm').attr('onclick', "$('#ConfirmationPopup').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            return false;
        }
    </script>
</asp:Content>

