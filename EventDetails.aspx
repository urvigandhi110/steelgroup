﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventDetails.aspx.cs" Inherits="EventDetails" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <title></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="180x180" href="<%= Page.ResolveUrl("~") %>images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<%= Page.ResolveUrl("~") %>images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<%= Page.ResolveUrl("~") %>images/favicons/favicon-16x16.png">
    <link rel="manifest" href="<%= Page.ResolveUrl("~") %>images/favicons/site.webmanifest">
    <link rel="mask-icon" href="<%= Page.ResolveUrl("~") %>images/favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="<%= Page.ResolveUrl("~") %>images/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="msapplication-config" content="<%= Page.ResolveUrl("~") %>images/favicons/browserconfig.xml">
    <meta name="theme-color" content="#000000">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.2/js/all.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700|Oswald:300,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css" />
    <link href="<%= Page.ResolveUrl("~") %>css/steelgroup.css" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~") %>css/eventpage-alt.css?v=14032018" rel="stylesheet" />
    <script defer src="https://use.fontawesome.com/releases/v5.0.2/js/v4-shims.js"></script>
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
</head>
<body class="eventpage">
    <form id="frmEvents" runat="server">
        <section id="menu">
            <nav class="navbar nav-masthead navbar-expand-md navbar-dark fixed-top">
                <a class="navbar-brand" href='<%= Page.ResolveUrl("~") +  "events/" %>'>
                    <img class="w-50" alt="" src="<%= Page.ResolveUrl("~") %>images/steelgroup-logo-svg-eventspage.svg" />
                    <span>Conferences</span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav">
                        <a class="nav-link" href='<%= Page.RouteData.Values["eventname"].ToString() %>'><i class="fas fa-home"></i>&nbsp;Home</a>
                        <a class="nav-link" href='<%= Page.RouteData.Values["eventname"].ToString() + "/Register" %>'><i class="fas fa-pen-square"></i>&nbsp;Register</a>
                        <a class="nav-link" href='<%= Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "/Online-Payment" %>'><i class="fas fa-credit-card"></i>&nbsp;Online Payment</a>
                        <a class="nav-link" href='<%= Page.RouteData.Values["eventname"].ToString() + "#downloads" %>'><i class="fas fa-file-pdf"></i>&nbsp;Brochure</a>
                        <a class="nav-link" href='<%= Page.RouteData.Values["eventname"].ToString() + "#agenda" %>'><i class="fas fa-calendar-alt"></i>&nbsp;Agenda</a>
                        <a class="nav-link" href='<%= Page.RouteData.Values["eventname"].ToString() + "#venue" %>'><i class="fas fa-map-marker"></i>&nbsp;Venue</a>
                        <a class="nav-link" href='<%= Page.RouteData.Values["eventname"].ToString() + "#speakers" %>'><i class="fas fa-microphone"></i>&nbsp;Speakers</a>
                        <a class="nav-link" href='<%= Page.RouteData.Values["eventname"].ToString() + "#sponsors" %>'><i class="fas fa-bullhorn"></i>&nbsp;Sponsors</a>

                    </div>

                </div>
            </nav>
        </section>
        <section id="slider">
            <div class="swiper-container swiper-slider swiper-container-mainslider">
                <div class="swiper-wrapper">
                    <asp:Repeater ID="rptEventSlider" runat="server">
                        <ItemTemplate>
                            <div class="swiper-slide">
                                <img class="img-fluid" src='<%# Page.ResolveUrl("~/cms") + Eval("eventImage").ToString().Replace("~/","/") + "?w=1920&h=1200&mode=crop" %>' />
                                <%--<img class="img-fluid" src='<%# Eval("eventImage").ToString().Replace("~/","http://localhost/steeladmin/") + "?w=1920&h=1200&mode=crop" %>' />--%>
                                <div class="caption">
                                    <div class="slidersocial">
                                        <a href="https://www.facebook.com/steelgroupindia/" target="_blank">
                                            <span class="fab fa-facebook"></span>
                                        </a>
                                        <a href="https://www.linkedin.com/showcase/steelgroup-conferences/" target="_blank">
                                            <span class="fab fa-linkedin"></span>
                                        </a>
                                        <a href="#">
                                            <span class="fab fa-twitter"></span>
                                        </a>
                                    </div>
                                    <p class="text-center d-block eventdetails">
                                        <span class="fas fa-calendar"></span>
                                        <span class="mr-4"><%# DateTime.Parse(Eval("eventStartDate").ToString()).ToString("MMMM dd, yyyy") %></span>
                                        <span class="fas fa-map-marker"></span>
                                        <span><%# Eval("eventVenue") %></span>
                                    </p>
                                    <h2 class="cover-heading"><%# Eval("eventName") %></h2>
                                    <p class="lead mt-3">
                                        <a href='<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/Register" %>' class="btn btn-lg btn-info">Register Now</a>
                                    </p>
                                </div>

                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev swiper-button-prev-mainslider"></div>
                <div class="swiper-button-next swiper-button-next-mainslider"></div>
            </div>
        </section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 col-12"></div>
                <div class="col-md-8 col-12">
                    <div class="highlights highlights-dark">

                        <div class="row no-gutters">
                            <div class="col-12 timer text-center">
                                <div id="days"></div>
                                <div id="hours"></div>
                                <div id="minutes"></div>
                                <div id="seconds"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-12"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <section id="timer">
            <div class="col-12 col-md-6 offset-md-3">
                <div class="row no-gutters">
                    <div class="col-6 col-md-3 highlightsingle">
                        <img src="<%= Page.ResolveUrl("~") %>images/eventicons-alt/companies.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                        <h4>
                            <asp:Literal ID="ltrlCompanies" runat="server"></asp:Literal></h4>
                        <p>Companies</p>
                    </div>
                    <div class="col-6 col-md-3 highlightsingle">
                        <img src="<%= Page.ResolveUrl("~") %>images/eventicons-alt/delegates.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                        <h4>
                            <asp:Literal ID="ltrlDelegates" runat="server"></asp:Literal></h4>
                        <p>Delegates</p>
                    </div>
                    <div class="col-6 col-md-3 highlightsingle">
                        <img src="<%= Page.ResolveUrl("~") %>images/eventicons-alt/panelists.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                        <h4>
                            <asp:Literal ID="ltrlPanelists" runat="server"></asp:Literal></h4>
                        <p>Panelists</p>
                    </div>
                    <div class="col-6 col-md-3 highlightsingle">
                        <img src="<%= Page.ResolveUrl("~") %>images/eventicons-alt/exhibitors.svg" alt="" class="img-fluid w-25 d-block mx-auto" />
                        <h4>
                            <asp:Literal ID="ltrlExhibitors" runat="server"></asp:Literal></h4>
                        <p>Exhibitors</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="aboutevent">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">About Conference</h3>
                        <p class="sectionsubheading">What is it All About</p>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-5 text-center">
                            <!-- Large modal -->
                            <h5 class="font-weight-bold">Conference Promo</h5>
                            <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
                                <img src="<%= Page.ResolveUrl("~") %>images/videobg.jpg" class="img-fluid" />
                            </a>

                            <div class="modal fade bd-example-modal-lg videomodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Conference Video</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <asp:Literal ID="ltrlVideoURL" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-7 abouttabs">

                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-1-tab" data-toggle="pill" href="#pills-1" role="tab" aria-controls="pills-1" aria-selected="true">About Event</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-2-tab" data-toggle="pill" href="#pills-2" role="tab" aria-controls="pills-2" aria-selected="false">Past Event Summary</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-3-tab" data-toggle="pill" href="#pills-3" role="tab" aria-controls="pills-3" aria-selected="false">About Steel Group</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-1" role="tabpanel" aria-labelledby="pills-1-tab">
                                    <asp:Literal ID="ltrlEventDesc" runat="server"></asp:Literal>
                                    <br class="py-1" />
                                    <asp:HyperLink ID="lnkDownloadBrochure" CssClass="btn btn-default eventbtn mt-2" runat="server">Download Brochure</asp:HyperLink>
                                </div>
                                <div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-2-tab">
                                    <asp:Literal ID="ltrlEventDescOld" runat="server"></asp:Literal>
                                </div>
                                <div class="tab-pane fade" id="pills-3" role="tabpanel" aria-labelledby="pills-3-tab">
                                    <p class="w-100 text-justify">
                                        Steelgroup.co.in is now a reputed and popular name amongst the Steel industry and serving to Steel industry as one of the leading information and networking platforms. We organize focused subject driven conferences for steel industry especially into Steel Flat products. The company was founded in 2010 by Mr. Ajay Tambe who is the Founder &amp; the CEO.
                                    </p>
                                    <p class="w-100 text-justify">
                                        Moreover, Steelgroup keep its members and subscribers updated with the e-newsletters and current affairs of the market, being circulated to over 15,000 industry professionals.
                                    </p>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="shareevent" runat="server">
            <div class="row no-gutters">
                <div class="col-12 text-center">
                    <h3 class="sectionheading text-white">Share This Event</h3>
                </div>
            </div>
            <div class="container">
                <div class="row no-gutters pt-5">
                    <div class="col-12 col-md-5 mb-5 mb-md-0 text-center text-md-left">
                        <h6 class="text-white">Share to a Friend on Email/Social Media</h6>
                        <div id="shareeventbuttons" style="font-size: 12px"></div>
                        <small class="text-muted"><%= hfHashTags.Value!=""?"#"+hfHashTags.Value.Replace(","," #"):"" %></small>
                    </div>
                    <div class="col-12 col-md-7  text-center text-md-left">
                        <h6 class="text-white">Invite a Friend/Colleague</h6>
                        <asp:ScriptManager ID="scrptInvite" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="updInvite" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnlInviteForm" runat="server">
                                    <div class="form-group row">
                                        <div class="col-12 col-md-3 mb-3 mb-md-0">
                                            <asp:TextBox ID="txtYourName" MaxLength="50" placeholder="Your Name" CssClass="form-control form-control-sm" runat="server" required></asp:TextBox>
                                        </div>
                                        <div class="col-12 col-md-3 mb-3 mb-md-0">
                                            <asp:TextBox ID="txtName" MaxLength="50" placeholder="Friend's Name" CssClass="form-control form-control-sm" runat="server" required></asp:TextBox>
                                        </div>
                                        <div class="col-12 col-md-3 mb-3 mb-md-0">
                                            <asp:TextBox ID="txtEmail" MaxLength="50" type="email" placeholder="Friend's Email ID" CssClass="form-control form-control-sm" runat="server" required></asp:TextBox>
                                        </div>
                                        <div class="col-12 col-md-3 mb-3 mb-md-0">
                                            <asp:TextBox ID="txtMobile" type="tel" MaxLength="15" placeholder="Friend's Mobile" CssClass="form-control form-control-sm" runat="server" required></asp:TextBox>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 text-center">
                                            <asp:Button ID="btnInvite" OnClick="btnInvite_Click" CssClass="btn btn-success" runat="server" Text="Invite Friend" />
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlInviteMessage" CssClass="text-white" Visible="false" runat="server">
                                    Thank you for inviting your friend. We will get back to him to join us in this event.
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </section>
        <a name="agenda"></a>
        <section id="agenda pb-0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center pt-1 pt-md-5 mt-2">
                        <h3 class="sectionheading">Conference Agenda</h3>
                        <p class="sectionsubheading">Do not miss a Topic</p>
                    </div>
                </div>
                <div class="row bg-light mt-3 py-5">
                    <div class="col-12 col-md-8 offset-md-2">
                        <div class="agendatabs">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <asp:Repeater ID="rptAgendaDaysTab" runat="server">
                                        <ItemTemplate>
                                            <a class="nav-item nav-link <%# Container.ItemIndex==0?"active":"" %>" id='<%# "nav-day-tab-" + (Container.ItemIndex+1).ToString() %>' data-toggle="tab" href='<%# "#nav-day-" + (Container.ItemIndex+1).ToString() %>' role="tab" aria-controls='<%# "#nav-day-" + (Container.ItemIndex+1).ToString() %>' aria-selected="<%# Container.ItemIndex==0?"true":"false" %>">Day-<%# Container.ItemIndex+1 %></a>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <asp:Repeater ID="rptAgendaMainTabBody" runat="server">
                                    <ItemTemplate>
                                        <div class="tab-pane fade <%# Container.ItemIndex==0?"show active":"" %>" id='<%# "nav-day-" + (Container.ItemIndex+1).ToString() %>' role="tabpanel" aria-labelledby='<%# "nav-day-tab-" + (Container.ItemIndex+1).ToString() %>'>
                                            <asp:HiddenField ID="hfday" Value='<%# DateTime.Parse(Eval("eventAgendaDate").ToString()).ToString("yyyy-MM-dd mm:mm:ss.fff") %>' runat="server" />
                                            <asp:Repeater ID="rptAgenda" runat="server">
                                                <ItemTemplate>
                                                    <div class="row no-gutters agendaitem">
                                                        <div class="col-12 col-md-3 text-center py-3">
                                                            <img src="<%= Page.ResolveUrl("~") %>images/agenda/reg-image.svg" class="rounded-circle agendaimg" />
                                                        </div>
                                                        <div class="col-12 col-md-9 py-3 px-1">
                                                            <div class="agendadetails">
                                                                <span><%# Eval("eventAgendaTime") %></span>
                                                                <h4><%# Eval("eventAgendaTitle") %></h4>
                                                                <p><%# HttpUtility.HtmlDecode(Eval("eventAgendaText").ToString()) %></p>
                                                            </div>
                                                        </div>
                                                        <hr class="offset-md-4 w-50 border-secondary" />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </div>

                        </div>
                    </div>
                    <div class="w-100 d-block py-2 text-center">
                        <asp:HyperLink ID="lnkDownloadAgenda" CssClass="btn btn-default eventbtn" runat="server">Download Agenda</asp:HyperLink>
                    </div>
                </div>
            </div>

        </section>
        <a name="downloads"></a>
        <section id="downloads" runat="server">
            <div class="row no-gutters">
                <div class="col-12 text-center">
                    <h3 class="sectionheading text-white">Downloads</h3>
                </div>
            </div>
            <div class="col-12 col-md-10 offset-md-1">
                <div class="row no-gutters pt-5">
                    <div class="col-6 col-md-2 highlightsingle text-center offset-md-1">
                        <asp:HyperLink ID="lnkRegform" runat="server" Target="_blank">
                            <i class="fas fa-file-alt"></i>
                            <h4>Registration Form</h4>
                        </asp:HyperLink>
                    </div>
                    <div class="col-6 col-md-2 highlightsingle text-center">
                        <asp:HyperLink ID="lnkSponsorshipForm" runat="server" Target="_blank">
                            <i class="fas fa-bullhorn"></i>
                            <h4>Sponsorship Form</h4>
                        </asp:HyperLink>
                    </div>
                    <div class="col-6 col-md-2 highlightsingle text-center">
                        <asp:HyperLink ID="lnkBrochure" runat="server" Target="_blank">
                            <i class="fas fa-book"></i>
                            <h4>Brochure</h4>
                        </asp:HyperLink>
                    </div>
                    <div class="col-6 col-md-2 highlightsingle text-center">
                        <asp:HyperLink ID="lnkAgenda" runat="server" Target="_blank">
                            <i class="fas fa-calendar-alt"></i>
                            <h4>Agenda</h4>
                        </asp:HyperLink>
                    </div>
                    <div class="col-6 col-md-2 highlightsingle text-center">
                        <asp:HyperLink ID="lnkLayout" runat="server" Target="_blank">
                            <i class="fas fa-map"></i>
                            <h4>Layout</h4>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </section>
        <a name="speakers"></a>
        <section id="speakers">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">Speakers</h3>
                        <p class="sectionsubheading">Know Who's Saying What</p>
                    </div>
                </div>
                <asp:Panel ID="pnlTopSpeaker" CssClass="row mb-4" runat="server">
                    <div class="col-12 col-md-10 offset-md-1">
                        <ul class="list-unstyled speakerslist">
                            <li class="media bg-light">
                                <asp:Image ID="imgTopSpeaker" CssClass="mr-4" runat="server" />
                                <div class="media-body">
                                    <h5 class="mt-0 mb-1">
                                        <asp:Literal ID="ltrlTopSpeakerName" runat="server"></asp:Literal><span class="badge badge-pill badge-dark font-weight-light small float-right"><asp:Literal ID="ltrlTopSpeakerType" runat="server"></asp:Literal></span></h5>
                                    <p class="font-italic text-muted mb-2">
                                        <asp:Literal ID="ltrlTopSpeakerDetails" runat="server"></asp:Literal>
                                    </p>
                                    <div class="clearfix"></div>
                                    <p class="desc">
                                        <asp:Literal ID="ltrlTopSpeakerIntro" runat="server"></asp:Literal>
                                    </p>
                                    <div class="clearfix"></div>
                                    <asp:HyperLink ID="lnkSpeakerLinkedIn" runat="server" CssClass="speakerlinks" Target="_blank">
                                        <div class="text-center">
                                            <span class="fab fa-linkedin"></span>
                                        </div>
                                        LinkedIn
                                    </asp:HyperLink>
                                    <asp:HyperLink ID="lnkSpeakerTwitter" runat="server" CssClass="speakerlinks" Target="_blank">
                                        <div class="text-center">
                                            <span class="fab fa-twitter"></span>
                                        </div>
                                        Twitter
                                    </asp:HyperLink>
                                    <asp:HyperLink ID="lnkSpeakerDetails" runat="server" CssClass="speakerlinks" Target="_blank">
                                          <div class="text-center">
                                            <span class="far fa-user"></span>
                                        </div>
                                        Details
                                    </asp:HyperLink>

                                </div>
                            </li>
                        </ul>
                    </div>
                </asp:Panel>
                <div class="row no-gutters justify-content-center">
                    <asp:Repeater ID="rptEventSpeaker" runat="server">
                        <ItemTemplate>
                            <div class="col-12 col-md-3 text-center speakerdetails mb-4">
                                <div class="imgblur rounded-circle  w-75 mx-auto">
                                    <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSpeakerPhoto").ToString().Replace("~/","/") + "?w=500&h=500&mode=crop&scale=both" %>' alt="" class="img-fluid d-block mx-auto rounded-circle" />
                                </div>
                                <a href="<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/speakers#" + SteelFunc.GenerateUrl(Eval("eventSpeakerName").ToString()) %>" class="btn btn-sm btn-outline-light detailsbtn">View Details</a>
                                <h4><%# Eval("eventSpeakerName") %></h4>
                                <p class="badge badge-pill"><%# Eval("eventSpeakerOrg").ToString() + " | " + Eval("eventSpeakerDesignation").ToString() + " | " + Eval("eventSpeakerCountry").ToString() %></p>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                </div>
                <div class="clearfix"></div>
                <div class="row justify-content-center mt-4">
                    <a href="<%= Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/speakers" %>" class="btn btn-default eventbtn mr-3">View All Speakers</a>
                    <a href="<%= Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/become-a-speaker" %>" class="btn btn-default eventbtn">Become a Speaker</a>
                </div>
            </div>
        </section>
        <a name="sponsors"></a>
        <section id="principalsponsors" runat="server">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">Principal Sponsors</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-10 offset-md-1">
                        <div class="row justify-content-center">
                            <asp:Repeater ID="rptPrincipal" runat="server">
                                <ItemTemplate>
                                    <div class="col-12 col-md-4 mt-5">
                                        <a target="_blank" class="sponsorinfolink" href='<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(Eval("eventSponsorName").ToString()) %>'>
                                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="platinumsponsors" runat="server">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">Platinum Sponsors</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-10 offset-md-1">
                        <div class="row justify-content-center">
                            <asp:Repeater ID="rptPlatinum" runat="server">
                                <ItemTemplate>
                                    <div class="col-12 col-md-4 mt-5">
                                        <a target="_blank" class="sponsorinfolink" href='<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(Eval("eventSponsorName").ToString()) %>'>
                                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section id="goldsponsors" runat="server">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">Gold Sponsors</h3>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <asp:Repeater ID="rptGold" runat="server">
                        <ItemTemplate>
                            <div class="col-6 col-md-3 mt-5">
                                <a target="_blank" class="sponsorinfolink" href='<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(Eval("eventSponsorName").ToString()) %>'>
                                    <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                                </a>

                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section id="silversponsors" runat="server">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">Silver Sponsors</h3>
                    </div>
                </div>
                <div class="row justify-content-center">

                    <asp:Repeater ID="rptSilver" runat="server">
                        <ItemTemplate>
                            <div class="col-6 col-md-n mt-5">
                                <div>
                                    <a target="_blank" class="sponsorinfolink" href='<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(Eval("eventSponsorName").ToString()) %>'>
                                        <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                                    </a>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section id="associatesponsors" runat="server">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">Associate Sponsors</h3>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <asp:Repeater ID="rptAssociate" runat="server">
                        <ItemTemplate>
                            <div class="col-6 col-md-2 mt-5">
                                <a class="sponsorinfolink" target="_blank" href='<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(Eval("eventSponsorName").ToString()) %>'>
                                    <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid d-block" />
                                </a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section id="clubbedsponsors" runat="server">
            <div class="container-fluid">
                <div class="row justify-content-center">

                    <asp:Panel ID="pnlNetworkingLunch" Visible="false" CssClass="col-6 col-md-m mt-5" runat="server">
                        <h6>Networking Lunch</h6>
                        <asp:HyperLink Target="_blank" ID="lnkNetworkingLunch" runat="server" CssClass="text-center text-dark d-block">
                            <asp:Image ID="imgNetworkingLunch" CssClass="img-fluid d-block" runat="server" />
                        </asp:HyperLink>
                    </asp:Panel>

                    <asp:Panel ID="pnlNetworkingDinner" Visible="false" CssClass="col-6 col-md-m mt-5" runat="server">
                        <h6>Networking Dinner</h6>
                        <asp:HyperLink Target="_blank" ID="lnkNetworkingDinner" runat="server" CssClass="text-center text-dark d-block">
                            <asp:Image ID="imgNetworkingDinner" CssClass="img-fluid d-block" runat="server" />
                        </asp:HyperLink>
                    </asp:Panel>
                    <asp:Panel ID="pnlDelegateKit" Visible="false" CssClass="col-6 col-md-m mt-5" runat="server">
                        <h6>Delegate Kit Sponsor</h6>
                        <asp:HyperLink Target="_blank" ID="lnkDelegateKit" runat="server" CssClass="text-center text-dark d-block">
                            <asp:Image ID="imgDelegateKit" CssClass="img-fluid d-block" runat="server" />
                        </asp:HyperLink>
                    </asp:Panel>
                    <asp:Repeater ID="rptCompanyHighlight" runat="server">
                        <ItemTemplate>
                            <div class="col-6 col-md-m mt-5">
                                <h6><%# Container.ItemIndex==0?"Company Highlight":"<br><br>" %></h6>
                                <a target="_blank" href='<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(Eval("eventSponsorName").ToString()) %>' class="text-center text-dark d-block">
                                    <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid d-block" /></a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </section>
        <section id="othersponsors" runat="server">
            <div class="row no-gutters">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Exhibitors</h3>
                </div>
            </div>
            <div class="container">
                <div class="row no-gutters justify-content-center">
                    <asp:Repeater ID="rptExhibitors" runat="server">
                        <ItemTemplate>
                            <div class="col-6 col-md-2 mt-5">
                                <a target="_blank" href='<%# Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors#" + SteelFunc.GenerateUrl(Eval("eventSponsorName").ToString()) %>' class="text-center text-dark d-block">
                                    <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                                </a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </section>
        <section id="partners" runat="server">
            <div class="row no-gutters">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Partners</h3>
                </div>
            </div>
            <div class="container">
                <div class="row no-gutters justify-content-center">
                    <asp:Repeater ID="rptPartners" runat="server">
                        <ItemTemplate>
                            <div class="col-6 col-md-m">
                                <a target="_blank" href='<%# Eval("eventSponsorURL") %>'><%# Eval("eventSponsorType") %>
                                    <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=270&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                                </a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

            </div>
        </section>

        <section id="organizers">
            <div class="container">
                <div class="row no-gutters justify-content-center">
                    <div class="col-12 col-md-6 text-center text-md-right pr-3" id="organizedby" runat="server">
                        <h3 class="sectionheading">Organized By</h3>
                        <img src="<%= Page.ResolveUrl("~") %>images/organizer-steel-group.jpg" />
                    </div>
                    <div class="col-12 col-md-6 text-center text-md-left pl-3" id="association" runat="server">
                        <h3 class="sectionheading">In Association with</h3>
                        <asp:HyperLink ID="lnkAssociation" runat="server" Target="_blank">
                            <asp:Image ID="imgassociation" runat="server" />
                        </asp:HyperLink>
                    </div>
                </div>
            </div>

        </section>

        <section class="w-100 pt-2 mb-5 pb-2 text-center d-block">

            <div class="row justify-content-center mt-4 no-gutters">
                <a href="<%= Page.ResolveUrl("~") +  "events/" + Page.RouteData.Values["eventname"].ToString() + "/sponsors" %>" class="btn btn-default eventbtn mr-md-3">View All Sponsors</a>
                <asp:HyperLink ID="lnkDownloadSponsorship" CssClass="btn btn-default eventbtn mt-3 mt-md-0" runat="server">Check Sponsorship Benefits</asp:HyperLink>
            </div>

        </section>
        <section id="participating" runat="server">
            <div class="row no-gutters">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Participating Companies</h3>
                </div>
            </div>
            <div class="swiper-container swiper-container-participating">
                <div class="swiper-wrapper swiper-wrapper-participating">
                    <asp:Repeater ID="rptParticipating" runat="server">
                        <ItemTemplate>
                            <div class="swiper-slide">
                                <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventSponsorLogo").ToString().Replace("~/","/") + "?w=500&h=300&mode=pad&scale=both&bgcolor=ffffff" %>' alt="" class="img-fluid" />
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="swiper-pagination swiper-pagination-participating"></div>
            </div>
            <div class="w-100 d-block py-2 text-center">
                <asp:HyperLink ID="lnkParticipatingDownload" CssClass="btn btn-default eventbtn" runat="server">Download List of Participants</asp:HyperLink>
            </div>
        </section>
        <section id="layout">
            <div class="row no-gutters">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">Exhibition Layout</h3>
                    <p class="sectionsubheading">Space for Everyone</p>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-12">
                    <asp:Image ID="imgLayout" CssClass="img-fluid" runat="server" />
                    <div class="w-100 d-block pt-2 mb-5 text-center">
                        <asp:HyperLink ID="lnkDownloadLayout" CssClass="btn btn-default eventbtn" runat="server">Download Layout</asp:HyperLink>
                    </div>
                </div>
            </div>
        </section>
        <a name="blog"></a>
        <section id="blogposts" runat="server">
            <div class="row no-gutters">
                <div class="col-12 text-center">
                    <h3 class="sectionheading">From the Blog</h3>
                    <p class="sectionsubheading">Where we put our ideas</p>
                </div>
            </div>
            <div class="container">
                <div class="row justify-content-center">
                    <asp:Repeater ID="rptBlogposts" runat="server">
                        <ItemTemplate>
                            <div class="col-12 col-md-6 mt-4">
                                <a href='<%# Eval("posturl") %>' target="_blank" class="visitblog">
                                    <div class="blogpost">
                                        <div class="imgthumb">
                                            <asp:Image ID="imgfeatured" CssClass="img-fluid p-2" ImageUrl='<%# Eval("thumbnail").ToString().Replace("s72-c","s200-c") %>' runat="server" />
                                        </div>
                                        <div class="postdetails px-3">
                                            <small><%# Eval("publishdate") %></small>
                                            <h5><%# Eval("title") %></h5>
                                            <p><%# Eval("description") %></p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="clearfix"></div>
                <div class="d-block text-center mt-5">
                    <a href="http://blog.steelgroup.co.in" target="_blank" class="btn btn-dark">Visit Steel Group Blog</a>
                </div>
            </div>
        </section>
        <a name="contact"></a>
        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">Contact us</h3>
                        <p class="sectionsubheading">The Organizing Team</p>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-12 col-md-8 offset-md-2">
                        <div class="row no-gutters">
                            <div class="col-12 col-md-4 text-center teambox">
                                <img src="<%= Page.ResolveUrl("~") %>images/team/ajay-team.jpg" class="rounded-circle m-3" />
                                <h4>Ajay Tambe</h4>
                                <p>Founder &amp; CEO</p>
                                <ul class="usersocial">
                                    <li><a href="mailto:ajay.tambe@steelgroup.co.in" target="_blank"><i class="fas fa-envelope"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/ajay-tambe-steelgroup-b8415920/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="https://api.whatsapp.com/send?phone=919322199557" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 text-center teambox">
                                <img src="<%= Page.ResolveUrl("~") %>images/team/amit-team.jpg" class="rounded-circle m-3" />
                                <h4>Amit Purohit</h4>
                                <p>Manager - Marketing</p>
                                <ul class="usersocial">
                                    <li><a href="mailto:amit@steelgroup.co.in" target="_blank"><i class="fas fa-envelope"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/ap6887/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                                    <li><a href="https://twitter.com/amit6887" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="https://api.whatsapp.com/send?phone=919926744333" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                                </ul>
                            </div>
                            <div class="col-12 col-md-4 text-center teambox">
                                <img src="<%= Page.ResolveUrl("~") %>images/team/dipti-team.jpg" class="rounded-circle m-3" />
                                <h4>Dipti Mistry</h4>
                                <p>Executive - Marketing</p>
                                <ul class="usersocial">
                                    <li><a href="mailto:dipti@steelgroup.co.in" target="_blank"><i class="fas fa-envelope"></i></a></li>
                                    <li><a href="https://www.linkedin.com/in/dipti-dhon-59bb62155/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <a name="venue"></a>
        <section id="venue">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h3 class="sectionheading">The Venue</h3>
                        <p class="sectionsubheading">Where to come</p>
                    </div>
                </div>
                <div class="row contactdetails" id="contactdetails" runat="server">
                    <div class="col-12 col-md-4 mb-2 mb-md-0">
                        <div class="row no-gutters">
                            <div class="col-4 text-right pr-4">
                                <i class="fas fa-map-marker"></i>
                            </div>
                            <div class="col-8">
                                <h4>Venue</h4>
                                <p>
                                    <asp:Literal ID="ltrlVenueAddress" runat="server"></asp:Literal>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-2 mb-md-0">
                        <div class="row no-gutters">
                            <div class="col-4 text-right pr-4">
                                <i class="fas fa-envelope"></i>
                            </div>
                            <div class="col-8">
                                <h4>Email Us</h4>
                                <p>info@steelgroup.co.in</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-2 mb-md-0">
                        <div class="row no-gutters">
                            <div class="col-4 text-right pr-4">
                                <i class="fas fa-phone"></i>
                            </div>
                            <div class="col-8">
                                <h4>Call us</h4>
                                <p>+91 9926 744 333</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-12">
                    <div class="gmap">
                        <asp:Literal ID="ltrlVenueMap" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>

        </section>
        <section id="footer">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col">
                        <div class="social">
                            <h2>Follow us</h2>
                            <a href="https://www.facebook.com/steelgroupindia" target="_blank">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/facebook.svg" /></a>
                            <a href="https://www.linkedin.com/showcase/steelgroup-conferences/" target="_blank">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/linkedin.svg" /></a>
                            <a href="#" target="_blank">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/instagram.svg" /></a>
                            <a href="#" target="_blank">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/twitter.svg" /></a>
                            <a href="http://blog.steelgroup.co.in" target="_blank">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/blogger.svg" /></a>
                            <a href="https://www.youtube.com/channel/UCu23dnoZijWfyKRctkJvGMQ" target="_blank">
                                <img src="<%= Page.ResolveUrl("~") %>images/social/youtube.svg" /></a>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters footerrow">
                    <div class="col-6 col-md-3">
                        <h4>COMPANY</h4>
                        <a href="<%= Page.ResolveUrl("~") %>about.aspx">About us</a>
                        <a href="<%= Page.ResolveUrl("~") %>events">Events</a>
                        <a href="<%= Page.ResolveUrl("~") %>news">News</a>
                        <a href="#">Steel Jobs</a>
                        <a href="<%= Page.ResolveUrl("~") %>Event-Terms.aspx">Terms &amp; Conditions</a>
                    </div>
                    <div class="col-6 col-md-3">
                        <h4>ENGAGE</h4>
                        <a href="<%= Page.ResolveUrl("~") %>Subscribe.aspx">Subscribe</a>
                        <a href="<%= Page.ResolveUrl("~") %>Advertise.aspx">Advertise</a>
                        <a href="<%= Page.ResolveUrl("~") %>contact.aspx">Contact us</a>
                        <a href="http://blog.steelgroup.co.in" target="_blank">Blog</a>
                    </div>
                    <div class="col-12 col-md-3">
                        <h4>UPCOMING</h4>
                        <asp:Repeater ID="rptEventFooter" runat="server">
                            <ItemTemplate>
                                <a href="<%# Page.ResolveUrl("~") +  "events/" + Eval("eventName").ToString().Replace(" ","-").Replace("-&","").ToLower() %>">
                                    <%# Eval("eventName").ToString() %>
                                </a>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="col-12 col-md-3">
                        <a href="<%= Page.ResolveUrl("~") %>" class="d-block text-center">
                            <img src="<%= Page.ResolveUrl("~") %>images/steel-group-footer-logo.png" class="img-fluid w-75 imglogofooter" />
                        </a>
                    </div>
                </div>
                <div class="row no-gutters copyright">
                    <div class="col">
                        <p class="text-center">Copyright &copy; 2018 Steel Group | All Rights Reserved | All logos, symbols &amp; Trademarks belong to their respective owners.</p>
                    </div>
                </div>
            </div>
        </section>
        <asp:HiddenField ID="hfeventDate" runat="server" />
        <asp:HiddenField ID="hfEventID" runat="server" />
        <asp:HiddenField ID="mailvals" runat="server" />
        <asp:HiddenField ID="hfHashTags" runat="server" />
    </form>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
    <script src="<%= Page.ResolveUrl("~") %>js/timer.js"></script>
    <script>
        $(window).scroll(function () {
            var wscroll = $(this).scrollTop();
            if (wscroll > 80) {
                $(".navbar").addClass("shrink-nav-dark animated fadeInDown");
            }
            else {
                $(".navbar").removeClass("shrink-nav-dark");
                $(".navbar").removeClass("animated");
                $(".navbar").removeClass("fadeInDown");
            }
        });
        $(document).ready(function () {
            //initialize swiper when document ready
            var mySwiper = new Swiper('.swiper-container-mainslider', {
                autoplay: {
                    delay: 5000,
                },
                navigation: {
                    nextEl: '.swiper-button-next-mainslider',
                    prevEl: '.swiper-button-prev-mainslider',
                }
            })
        });
        var loc = window.location.pathname;

        $('.navbar-nav').find('a.nav-link').each(function () {
            if ('/' + $(this).attr('href') == loc) {
                $(this).toggleClass('active');
                return;
            }
        });//active open
        setInterval(function () { makeTimer('<%= hfeventDate.Value %>'); }, 1000);

    </script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
    <script>
        $("#shareeventbuttons").jsSocials({
            shares: ["email", "twitter", "facebook", "linkedin", "whatsapp"],
            text: 'Check out this Conference, organized by Steel Group #' + '<%= hfHashTags.Value.Replace(","," #") %>'
        });

        var swiper = new Swiper('.swiper-container-participating', {
            slidesPerView: 8,
            spaceBetween: 20,
            autoplay: {
                delay: 3000,
                disableOnInteraction: true,
            },
            pagination: {
                el: '.swiper-pagination-participating',
                clickable: true,
            },
        });

        $('.videomodal').on('hidden.bs.modal', function () {
            $(".videomodal iframe").attr("src", $(".videomodal iframe").attr("src"));
        });
    </script>
</body>
</html>
