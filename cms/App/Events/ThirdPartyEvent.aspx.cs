﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_ThirdPartyEvent : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            string mainquery = "INSERT INTO thirdPartyEvents(tpEventID,tpEventName,tpEventDesc,tpEventStartDate,tpEventEndDate,tpEventOrganizer,tpEventWebsite,tpEventVenue,";
            string valuesquery = "VALUES(@tpEventID,@tpEventName,@tpEventDesc,@tpEventStartDate,@tpEventEndDate,@tpEventOrganizer,@tpEventWebsite,@tpEventVenue,";

            string filenameeventBrochure, filenameeventImage;

        
            if (fileBrochure.HasFile)
            {
                filenameeventBrochure = "~/uploads/event/brochure/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileBrochure.FileName);
                fileBrochure.SaveAs(Server.MapPath(filenameeventBrochure));
                steelcmd.Parameters.AddWithValue("@tpEventBrochure", filenameeventBrochure);
                mainquery += "tpEventBrochure,";
                valuesquery += "@tpEventBrochure,";
            }


            if (fileImage.HasFile)
            {
                filenameeventImage = "~/uploads/event/images/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage.FileName);
                fileImage.SaveAs(Server.MapPath(filenameeventImage));
                steelcmd.Parameters.AddWithValue("@tpEventImage", filenameeventImage);
                mainquery += "tpEventImage,";
                valuesquery += "@tpEventImage,";
            }
         

            mainquery += "creationDate,creationBy,creationIP)";
            valuesquery += "@creationDate,@creationBy,@creationIP)";

            string newEventID = "TPE" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(10000, 99999);

            steelcmd.CommandText = mainquery + " " + valuesquery;

            steelcmd.Parameters.AddWithValue("@tpEventID", newEventID);
            steelcmd.Parameters.AddWithValue("@tpEventName", txtEventName.Text);
            steelcmd.Parameters.AddWithValue("@tpEventStartDate", txtStartDate.Text);
            steelcmd.Parameters.AddWithValue("@tpEventEndDate", txtEndDate.Text);
            steelcmd.Parameters.AddWithValue("@tpEventOrganizer", txtOrganizer.Text);
            steelcmd.Parameters.AddWithValue("@tpEventWebsite", txtEventWebsite.Text);
            steelcmd.Parameters.AddWithValue("@tpEventVenue", txtVenue.Text);
            steelcmd.Parameters.AddWithValue("@tpEventDesc", txtEventDesc.Text);
            steelcmd.Parameters.AddWithValue("@creationDate", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@creationBy", HttpContext.Current.User.Identity.Name);
            steelcmd.Parameters.AddWithValue("@creationIP", Request.UserHostAddress);

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Third Party Event " + txtEventName.Text + " has been posted Successfully.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();

            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Third Party Event could not be saved. Please check details, images and document sizes and try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}