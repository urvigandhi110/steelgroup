﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class unsubscribe : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string subscriberID = Request.QueryString["subscriberID"].ToString();

            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT * FROM subscribermaster WHERE subscriberID=@subscriberID");
            steelcmd.Parameters.AddWithValue("@subscriberID", subscriberID);

            steelcmd.Connection = steelcon;
            steelcon.Open();

            rdrd = steelcmd.ExecuteReader();

            if (rdrd.Read())
            {
                ltrlEmailID.Text = rdrd["subscriberEmail"].ToString();
            }
            else
            {
                Response.Redirect("Default.aspx", false);
            }
            rdrd.Close();

            steelcon.Close();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        {
            Response.Redirect("Default.aspx", false);
        }
    }

    protected void btnUnsubscribe_Click(object sender, EventArgs e)
    {
        try
        {
            string subscriberID = Request.QueryString["subscriberID"].ToString();

            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("UPDATE subscribermaster SET subscriberStatus=@subscriberStatus,datemodified=@datemodified,modifiedby=@modifiedby,modifiedIP=@modifiedIP WHERE subscriberID=@subscriberID");
            steelcmd.Parameters.AddWithValue("@subscriberID", subscriberID);
            steelcmd.Parameters.AddWithValue("@datemodified", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@modifiedby", "Unsubscribe email");
            steelcmd.Parameters.AddWithValue("@modifiedIP", Request.UserHostAddress);
            steelcmd.Parameters.AddWithValue("@subscriberStatus", "Cancelled");

            steelcmd.Connection = steelcon;
            steelcon.Open();

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlSubscribe.Visible = false;
                pnlSubscribeMessage.Visible = true;

                ltrlSubscribeMessageText.Text = "Your email ID has been unsubscribed successfully to our email newsletters. Hope you had a good time with our updates and news along with other 10,000 fellow subscribers. You can also contact us to put your advertisement on email newsletters if you want. Want to subscribe again? Fill your email ID in the box below.";
            }


            steelcon.Close();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        {
            //ltrlErrorMessage.Text = "You are already subscribed to us.";
        }
    }
}