﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class _Default : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;
    SqlDataAdapter da;
    DataSet ds;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    void Bindlist()
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT SliderID,SliderTitle,SliderText,SliderImage,sliderHighlightText FROM slidermaster WHERE publish='Yes' ORDER BY datecreated DESC; SELECT Top 4 newsID,newsCategory, newsHeadline, newsImage, dbo.fngettimeinagoformat(datecreated,GETDATE()) as datediff FROM newsmaster WHERE newsOnTop='Yes' AND publish='Yes' AND newsCategory<>'Ticker' ORDER BY newsdate DESC; SELECT advDimensions,advImage,advLinkURL,advType FROM adsmaster WHERE advEndDate>=GETDATE() AND publish='Yes' ORDER BY datecreated desc; SELECT TOP 5 newsID,newsHeadline,newsCategory, datecreated FROM newsmaster WHERE publish='Yes' AND newsCategory='Ticker'; SELECT TOP 5 newsID,newsCategory, newsHeadline, newsImage,newsLeadPara, dbo.fngettimeinagoformat(datecreated,GETDATE()) as datediff FROM newsmaster WHERE publish='Yes' AND newsFeatured='Yes' AND newsCategory<>'Ticker' ORDER BY newsdate DESC; SELECT Top 4 newsID,newsCategory, newsHeadline, newsImage, dbo.fngettimeinagoformat(datecreated,GETDATE()) as datediff FROM newsmaster WHERE newsFeatured<>'Yes' AND newsOnTop<>'Yes' AND newsCategory<>'Ticker' AND publish='Yes' ORDER BY newsdate DESC;SELECT TOP 6 eventID,eventName,eventImage1,eventStartDate,eventCity,eventCountry FROM eventsmaster WHERE publish='Yes' ORDER BY YEAR(eventStartDate) DESC, MONTH(eventStartDate), DAY(eventStartDate);");
            //steelcmd = new SqlCommand("SELECT SliderID,SliderTitle,SliderText,SliderImage,sliderHighlightText FROM slidermaster WHERE publish='Yes' ORDER BY datecreated DESC; SELECT Top 4 newsID,newsCategory, newsHeadline, newsImage, dbo.fngettimeinagoformat(datecreated,GETDATE()) as datediff FROM newsmaster WHERE newsOnTop='Yes' AND publish='Yes' AND newsCategory<>'Ticker'; SELECT advDimensions,advImage,advLinkURL,advType FROM adsmaster WHERE advEndDate>=GETDATE() AND publish='Yes' ORDER BY datecreated desc; SELECT TOP 5 newsID,newsHeadline, datecreated FROM newsmaster WHERE publish='Yes' AND newsCategory='Ticker'; SELECT TOP 5 newsID,newsCategory, newsHeadline, newsImage,newsLeadPara, dbo.fngettimeinagoformat(datecreated,GETDATE()) as datediff FROM newsmaster WHERE publish='Yes' AND newsFeatured='Yes' AND newsCategory<>'Ticker'; SELECT Top 4 newsID,newsCategory, newsHeadline, newsImage, dbo.fngettimeinagoformat(datecreated,GETDATE()) as datediff FROM newsmaster WHERE newsFeatured<>'Yes' AND newsOnTop<>'Yes' AND newsCategory<>'Ticker' AND publish='Yes';SELECT TOP 6 eventID,eventName,eventImage1,eventStartDate,eventCity,eventCountry FROM eventsmaster WHERE publish='Yes'");

            steelcmd.Connection = steelcon;
            steelcon.Open();


            da = new SqlDataAdapter();
            ds = new DataSet();

            da.SelectCommand = steelcmd;
            da.Fill(ds);

            //Slider 
            rptSlider.DataSource = ds.Tables[0];
            rptSlider.DataBind();

            //Slider Buttons
            rptSliderButtons.DataSource = ds.Tables[0];
            rptSliderButtons.DataBind();

            //Top News
            rptNewsOnTop.DataSource = ds.Tables[1];
            rptNewsOnTop.DataBind();

            //First Fold Ads
            ds.Tables[2].DefaultView.RowFilter = "advType LIKE '%FF:%'";

            rptFFAds.DataSource = ds.Tables[2].DefaultView;
            rptFFAds.DataBind();

            //Ticker News
            rptNewsTicker.DataSource = ds.Tables[3];
            rptNewsTicker.DataBind();

            //Second Fold News
            rptNewsSF.DataSource = ds.Tables[5];
            rptNewsSF.DataBind();

            //Featured SLider News Buttons
            rptFeaturedNewsButtons.DataSource = ds.Tables[4];
            rptFeaturedNewsButtons.DataBind();

            //Featured SLider News
            rptFeaturedNews.DataSource = ds.Tables[4];
            rptFeaturedNews.DataBind();

            //Second Fold Ads
            ds.Tables[2].DefaultView.RowFilter = "";
            ds.Tables[2].DefaultView.RowFilter = "advType LIKE '%HS:%'";

            rptSFAds.DataSource = ds.Tables[2].DefaultView;
            rptSFAds.DataBind();

            //Ad in Middle
            ds.Tables[2].DefaultView.RowFilter = "";
            ds.Tables[2].DefaultView.RowFilter = "advType LIKE '%Mid:%'";

            DataTable dt = ds.Tables[2].DefaultView.ToTable();

            if (dt.Rows.Count > 0)
            {
                lnkMidAd.NavigateUrl = dt.Rows[0]["advLinkURL"].ToString();
                lnkMidAd.Target = "_blank";
                imgAdinMid.ImageUrl = Page.ResolveUrl("~/cms") + dt.Rows[0]["advImage"].ToString().Replace("~/", "/") + "?w=" + dt.Rows[0]["advDimensions"].ToString().Split('x')[0] + "&h=" + dt.Rows[0]["advDimensions"].ToString().Split('x')[1] + "&mode=crop";
            }
            else
            {
                lnkMidAd.Visible = false;
            }


            ds.Tables[6].DefaultView.RowFilter = "eventStartDate>=#" + DateTime.Today.ToString("yyyy-MM-dd hh:mm:ss.fff") + "#";
            ds.Tables[6].DefaultView.Sort = "eventStartDate";

            //Upcoming Events
            rptEventsList.DataSource = ds.Tables[6].DefaultView;
            rptEventsList.DataBind();


            da.Dispose();
            ds.Clear();
            ds.Dispose();
            dt.Dispose();
            steelcon.Close();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bindlist();
        }

    }
}