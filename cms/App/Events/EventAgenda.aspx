﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="EventAgenda.aspx.cs" Inherits="App_Events_EventAgenda" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/themes/dark.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Add Agenda</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:panel id="pnlMessage" visible="false" runat="server" cssclass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Please Enter the time slot, title and description of agenda-</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="txtTimeSlot">Select Date for Agenda</label>
                    <asp:dropdownlist id="ddlEventDate" autopostback="true" onselectedindexchanged="ddlEventDate_SelectedIndexChanged" cssclass="form-control" runat="server">
                        <asp:ListItem>Select a Date</asp:ListItem>
                    </asp:dropdownlist>
                </div>
                <asp:repeater id="rptInputAgenda" runat="server">
                    <ItemTemplate>
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="txtTimeFrom"><%# Container.DataItem %>. Time Start</label>
                                <asp:TextBox ID="txtTimeFrom" placeholder="Start Time" CssClass="form-control flatpickr" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="txtTimeTo">Time End</label>
                                <asp:TextBox ID="txtTimeTo" placeholder="End Time" CssClass="form-control flatpickr" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="txtTitle">Title</label>
                                <asp:TextBox ID="txtTitle" placeholder="Please Enter Title" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="txtDesc">Description</label>
                                <asp:TextBox ID="txtDesc" placeholder="Please Enter Description" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>

                        </div>
                    </ItemTemplate>
                </asp:repeater>
                <div class="form-group">
                    <asp:button id="btnSubmit" onclick="btnSubmit_Click" runat="server" text="Save Agenda" cssclass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.js"></script>
    <script>
        $(".flatpickr").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i K",
            allowInput: true
        });
    </script>
</asp:Content>

