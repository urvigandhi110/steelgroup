﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.IO;

public partial class Register : System.Web.UI.Page
{

    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        rbGroup.InputAttributes.Add("class", "custom-control-input");
        rbIndividual.InputAttributes.Add("class", "custom-control-input");
        rbOnline.InputAttributes.Add("class", "custom-control-input");
        rbPayAtEvent.InputAttributes.Add("class", "custom-control-input");
//        rbOnline.InputAttributes.Add("disabled", "");

        string eventname = Page.RouteData.Values["eventname"].ToString();

        steelcmd = new SqlCommand();
        steelcon = new SqlConnection(connstr);
        steelcon.Open();
        steelcmd.Connection = steelcon;

        steelcmd.CommandText = "SELECT eventID, eventName,eventImage1,eventStartDate,eventVenue,eventVenueAddress,eventVenueMapURL,eventBrochure,eventAgenda FROM eventsmaster WHERE publish='Yes' AND Lower(replace(replace(eventName,' ','-'),'-&',''))=@eventname";
        steelcmd.Parameters.AddWithValue("@eventname", eventname);

        rdrd = steelcmd.ExecuteReader();
        if (rdrd.Read())
        {
            hfEventID.Value = rdrd["eventID"].ToString();
            //eventname,image,agenda,brochure
            mailvals.Value = rdrd["eventName"].ToString() + "$$" + rdrd["eventImage1"].ToString() + "$$" + rdrd["eventAgenda"].ToString() + "$$" + rdrd["eventBrochure"].ToString();

            Literal ltrlEventVenue = (Literal)Master.FindControl("ltrlEventVenue");
            Literal ltrlEventDate = (Literal)Master.FindControl("ltrlEventDate");
            Literal ltrlEventName = (Literal)Master.FindControl("ltrlEventName");
            Literal ltrlVenueAddress = (Literal)Master.FindControl("ltrlVenueAddress");
            Literal ltrlVenueMap = (Literal)Master.FindControl("ltrlVenueMap");
            HiddenField hf = (HiddenField)Master.FindControl("hfimageurl");

            hf.Value = Page.ResolveUrl("~/cms") + rdrd["eventImage1"].ToString().Replace("~/", "/") + "?w=1920&h=1200&mode=crop";
            ltrlEventVenue.Text = rdrd["eventVenue"].ToString();
            ltrlEventDate.Text = DateTime.Parse(rdrd["eventStartDate"].ToString()).ToString("MMMM dd, yyyy");
            ltrlEventName.Text = rdrd["eventName"].ToString();
            ltrlVenueAddress.Text = rdrd["eventVenueAddress"].ToString();
            ltrlVenueMap.Text = "<iframe src=\"" + rdrd["eventVenueMapURL"].ToString() + "\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border: 0\" allowfullscreen></iframe>";


        }

        rdrd.Close();
        steelcon.Close();
        steelcmd.Dispose();
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        try
        {

            string eventname = Page.RouteData.Values["eventname"].ToString();

            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            string newRegID = "REG" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(1000, 9999);

            steelcmd.CommandText = "INSERT INTO eventregistration(regID,eventID,regType,regName,regDesignation,regOrg,regEmailOrg,regEmailPersonal,regMobile,regPhoneOrg,regAddress,regCity,regState,regCountry,regZip,paymentMode,paymentStatus,regStatus,datecreated,createdby,createdIP,numDelegates,interestedAs,companyType) VALUES(@regID,@eventID,@regType,@regName,@regDesignation,@regOrg,@regEmailOrg,@regEmailPersonal,@regMobile,@regPhoneOrg,@regAddress,@regCity,@regState,@regCountry,@regZip,@paymentMode,@paymentStatus,@regStatus,@datecreated,@createdby,@createdIP,@numDelegates,@interestedAs,@companyType)";

            steelcmd.Parameters.AddWithValue("@regID", newRegID);
            steelcmd.Parameters.AddWithValue("@eventID", hfEventID.Value);
            steelcmd.Parameters.AddWithValue("@regType", rbIndividual.Checked ? "Individual" : "Group");
            steelcmd.Parameters.AddWithValue("@regName", txtFullName.Text);
            steelcmd.Parameters.AddWithValue("@regDesignation", txtDesignation.Text);
            steelcmd.Parameters.AddWithValue("@regOrg", txtOrgName.Text);
            steelcmd.Parameters.AddWithValue("@regEmailOrg", txtOrgEmail.Text);
            steelcmd.Parameters.AddWithValue("@regEmailPersonal", txtPersonalEmail.Text);
            steelcmd.Parameters.AddWithValue("@regMobile", txtMobile.Text);
            steelcmd.Parameters.AddWithValue("@regPhoneOrg", txtOrgPhone.Text);
            steelcmd.Parameters.AddWithValue("@regAddress", txtAddress.Text);
            steelcmd.Parameters.AddWithValue("@regCity", txtCity.Text);
            steelcmd.Parameters.AddWithValue("@regState", txtState.Text);
            steelcmd.Parameters.AddWithValue("@regCountry", ddlCountry.SelectedItem.Text);
            steelcmd.Parameters.AddWithValue("@regZip", txtZip.Text);
            steelcmd.Parameters.AddWithValue("@paymentMode", rbOnline.Checked ? "Online" : "Offline");
            steelcmd.Parameters.AddWithValue("@paymentStatus", "NA");
            steelcmd.Parameters.AddWithValue("@regStatus", "Registered");
            steelcmd.Parameters.AddWithValue("@datecreated", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@createdby", "User");
            steelcmd.Parameters.AddWithValue("@createdIP", Request.UserHostAddress);
            steelcmd.Parameters.AddWithValue("@numDelegates", txtDelegates.Text);
            steelcmd.Parameters.AddWithValue("@interestedAs", ddlInterestedAs.SelectedValue);
            steelcmd.Parameters.AddWithValue("@companyType", txtCompanyType.Text);

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {

               //Preparing registrant mail to send
                StreamReader readerRegistrant = new StreamReader(Server.MapPath("~/templates/eventreg.txt"));
                string readFileRegistrant = readerRegistrant.ReadToEnd();
                readFileRegistrant = readFileRegistrant.Replace("$$SPONSORSURL$$", "https://www.steelgroup.co.in/events/" + eventname + "/sponsors");
                readFileRegistrant = readFileRegistrant.Replace("$$SPEAKERSURL$$", "https://www.steelgroup.co.in/events/" + eventname + "/speakers");
                //eventname,image,agenda,brochure
                string[] stringSeparators = new string[] { "$$" };

                readFileRegistrant = readFileRegistrant.Replace("$$AGENDAURL$$", "https://www.steelgroup.co.in/cms/" + mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[2].Replace("~/", ""));
                readFileRegistrant = readFileRegistrant.Replace("$$BROCHUREURL$$", "https://www.steelgroup.co.in/cms/" + mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[3].Replace("~/", ""));
                readFileRegistrant = readFileRegistrant.Replace("$$REGNAME$$", txtFullName.Text.Split(' ')[0]);
                readFileRegistrant = readFileRegistrant.Replace("$$EVENTNAME$$", mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[0]);
                readFileRegistrant = readFileRegistrant.Replace("$$IMAGEURL$$", "https://www.steelgroup.co.in/cms/" + mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[1].Replace("~/", "") + "?w=1260&h=300&mode=crop");

                string resp = SteelFunc.sendmail(txtPersonalEmail.Text, "Event Registration Confirmation", readFileRegistrant);
                readerRegistrant.Close();

                //Preparing ADMIN mail to send
                StreamReader readerAdmin = new StreamReader(Server.MapPath("~/templates/eventadmin.txt"));
                string readFileAdmin = readerAdmin.ReadToEnd();
                readFileAdmin = readFileAdmin.Replace("$$EVENTNAME$$", mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[0]);
                readFileAdmin = readFileAdmin.Replace("$$IMAGEURL$$", "https://www.steelgroup.co.in/cms/" + mailvals.Value.Split(stringSeparators, StringSplitOptions.None)[1].Replace("~/", "") + "?w=1260&h=300&mode=crop");
                readFileAdmin = readFileAdmin.Replace("$$REGNAME$$", txtFullName.Text);
                readFileAdmin = readFileAdmin.Replace("$$EVENTID$$", hfEventID.Value);
                readFileAdmin = readFileAdmin.Replace("$$REGTYPE$$", rbIndividual.Checked ? "Individual" : "Group");
                readFileAdmin = readFileAdmin.Replace("$$COMPANYNAME$$", txtOrgName.Text);
                readFileAdmin = readFileAdmin.Replace("$$COMPANYTYPE$$", txtCompanyType.Text);
                readFileAdmin = readFileAdmin.Replace("$$INTERESTEDAS$$", ddlInterestedAs.SelectedItem.Text);
                readFileAdmin = readFileAdmin.Replace("$$NUM$$", txtDelegates.Text);
                readFileAdmin = readFileAdmin.Replace("$$CORPEMAIL$$", txtOrgEmail.Text);
                readFileAdmin = readFileAdmin.Replace("$$PERSEMAIL$$", txtPersonalEmail.Text);
                readFileAdmin = readFileAdmin.Replace("$$MOBILE$$", txtMobile.Text);
                readFileAdmin = readFileAdmin.Replace("$$LOCATION$$", txtCity.Text + ", " + txtState.Text + ", " + ddlCountry.SelectedItem.Text);
                readFileAdmin = readFileAdmin.Replace("$$REGDATE$$", DateTime.Now.ToString("d MMM yyyy"));
                resp = SteelFunc.sendmail("amit@steelgroup.co.in;ajay.tambe@steelgroup.co.in", "New Event Registration Recieved: " + txtFullName.Text, readFileAdmin);
                readerAdmin.Close();

                if(rbOnline.Checked)
                {
                    Response.Redirect(Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "/Online-Payment",false);
                }
                else
                {

                    pnlMessage.Visible = true;
                    pnlMessage.CssClass = "alert alert-success alert-dismissible rounded-0";
                    ltrlmsg.Text = "Your Registration has been submitted. Your online registration number is: <strong>" + newRegID + "</strong>. Please save this number for reference. We will get back to you soon. Meanwhile you can contact us here- <a class=\"btn btn-sm btn-success\" href=\"#contact\">Contact us</a>";

                    steelcon.Close();
                    steelcmd.Dispose();
                    steelcon.Dispose();
                }

            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible rounded-0";
                ltrlmsg.Text = "Your registration was not successful. Please check all details and try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }

    protected void rbIndividual_CheckedChanged(object sender, EventArgs e)
    {
        if (rbIndividual.Checked)
        {
            pnlNumDelegates.Visible = false;
        }
        else
        {
            pnlNumDelegates.Visible = true;
        }
    }
}