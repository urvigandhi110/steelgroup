﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_Events_AddEventSpeaker : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string eventID = Request.QueryString["eventID"].ToString();

            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            string filename = "~/uploads/event/speakers/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(filePhoto.FileName);

            if (filePhoto.HasFile && !System.IO.File.Exists(Server.MapPath(filename)))
            {
                filePhoto.SaveAs(Server.MapPath(filename));
            }
            else
            {
                filename = "~/uploads/event/speakers/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(filePhoto.FileName);
                filePhoto.SaveAs(Server.MapPath(filename));
            }

            string newSpeakerID = "SPK" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(100, 999);

            steelcmd.CommandText = "INSERT INTO eventspeakers(eventSpeakerID,eventSpeakerName,eventSpeakerOrg,eventSpeakerPhoto,eventSpeakerDesc,eventSpeakerLinkedin,eventID,datecreated,createdby,createdIP,publish,eventSpeakerType,eventSpeakerDesignation,eventSpeakerCountry,eventSpeakerTwitter,eventSpeakerIntro) VALUES(@eventSpeakerID,@eventSpeakerName,@eventSpeakerOrg,@eventSpeakerPhoto,@eventSpeakerDesc,@eventSpeakerLinkedin,@eventID,@datecreated,@createdby,@createdIP,@publish,@eventSpeakerType,@eventSpeakerDesignation,@eventSpeakerCountry,@eventSpeakerTwitter,@eventSpeakerIntro)";

            steelcmd.Parameters.AddWithValue("@eventSpeakerID", newSpeakerID);
            steelcmd.Parameters.AddWithValue("@eventSpeakerName", txtSpeakerName.Text);
            steelcmd.Parameters.AddWithValue("@eventSpeakerOrg", txtOrganization.Text);
            steelcmd.Parameters.AddWithValue("@eventSpeakerPhoto", filename);
            steelcmd.Parameters.AddWithValue("@eventSpeakerDesc", txtDescription.Text);
            steelcmd.Parameters.AddWithValue("@eventSpeakerIntro", txtIntro.Text);
            steelcmd.Parameters.AddWithValue("@eventSpeakerTwitter", txtTwitter.Text);
            steelcmd.Parameters.AddWithValue("@eventSpeakerCountry", txtCountry.Text);
            steelcmd.Parameters.AddWithValue("@eventSpeakerDesignation", txtSpeakerDesignation.Text);
            steelcmd.Parameters.AddWithValue("@eventSpeakerLinkedin", txtSpeakerLinkedin.Text);
            steelcmd.Parameters.AddWithValue("@eventID", eventID);
            steelcmd.Parameters.AddWithValue("@datecreated", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@createdby", HttpContext.Current.User.Identity.Name);
            steelcmd.Parameters.AddWithValue("@createdIP", Request.UserHostAddress);
            steelcmd.Parameters.AddWithValue("@publish", "Yes");
            steelcmd.Parameters.AddWithValue("@eventSpeakerType", ddlSpeakerType.SelectedItem.Text);

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "Speaker has been added Successfully to event.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();

            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "Speaker could not be added. Please check all details, image size, type and try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}