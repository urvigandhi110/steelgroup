﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class explara : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        string eventname = Page.RouteData.Values["eventname"].ToString();
        if (eventname == "bangladesh-steel-2018")
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;

            steelcmd.CommandText = "SELECT eventID, eventName,eventImage1,eventStartDate,eventVenue,eventVenueAddress,eventVenueMapURL FROM eventsmaster WHERE publish='Yes' AND Lower(replace(replace(eventName,' ','-'),'-&',''))=@eventname";
            steelcmd.Parameters.AddWithValue("@eventname", eventname);

            rdrd = steelcmd.ExecuteReader();
            if (rdrd.Read())
            {

                Literal ltrlEventVenue = (Literal)Master.FindControl("ltrlEventVenue");
                Literal ltrlEventDate = (Literal)Master.FindControl("ltrlEventDate");
                Literal ltrlEventName = (Literal)Master.FindControl("ltrlEventName");
                Literal ltrlVenueAddress = (Literal)Master.FindControl("ltrlVenueAddress");
                Literal ltrlVenueMap = (Literal)Master.FindControl("ltrlVenueMap");
                HiddenField hf = (HiddenField)Master.FindControl("hfimageurl");

                hf.Value = Page.ResolveUrl("~/cms") + rdrd["eventImage1"].ToString().ToString().Replace("~/", "/") + "?w=1920&h=1200&mode=crop";
                ltrlEventVenue.Text = rdrd["eventVenue"].ToString();
                ltrlEventDate.Text = DateTime.Parse(rdrd["eventStartDate"].ToString()).ToString("MMMM dd, yyyy");
                ltrlEventName.Text = rdrd["eventName"].ToString();
                ltrlVenueAddress.Text = rdrd["eventVenueAddress"].ToString();
                ltrlVenueMap.Text = "<iframe src=\"" + rdrd["eventVenueMapURL"].ToString() + "\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border: 0\" allowfullscreen></iframe>";


            }

            rdrd.Close();
            steelcon.Close();
            steelcmd.Dispose();
        }
        else
        {
            Response.Redirect(Page.ResolveUrl("~") + "events/" + Page.RouteData.Values["eventname"].ToString() + "/Register");
        }

    }
}