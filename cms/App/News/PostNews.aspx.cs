﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class App_News_PostNews : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {
        chkOnTop.InputAttributes.Add("class", "checkbox-template");
        chkFeatured.InputAttributes.Add("class", "checkbox-template");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            steelcmd = new SqlCommand();
            steelcon = new SqlConnection(connstr);
            steelcon.Open();
            steelcmd.Connection = steelcon;
            string filename = "~/uploads/news/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage.FileName);

            if (fileImage.HasFile && !System.IO.File.Exists(Server.MapPath(filename)))
            {
                fileImage.SaveAs(Server.MapPath(filename));
            }
            else
            {
                filename = "~/uploads/news/" + Guid.NewGuid().ToString().Replace("-", "") + System.IO.Path.GetExtension(fileImage.FileName);
                fileImage.SaveAs(Server.MapPath(filename));
            }

            string newnewsID = "NWS" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(100, 999);

            steelcmd.CommandText = "INSERT INTO newsmaster(newsID,newsCategory,newsOnTop,newsFeatured,newsHeadline,newsLeadPara,newsText,newsImage,newsImageDesc,newsSourceLink,newsDate,datecreated,createdby,createdIP,publish) VALUES(@newsID,@newsCategory,@newsOnTop,@newsFeatured,@newsHeadline,@newsLeadPara,@newsText,@newsImage,@newsImageDesc,@newsSourceLink,@newsDate,@datecreated,@createdby,@createdIP,@publish)";

            steelcmd.Parameters.AddWithValue("@newsID", newnewsID);
            steelcmd.Parameters.AddWithValue("@newsCategory", ddlNewsCat.SelectedItem.Text);
            steelcmd.Parameters.AddWithValue("@newsOnTop", chkOnTop.Checked ? "Yes" : "No");
            steelcmd.Parameters.AddWithValue("@newsFeatured", chkFeatured.Checked ? "Yes" : "No");
            steelcmd.Parameters.AddWithValue("@newsHeadline", txtNewsHeadline.Text);
            steelcmd.Parameters.AddWithValue("@newsLeadPara", txtLeadPara.Text);
            steelcmd.Parameters.AddWithValue("@newsText", txtNewsArticle.Text);
            steelcmd.Parameters.AddWithValue("@newsImage", filename);
            steelcmd.Parameters.AddWithValue("@newsImageDesc", txtImageDesc.Text);
            steelcmd.Parameters.AddWithValue("@newsSourceLink", txtNewsSource.Text);
            steelcmd.Parameters.AddWithValue("@newsDate", txtNewsDate.Text);
            steelcmd.Parameters.AddWithValue("@datecreated", DateTime.Now);
            steelcmd.Parameters.AddWithValue("@createdby", HttpContext.Current.User.Identity.Name);
            steelcmd.Parameters.AddWithValue("@createdIP", Request.UserHostAddress);
            steelcmd.Parameters.AddWithValue("@publish", "Yes");

            int res = steelcmd.ExecuteNonQuery();

            if (res > 0)
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-success alert-dismissible";
                ltrlmsg.Text = "News has been posted Successfully.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();

            }
            else
            {
                pnlMessage.Visible = true;
                pnlMessage.CssClass = "alert alert-warning alert-dismissible";
                ltrlmsg.Text = "News could not be saved. Please check details, image size, type and try again.";
                steelcon.Close();

                steelcmd.Dispose();
                steelcon.Dispose();
            }
        }
        catch(Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}