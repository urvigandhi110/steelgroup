﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;

public partial class App_Admin_AddAdmin : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        steelcmd = new SqlCommand();
        steelcon = new SqlConnection(connstr);
        steelcon.Open();
        steelcmd.Connection = steelcon;

        string filename = "~/uploads/admins/" + fileImage.FileName;

        if (fileImage.HasFile && !System.IO.File.Exists(Server.MapPath(filename)))
        {
            fileImage.SaveAs(Server.MapPath(filename));
        }
        else
        {
            filename = "~/uploads/admins/" + Guid.NewGuid().ToString().Replace("-", "") + fileImage.FileName;
            fileImage.SaveAs(Server.MapPath(filename));
        }


        string newadminid = "ADM" + DateTime.Today.ToString("ddMMyyyy") + new Random().Next(100, 999);

        steelcmd.CommandText = "INSERT INTO adminMaster(adminID,adminName,adminEmail,adminPasswd,adminPhoto,adminusername,datecreated,createdby,createdIP) VALUES(@adminID,@adminName,@adminEmail,@adminPasswd,@adminPhoto,@adminusername,@datecreated,@createdby,@createdIP)";
        steelcmd.Parameters.AddWithValue("@adminID", newadminid);
        steelcmd.Parameters.AddWithValue("@adminName", txtAdminName.Text);
        steelcmd.Parameters.AddWithValue("@adminEmail", txtAdminEmail.Text);
        steelcmd.Parameters.AddWithValue("@adminPasswd", FormsAuthentication.HashPasswordForStoringInConfigFile(txtAdminPasswd.Text, "sha1"));
        steelcmd.Parameters.AddWithValue("@adminusername", txtAdminUsername.Text);
        steelcmd.Parameters.AddWithValue("@adminPhoto", filename);
        steelcmd.Parameters.AddWithValue("@datecreated", DateTime.Now);
        steelcmd.Parameters.AddWithValue("@createdby", HttpContext.Current.User.Identity.Name);
        steelcmd.Parameters.AddWithValue("@createdIP", Request.UserHostAddress);
        steelcmd.Parameters.AddWithValue("@publish", "Yes");


        int res = steelcmd.ExecuteNonQuery();

        if (res > 0)
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-success alert-dismissible";
            ltrlmsg.Text = "New admin has been uploaded Successfully.";
            steelcon.Close();

            steelcmd.Dispose();
            steelcon.Dispose();

        }
        else
        {
            pnlMessage.Visible = true;
            pnlMessage.CssClass = "alert alert-warning alert-dismissible";
            ltrlmsg.Text = "Admin could not be saved. Please check file details, size, type and try again.";
            steelcon.Close();

            steelcmd.Dispose();
            steelcon.Dispose();
        }
    }
}