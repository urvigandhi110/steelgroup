﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="AddEventSpeaker.aspx.cs" Inherits="App_Events_AddEventSpeaker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://cdn.ckeditor.com/4.7.0/standard/ckeditor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Add Event Speaker</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Please Enter the following Speaker details-</h3>
            </div>
            <div class="card-body">
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Speaker Type <small class="text-danger">*Required</small></label>
                            <asp:DropDownList ID="ddlSpeakerType" runat="server" CssClass="form-control">
                                <asp:ListItem Value="">Select a Type</asp:ListItem>
                                <asp:ListItem>Key Note Speaker</asp:ListItem>
                                <asp:ListItem>Speaker</asp:ListItem>
                                <asp:ListItem>Panelist</asp:ListItem>
                                <asp:ListItem>Moderator</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Speaker Name <small class="text-danger">*Required</small></label>
                            <asp:TextBox ID="txtSpeakerName" placeholder="Please enter Speaker Name here" runat="server" CssClass="form-control" required></asp:TextBox>
                        </div>
                    </div>

                </div>
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Speaker Organization <small class="text-danger">*Required</small></label>
                            <asp:TextBox ID="txtOrganization" placeholder="Please enter Organization Name here" runat="server" CssClass="form-control" required></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Speaker Designation <small class="text-danger">*Required</small></label>
                            <asp:TextBox ID="txtSpeakerDesignation" placeholder="Please enter Designation here" runat="server" CssClass="form-control" required></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Speaker Country <small class="text-danger">*Required</small></label>
                            <asp:TextBox ID="txtCountry" placeholder="Please enter Country here" runat="server" CssClass="form-control" required></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Speaker Photo <small class="text-danger">*Required</small></label>
                            <asp:FileUpload ID="filePhoto" CssClass="form-control" runat="server" required />

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Speaker Introduction (Small) <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtIntro" placeholder="Enter Speaker Introduction" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Speaker Description (Detailed) </label>
                    <asp:TextBox ID="txtDescription" placeholder="Enter Speaker Description" TextMode="MultiLine" runat="server" CssClass="form-control contentBox"></asp:TextBox>
                </div>
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Speaker LinkedIn</label>
                            <asp:TextBox ID="txtSpeakerLinkedin" placeholder="Please enter LinkedIn URL here" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label class="form-control-label">Speaker Twitter </label>
                            <asp:TextBox ID="txtTwitter" placeholder="Please enter Twitter URL here" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Add Speaker" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
    <script>
        CKEDITOR.replaceAll('contentBox');
        CKEDITOR.config.htmlEncodeOutput = true;
    </script>

</asp:Content>

