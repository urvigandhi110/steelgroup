﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class SponsoredEvents : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;
    SqlDataAdapter da;
    DataSet ds;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT * FROM thirdPartyEvents ORDER BY tpEventStartDate; select Year(tpEventStartDate) as tpyear, COUNT(*) as cnt from thirdPartyEvents GROUP BY Year(tpEventStartDate) ORDER BY Year(tpEventStartDate) desc");
            steelcmd.Connection = steelcon;
            steelcon.Open();


            da = new SqlDataAdapter();
            ds = new DataSet();

            da.SelectCommand = steelcmd;
            da.Fill(ds);

            rptMainTabs.DataSource = ds.Tables[1];
            rptMainTabPills.DataSource = ds.Tables[1];

            rptMainTabs.DataBind();
            rptMainTabPills.DataBind();

            foreach (RepeaterItem item in rptMainTabs.Items)
            {
                HiddenField hf = (HiddenField)item.FindControl("hfyear");
                Repeater rpt = (Repeater)item.FindControl("rptSponsored");

                ds.Tables[0].DefaultView.RowFilter = "";
                ds.Tables[0].DefaultView.RowFilter = "tpEventStartDate < #12/31/" + hf.Value + "# AND tpEventStartDate > #1/1/" + hf.Value + "#";

                rpt.DataSource = ds.Tables[0].DefaultView;
                rpt.DataBind();
            }


            da.Dispose();
            ds.Clear();
            ds.Dispose();
            steelcon.Close();
            steelcmd.Dispose();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
}