﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

public partial class SpeakerDetails : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;


    protected void Page_Load(object sender, EventArgs e)
    {
        string eventname = Page.RouteData.Values["eventname"].ToString();
        string speakername = Page.RouteData.Values["speakername"].ToString();

        steelcmd = new SqlCommand();
        steelcon = new SqlConnection(connstr);
        steelcon.Open();
        steelcmd.Connection = steelcon;

        steelcmd.CommandText = "SELECT eventID, eventName,eventImage1,eventStartDate,eventVenue,eventVenueAddress,eventVenueMapURL,eventBrochure,eventAgenda,eventLayout FROM eventsmaster WHERE publish='Yes' AND Lower(replace(replace(eventName,' ','-'),'-&',''))=@eventname; SELECT s.eventSpeakerID,s.eventSpeakerName,s.eventSpeakerOrg,s.eventSpeakerPhoto,s.eventSpeakerLinkedin,s.eventSpeakerType,s.eventSpeakerDesignation,s.eventSpeakerCountry,s.eventSpeakerTwitter,s.eventSpeakerDesc FROM eventSpeakers s JOIN eventsmaster e ON e.eventID=s.eventID WHERE Lower(replace(replace(e.eventName,' ','-'),'-&',''))=@eventname AND Lower(replace(replace(eventSpeakerName,' ','-'),'-&',''))=@speakername ORDER BY eventSpeakerName; SELECT spo.eventSponsorID,spo.eventSponsorName,spo.eventSponsorLogo,spo.eventSponsorURL,spo.eventSponsorType FROM eventSponsors spo JOIN eventsmaster e ON e.eventID=spo.eventID WHERE Lower(replace(replace(e.eventName,' ','-'),'-&',''))=@eventname AND spo.eventSponsorType IN ('Platinum','Gold','Silver','Associate','Networking Lunch','Networking Dinner') ORDER BY case when eventSponsorType like 'Platinum' then 0 else 1 end,case when eventSponsorType like 'Gold' then 0 else 1 end,case when eventSponsorType like 'Silver' then 0 else 1 end,case when eventSponsorType like 'Associate' then 0 else 1 end,case when eventSponsorType like 'Networking Lunch' then 0 else 1 end,case when eventSponsorType like 'Networking Dinner' then 0 else 1 end";
        steelcmd.Parameters.AddWithValue("@eventname", eventname);
        steelcmd.Parameters.AddWithValue("@speakername", speakername);

        rdrd = steelcmd.ExecuteReader();
        if (rdrd.Read())
        {


            Literal ltrlEventVenue = (Literal)Master.FindControl("ltrlEventVenue");
            Literal ltrlEventDate = (Literal)Master.FindControl("ltrlEventDate");
            Literal ltrlEventName = (Literal)Master.FindControl("ltrlEventName");
            Literal ltrlVenueAddress = (Literal)Master.FindControl("ltrlVenueAddress");
            Literal ltrlVenueMap = (Literal)Master.FindControl("ltrlVenueMap");
            HiddenField hf = (HiddenField)Master.FindControl("hfimageurl");

            hf.Value = Page.ResolveUrl("~/cms") + rdrd["eventImage1"].ToString().ToString().Replace("~/", "/") + "?w=1920&h=1200&mode=crop";
            ltrlEventVenue.Text = rdrd["eventVenue"].ToString();
            ltrlEventDate.Text = DateTime.Parse(rdrd["eventStartDate"].ToString()).ToString("MMMM dd, yyyy");
            ltrlEventName.Text = rdrd["eventName"].ToString();
            ltrlVenueAddress.Text = rdrd["eventVenueAddress"].ToString();
            ltrlVenueMap.Text = "<iframe src=\"" + rdrd["eventVenueMapURL"].ToString() + "\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border: 0\" allowfullscreen></iframe>";
            ltrlEVentNameBC.Text = rdrd["eventName"].ToString();

            imgBrochure.ImageUrl = Page.ResolveUrl("~/cms") + rdrd["eventBrochure"].ToString().ToString().Replace("~/", "/") + "?width=500&pdfwidth=700";
            imgAgenda.ImageUrl = Page.ResolveUrl("~/cms") + rdrd["eventAgenda"].ToString().ToString().Replace("~/", "/") + "?width=500&pdfwidth=700";

            lnkBrochureSidebar.NavigateUrl = Page.ResolveUrl("~/cms") + rdrd["eventBrochure"].ToString().Replace("~/", "/");
            lnkAgendaSidebar.NavigateUrl = Page.ResolveUrl("~/cms") + rdrd["eventAgenda"].ToString().Replace("~/", "/");

            lnkBrochureSidebar.Attributes.Add("download", "Brochure-" + rdrd["eventName"].ToString().Replace(" ", "-").ToLower());
            lnkAgendaSidebar.Attributes.Add("download", "Agenda-" + rdrd["eventName"].ToString().Replace(" ", "-").ToLower());

        }
        if (rdrd.NextResult())
        {
            if (rdrd.Read())
            {
                ltrlSpeakerName.Text = rdrd["eventSpeakerName"].ToString();
                ltrlSpeakerNameBC.Text = rdrd["eventSpeakerName"].ToString();
                ltrlSpeakerDetails.Text = HttpUtility.HtmlDecode(rdrd["eventSpeakerDesc"].ToString());
                ltrlOrg.Text = rdrd["eventSpeakerOrg"].ToString();
                ltrlDesignation.Text = rdrd["eventSpeakerDesignation"].ToString();
                ltrlCountry.Text = rdrd["eventSpeakerCountry"].ToString();
                imgSpeaker.ImageUrl = Page.ResolveUrl("~/cms") + rdrd["eventSpeakerPhoto"].ToString().ToString().Replace("~/", "/") + "?w=200&h=250&mode=crop";
                ltrlSpeakerType.Text = rdrd["eventSpeakerType"].ToString();
                lnkLinkedin.NavigateUrl = rdrd["eventSpeakerLinkedin"].ToString();
                lnkTwitter.NavigateUrl = rdrd["eventSpeakerTwitter"].ToString();
            }
        }

        if (rdrd.NextResult())
        {
            rptSponsorAds.DataSource = rdrd;
            rptSponsorAds.DataBind();
        }

        rdrd.Close();
        steelcon.Close();
        steelcmd.Dispose();
    }
}