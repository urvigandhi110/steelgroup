﻿<%@ Page Title="Steel Group - Leader in Steel Conferences, Jobs and News" Language="C#" MasterPageFile="~/SteelMain.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/css/swiper.min.css" />
    <style>
        .swiper-container-ads {
            width: 100%;
            height: 70vh;
        }

        .swiper-container-clientele {
            width: 100%;
            height: 13rem;
        }

            .swiper-container-clientele .swiper-slide img {
                filter: grayscale(1);
            }

                .swiper-container-clientele .swiper-slide img:hover {
                    filter: grayscale(0);
                    transition: filter ease 1s;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">
    <section id="firstfold">
        <div class="row no-gutters">
            <div class="col-12 col-md-9">
                <div class="row no-gutters">
                    <div class="col-12 col-md-6">
                        <div id="steelcarousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <asp:Repeater ID="rptSliderButtons" runat="server">
                                    <ItemTemplate>
                                        <li data-target="#steelCarousel" data-slide-to="<%# Container.ItemIndex %>" class="<%# Container.ItemIndex==0?"active":"" %>"></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ol>
                            <div class="carousel-inner">
                                <asp:Repeater ID="rptSlider" runat="server">
                                    <ItemTemplate>
                                        <div class="carousel-item <%# Container.ItemIndex==0?"active":"" %>">
                                            <img class="d-block w-100" src="<%# Page.ResolveUrl("~/cms") + Eval("sliderImage").ToString().Replace("~/","/") + "?w=1000&h=1000&mode=crop" %>" alt="Steel Group">
                                            <div class="carousel-caption">
                                                <h5><%# Eval("sliderTitle") %></h5>
                                                <p><%# Eval("sliderText") %></p>
                                                <span class="highlight"><%# Eval("sliderHighlightText") %></span>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <a class="carousel-control-prev" href="#steelcarousel" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#steelcarousel" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="banner-sec">
                            <div class="container">
                                <div class="row no-gutters">
                                    <div class="col-12">
                                        <div class="row no-gutters">
                                            <asp:Repeater ID="rptNewsOnTop" runat="server">
                                                <ItemTemplate>
                                                    <div class="col-6">
                                                        <div class="card m-2">
                                                            <img class="img-fluid" src="<%# Page.ResolveUrl("~/cms") + Eval("newsImage").ToString().Replace("~/","/") + "?w=312&h=200&scale=both&bgcolor=white" %>" alt='<%# Eval("newsHeadline") %>'>
                                                            <div class="card-img-overlay"><span class="badge badge-pill badge-danger"><%# Eval("newsCategory") %></span> </div>
                                                            <div class="card-body">
                                                                <div class="news-title">
                                                                    <h2 class=" title-small"><a href='<%# Page.ResolveUrl("~/news/") + Eval("newsCategory").ToString() + "/" + SteelFunc.GenerateUrl(Eval("newsHeadline").ToString()) %>'><%# Eval("newsHeadline").ToString().Length>48?Eval("newsHeadline").ToString().Substring(0,45)+"...":Eval("newsHeadline").ToString() %></a></h2>
                                                                </div>
                                                                <p class="card-text"><small class="text-time"><em><%# Eval("datediff") %></em></small></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <h5 class="font-weight-bold py-1 px-3"><i class="fas fa-bullhorn"></i>Sponsored</h5>
                <div class="clearfix"></div>
                <div class="swiper-container swiper-container-ads">
                    <div class="swiper-wrapper">
                        <asp:Repeater ID="rptFFAds" runat="server">
                            <ItemTemplate>
                                <div class="swiper-slide pref pref-ffside">
                                    <a href='<%# Eval("advLinkURL") %>' target="_blank">
                                        <img class="img-fluid" src="<%# Page.ResolveUrl("~/cms") + Eval("advImage").ToString().Replace("~/","/") + "?w=" + Eval("advDimensions").ToString().Split('x')[0] + "&h=" + Eval("advDimensions").ToString().Split('x')[1] + "&mode=crop" %>" />
                                    </a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="swiper-pagination"></div>

                </div>
            </div>
        </div>
    </section>
    <section id="newsticker" class="widget-block flasher-sec float-left w-100">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 pr-md-0">
                    <div class="heading-box">News Flash</div>
                </div>
                <div class="col-md-10 pl-md-0">
                    <div class="content-box">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <asp:Repeater ID="rptNewsTicker" runat="server">
                                    <ItemTemplate>
                                        <div class="carousel-item <%# Container.ItemIndex==0?"active":"" %>">
                                            <span class="time-box"><%# Convert.ToDateTime(Eval("datecreated").ToString()).ToString("h:mm tt") %></span><a href='<%# Page.ResolveUrl("~/news/") + Eval("newsCategory").ToString() + "/" + SteelFunc.GenerateUrl(Eval("newsHeadline").ToString()) %>' class="text-dark"><%# Eval("newsHeadline") %></a>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="control-box">
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <section id="midad">
        <div class="container-fluid">
            <div class="pref pref-mid">
                <asp:HyperLink ID="lnkMidAd" runat="server">
                    <asp:Image ID="imgAdinMid" CssClass="img-fluid" runat="server" />
                </asp:HyperLink>
            </div>
        </div>
    </section>
    <section id="newscorner" class="banner-sec float-left w-100 pt-4 pb-5">
        <div class="container-fluid">
            <div class="row px-md-3">
                <div class="col-12 col-md-6">
                    <div class="row">
                        <asp:Repeater ID="rptNewsSF" runat="server">
                            <ItemTemplate>
                                <div class="col-6">
                                    <div class="card mb-4">
                                        <img class="img-fluid" src="<%# Page.ResolveUrl("~/cms") + Eval("newsImage").ToString().Replace("~/","/") + "?w=312&h=200&scale=both&bgcolor=white" %>" alt='<%# Eval("newsHeadline") %>'>
                                        <div class="card-img-overlay"><span class="badge badge-pill badge-danger"><%# Eval("newsCategory") %></span> </div>
                                        <div class="card-body p-2">
                                            <div class="news-title">
                                                <h2 class=" title-small"><a href='<%# Page.ResolveUrl("~/news/") + Eval("newsCategory").ToString() + "/" + SteelFunc.GenerateUrl(Eval("newsHeadline").ToString()) %>'><%# Eval("newsHeadline") %></a></h2>
                                            </div>
                                            <p class="card-text"><small class="text-time"><em><%# Eval("datediff") %></em></small></p>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="col-12 col-md-6 top-slider">
                    <h5>Latest &amp; Sponsored News</h5>
                    <div id="steelFeaturedNews" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <asp:Repeater ID="rptFeaturedNewsButtons" runat="server">
                                <ItemTemplate>
                                    <li data-target="#steelFeaturedNews" data-slide-to="<%# Container.ItemIndex %>" class="<%# Container.ItemIndex==0?"active":"" %>"></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <asp:Repeater ID="rptFeaturedNews" runat="server">
                                <ItemTemplate>
                                    <div class="carousel-item <%# Container.ItemIndex==0?"active":"" %>">
                                        <div class="news-block">
                                            <div class="news-media">
                                                <img class="img-fluid" src="<%# Page.ResolveUrl("~/cms") + Eval("newsImage").ToString().Replace("~/","/") + "?w=640&h=419&scale=both&bgcolor=white" %>" alt='<%# Eval("newsHeadline") %>'>
                                            </div>
                                            <div class="news-title">
                                                <h2 class=" title-large"><a href='<%# Page.ResolveUrl("~/news/") + Eval("newsCategory").ToString() + "/" +  SteelFunc.GenerateUrl(Eval("newsHeadline").ToString()) %>'><%# Eval("newsHeadline") %></a></h2>
                                            </div>
                                            <div class="news-des"><%# Eval("newsLeadPara").ToString().Length>250?Eval("newsLeadPara").ToString().Substring(0,250):Eval("newsLeadPara").ToString() %>...</div>
                                            <div class="time-text"><strong><%# Eval("datediff") %></strong></div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <%--Events Section--%>
    <div class="container-fluid">
        <div class="row">
            <%--Second fold from here on in 2 columns--%>
            <%--Column 1 : Content--%>
            <div class="col-12 col-md-9">
                <section id="eventscorner">
                    <h4 class="eventsheading">Our Events</h4>
                    <div class="row">
                        <asp:Repeater ID="rptEventsList" runat="server">
                            <ItemTemplate>
                                <div class="col-12 col-md-4">
                                    <a href='<%# "events/" + Eval("eventName").ToString().Replace(" ","-").Replace("-&","").ToLower() %>'>
                                        <div class="eventsingle">
                                            <img src='<%# Page.ResolveUrl("~/cms") + Eval("eventImage1").ToString().Replace("~/","/") + "?w=500&h=500&mode=crop" %>' alt="" class="img-fluid" />
                                            <div class="eventsingleoverlay">
                                                <h4><%# Eval("eventName") %></h4>
                                                <p><%# DateTime.Parse(Eval("eventStartDate").ToString()).ToString("dd MMM yyyy") %>, <%# Eval("eventCity").ToString() + ", " + Eval("eventCountry").ToString() %></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-12 text-center">
                                <a href="/events" class="btn btn-danger btn-events mt-4">
                                    <i class="fas fa-newspaper"></i>
                                    View All Events</a>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <%--Column 2 : Ads and others--%>
            <div class="col-12 col-md-3">
                <section id="secondsidepref">
                    <h5 class="font-weight-bold pt-4 mt-2 mb-3"><i class="fas fa-bullhorn"></i>&nbsp;Sponsored</h5>
                    <asp:Repeater ID="rptSFAds" runat="server">
                        <ItemTemplate>
                            <a href='<%# Eval("advLinkURL") %>' target="_blank" class="sfsidead">
                                <img class="img-fluid" src="<%# Page.ResolveUrl("~/cms") + Eval("advImage").ToString().Replace("~/","/") + "?w=" + Eval("advDimensions").ToString().Split('x')[0] + "&h=" + Eval("advDimensions").ToString().Split('x')[1] + "&mode=crop" %>" />
                            </a>
                        </ItemTemplate>
                    </asp:Repeater>
                </section>
            </div>
        </div>
    </div>
    <%--Events Section--%>
    <section id="about">
        <div class="row">
            <div class="col-12 text-center">
                <h3 class="sectionheading">About us</h3>
                <p class="sectionsubheading">What is Steel Group</p>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-12 col-md-4 text-center">
                <iframe width="400" height="250" src="https://www.youtube.com/embed/SKKOLy8ADGY?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-12 col-md-8">
                <p class="aboutsectionpara">
                    Steelgroup.co.in is now a reputed and popular name amongst the Steel industry and serving to Steel industry as one of the leading information and networking platforms. We organize focused subject driven conferences for steel industry especially into Steel Flat products. The company was founded in 2010 by Mr. Ajay Tambe who is the Founder & the CEO.
                    <br />
                    Moreover, Steelgroup keep its members and subscribers updated with the e-newsletters and current affairs of the market, being circulated to over 15,000 industry professionals.
                </p>
                <a href="about.aspx" class="btn btn-outline-danger text-uppercase">
                    <i class="fas fa-external-link-square-alt mr-3"></i>Read More
                </a>
            </div>
        </div>
    </section>
    <section id="clientele">
        <div class="row no-gutters">
            <div class="col-12 text-center">
                <h3 class="sectionheading">Our Clientele</h3>
                <p class="sectionsubheading">Esteemed Partners</p>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="swiper-container swiper-container-clientele">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="images/sponsors/cl01.jpg" class="img-fluid" />
                </div>
                <div class="swiper-slide">
                    <img src="images/sponsors/cl02.jpg" class="img-fluid" />
                </div>
                <div class="swiper-slide">
                    <img src="images/sponsors/cl03.jpg" class="img-fluid" />
                </div>
                <div class="swiper-slide">
                    <img src="images/sponsors/cl04.jpg" class="img-fluid" />
                </div>
                <div class="swiper-slide">
                    <img src="images/sponsors/cl05.jpg" class="img-fluid" />
                </div>
                <div class="swiper-slide">
                    <img src="images/sponsors/cl06.jpg" class="img-fluid" />
                </div>
                <div class="swiper-slide">
                    <img src="images/sponsors/cl07.jpg" class="img-fluid" />
                </div>
                <div class="swiper-slide">
                    <img src="images/sponsors/cl08.jpg" class="img-fluid" />
                </div>
                <div class="swiper-slide">
                    <img src="images/sponsors/cl09.jpg" class="img-fluid" />
                </div>
                <div class="swiper-slide">
                    <img src="images/sponsors/cl10.jpg" class="img-fluid" />
                </div>
                <div class="swiper-slide">
                    <img src="images/sponsors/cl11.jpg" class="img-fluid" />
                </div>
            </div>
            <div class="swiper-pagination swiper-pagination-clientele"></div>
        </div>
    </section>
    <%--Sponsors Section--%>
    <div class="row no-gutters mt-5">
        <div class="col-12 text-center">
            <h3 class="sectionheading">Subscribe to Our Newsletter</h3>
            <p class="sectionsubheading">Get Tailored News Updates in your inbox</p>
        </div>
    </div>
    <%--Sponsors Section--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.6/js/swiper.min.js"></script>
    <script>
        var swiper = new Swiper('.swiper-container-ads', {
            direction: 'vertical',
            slidesPerView: 3,
            spaceBetween: 20,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });

        var swiper2 = new Swiper('.swiper-container-clientele', {
            slidesPerView: 'auto',
            autoplay: {
                delay: 3000,
                disableOnInteraction: true,
            },
            pagination: {
                el: '.swiper-pagination-clientele',
                clickable: true,
            },
        });
    </script>
</asp:Content>

