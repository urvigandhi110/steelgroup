﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SteelAdmin.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="App_Admin_ChangePassword" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageHeading" runat="Server">
    <h2 class="no-margin-bottom">Change Password</h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="mainContent" runat="Server">
    <div class="col-lg-12">
        <asp:Panel ID="pnlMessage" Visible="false" runat="server" CssClass="alert alert-warning alert-dismissible fade" role="alert">
            <asp:Literal ID="ltrlmsg" runat="server"></asp:Literal>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </asp:Panel>
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h6">Hi
                    <asp:Literal ID="ltrlAdminName" runat="server"></asp:Literal>,Please Enter the following details to change your password -</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="form-control-label">Current Password <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtOld" TextMode="Password" placeholder="Please enter Current Password here" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">New Password <small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtNew" TextMode="Password" placeholder="Please enter new password here" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-control-label">Confirm New Password<small class="text-danger">*Required</small></label>
                    <asp:TextBox ID="txtConfNew" TextMode="Password" placeholder="Please enter new password again" runat="server" CssClass="form-control" required></asp:TextBox>
                </div>

                <div class="form-group">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" ValidationGroup="changepasswd" Text="Change Password" CssClass="btn btn-primary" />
                    <br />
                    <asp:CompareValidator ID="cmpPasswds" ValidationGroup="changepasswd" runat="server" ErrorMessage="New Passwords do not match" ControlToCompare="txtNew" ControlToValidate="txtConfNew" Font-Size="X-Small" ForeColor="Red"></asp:CompareValidator><asp:ValidationSummary ID="valsumm" ShowMessageBox="true" ShowSummary="false" ValidationGroup="changepasswd" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

