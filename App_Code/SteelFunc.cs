﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;


/// <summary>
/// Summary description for SteelFunc
/// </summary>
public class SteelFunc
{
    public static DataView GetTopDataViewRows(DataView dv, Int32 n)
    {
        DataTable dt = dv.Table.Clone();

        for (int i = 0; i < n; i++)
        {
            if (i >= dv.Count)
            {
                break;
            }
            dt.ImportRow(dv[i].Row);
        }
        return new DataView(dt, dv.RowFilter, dv.Sort, dv.RowStateFilter);
    }

    public static string GenerateUrl(string name)
    {
        return name.Trim().Replace("%", "-").Replace(":", "-").Replace(",", "-").Replace(".", "-").Replace(" ", "-").Replace("--", "-").ToLower().Replace("'","");
    }
    public SteelFunc()
    {

    }

    public static string sendmail(string toid, string subject, string body)
    {
        try
        {
            MailMessage mm = new MailMessage();
            MailAddress fromaddress = new MailAddress("no-reply@steelgroup.co.in", "Steel Group");
            string ToAddress = "";
            if (toid.Contains(";"))
            {
                mm.To.Add(toid.Split(';')[0]);
                mm.To.Add(toid.Split(';')[1]);
            }
            else
            {
                ToAddress = toid;
                mm.To.Add(ToAddress);
            }

            mm.From = fromaddress;
            mm.Subject = subject;
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            NetworkCredential nw = new NetworkCredential("no-reply@steelgroup.co.in", "Qeo1y2&5");
            smtp.Host = "mail.steelgroup.co.in";
            smtp.Credentials = nw;
            smtp.Send(mm);
            return "sent";
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }

    public static string sendPPTmail(string toid, string subject, string body, string attachmentURL, string filename)
    {
        try
        {
            MailMessage mm = new MailMessage();
            MailAddress fromaddress = new MailAddress("no-reply@steelgroup.co.in", "Steel Group");
            string ToAddress = "";
            if (toid.Contains(";"))
            {
                mm.To.Add(toid.Split(';')[0]);
                mm.To.Add(toid.Split(';')[1]);
            }
            else
            {
                ToAddress = toid;
                mm.To.Add(ToAddress);
            }

            mm.From = fromaddress;
            mm.Subject = subject;
            mm.Body = body;
            mm.IsBodyHtml = true;

         

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(attachmentURL);
            using (HttpWebResponse HttpWResp = (HttpWebResponse)req.GetResponse())
            using (Stream responseStream = HttpWResp.GetResponseStream())
            using (MemoryStream ms = new MemoryStream())
            {
                responseStream.CopyTo(ms);
                ms.Seek(0, SeekOrigin.Begin);
                Attachment attachment = new Attachment(ms, filename);
                mm.Attachments.Add(attachment);

                SmtpClient smtp = new SmtpClient();
                NetworkCredential nw = new NetworkCredential("no-reply@steelgroup.co.in", "Qeo1y2&5");
                smtp.Host = "mail.steelgroup.co.in";
                smtp.Credentials = nw;
                smtp.Send(mm);
                return "sent";

            }

           
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
}