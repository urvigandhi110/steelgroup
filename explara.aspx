﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EventMaster.master" AutoEventWireup="true" CodeFile="explara.aspx.cs" Inherits="explara" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="Server">

    <section style="background-color: #f1f1f1; padding: 4rem 0;">
        <div class="row no-gutters">
            <div class="col-12 text-center">
                <h3 class="sectionheading">Register &amp; Pay Online</h3>
                <p class="sectionsubheading">Hurry! Reserve your seat today.</p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-12 col-md-10 offset-md-1">
                <iframe src="https://asia.explara.com/widget-new/bangladesh-steel-2018" frameborder="0" style="width: 100%;" height="750"></iframe>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="endScripts" runat="Server">
</asp:Content>

