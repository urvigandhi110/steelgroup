﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{
    SqlConnection steelcon;
    SqlCommand steelcmd;
    SqlDataReader rdrd;

    string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["steelgroupconn"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            steelcon = new SqlConnection(connstr);
            steelcmd = new SqlCommand("SELECT (SELECT COUNT(*) FROM adminmaster) as admincount,(SELECT COUNT(*) FROM jobsmaster) as jobcount,(SELECT COUNT(*) FROM eventregistration) as regcount,(SELECT COUNT(*) FROM eventsmaster) as eventcount, (SELECT COUNT(*) FROM newsmaster) as newscount, (SELECT COUNT(*) FROM subscribermaster) as subscribercount, (SELECT COUNT(*) FROM adsmaster) as adscount");

            steelcmd.Connection = steelcon;
            steelcon.Open();
            rdrd = steelcmd.ExecuteReader();
            if (rdrd.Read())
            {
                ltrlNewsCount.Text = rdrd["newscount"].ToString();
                ltrlSubscriberCount.Text = rdrd["subscribercount"].ToString();
                ltrlAdsCount.Text = rdrd["adscount"].ToString();
                ltrlEventCount.Text = rdrd["eventcount"].ToString();
                ltrlEventReg.Text = rdrd["regcount"].ToString();
                ltrlJobs.Text = rdrd["jobcount"].ToString();
                ltrlAdminCount.Text = rdrd["admincount"].ToString();
            }

            steelcon.Close();
        }
        catch (Exception ex)
        { }
    }
}